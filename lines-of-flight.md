# Lines of flight

From [[Deleuze & Guattari]].

In French, "ligne de fuite".  It could be translated at fleeing from something.  But also it can be translated as leaking, flowing, leading to a vanishing point.

> Fuite covers not only the act of fleeing or eluding but also flowing, leaking, and disappearing into the distance (the vanishing point in a painting is a point de fuite). It has no relation to flying.
> 
> &#x2013; Brian Massumi

<!--quoteend-->

> Multiplicities are defined by the outside: by the abstract line, the line of flight or deterritorialization according to which they change in nature and connect with other multiplicities. 
> 
> &#x2013; [[A Thousand Plateaus]]

