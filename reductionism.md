# Reductionism

> The practice of considering a problem in isolation, as if it had no implications for the wider system to which it belongs, and as if interventions could be designed without taking account either of their wider consequences or of their effects over the longer term.
> 
> &#x2013; [[Lean Logic]] ([Reductionism - LEAN LOGIC](https://leanlogic.online/glossary/reductionism/))

