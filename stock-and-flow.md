# Stock and flow

Another metaphor for [[the blog and wiki combo]].

Heard about this first from [Tom Critchlow's notes](https://tomcritchlow.com/2019/02/17/building-digital-garden/) on it.


## Flow

> Flow is the feed. It’s the posts and the tweets. It’s the stream of daily and sub-daily updates that reminds people you exist.
> 
> &#x2013; [Stock and flow / Snarkmarket](http://snarkmarket.com/2010/4890) 


## Stock

> "Stock is the durable stuff. It’s the content you produce that’s as interesting in two months (or two years) as it is today. It’s what people discover via search. It’s what spreads slowly but surely, building fans over time."
> 
> &#x2013; [Stock and flow / Snarkmarket](http://snarkmarket.com/2010/4890) 

Stock sounds a bit more like articles, rather than the wiki as such.

You want a balance of the two.

> You can tell that I want you to stop and think about stock here. I feel like we all got really good at flow, really fast. But flow is ephemeral, while stock sticks around. Stock is capital. Stock is protein.
> 
> &#x2013; [Stock and flow / Snarkmarket](http://snarkmarket.com/2010/4890) 

