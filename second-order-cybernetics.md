# second-order cybernetics

AKA the cybernetics of [[cybernetics]].

Second-order cybernetics includes more learning and reflection on goals.  It's not just about efficiency.  ([[Liss C. Werner, "Cybernetics: State of the Art"]])

> the cybernetic shift from object to message
> 
> &#x2013; [[The Knowledge Ecology]]

^ Not 100% sure but I feel like this shift is more a part of second-order cybernetics.

