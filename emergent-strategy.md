# Emergent Strategy

URL
: https://www.akpress.org/emergentstrategy.html

Looks very interesting to me in that it appears to draw inspiration from [[emergence]] and some of the [[Evolutionary and adaptive systems]] stuff.

> brown’s “emergent strategy” for activists revels not in conflict with corporate opponents but in apparitions of friendship in online threads and tips for weaving consensus process.
> 
> &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]

Based on some of the ideas of [[Octavia E. Butler]].


## Log


## <span class="timestamp-wrapper"><span class="timestamp">[2021-07-17 Sat]</span></span>

Started reading Emergent Strategy yesterday.  I found the writing style a bit - can't think of the word - loose, maybe?  At first.  More like it is a blog post than a book.  Kind of like a journal.  With use of CAPS in places, and punctuation like?!  Maybe I'm just too used to some of these academic books. In a way it's kind of refreshing to have something a bit more personal rather than dry and dense.

All that aside - the ideas are great.  Absolutely spot on to my interests.  Tying various complexity / evolutionary and adaptive stuff to social and [[Political organisation]].  Explicitly making the link.  As well as [[Octavia Butler]] related stuff.

> Octavia Butler / (amb)
> 
> -   All successful life is (Fractal)
> -   Adaptable, (Adaptive)
> -   Opportunistic, (Nonlinear/Iterative)
> -   Tenacious, (Resilient/Transformative Justice)
> -   Interconnected, and (Interdependent/Decentralized)
> -   Fecund. (Creates More Possibilities)
> -   Understand this. (Scholarship, Reflection)
> -   Use it. (Practice/Experiment)
> -   Shape God. (Intention)

I like the Elements of Emergent Strategy very much.

> -   Fractal.  The relationship between small and large.
> -   Adaptive. How we change.
> -   Interdependence and decentralization.  Who we are and how we share.
> -   Non-linear and iterative.  The pace and pathways of change.
> -   Resilience and transformative justice.  How we recover and transform.
> -   Creating more possibilities.  How we move towards life.

