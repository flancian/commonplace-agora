# The IndieWeb and the right to repair are related

-   tags: [[right to repair]] | [[IndieWeb]]

I feel like there's quite a lot of overlaps between the two.  Both in terms of goals and how we operate.  It might be a bit of a tenuous link in some places&#x2026; but fun to think about.

I remember Nicole from Mozilla saying to Tantek at MozFest 2019, at our Restart stall - "hey Tantek check this out, it's like IndieWeb but for your stuff!"

See also [[Goodbye iSlave]], where Jack Qiu links device manufacture and device addiction as two parts of the same system.

In this article they are referenced as two parallel means to counteract the dominance of the big GAMA tech firms:

> A “right to repair” would stop planned obsolescence in phones, or firms buying up competitors just to cut them off from the cloud they need to run. A “right to interoperate” would force systems from different providers, including online platforms, to talk to each other in real time, allowing people to leave a social network without leaving their friends.
> 
> &#x2013; [Privacy is not the problem with the Apple-Google contact-tracing toolkit | Mi&#x2026;](https://www.theguardian.com/commentisfree/2020/jul/01/apple-google-contact-tracing-app-tech-giant-digital-rights)  


## Goals

To be able to fix your own stuff is a bit like being able to own your own data.  Access to spare parts, repair guides, is maybe like having access to APIs of the big silos.


## Organisation

-   Local groups that help people fix / build their sites.
-   National(ish) meetups roughly yearly.
-   A regular 'summit'.

