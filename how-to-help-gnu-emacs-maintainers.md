# How to help GNU Emacs maintainers?

URL
: https://bzg.fr/en/how-to-help-gnu-emacs-maintainers/

Author
: [[Bastien Guerry]]

Nice article and video not just on [[org-mode]] and [[Emacs]] but on maintaining software projects in general.  ([[free software project management]]).  A similar sentiment as seen elsewhere but comes with a good amount of weight I think as Bastien has been involved in stewarding a large software project for many years.

I like the definition of [[Spiderman Syndrome]] - a new one to me, similar to [[Benevolent Dictator For Life]], the difference here being the idea that a maintainer is a person with both great power and great responsibility (but still a 'superhero' of some kind).

I like the definition of ACDC - [[Asynchronous Collective Distributed Care]].

> But if you start thinking of the project as something that enables you to do amazing things, and of the maintenance as something that enables contributions, you will see how important and rewarding it is to become an enabler.

^ pretty good description of [[commoning]]&#x2026;

