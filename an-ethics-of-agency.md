# An ethics of agency

Chris Webber's ethical framework.

> the foundation of maximizing [[agency]] "for you, for me, for everyone" and minimizing subjection
> 
> &#x2013; [11: An Ethics of Agency &#x2013; FOSS and Crafts](https://fossandcrafts.org/episodes/11-an-ethics-of-agency.html) 

Chris wanted to rework the four (/three) free software freedoms in to a consequentialist framework (instead of Kantian).

Chris makes a link between ethics of agency and the [[Declaration of Digital Autonomy]].  I think he says the declaration is more principles, and the ethics of agency is more of a framework?  Something like that. 

