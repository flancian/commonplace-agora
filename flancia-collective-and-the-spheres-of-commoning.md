# Flancia Collective and the spheres of commoning

As a fun thought experiment, I'd like to have a think about how [[Flancia Collective]] is doing stuff in each of the [[Spheres of commoning]].

Also to be thought about: what commons are we actually stewarding?  And who are the commoners?

We are creating digital commons of software and a knowledge commons, I guess.

Mike Hales' handbook on meet.coop would be a good reference point I reckon.

I guess should look at each of the patterns through the lens of how does Flancia Collective do these things?  Or how do we do these things in relation to a particular commons?  Comes back to defining what actually are our commons.


## Social life ([[The Social Life of Commoning]])


### Cultivate Shared Purpose and Values

-   We kind of have this, but I don't know what it means to cultivate it, and whether we're doing that.


### [[Ritualize Togetherness]]

-   We have a few chat spaces on Element, but I don't feel like  async chat really counts as togetherness.
-   [[node club]] - kind of a shared 'custom' each week.  Kind of async again though.  As a way of fleshing out the knowledge commons.


### Contribute Freely

-   We do this - both code and info.


### Practice Gentle Reciprocity


### Trust Situated Knowing


### [[Deepen Communion with Nature]]

-   Bit of a wild card one for digital commons.  But could link it in via [[sustainable tech]].


### Preserve Relationships in Addressing Conflicts


### Reflect on Your Peer Governance


## [[Peer governance]]


### Bring Diversity into Shared Purpose

-   Some diversity in views.  But not a huge amount of diversity in background.


### Create Semi-Permeable Membranes


### Honor Transparency in a Sphere of Trust


### Share Knowledge Generously

-   Check.


### Assure Consent in Decision Making

-   Happens roughly through Element chat.


### Rely on Heterarchy


### Peer Monitor & Apply Graduated Sanctions


### Relationize Property


### [[Keep Commons & Commerce Distinct]]

-   We have no money associated with Flancia/Agora as of yet.


### [[Finance Commons Provisioning]] / [[Choose Commons-Friendly Financing]]

-   We have no financing associated with Flancia/Agora as of yet.


## Provisioning ([[Provisioning Through Commons]])


### Make & Use Together


### Support Care & Decommodified Work


### Share the Risks of Provisioning


### Contribute & Share


### Pool, Cap & Divide Up


### Pool, Cap & Mutualize


### Trade with Price Sovereignty


### [[Use Convivial Tools]]

-   I think we do, yeah.  We tend to work with [[Free software]].


### Rely on Distributed Structures

-   We **like** distributed structures, for sure.  And we'd like Agora to be distributed (or at least federated).  Do we make use of them though?


### Creatively Adapt & Renew

