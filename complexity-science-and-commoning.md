# Complexity Science and Commoning

This is a section in [[Free, Fair and Alive]].  I personally am very interested in the intersection of [[complexity science]] and [[commoning]].

> **Complexity science has important things to say about the commons, too, because it sees the world as a dynamic, evolving set of living, integrated systems.**
> 
> &#x2013; [[Free, Fair and Alive]]

They both have a focus on the **relationships** between things, not just the things themselves.

> a shift towards a [[relational ontology]] has created a new paradigm of discovery, [[complexity science]], which is revolutionizing biology, chemistry, evolutionary sciences, physics, economics, and social sciences, among other fields.
> 
> &#x2013; [[Free, Fair and Alive]]

[[Doughnut Economics]] includes this relational ontology in a framework for ecomonics.

> Kate Raworth, in her brilliant book Doughnut Economics, has proposed a real-world economic framework that recognizes a new ontology — that people are social and relational (not rational and individualistic); that the world is dynamically complex (not mechanical and tending toward equilibrium); and that our economic systems must be regenerative by design.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> By viewing the world through this window — a relational ontology that moves beyond mechanical metaphors and individualism — it becomes possible to offer much better explanations for all sorts of human and ecological phenomena.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> We can begin to understand a commons as a _life-form_, not as a "resource," and as an organic, integrated system, not a collection of discrete parts.
> 
> &#x2013; [[Free, Fair and Alive]]

Yes I like this.  The commons as a complex system and as a life-form that persists.  One can then see how things such as [[homeostasis]] and [[autopoesis]] apply to it.  I think this is where [[Andreas Weber]] takes things.

> The window on reality that a commons expresses is more encompassing and real (in our estimation!) than ontologies that consign relational dynamics to the background as “exogenous variables.” 
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The philosopher and biologist [[Andreas Weber]] has expressed the view of being that we take in this book: "The world is not populated by singular, autonomous, sovereign beings. It is comprised of a constantly oscillating network of dynamic interactions in which one thing changes through the change of another. The relationship counts, not the substance."
> 
> &#x2013; [[Free, Fair and Alive]]

