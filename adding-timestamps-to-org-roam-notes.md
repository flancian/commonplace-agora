# Adding timestamps to org-roam notes



## Adding timestamps at note creation

From [Zaeph's config](https://github.com/zaeph/.emacs.d/blob/4548c34d1965f4732d5df1f56134dc36b58f6577/init.el#L4586), include this in your capture template:

```elisp
:head "#+TITLE: ${title}\n#+CREATED: %U\n#+LAST_MODIFIED: %U\n\n"
```


## Modifying timestamps on save

I would prefer to do this on org-roam files only.
See  [Update a field (#+LAST<sub>MODIFIED</sub>: ) at save - How To - Org-roam](https://org-roam.discourse.group/t/update-a-field-last-modified-at-save/321/19).

Didn't work for me straight off though.

```elisp
(setq time-stamp-active t
      time-stamp-start "#\\+LAST_MODIFIED:[ \t]*"
      time-stamp-end "$"
      time-stamp-format "\[%04y-%02m-%02d %3a %02H:%02M\]")
(add-hook 'before-save-hook 'time-stamp nil)
```

