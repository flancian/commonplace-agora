# Glitch Feminism

Book by Legacy Russell.

> Our software and our wetware are constantly glitching. How could it be otherwise? Rather than try for perfect order, let’s embrace the glitch and find out how else it all could play out. Let’s just admit we’re done with the old empire of imperatives about both flesh and tech and tune in to those who are playing in the ruins, hacking their way through to another life. This book takes you there.
> 
> &#x2013; [[McKenzie Wark]]

[[Glitch]].

