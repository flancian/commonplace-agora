# Radical Technologies

> Behind every handset is another story: that of the labor arrangement, [[supply chains]] and flows of capital that we implicate ourselves in from the moment we purchase one, even before switching it on for the first time

