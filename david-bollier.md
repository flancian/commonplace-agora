# David Bollier

Scholar and activist with a focus on the [[Commons]] and [[commoning]].


## Interviews

Great interview with David Bollier on the [[commons]] (natural, urban, digital), [[commoning]], re-recognising the value in things that sit outside of market economics and exchange value. (Contains the bombshell that apparently 20% of the human genome has already been enclosed and patented. WTF?!)

https://thenextsystem.org/learn/stories/episode-17-social-transformation-through-commons-w-david-bollier

