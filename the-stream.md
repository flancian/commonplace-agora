# the Stream

It's got a bit of history behind it (see below), but in a nutshell I think of the stream as the type of stuff that gets posted on Twitter or Facebook.  Reverse chronological microblogging, essentially.

![[images/the-stream.jpg]]

I guess the stream refers to both 'my stream', and the big stream of things that I subscribe to.


## Why is it useful

For me the stream is useful because:

-   my stream is a way to share my thoughts
-   people responding to my stream is a way to learn
-   the big stream is a way to seek out new ideas and learn from others


## History of the metaphor

> The Stream is a newer metaphor with old roots. We can think of the”event stream” of programming, the “lifestream” proposed by researchers in the 1990s. More recently, the term stream has been applied to the never ending parade of twitter, news alerts, and Facebook feeds.
> 
> In the stream metaphor you don’t experience the Stream by walking around it and looking at it, or following it to its end. You jump in and let it flow past. You feel the force of it hit you as things float by.
> 
> It’s not that you are passive in the Stream. You can be active. But your actions in there — your blog posts, @ mentions, forum comments — exist in a context that is collapsed down to a simple timeline of events that together form a narrative.
> 
> In other words, the Stream replaces topology with serialization. Rather than imagine a timeless world of connection and multiple paths, the Stream presents us with a single, time ordered path with our experience (and only our experience) at the center. 
> 
> &#x2013; [The Garden and the Stream: A Technopastoral ](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/) 

-   tags: [[Hypertext]]


## Misc

Active vs passive streams?

e.g. a stream of notes (active), vs a stream of all recent changes (passive).

