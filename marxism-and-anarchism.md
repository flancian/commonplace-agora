# Marxism and anarchism

Two major strands of [[revolutionary socialism]], hoping to achieve similar ends but by pretty different means.

> In the conflict between Marx and [[Bakunin]] that defined the First International, at issue were both the method and goals of organizing the proletariat against the bourgeoisie.
> 
> &#x2013; [Is Post-Structuralist Political Theory Anarchist? | The Anarchist Library](https://theanarchistlibrary.org/library/todd-may-is-post-structuralist-political-theory-anarchist) 

<!--quoteend-->

> In Marx’s view, it was necessary that there be a centralized leadership coordinating the struggle. Further, the goal of the struggle would be proletarian state ownership of the means of production. All this was incompatible, in Bakunin’s eyes, with the aims of the workers and would lead unavoidably to a new repressive political structure.
> 
> &#x2013; [Is Post-Structuralist Political Theory Anarchist? | The Anarchist Library](https://theanarchistlibrary.org/library/todd-may-is-post-structuralist-political-theory-anarchist) 

<!--quoteend-->

> What Bakunin found onerous in Marx’s politics, both in its strategy and its goal, was the idea of representation as a political concept. Where there is representation, there is oppression. Anarchism can be defined as the struggle against representation in public life.
> 
> &#x2013; [Is Post-Structuralist Political Theory Anarchist? | The Anarchist Library](https://theanarchistlibrary.org/library/todd-may-is-post-structuralist-political-theory-anarchist) 

<!--quoteend-->

> While those who followed Marx in the [[First International]] argued for the development of political parties who would, either through revolution or election, seize state power, the anarchists championed the building of organisations that were prefigurative, insofar as they aimed at creating the conditions of the future society in the present.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> While the Marxists saw the state as a potentially positive force that could bring about an end to economic exploitation, the anarchists challenged this, arguiing that th actions of teh sate, whether controlled by capitalists or by socialist revolutionaries, would inevitably create forms of authoritarian domination.
> 
> &#x2013; [[Anarchist Cybernetics]]

