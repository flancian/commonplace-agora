# Caliban and the witch

[[Silvia Federici]].

Witch trials coincided with [[enclosure of the commons]], and the removal of women from the public sphere and the appropriation of their labour for free.  A means to subjugate women who tried to assert themselves.

-   [As feministas devem reivindicar a imagem da bruxa? | SILVIA FEDERICI - YouTube](https://www.youtube.com/watch?v=iO4rNi4WIIw&feature=youtu.be)


## Criticism

> Tangent:  There’s a shoddy (but in some academic circles popular) book called “Caliban and the Witch” about the Early Modern witch craze.  The thesis of this book is that the witch panic was a kind of proto-capitalist plot to wipe out resistance to the nascent economic system.
> 
> &#x2013; https://twitter.com/normative/status/1454079910079877126

^ I don't think this guy's criticism is particularly good.  But interesting to see anyway.

