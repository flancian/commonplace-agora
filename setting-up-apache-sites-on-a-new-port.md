# Setting up Apache sites on a new port

For our staging environments, it's useful to have multiple ports available to test different features that are in flight at the same time.

Notes on the basic config for this, in [[ufw]] and [[Apache]].


## Firewall

```shell
ufw allow 8443
```


## Set up the config in Apache


### Edit /etc/apache2/ports.conf

Add

> Listen 8443

to the relevant part of the file.


### Set up Apache site config

Same as usual, except

```xml
<VirtualHost *:8443>
```

