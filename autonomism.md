# Autonomism

> Autonomism, also known as autonomist [[Marxism]] or autonomous Marxism, is a set of [[anti-authoritarian]] left-wing political and social movements and theories.
> 
> &#x2013; [Autonomism - Wikipedia](https://en.wikipedia.org/wiki/Autonomism)  

<!--quoteend-->

> emphasises the ability of the working class to force changes to the organization of the capitalist system independent of the state, trade unions or political parties. 
> 
> &#x2013; [Autonomism - Wikipedia](https://en.wikipedia.org/wiki/Autonomism)  

<!--quoteend-->

> less concerned with party political organization than other Marxists, focusing instead on self-organized action outside of traditional organizational structures. 
> 
> &#x2013; [Autonomism - Wikipedia](https://en.wikipedia.org/wiki/Autonomism)  

<!--quoteend-->

> broader definition of the working class than do other Marxists: as well as wage-earning workers (both white collar and blue collar), autonomists also include in this category the unwaged (students, the unemployed, homemakers, etc.), who are traditionally deprived of any form of union representation
> 
> &#x2013; [Autonomism - Wikipedia](https://en.wikipedia.org/wiki/Autonomism)  


## Twin pages

-   https://en.wikipedia.org/wiki/Autonomism

