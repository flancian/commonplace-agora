# Free software enacts 'from each according to their ability, to each according to their need'

[[Free software]] enacts [[From each according to their ability, to each according to their need]].

If you're able to contribute, you can.  (There are barriers to contribution though.)

You can use free software, even if you don't make any contribution.  (You need to know how to use it, though.

> "It helps underprivileged people to gain access to things which usually they cannot afford if it was not free, which would allow everyone to gain knowledge and learn." —Kedar Vijay Kulkarni, a software quality engineer at Red Hat
> 
> &#x2013; [Why I love free software | Opensource.com](https://opensource.com/article/19/2/why-i-love-free-software) 


## Epistemic status

Type
: [[claim]]

Gut feeling
: 9

Confidence
: 6

