# utopophobia

> We have good reason to doubt the cogency of what has been called Marx’s '[[utopophobia]]'
> 
> &#x2013; [[Platform socialism]]

<!--quoteend-->

> Without a clear vision of the future and an alternative to the ideological framework of ‘[[capitalist realism]],’ it can be difficult to imagine how another world could be possible
> 
> &#x2013; [[Platform socialism]]

