# CTZN

Decentralised social network.  Something to do with [[Hypercore Protocol]].

-   https://github.com/pfrazee/ctzn
-   https://ctznry.com/

I created an account: I am neil@ctzn.one.  I like many things about [[Hypercore Protocol]] so interested to see how this works.  I am guessing it has more of a P2P model, similar to the Beaker chat, with the servers acting as 'pubs'.  That said, the idea of another chat silo doesn't fill me with joy, so I wonder where the data is stored.  I still think the [[IndieWeb]] (or [[Solid]]) approach is the best philosophy (for me personally).

> Decentralized hosting (anybody can create a server, and data syncs using the Hypercore Protocol).
> 
> &#x2013; [GitHub - pfrazee/ctzn: A distributed social network mad science experiment](https://github.com/pfrazee/ctzn) 


## [[Servers should be background infrastructure]].


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-04-17 Sat]</span></span>

-   Super easy to get set up and posting
-   Really like cross server community interest groups, always missed this in Fediverse
-   Really intrigued by potential of  customisable [[reaction]]s - feels like it could avoid gamification while avoiding the [[+1 problem]]
-   No clue where my data is being stored

