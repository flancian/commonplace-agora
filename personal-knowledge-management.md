# personal knowledge management

> Personal knowledge management (PKM) is a process of collecting information that a person uses to gather, classify, store, search, retrieve and share knowledge in their daily activities and the way in which these processes support work activities)
> 
> &#x2013; [Personal knowledge management - Wikipedia](https://en.wikipedia.org/wiki/Personal_knowledge_management) 

Is it specifically **work** focused?

Came across Harold Jarche [via Ton](https://www.zylstra.org/blog/2019/06/the-blog-and-wiki-combo/), he teaches about Personal Knowledge **Mastery**, and talks about [[seek, sense, and share]]]].  (Less interested in the focus on _mastery_ and _leadership_, but I reckon most of it is still applicable outside of that lens).

