# exomemory

In [[The Quantum Thief]]:

> Exomemory is a kind of collective consciousness for inhabitants and visitors to Mars. It is a combination of statistical recording, public archive, and library of facts. Accessing exomemory involves 'blinking. The use of exomemory is what allows the Voice to decide the best course of action for the Martian society. 
> 
> &#x2013; [Exomemory | The Quantum Theif Wiki | Fandom](https://exomemory.fandom.com/wiki/Exomemory) 

<!--quoteend-->

> The exomemory stores data – all data – that the [[Oubliette]] gathers, the environment, senses, thoughts, everything. The gevulot keeps track of who can access what, in real time. It’s not just one public/private key pair, it’s a crazy nested hierarchy, a tree of nodes where each branch can only by unlocked by the root node. You meet someone and agree what you can share, what they can know about you, what you can remember afterwards
> 
> &#x2013; [[The Quantum Thief]]

