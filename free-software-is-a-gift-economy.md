# Free software is a gift economy

Not sure I agree with this claim or not. [[I like free software]] and [[I like gift economies]] so I'd be fine if it was a good one.

But part of me feels like it's actually more communist (from each according to their ability, to each according to their need).  [[Gift economies]] as I understand it have some expectation of (delayed) reciprocity that I'm not sure exists in free software.

Note that this doesn't necessary claim that free software is **exclusively** a gift economy.  Different projects could be different types of exchange systems.  

See

-   [[The Hi-Tech Gift Economy]]
-   [[Maggie Appleton on Open Source as a Gift Economy]]
-   [Participation in the open source gift economy –](https://rhnh.net/2009/11/07/participation-in-the-open-source-gift-economy/)

for some places to possibly dig in to this.


## Epistemic status

Type
: [[claim]]

Gut feeling
: 4

Confidence
: 2

