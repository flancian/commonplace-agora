# Upcycling broken laptops

First thing - try and fix it, and donate it to someone else!  But if it's beyond repair, there's a bunch of things that you can do with individual parts.

This video from DIY Perks has a whole bunch of cool ideas for projects to do with parts from end-of-life laptops: [Things you can make from old, dead laptops](https://www.youtube.com/watch?v=WLP_L7Mgz6M) 

-   turn the speakers into portable speakers
-   use the heatsink to cool high-power LEDs and make a lamp
-   screen where the LCD is broken - use the rest of it with the soft diffused light, make a tracing backlight, or a false window
-   or use the various bits from the display assembly
    -   diffusion sheets to make a nice lamp
    -   fresnel lenses to make another nice lamp
    -   get creative with the acrylic and make a feature light
-   if the screen is OK, turn it into a secondary display of some kind
-   make a headset/comms mic from the laptop's microphone
-   storage drive
    -   external drive in USB caddy if it's working
    -   a gimmicky tool cleaner
    -   mirror to make an analog clock
    -   turn the read/write head in to laser VU meter
-   batteries
    -   take the cells from the batteries into mini battery banks to power the other things
-   webcams turned in to a security system

