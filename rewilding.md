# Rewilding

> conservation efforts aimed at restoring and protecting natural processes and wilderness areas. 
> 
> &#x2013; [Rewilding (conservation biology) - Wikipedia](https://en.wikipedia.org/wiki/Rewilding_(conservation_biology)) 

