# Adding a 404 page with .htaccess

I wanted to add a 404 page to my garden, especially as I'm currently renaming pages.  Might end up with some broken links.

Easy enough - I created a 404.org in org-mode.  It gets published as 404.html.

And in .htaccess, just needed to add:

```nil
ErrorDocument 404 /404.html
```

