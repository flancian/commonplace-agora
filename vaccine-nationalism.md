# vaccine nationalism

> National leaders will not prioritise another population over their own; realistic plans must treat vaccine nationalism as a given
> 
> -   [The Guardian view on vaccine justice: the developing world won’t wait | Edito&#x2026;](https://www.theguardian.com/commentisfree/2022/feb/04/the-guardian-view-on-vaccine-justice-the-developing-world-wont-wait)

