# Ray Ison and Ed Straw, "The Hidden Power of Systems Thinking: Governance in a Climate Emergency"

URL
: https://newbooksnetwork.com/the-hidden-power-of-systems-thinking

Topics
: [[Systems thinking]], [[governance]], [[Climate change]]

Featuring
: [[Ray Ison]], [[Ed Straw]]

Politics is failing our home, the [[biosphere]], massively.  The biosphere and climate change needs to be at the centre of [[governance]].  [[Systems thinking]] is a means to do this.

Enjoyed this. Sounds like a good book.  

