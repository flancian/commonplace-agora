# Ada Lovelace

> The [[Analytical Engine]] weaves algebraic patterns, just as the [[Jacquard loom]] weaves flowers and leaves.

