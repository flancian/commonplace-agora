# index page for my journal files

Put this function at the top of a file `journal/index.org`.

It pulls out all the org files in that folder, and formats them at a bulleted list of links to each of those journal .orgs.

```emacs-lisp
(mapconcat
 (lambda (x)
   (concat
    "- "
    (org-link-make-string
     (concat "file:" x)
     (file-name-sans-extension x))))
 (nreverse (file-expand-wildcards "*.org")) "\n")
```

I had to also `(setq org-confirm-babel-evaluate nil)` in order to not have my gitlab pipeline prompt me for code execution.

