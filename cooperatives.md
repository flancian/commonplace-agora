# Cooperatives

I'm very much into the idea of cooperatives.  I don't have any experience of working in one myself (yet), but have worked with some, and try to buy goods/services from them when I can.

> A co-operative is an autonomous association of persons united voluntarily to meet their common economic, social, and cultural needs and aspirations through a jointly-owned and democratically-controlled enterprise.
> 
> &#x2013; [Cooperative identity, values & principles | ICA](https://www.ica.coop/en/cooperatives/cooperative-identity)


## [[Cooperative values]]


## [[Coops and commoning]]


## [[Coops and socialism]]


## Are they a challenge to capitalism?

> …if cooperatives are islands in a sea of capitalism, we need better catamarans, bridges, and data lines to connect them to each other and to other transformative economies.
> 
> &#x2013; [Manifesto – DisCO.coop](http://disco.coop/manifesto/) 

<!--quoteend-->

> I think the same could be said about [[free software]]. Both coops and free software are non capitalist, but in and of themselves aren't a strategy to end capitalism or even a real threat to the capitalist system. In the worst vein, they provide a strategy for a privileged few to avoid some exploitative aspects of capitalism. There is enormous potential to organize within these movements to actively struggle against capitalism. But I'm not sure how to do this.
> 
> &#x2013;  [@jamiem@social.coop](https://social.coop/@jamiem/105650661094861507) 


## [[Worker cooperatives]]


## [[Open cooperativism]]


## [[Open-value cooperatives]]


## [[Distributed Cooperative Organisations]]


## Copper lane cohousing

Novara Media https://youtu.be/yjbtCVTWCas​

> Cooperatives formalize the practice of commoning and facilitate legally regulated operations in the marketplace.
> 
> &#x2013; [[The DisCO Elements]]

<!--quoteend-->

> Cooperatives worldwide have a combined turnover of US$3 trillion, which is similar to the aggregate market capitalization of Silicon Valley’s greatest players (Microsoft, Amazon, Google, Apple and Facebook).
> 
> &#x2013; [[The DisCO Elements]]


## Links

-   [Co-ops in Spain’s Basque Region Soften Capitalism’s Rough Edges - The New Yor&#x2026;](https://www.nytimes.com/2020/12/29/business/cooperatives-basque-spain-economy.html)
-   [GitHub - hng/tech-coops: A list of tech coops and resources concerning tech c&#x2026;](https://github.com/hng/tech-coops/)
-   [#PlatformCoop Directory – The Internet of Ownership](https://ioo.coop/directory/)

<!--listend-->

-   [What is a Co-operative? - Invidious](https://vidi.noodlemaps.net/watch?v=90FL_bBE4mw)

