# Blockchain

Lots of hype, tarnished by crypto-libertarianism and environmental impacts of e.g. [[Bitcoin]], but interesting technology nonetheless.  [[Distributed ledger]]s of one sort or another could be used for beneficial purposes (see e.g. [[DisCOs]]).

> Both blockchain and digital currencies offer a great deal of potential, but Bitcoin itself is a deflationary, capitalist medium that functions primarily as an investment asset to concentrate wealth, and is mostly favored by right-libertarian hard money ideologues.
> 
> &#x2013; [Center for a Stateless Society » Review: A DisCO Manifesto](https://c4ss.org/content/52450) 

<!--quoteend-->

> However, the really interesting point about Bitcoin is not the currency itself, but rather the fact that its protocol – the blockchain – can be used to implement many other applications which have nothing to do with money.
> 
> &#x2013; [Ethereum: Freenet or Skynet? - Guerrilla Translation!](https://www.guerrillatranslation.org/2014/11/20/ethereum-freenet-or-skynet/) 

The actual technology behind the blockchain originated from the extropians(?) - something like that. Slightly crazy people who wanted to conceive of a way to live forever. I don't think that it's lineage necessarily should rule out a technology, however, they didn't seem to think blockchain was going in the right direction. There was some thought that it could be used by the horizontalist left for its own ends. But that wasn't fleshed out with a great deal of detail.


## Environmental impact

> If you understand the difference between [[Proof of Work]] and [[Proof of Stake]], then you should see that PoS blockchains don't have the horrendous problem that PoW do. Sure, they consume some energy, just like any other computing. But not on principle, as with PoW.
> 
> -   https://twitter.com/interstar/status/1455272029091926019

[The 15 Most Sustainable Cryptocurrencies for 2021 - LeafScore](https://www.leafscore.com/blog/the-9-most-sustainable-cryptocurrencies-for-2021/)

