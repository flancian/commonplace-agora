# Lisp

> What all this boils down to is that most programming languages have similar semantic powers. However, basic Lisp code is different from code in any other major programming language in that it has a far simpler syntax. Having a simple syntax is a defining feature of the Lisp language.
> 
> &#x2013; Land of Lisp

Cool how Lisp deals with big ints and fractions.

Code mode and data mode.  That's helped understand a little bit what the quote is for.

> By using quoting, you can tell Lisp, “This next part
> isn’t a command. It’s just a chunk of data for my program.”
> 
> &#x2013; Land of Lisp

<!--quoteend-->

> The creators of the other popular dialect of Lisp, Scheme, felt differently about this issue, and preferred to keep the concepts of falsity and empty list completely separate, at a slight cost to
> code brevity.
> 
> &#x2013; Land of Lisp

Certainly so far, Racket (and probably that's as a result of coming from Scheme?) looks nicer than Common Lisp.

The various def functions are a bit ugly.  The equality functions are very ugly (but that's also in Scheme?)

> However, we’ll do this in a way that avoids constraining our code by artificially forcing the human notion of text into its design. This will allow us to write code that focuses on the strengths of a computer.
> 
> &#x2013; Land of Lisp

Fair enough - but not 100% convinced we should cater to the computer and not the human&#x2026;

> By keeping your source data structures free from assumptions regarding the output format from the start, your coding can take full advantage of your programming language.
> 
> &#x2013; Land of Lisp

Quasiquoting is cool.

> This feature of Lisp, called quasiquoting, allows us to create chunks of data that have small pieces of Lisp code embedded in them.
> 
> &#x2013; Land of List

Love functional paradigm and higher order functions manipulating a list.

Homoiconicity - using the same data structures to store both data and code.

\`lambda\` is a macro.

Have been enjoying the LOL and ROR books.  Stopped with ROR when it started getting graphical for now - couldn't get it to work out of the box with Emacs.  Switched to LOL.  Enjoying Racket as a language more than CL, but LOL is a great book, and I prefer the text adventure example to be honest.

Really like functional programming.  We did an ML course in university which was pretty terrible, wish I'd gotten the ideas behind it at the time&#x2026;

> Lisp may stand for List Processing, but it's really tree processing
> 
> &#x2013; [[The Nature of Lisp]]


## In contrast with other langs

> Together, FORTRAN and ALGOL spawned a long series of programming languages, called the ALGOL family. All mem- bers of this family are made from more or less the same material and for the same kind of programmers—those who love the machine more than the problem.
> 
> &#x2013; Realm of Racket

