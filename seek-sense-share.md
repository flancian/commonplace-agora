# seek, sense, share

&#x2013; http://jarche.com/2014/02/the-seek-sense-share-framework/

> Seeking is finding things out and keeping up to date. Building a network of colleagues is helpful in this regard. It not only allows us to “pull” information, but also have it “pushed” to us by trusted sources. Good curators are valued members of knowledge networks.
> 
> Sensing is how we personalize information and use it. Sensing includes reflection and putting into practice what we have learned. Often it requires experimentation, as we learn best by doing.
> 
> Sharing includes exchanging resources, ideas, and experiences with our networks as well as collaborating with our colleagues.
> 
> &#x2013; [The Seek > Sense > Share Framework](http://jarche.com/2014/02/the-seek-sense-share-framework/), Harold Jarche

