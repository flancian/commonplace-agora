# Reweirding the web

([[Emergent outlines]])


## Outline


### Brief summary of how we got here

Something along the lines of how web 1.0 to web 2.0 broke the web.  The problem with platforms.  The loss of agency, individuality, creativity, but also the shift of power dynamics.

You could create a website for free back then.  But to communicate with others, you still needed some kind of sidechannel.  So perhaps this is what IndieWeb ideas bring to it?


### A little revenge from the old internet

The starting point being the question at the end of [[404 Page Not Found]]:

> It’s about time for a little revenge from the old internet

^ what does that mean?  How could it happen?

So then the need for ease of individual creativity, and individual publication.  Peer-to-peer made easy.

[[IndieWeb]].  [[Digital garden]]s. [[Beaker Browser]]. [[peer-to-peer networks]].

[[No Servers!  No Admins!]], [[Information Civics]], etc.

Social media platforms have turned the web into 

If you think of it as a historical narrative, maybe 1.0 was those in the know publishing anything; 2.0 was the facade of anybody publishing anything, as long as it goes through the central authority; web 3.0 could be anyone publishing anything, but peer-to-peer.


### Why does it matter for people to be able to publish?

-   


### And why does it matter if it goes through a central authority?

-   [[Authority in networks]]
-   [[Centralised applications are authoritarian]]


### [[Weird corridors]]

Analogy to green / wildlife corridors.  Prefigurative pathways that are re-enabling the weird web.  Maybe of interest to [[cyberflâneur]]s?

Web rings, aggregators, etc.


### How to enable people to do it?

A way of finding niches? [[Building the new web in the shell of the old]]. What about [[Four types of anti-capitalism]] as a reference point.  e.g. finding niches outside is one type.  repurposing inside is another?    maybe anti-trust is another.  etc.

IndieWeb is one option.  Beaker Browser is another.  Self-hosting.  These are all complicated for individuals.

As much as possible, moving away from client-server.  But making that simple.

For web 3000, it should be as easy to get started as Facebook, but not controlled by central authorities.  Could be state like the British Digital Cooperative.  Could be municipal.  Could be mutual aid.  Anyone should be able to migrate between options - interoperability and API access.  People own their own data and let others access it.


### Accessibility

Are weird designs accessible?  If not, this is not good - how to make weird also accessible.


### [[Re-ification]]

Nostalgia fetish.  Just bearing in mind that re-doing things might not necessarily be the best way to do things.  Rewild.  Redecentralise.  etc.  Maybe things were better, but always evaluate critically.  Avoid primitivism.


## References

-   [[Seeding the Wild]]
-   [[404 Page Not Found]]
-   [[Rediscovering the Small Web]]
-   [[The Web We Have to Save]]
-   [[How Blogs Broke The Web]]


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-04-10 Sat]</span></span>

Heh, turns out there's a post already out there called [[Rewilding the web]] from a few months back.  It's really good, too, and kind of much what I wanted to write.  Nay bother, just means its a good topic/idea, and my reweirding spin is a nice touch to add. I think.


## Random notes/offcuts

Thinking about 'rewilding the web'.  After the section named 'Rewilding the network commons' in [[Seeding the Wild]].
Rewilding having a double meaning - moving back from the strip malls of the social media platforms to more organic unstructured web.

Don't really know enough about rewilding though to know if it's a great analogy.  Also wary of nature appropriation.  Maybe **reweirding** the web? Actually yeah I prefer reweirding.  It can then cover both rewilding in letting things go back to how things were in [[web 1.0]] ('rewilding'), and the preference for how things were in web 1.0 ([[weird web]], reweirding).  Also weirding is not so far from sounding like weaving (Weaving the web).   

Digital garden analogy fits in alright to rewilding/reweirding.

-   include mention of Marx, vectoralism, etc, Jameson?

