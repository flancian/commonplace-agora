# pluriverse

> Pluriverse names an understanding of the world in which countless groups of people create and re-create their own distinctive cultural realities, each of which constitutes a world.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> This term is necessary because many contemporary crises stem from the belief that there is a One-World World, a kind of single Euro-modern reality.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> To say that the world is a pluriverse is to say that there is no single source of being (that is, to invoke a plural ontology) and that no knowledge system is inherently superior to others.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> A pluriverse is “a world in which many worlds fit,” as the [[Zapatistas]] say.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> This points to a conundrum: how can the different societies that constitute the human species accept that many worlds must coexist together on a single planet?
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> a fractal federation of countless unique and yet connected worlds
> 
> &#x2013; [[Free, Fair and Alive]]

