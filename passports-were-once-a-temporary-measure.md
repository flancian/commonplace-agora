# Passports were once a temporary measure

I find this little nugget fascinating:  

> The history of passports – which were introduced as a seemingly temporary measure during the first world war, but were retained in response to fears about spreading the Spanish flu – shows that pandemics can significantly influence our social infrastructure. 
> 
> &#x2013; [Privacy is not the problem with the Apple-Google contact-tracing toolkit](https://www.theguardian.com/commentisfree/2020/jul/01/apple-google-contact-tracing-app-tech-giant-digital-rights)  

The temporary becomes the norm.

