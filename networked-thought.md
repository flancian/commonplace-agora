# networked thought

> an explorative approach to problem-solving, whose aim is to consider the complex interactions between nodes and connections in a given problem space
> 
> [&#x2026;]
> 
> networked thinking encourages non-linear, second-order reflection in order to let a new idea emerge
> 
> [&#x2026;]
> 
> Thinking in networks can be done at an individual level, but the power of networked thinking becomes apparent in a collaborative setting
> 
> &#x2013; [Networked thinking: a quiet cognitive revolution - Ness Labs](https://nesslabs.com/networked-thinking) 

Networked thinking is based on two key principles: divergence and emergence. 

> Starting from any relevant node in the network, the divergent phase consists in branching out from that original point in many directions, without trying to evaluate the validity of any particular idea.
> 
> &#x2013; [Networked thinking: a quiet cognitive revolution - Ness Labs](https://nesslabs.com/networked-thinking) 

<!--quoteend-->

> When enough nodes are added to the network, patterns start to emerge. It may be specific clusters, or strong ties between particular nodes. 
> 
> &#x2013; [Networked thinking: a quiet cognitive revolution - Ness Labs](https://nesslabs.com/networked-thinking) 

<!--quoteend-->

> in my view, "digital gardening" is a synomym for "personal knowledge gardening", where the term "knowledge gardening" was first coined as a synomym for Douglas Engelbart's grand concept of [[Dynamic Knowledge Repositories]] and [[Networked Improvement Communities]].
> 
> That is, knowledge gardening is a space in which groups come together to tend their thoughts collectively, but, again, through my lens, that happens best if the participants are, first, self-sovereign and comfortable with that which they contribute. For that, I see self-sovereign conversation preceding even group-sovereign conversation; one always has personal thoughts which ought to be given the same epistemic status as those one would share with others.
> 
> I tend to think more like Ward Cunningham and his FederatedWiki, in which each wiki is self-sovereign, but public (I would still want private pages), and the process of federation is more like bacterial conjugation where bits of information are cloned into other wikis and manipulated there, each wiki maintaing full provenance and edit histories, knowing what others are doing as well.
> 
> At the same time, I am a topic mapper by trade, meaning, I want to maintain a personal topic map - my universe of discourse - and be able to federate public aspects of that with others in a group way: I am interested in the wormholes that will emerge between the self-sovereign thought processes of many people.
> 
> &#x2013; Jack Park

<!--quoteend-->

> In short, increasing the rate of innovation can be achieved by fostering an environment with enough sociability (so we can collaborate and combine ideas), enough transmission fidelity (so we don’t keep on re-inventing the wheel), and enough transmission variance (so we can incrementally improve the wheel instead).
> 
> &#x2013; [The collective brain: where does innovation come from? - Ness Labs](https://nesslabs.com/collective-brain) 

