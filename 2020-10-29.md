# 2020-10-29



## Doctorow and Amazon

The dedication to Amazon in Doctorow's Little Brother didn't age well:

"Amazon's in the process of reinventing what it means to be a bookstore in the twenty-first century and I can't think of a better group of people to be facing down that thorny set of problems."


## Mobilizon

Cool that [[Mobilizon]] (federated Facebook groups/events alternative) is released now.  Hope it does well!

https://joinmobilizon.org/en/

