# social contract

> foundational concept in constructing the contemporary idea of "nation." It is a weak form of democracy, relying on regular yet infrequent participation (and sometimes even discouraging active participation).
> 
> &#x2013; [[Building an open infrastructure for civic participation]]

<!--quoteend-->

> ponder the general belief system developed during the Renaissance and expanded in the eighteenth and nineteenth centuries by the capitalist societies that arose from it. We moderns live within a grand narrative about individual freedom, property, and the state developed by philosophers such as [[René Descartes]], [[Thomas Hobbes]], and [[John Locke]]. The OntoStory that we tell ourselves sees individuals as the primary agents of a world filled with inert objects that have fixed, essential qualities. (Most notably, we have a habit of referring to “nature” and “humanity,” as if each were an entity separate from the other.) This Western, secular narrative claims that we humans are born with boundless freedom in a prepolitical “[[state of nature]].” But our imagined ancestors (who exactly? when? where?) were allegedly worried about protecting our individual property and liberty, and so they supposedly came together (despite their radical individualism) to forge a “social contract” with each other. As the story goes, everyone authorized the establishment of a state to become the guarantor of everyone’s individual liberty and property.
> 
> &#x2013; [[Free, Fair and Alive]]

