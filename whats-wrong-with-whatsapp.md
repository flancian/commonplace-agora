# What's wrong with WhatsApp

[[WhatsApp]].


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

“Group chats are making the internet fun again”, the technology critic Max Read argued that groups have become “an outright replacement for the defining mode of social organization of the past decade: the platform-centric, feed-based social network.”

A bit like small.tech&#x2026; but by s huge platform 


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

The age of the message board, be it physical or digital, where information can be posted once for anyone who needs it, is over.


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

The problem, from the point of view of institutions, is that WhatsApp use seems fuelled by a preference for informal, private communication as such


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

Trolling, flaming, doxing, cancelling and pile-ons are all risks that go with socialising within a vast open architecture.


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

“Open” platforms such as Twitter are reminders that much social activity tends to be aimed at a small and select community, but can be rendered comical or shameful when exposed to a different community altogether.


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

WhatsApp is a machine for generating feelings of faux pas, as comments linger in a group’s feed, waiting for a response.


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

Since the 90s, the internet has held out a promise of connectivity, openness and inclusion, only to then confront inevitable threats to privacy, security and identity. By contrast, groups make people feel secure and anchored, but also help to fragment civil society into separate cliques, unknown to one another. This is the outcome of more than 20 years of ideological battles over what sort of social space the internet should be


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

O’Reilly coined the term “web 2.0” to describe a new wave of websites that connected users with each other, rather than with existing offline institutions. Later that year, the domain name facebook.com was purchased by a 21-year-old Harvard student, and the age of the giant social media platforms was born.


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

The more idealistic tech gurus who attended ETech insisted that the internet should remain an open public space, albeit one in which select communities could cluster for their own particular purposes, such as creating open-source software projects or Wikipedia entries. The untapped potential of the internet, they believed, was for greater democracy. But for companies such as Facebook, the internet presented an opportunity to collect data about users en masse. The internet’s potential was for greater surveillance. The rise of the giant platforms from 2005 onwards suggested the latter view had won out. And yet, in a strange twist, we are now witnessing a revival of anarchic, self-organising digital groups – only now, in the hands of Facebook as well. The two competing visions have collided


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

group dynamics” in the 40s. The central proposition of this school was that groups possess psychological properties that exist independently of their individual members. In groups, people find themselves behaving in ways that they never would if left to their own devices.


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

Shirky highlighted one area of Bion’s work in particular: how groups can spontaneously sabotage their own stipulated purpose


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

Bion’s concerns originated in fear of humanity’s darker impulses, but the vision Shirky was putting to his audience that day was a more optimistic one. If the designers of online spaces could preempt disruptive “group dynamics”, he argued, then it might be possible to support cohesive, productive online communities that remained open and useful at the same time. Like a well designed park or street, a well-designed online space might nurture healthy sociability without the need for policing, surveillance or closure to outsiders


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

As late as 2005, the hope was that the social web would be built around democratic principles and bottom-up communities. Facebook abandoned all of that, by simply turning the internet into a multimedia telephone directory


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

. Partly thanks to WhatsApp, the unmoderated, self-governing, amoral collective – larger than a conversation, smaller than a public – has become a dominant and disruptive political force in our society, much as figures such as Bion and Lewin feared.


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

In the safety of the group, it becomes possible to have one’s cake and eat it, to be simultaneously radical and orthodox, hyper-sceptical and yet unreflective


## TODO What's wrong with WhatsApp | WhatsApp | The Guardian

In the safety of the group, it becomes possible to have one’s cake and eat it, to be simultaneously radical and orthodox, hyper-sceptical and yet unreflective


## TODO Whats wring with whatsapp

Onteretkng view. Seems more about problem of group behaviour than WhatsApp specifically. WhatsApp being a bit of a red herring in some ways.

Like the metadata gathering is partly whats wrong with WhatsApp. Closed group dynamics equally applies to telegram, signal, what have you. WhatsApp is just a convenient stand in for 'chat group'.

Would be jntredting to look at thst shjrky piece.

