# Blogging

> Blogs are, in many ways, the child of BBS culture and mailing lists. They are a unique innovation on that model, allowing each person to control their part of the conversation on their own machine and software while still being tied to a larger conversation through linking, [[backlinks]], tags, and RSS feeds.
> 
> &#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/) 

<!--quoteend-->

> Blogs value a separation of voices, the development of personalities, new posts over revision of old posts. 
> 
> &#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/) 

<!--quoteend-->

> They are read serially, and the larger meaning is created out of a narrative that expands and elaborates themes over time, becoming much more than the sum of its parts to the daily reader.
> 
> &#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/) 

Kind of the idea that you are following a person, not a blog.  I'm sure Ton or someone else on the IndieWeb has mentioned that before.

> Through reading a good blogger on a regular basis, one is able to watch someone of talent think through issues
> 
> &#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/) 

Same.

-   [[the Stream]].

[[- Blog tooling]].


## Blogging more

> It strikes me there are a few flavors of epistemic uncertainty for the blogger

[[Ton]] has talked before about the barriers to blogging and ways to navigate them in his post on [blogging more](https://www.zylstra.org/blog/2018/01/how-to-blog-more/).


## Blogging together

> I think when we talk about “[[networked communities]]” that’s one of the ideas we’re getting at: that we can be part of multiple communities at once, with shared, partially overlapping sets of interests. And that we can do this while tending our own digital garden, without having to maintain accounts on a dozen different third-party platforms.
> 
> &#x2013; [Weaving a public web, or, why don’t I blog more? – Brendan Schlagel](https://www.brendanschlagel.com/2019/09/01/weaving-a-public-web-or-why-dont-i-blog-more/) 

<!--quoteend-->

> I’m hoping that starting up a few will lower the psychological barriers to publishing and give me some momentum to continue posting more regularly.
> 
> &#x2013; [Weaving a public web, or, why don’t I blog more? – Brendan Schlagel](https://www.brendanschlagel.com/2019/09/01/weaving-a-public-web-or-why-dont-i-blog-more/) 

[[Blogchains]] as a way of lowering barriers to [[blogging]].

> And networks can’t self-sustain without participation - so maybe the best argument for blogging is not because it’s good for you but because it’s good for… us?
> 
> &#x2013; [Networked Communities 2 - Blogging as a Social Act](https://tomcritchlow.com/2019/09/04/networked-communities-2/) 

<!--quoteend-->

> And this cross-domain blogchain is another way to experiment

Blogchains and "blogging together".

