# Understory

URL
: https://understory.coop

[[Understory]] has a really very tasty looking website.

-   "Grow your mind".  "Own your data".  "Link your ideas".
-   An experimental digital garden-y social platform thing, built on [[Solid]].  But from the blurb, without needing to faff about coding.

