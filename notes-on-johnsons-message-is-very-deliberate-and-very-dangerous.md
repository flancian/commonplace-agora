# Notes on "Johnson's message is very deliberate and very dangerous"

> I think it’s very deliberate — and if **their ultimate goal is to retain power rather than save lives** — very smart.

https://medium.com/@jonjalex/johnsons-message-is-very-deliberate-and-very-dangerous-here-s-how-to-combat-it-d336cae96348

> **we have been the weak and hapless victims who need to be told what to do**; and Johnson and his government have been the strong, all-knowing heroes who will save us. This is a version of what I call the **Subject story**.

<!--quoteend-->

> the government’s framing is failing and they need to change it.

<!--quoteend-->

> **Consumer story. Covid19 will now become an inconvenient hindrance to our lives, but one that each of us needs to take personal responsibility for dealing with**, and getting back to normal as best we can. In this story, government steps back and gets out of the way, because people are best left to look out for themselves

<!--quoteend-->

> if you get the disease, it will be your fault — because **you will not have stayed sufficiently alert**.

<!--quoteend-->

> **Citizen story**, Covid19 is more like a force of nature, in the face of which we are all in the same boat and must all work together, than a war.

<!--quoteend-->

> they are trying to push responsibility for whether we live or die from themselves onto each of us, but without giving us any of the tools or information we need to make the right decisions — all of the responsibility, none of the power.

