# appearing

> Gradually, we begin to conflate visibility with value. If something is being talked about and seen, we assume that it must be important in some way. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> Ask yourself — what compels us to buy the latest tech gadget? Why do we spill our feelings out on Facebook, in posts that are archived on servers deep underground? Which is more important, the expression of the feeling itself, or the knowledge that it will be documented and seen by others? Why do we incessantly take selfies, or record our every moment for posterity?
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

<!--quoteend-->

> Are we afraid of being a nobody — of being on “the margin of existence?” **If you’re concerned with how you appear, then are you really living?**
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

