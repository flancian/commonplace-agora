# Rediscovering the Small Web

&#x2013; [Rediscovering the Small Web - Neustadt.fr](https://neustadt.fr/essays/the-small-web/) 


## Modern gatekeepers

> Today, most of the time spent on the web is either on a small number of very dominant platforms like Facebook and LinkedIn, or mediated through them. 

<!--quoteend-->

> There is so much "content" that is constantly pushed at you as a user that **very few of us actually venture out to browse and explore anymore**. We simply don't need to. But these platforms thrive on "user engagement"—likes, comments, clicks and shares—and their algorithms are more likely to give visibility to content that generates this behavior. **Instead of browsing, the web is for many an endless and often overwhelming stream of content and commentary** picked out by algorithms based on what they think you already like and will engage with. **It's the opposite of exploration.** 

<!--quoteend-->

> When you're not receiving information passively and instead actually actively looking for something, you most likely have the same singular point of entry as about 87% of all web users: Google.

<!--quoteend-->

> the smaller, amateur web gets hidden in the shadows of web professionals who design around specific keywords and audiences.


## Commercial web

> There has always been a place for commerce and marketing on the web. [&#x2026;]  But today's web is mostly commercial. The smaller web of individuals has neither the resources nor the will to compete for visibility and audience the way the commercial web does. [&#x2026;] Compared to the small web, this commercial web is tactical and predatory.


## Product-oriented websites

> But the web is not always "profit-oriented" and it certainly does not need to be "user-centric" (and I say this as a UX consultant).

<!--quoteend-->

> It is worth remembering a website does not have to be a product; it can also be art. The web is also a creative and cultural space that need not confine itself to the conventions defined by commercial product design and marketing.


## The Web as a Creative Space

> A painter wouldn’t add more red to her painting or change the composition because market data showed that people liked it better. It’s her creative vision; some people might like it, others might not. But it is her creation, with her own rules. The question of "performance" is simply irrelevant. It's the same thing with the small web.

Agreed, with the one exception of accessibility.  That's important for inclusivity.

> If the commercial web is "industrial", you could say that the small web is "artisanal". One is not better than the other. 
