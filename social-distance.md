# Social distance

As described by [[Ton]], a way of helping to filter down the wealth of information out there that you could be reading, by how socially close you are to people.

&#x2013; [Feed Reading By Social Distance – Interdependent Thoughts](https://www.zylstra.org/blog/2019/06/feed-reading-by-social-distance/) 

