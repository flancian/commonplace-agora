# A Thousand Plateaus

[[Deleuze & Guattari]].

> Write, form a [[rhizome]], increase your territory by [[deterritorialization]], extend the [[line of flight]] to the point where it becomes an abstract machine covering the entire plane of consistency.

