# Problem with koreader: "Generator error"

Rakuten forced an update on my [[Kobo]], and now trying to launch [[koreader]] via NickelMenu gives the errors:

> Generator error
> 
> kfmon: error checking '/tmp/kfmon-ipc.ctl': stat:No such file or directory (src/generator<sub>c.c</sub>:93) 

Someone here seems to be having the same problem: https://www.mobileread.com/forums/showthread.php?p=4027804

Someone else tells them:

> That's because you need to reinstall KFMon after an update. 

OK, I'll try and reinstall KFMon.  Can't remember how I did that previously, off the top of my head&#x2026;

Ah good, I noted it down here: [[Installing and setting up koreader on my Kobo]].

> Installation all worked fine via the zip and script in this thread: https://www.mobileread.com/forums/showthread.php?t=314220
> 
> Unzip the zip script, run the script.  Bingo, as simple as that.

The download link isn't working though.

Luckily I have the downloads from last time.  It's 2020.09 version, whereas the new one is 2021.02.  But I can live with that for now, just to get it working again.

OK, so I reinstalled.  The 'Generator Error' has gone, but now I get:

> "See KoboEreader /.adds/nm/doc for instructions on how to customize this m&#x2026;"

and nothing seems to happen.

OK, ran the installer again, and it went through an 'Importing content' message that it didn't the first time.  I think something just borked up when unmounting the kobo the first time.

Woop, that got it working now!

