# Topic map

> A topic map is a standard for the representation and interchange of knowledge, with an emphasis on the findability of information.
> 
> &#x2013; [Topic map - Wikipedia](https://en.wikipedia.org/wiki/Topic_map) 

<!--quoteend-->

> Topic maps were originally developed in the late 1990s as a way to represent back-of-the-book index structures so that **multiple indexes from different sources could be merged**.
> 
> &#x2013; [Topic map - Wikipedia](https://en.wikipedia.org/wiki/Topic_map) 

