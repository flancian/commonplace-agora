# Some books I've read

A potted history of books that I've read.

You can also check out my bookshelf: https://noodlemaps.net/calibre/


## Currently reading

-   [[Platform socialism]]


### On the shelf&#x2026;

-   [[A People's Guide to Capitalism]]
-   [[Doughnut Economics]]
-   [[The Next Revolution]]
-   [[October (book)]] by China Mieville
-   [[The Twittering Machine]] by Richard Seymour
-   [[Small is Beautiful]] by E. F. Schumacher
-   [[Radical Technologies]]
-   [[The Shallows]]
-   [[Cybernetic Revolutionaries]]
-   [[The Society of the Spectacle]]


### Interested to read

-   [[Twitter and Tear Gas]]
-   Ministry of the Future by [[Kim Stanley Robinson]]
-   [[The Garden of Forking Paths]]
-   [[Goodbye iSlave]]
-   Alone Together
-   Pressed for Time
-   [[The Sane Society]]
-   The City, Not Long After
-   [[The Year 200]]
-   [[The Cybernetic Brain]]
-   [[Red Plenty]]
-   The Web of Life
-   [[bolo'bolo]]


## Read


### 2021

-   [[Free, Fair and Alive]]
-   [[Anarchist Cybernetics]]
-   [[The Left Hand of Darkness]]
-   [[Undoing Optimization: Civic Action in Smart Cities]]
-   [[Parable of the Sower]]
-   [[Infinite Detail]]


### 2020

-   [[Future Histories]]
-   [[Hello World]]
-   [[A Closed and Common Orbit]] by Becky Chambers
-   [[The Long Way to a Small, Angry Planet]] by Becky Chambers
-   [[84K]]
-   [[Ctrl+S]]
-   [[The Three Body Problem]]
-   [[Dune]]
-   [[A Memory of Empire]]


### 2019

-   [[Autonomous]]
-   [[Aurora Rising]]
-   [[Elysium Fire]]
-   [[Red Mars]]
-   [[Use of Weapons]]
-   [[The Player of Games]]
-   [[Consider Phlebas]]
-   [[Ancillary Justice]]


### 2018

-   [[Perdido Street Station]]
-   [[Walkaway]]


### 2017

-   [[The Dispossessed]]
-   [[The Quantum Thief]]
-   The Fractal Prince
-   The Causal Angel


### ????

-   [[The City &amp; the City]]
-   [[Oryx and Crake]]
-   [[Cryptonomicon]]
-   [[Snow Crash]]
-   [[Neuromancer]]
-   [[Altered Carbon]]
-   [[Naked Lunch]]
-   [[Jackson Rising]]: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi
-   [[Elinor Ostrom's Rules for Radicals]]: Cooperative alternatives beyond markets and states
-   [[Four Futures]]
-   [[Inventing the Future]]

