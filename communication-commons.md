# communication commons

> communication commons promises meaningful engagement, participation, and deliberation—practices associated with the exercise of other types of democratic rights
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

