# Haitian Revolution

> Beginning in 1791 and lasting through 1804, the Haitian Revolution was the only slave revolt in the “New World” to turn into a full blown revolution, and lead the the formation of the first Black republic. One of the main leaders of the revolution was [[Toussaint Louverture]], a former slave, who is pictured in the poster.
> 
> &#x2013; [Justseeds | The Haitian Revolution](https://justseeds.org/product/the-haitian-revolution/) 

