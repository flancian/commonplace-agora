# Learning from indigenous culture

Just an interesting linkage that I've noticed in a couple of places recently.  I've seen [[Chris]] mention a few times the [mnemonic systems used by indigenous peoples](https://boffosocko.com/2020/08/14/cookbooks-may-make-good-timeful-texts-andy-matuschak/).  And there was a chapter in [[Future Histories]] on lessons to be learned from [[indigenous communities on ownership and governance]].

Chris recommends the book [The World Until Yesterday: What Can We Learn from Traditional Societies?](https://boffosocko.com/2013/08/03/book-review-of-jared-diamonds-the-world-until-yesterday-what-can-we-learn-from-traditional-societies/).

