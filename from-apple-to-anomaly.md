# From Apple to Anomaly

An exhibition at the Barbican.  Really good.  Visually very powerful, also thought-provoking with regard to [[algorithmic bias]].

Based around [[ImageNet]], a project of 14 million photos used as a training set for machine learning.  Those 14 million photos have been categorised by people on Mechanical Turk.

The piece highlights the inherent biases in that initial categorisation.  You see 30,000 physical prints of photos arranged around certain terms, and it starts off very factual ('apple', 'soil') and moves into terms more laced with interpretation ('criminal', 'alcoholic').  

It looks fantastic.

