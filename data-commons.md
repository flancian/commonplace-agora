# data commons

> In Bristol, data commons, rather than being only stocks or stores of citizen-generated data, became sites to develop relationships of solidarity through the tolerance of friction, tension, and dispute about how data connects with things that matter to people. 
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> in an early definition of “data commons,” human-computer-interaction researchers Dana Cuff and colleagues described them as “repositories generated through decentralized collection, shared freely, and amenable to distributed sense-making” and noted that these repositories “have been proposed as transforming the pursuit of science but also advocacy, art, play, and politics.”
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

