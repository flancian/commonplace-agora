# ‘The government failed’: Dominic Cummings takes aim at No 10’s Covid response

URL
: https://www.theguardian.com/news/audio/2021/may/27/the-government-failed-dominic-cummings-takes-aim-at-no-10s-covid-response

Publisher 
: [[The Guardian]]

Summary
: "Former aide lashed out at every aspect of the government’s approach and Boris Johnson and Matt Hancock in particular"

[[Dominic Cummings]]. [[Boris Johnson]].  [[Coronavirus]].

Hard to know how much of Dominic Cummings to actually believe.  But some incredible stuff if true.  Including the deputy cabinet secretary apparently in March(!): "there is no plan; we're in huge trouble. [&#x2026;] I think we're absolutely fucked."

