# nowtions

Things I'm currently thinking about.  (like a /now page, but for [[notion]]s&#x2026; sorry).  Similar idea to e.g. [[Andy Matuschak]]'s  [§What’s top of mind](https://notes.andymatuschak.org/%C2%A7What%E2%80%99s_top_of_mind).


## Web, wikis

-   [[Alternative social media]]
-   [[Digital self-governance]]
-   [[Interlinking wikis]]
-   WTF is [[Web 3.0]]?
-   [[Digital garden]]s
-   [[IndieWeb]]


## Political organisation

-   [[Horizontalism vs verticalism]]
    -   [[Viable system model]]
    -   [[Institutional Analysis and Development]]
    -   [[Anarchist Cybernetics]], [[Heterarchy]]


## Commons, Commoning, Cooperatives

-   [[commoning]], through reading [[Free, Fair and Alive]]

