# niche construction

> Like the reductionist view of history, the traditional approach to evolution was based on a one-way flow: an environment poses a set of “problems” to organisms, and the organisms best adapted to “solve” the problems leave the most offspring, leading to the process of natural selection. The particular way in which an organism finds its own survival strategy, whether it's spiders weaving webs or bees turning pollen into honey, is called an evolutionary niche. However, in recent years, researchers have suggested there's really a two-way flow going on, which they call “niche construction.”
> 
> &#x2013; [[The Patterning Instinct]]

