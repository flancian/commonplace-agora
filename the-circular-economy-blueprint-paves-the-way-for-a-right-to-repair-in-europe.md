# The Circular Economy Blueprint paves the way for a Right to Repair in Europe

-   tags: [[right to repair]] | [[circular economy action plan]]


## summary

-   Right to Repair was included in the [[European Commission]]'s Circular Economy Action Plan
-   first step towards making repair commonplace in Europe


## what it lays out

-   five year blueprint
-   move away from take-make-use-dispose
-   towards sustainable products, services and business models
-   provides a policy framework preventing wasteful consumption and built-in obsolescence


## next steps

-   publication of forthcoming [[ecodesign]] work plan


## things included

-   Sustainable product policy legislative initiative that aims to improve, among other sustainability aspects, repairability of all products on the European market and counter premature obsolescence – electronics and ICT products are identified as a priority

-   Revision of EU consumer law to establish a new ‘[[right to repair]]’ and provide point of sale information on lifespan of products, as well as availability of repair services, [[spare parts]] and [[repair manuals]]

-   [[Circular Electronics Initiative]], with focus on mobile phones, tablets, laptops and printers and including focus on both hardware and software as well as the introduction of common charger

-   Revision of economic instruments to enable Member States to use VAT rates to promote repair services

