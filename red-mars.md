# Red Mars

&#x2013; Kim Stanley Robinson

I kind of liked it but not to the degree I thought I might from the hype I'd read. It didn't knock my socks off like say The Dispossessed did. Could have gone for more politics, less description of Martian geology.

There was a bit of politics I guess.  [[Imperialism]], [[colonialism]], [[capitalism]] and extraction.  Arkady and his rebellion.  I guess it just didn't draw me in that much, narratively.  Also some discusison of whether they should or shouldn't change Mars' geology.  I found that a bit less interesting.

Someone online told me that the next two in the trilogy get a bit more into the politics of it.  I think I also recall that they said Robinson was a bit centrist, so the politics never get totally radical.  But I've got a few articles somewhere saying that he has socialist politics and that you should read him.  So.  I think I'll continue with the trilogy.

Finished: August 2019 sometime.

