# cybernetics

> the study of “control and communication in the animal and the machine.
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> the postwar science of communication and control, looked for commonalities in biological, mechanical, and social systems.
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> Cybernetics often mixed metaphors from engineering and biology to describe the behavior of complex systems ranging from the electromechanical operation of a computer to the function of the human brain. Some members of the cybernetics community viewed cybernetics as a universal language for the scientific study of machines, organisms, and organizations
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> The study of feedback processes in machines, organisms, and social organizations became a distinguishing feature of cybernetic work and departed from the linear cause-and-effect relationships that, until then, had dominated scientific practice
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> the steering that systems undergo is not conducted by an outside agency but by the system itself and its constituent parts.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> concerned how any kind of system, be that an engineering or electronic system or a biological or social system, was organised; what the different parts of the system were, what function they played; and, importantly, how they operated together to make up the whole.
> 
> &#x2013; [[Anarchist Cybernetics]]


## Cross-discipline

> From its earliest days cybernetics valued the cross-disciplinary pollination that occurred when experts from a variety of fields convened to discuss a common problem
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> Cybernetics “is likely to reveal a great number of interesting and suggestive parallelisms between machine and brain and society,” Ashby predicted. “And it can provide the common language by which discoveries in one branch can readily be made use of in the others
> 
> &#x2013; [[Cybernetic Revolutionaries]]


## Cybernetics and the Vietnam War

> Systems analysis and computer modeling also played important roles in formulating strategies used by the U.S. government during the Vietnam War. These approaches allowed the government to compile detailed quantitative maps of the political climate in different regions of Vietnam and use these data to guide U.S. wartime tactics. Secretary of Defense Robert S. McNamara championed these so-called scientific approaches and used them to create what he believed to be objective policies that emphasized cost effectiveness and centralized decision making
> 
> &#x2013; [[Cybernetic Revolutionaries]]


## Cybernetics and city planning

> Pittsburgh city planners drew explicitly from the work of defense intellectuals at RAND and elsewhere and used these approaches to predict future city processes, such as determining residential patterns
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> Lindsay’s view of the city as an information system spurred the creation of computerized data systems to increase data sharing among city departments and centralize decision making and control, although such efforts did not succeed in cutting city operating expenses nor, as Light observes, did they make life noticeably better for city residents.
> 
> &#x2013; [[Cybernetic Revolutionaries]]


## Criticisms

> the interpretative flexibility and broad applicability of cybernetic ideas also caused some in the scientific community to dismiss the field as a pseudoscience that lacked disciplinary rigor
> 
> &#x2013; [[Cybernetic Revolutionaries]]

With regards to application to social systems:

> These approaches have subsequently been criticized for oversimplifying the dynamics of social systems and for encouraging policy makers, academics, and Wall Street bankers to place too much trust in numbers
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> In addition, critics have pointed out that quantitative approaches encourage top-down management hierarchies that have grafted the structure and culture of the military onto the civilian agencies, businesses, and institutions of a democracy
> 
> &#x2013; [[Cybernetic Revolutionaries]]

<!--quoteend-->

> if we use ‘control’ in the sense of ‘maintain a large number of critical variables within limits of tolerance’. 
> 
> &#x2013; [[Anarchist Cybernetics]]


## See also

-   [[Viable System Model]]

