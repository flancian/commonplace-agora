# Preparing for org-roam v2

A big redesign is on the way for how [[org-roam]] works - lots of discussion [here](https://org-roam.discourse.group/t/org-roam-major-redesign/).  The biggest change seems to be a move to use id links rather than file links.

Lots of people in the thread seem happy about this.  A couple of things that I'm concerned about:

-   will this work fine with org-publish?
-   are you then more dependent on org / org-roam to view these files?
    
    I think things will probably work with org-publish fine, as id links are a long-standing org-mode thing in their own right, not a thing new to org-roam.
    
    The second point bugs me a bit.  I sometimes read my private wiki in orgzly / orgro on my phone.  I wonder how this will be affected.  Also I just like file links as being slightly more software-agnostic.
    
    Hey ho.  I'll try using id links for a bit now.  They work already in org-roam v1 from what I understand, as well as file links.
    
    Some snippets I'll add to my config:

<!--listend-->

```elisp
(setq org-id-link-to-org-use-id t)
(setq org-id-method 'ts) 
(setq org-roam-prefer-id-links t)
```

and 

```elisp
(defun ngm/org-hide-properties ()
  "Hide org headline's properties using overlay."
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward
	    "^ *:PROPERTIES:\n\\( *:.+?:.*\n\\)+ *:END:\n" nil t)
      (overlay-put (make-overlay
		    (match-beginning 0) (match-end 0))
		   'display ""))))

(add-hook 'org-mode-hook #'ngm/org-hide-properties)
```


## org-publish


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-27 Sat]</span></span>

> Unable to resolve link: "20210326T231455.451131"

during publish in the gitlab pipeline.  But weirdly, it's working locally if you do `org-publish-current-file`.

Here's one thing that might help get things published (via https://github.com/syl20bnr/spacemacs/issues/13387)

Hmm, actually, it also doesn't work locally if I run `make republish` rather than org-publish-current-file.  Is there a difference between publish current file and publish project?

I wonder if it's something do with relative links.  Weird though.

It seems like maybe org-publish just only works with file links for linking between files?  https://orgmode.org/manual/Publishing-links.html#Publishing-links However, still confused as to why `org-publish-current-file` works.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-04-04 Sun 09:23]</span></span>

I note [here](https://github.com/jethrokuan/braindump/blob/master/publish.el#L8) Jethro's use of `org-id-extra-files` in his export process:

```elisp
(let ((org-id-extra-files (find-lisp-find-files org-roam-directory "\.org$")))
(org-hugo-export-wim-to-md))
```

I wonder if this would resolve the issue I was having with org ids and exports?  From what I can tell of Jethro's export he does not use org-publish, he loops through his files one-by-one from his Ninja build and calls export on each file from there.  But still, I imagine this might help.

Test id link: [[org-roam]].

^ This appears to work!  During processing, when it hit this id link, `org-publish-project output`

> 1452 files scanned, 9 files contains IDs, and 34 IDs found.

It took a while.  Presumably it would only occur once per publish.


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-07-24 Sat]</span></span>

OK, so I've been pushed to do the update. 

-   I've run `M-x org-roam-migrate-wizard`.  No errors.  It was fairly quick, maybe 15-20 minutes.  (For reference, I have ~2000 note files).
-   It terms of editing my actual notes, it seems to just be adding an ID property to each of them, and updating the links in other notes to use an ID link.

Various problems.


#### "template needs to specify :if-new"

https://org-roam.discourse.group/t/v2-error-running-org-roam-dailies-find-today/1511/7

I was using a custom `org-roam-dailies-capture-templates`, in order to allow a different dailies folder. Just removing that seems fine, as the new built-in one works with `org-roam-dailies-directory` it seems.


#### spacemacs shortcuts are wrong

I guess the spacemacs layer needs updating.


#### org-id-open: Cannot find entry with ID

Dunno why.

From here: 

https://org-roam.discourse.group/t/org-roam-v2-org-id-id-link-resolution-problem/1491/4 

(org-id-update-id-locations (org-roam&#x2013;list-all-files))


#### capture template has file date on it


#### gitlab runner problems


##### needs gcc now for building emacs-sqlite

org-roam v2  has reverted from emacs-sqlite3 to emacs-sqlite.  This gets built during installation I believe, which requireds gcc.  My gitlab pipeline didn't have this.

I added:

```shell
apt-get --yes --force-yes install build-essential
```

in order for it to work.

see:  https://gitlab.com/ngm/commonplace/-/commit/d661df58e2b9801cbdad420160d17161b001f1d8


##### org-roam-db-build-cache has gone

You now need to use `org-roam-db-sync`.
see: https://gitlab.com/ngm/commonplace/-/commit/b4c257b251e0377ee9935b9323e12d4dd301ce3b


#### publishing issues

backlinks function was failing.

`org-roam--org-roam-file-p` has become `org-roam-file-p`.

That stops the build failing.

But I think something has also changed in the actual query itself, as no backlinks are being included.  I'll have to come back to that.

