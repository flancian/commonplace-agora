# info daemons

Some kind of virtual assistant that helps you with your [[information strategy]].

-   Fictional 
    -   [[Tom]] in [[Hyperland]]
    -   [[The Librarian]] in [[Snow Crash]]
    -   [[Jane]] in [[Speaker for the Dead]]
-   Real
    -   Clippy(!)
        -   Clippy might have sucked, but it's not a good counter argument to the idea of virtual assistants. (If a bridge collapses, do we say bridges are a terrible idea?)
-   Things that need more work
    -   [[Huginn]]
    -   Node-Red (https://nodered.org/)
    -   [[geist]]s
    -   Dual https://paulbricman.com/thoughtware/dual

Why do we need virtual assistants? How about we just work together and collaborate on things with other humans?  Maybe a virtual assistant does the drudge that you wouldn't want to ask another person to do. 

