# client-server model

> It may be called the “client-server” model, but most of us clients aren’t really looking to connect to a “server.” What we want is to connect to a website. More accurately, we want to connect to friends or content. This desire for connection however is currently owned by platform giants rolling out huge server farms that harvest the world for data, while we are held captive, force-fed through feeds.
> 
> &#x2013; [[Seeding the Wild]]

