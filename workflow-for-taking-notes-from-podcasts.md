# Workflow for taking notes from podcasts



## Making notes

I don't have a very good workflow for taking notes from podcasts.  I listen to them in [[AntennaPod]] usually while either going for a walk, or doing some household chores.  I'd like a way to capture when a particular comment or point in the podcast resonates with me.

I suppose MVP would be just to be able to bookmark that timestamp in the podcast.  That's not too difficult to do with AntennaPod - I can share to orgzly.

Better is to make a note with it as well.

The trouble with both of those is that often my hands are full.  I wonder if I can try voice commands for it.  I'd prefer not to use google voice stuff, but worth a shot.


## Finding podcasts

listennotes is good for finding podcasts on a particular topic.  But a silo.


## See

-   [Learning effectively with podcasts - Networked Thought and Learning](https://networkedthought.substack.com/p/learning-effectively-with-podcasts)

