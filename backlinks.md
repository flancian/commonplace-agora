# backlinks

> instead of having your notes be siloed into different folders and subfolders, you can let your notes be automatically organized based on how they relate to similar topics and points. A lot of people like to use the brain metaphor in that the bidirectional nature resembles how our brains work and how it makes connections with everything that we are thinking and learning about.
> 
> &#x2013; [How I use Obsidian to manage my goals, tasks, notes, and software development knowledge base](https://joshwin.imprint.to/post/how-i-use-obsidian-to-manage-my-goals-tasks-notes-and-software-development-knowledge-base) 


## Resources

-   https://maggieappleton.com/bidirectionals
-   [Backlinks in Cardigan Bay – Smart Disorganized](http://sdi.thoughtstorms.info/?p=1411)

