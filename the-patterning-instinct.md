# The Patterning Instinct

Author
: [[Jeremy Lent]]

> In this respect, the book shares with the postmodern critique of Western civilization, recognizing those capitalized universal abstractions such as Reason, Progress, and Truth to be culture-specific constructions. In fact, a significant portion of the book is devoted to tracing how these patterns of thought first arose and then infused themselves so deeply into the Western mind-set as to become virtually invisible to those who use them.

<!--quoteend-->

> It's a perpetual, bidirectional feedback loop. From this perspective, the currently fashionable [[reductionist view of history]] is half right: it captures a one-way causative flow from environment to cognition but misses the reciprocal causative flow in the other direction

<!--quoteend-->

> However, a more convincing explanation—and one that forms a foundation of this book—is that each society shapes the cognitive structure of individuals growing up in its culture through imprinting its own pattern of meaning on each infant's developing mind.

<!--quoteend-->

> Or, in its simplest terms: language has a patterning effect on cognition

^ Reminds me how in [[Free, Fair and Alive]] they talk about the need for an '[[OntoShift]]'.


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-12-24 Fri]</span></span>

Only on the preface&#x2026; but think I'll like it.  Don't know if I completely agree (yet) with the idea of 'cognitive history' - it sounds a bit in tension with [[Historical materialism]].  But maybe it might be a synthesis of the thesis of history being shaped by ideas and the antithesis of historical materialism.  I think it does allow for the luck of geography and material conditions of a society to shape the route of its progress - but also suggests that certain ways of thinking also have a large impact on this too.

