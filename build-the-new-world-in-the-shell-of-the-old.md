# Build the new world in the shell of the old

-   https://theanarchistlibrary.org/library/the-anarchist-faq-editorial-collective-an-anarchist-faq-09-17#toc8

Not that quoting Friedman is a desirable thing, but he was right about this:

> Only a crisis - actual or perceived - produces real change. When that crisis occurs, the actions that are taken depend on the ideas that are lying around. That, I believe, is our basic function: to develop alternatives to existing policies, to keep them alive and available until the politically impossible becomes the politically inevitable.
> 
> &#x2013; [[Milton Friedman]] 

