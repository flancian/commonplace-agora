# HyperCard

A kind of visual [[hypertext]]. 

-   [30-plus years of HyperCard, the missing link to the Web | Ars Technica](https://arstechnica.com/gadgets/2019/05/25-years-of-hypercard-the-missing-link-to-the-web/)
-   [Apple's HyperCard was inspired by an acid trip / Boing Boing](https://boingboing.net/2018/06/18/apples-hypercard-was-inspire.html)

