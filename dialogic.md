# dialogic

> Dialogic refers to the use of conversation or shared dialogue to explore the meaning of something.
> 
> &#x2013; [Dialogic - Wikipedia](https://en.wikipedia.org/wiki/Dialogic) 

Dialogic is key to me for learning.  

I first came across the term in [[Federated Education: New Directions in Digital Collaboration]].

