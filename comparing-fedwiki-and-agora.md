# Comparing FedWiki and Agora

I'm inspired by the way in which FedWiki is described in the section in [[Free, Fair and Alive]] on  [[A Platform Designed for Collaboration: Federated Wiki]].  And I feel Agora achieves this same principle, but in a different way.

In some ways better in my opinion - I think [[FedWiki]] is very advanced and does a great job of neighbourhoods, cloning pages and parts of pages, tracing lineage, etc.  But it feels a bit like a monoculture - I have no idea how much of it works outside of the FedWiki ecosystem.

Agora on the other hand I see as an aggregator and a bridge of digital gardens.  And these digital gardens can be made in whichever way you choose - as long as you can output them in a standard markdown format, they can be pulled in to the Agora.  My garden can (and does) live entirely outside of the Agora, in parallel with living inside of it.  This is really important.

See: http://vera.wiki.anagora.org/view/comparing-fedwiki-and-agora


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-11-05 Fri]</span></span>

-   Had a bit of a play with [[FedWiki]] at the farm [[vera]] set up.  I'm at http://neil.wiki.anagora.org.
    -   It is pretty wild, let me tell you.  It kind of feels like magic, the way you can navigate links between wikis, and see other pages in your neighbourhood.
    
    -   I feel like I will have some galaxy brain moment when it properly settles in.
    
    -   I could honestly imagine using it as my daily driver for the kind of federation features it offers.  But I'm far too tied to local-first editing in org-roam right now.  I think it'll take me a while to move from this paradigm.
    
    -   I think this is what [[Agora]] does for me - some of that federated wiki goodness, but just layered on top of what I'm already using - not needing to move elsewhere.
    
    -   FedWiki is far more advanced, but Agora grows all the time and may gain some of these federated features too.

