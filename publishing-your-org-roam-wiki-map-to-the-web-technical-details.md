# Publishing your org-roam wiki map to the web: technical details

-   org-roam-graph&#x2013;dot is where the url of the nodes is being set.
-   it's hard coded to be a local file with roam-protocol at the moment
-   just need to override that
-   it uses the org file path
-   so would need to make use of the publish filepath
-   dunno how to do that in an abstracted way
-   do some good old clone and own for now
-   [X] first pass - just get it to run on publish, don't worry about urls
    -   use org-roam-graph&#x2013;build
        -   without an arg, it'll build the full thing
-   [X] next pass: with web-based urls
-   next pass: break it into smaller images, per file
-   next pass: https://github.com/dagrejs/dagre-d3

<!--listend-->

-   make it more visually appealing and navigable (perhaps by publishing just the .dot file and using something like [dagre-d3](https://github.com/dagrejs/dagre-d3) to visualise it)
-   break it up into a map per page, so you get a local map, too.  To be seen whether this is a useful sensemaking aid or not.  Maybe it would just be another useful navigational aid. (by making use of org-roam's function for publishing a graph just for the current page)

