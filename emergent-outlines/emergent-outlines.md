# Emergent outlines



## Vectoralism, PKMs and IndieWeb


### Notes

-   [[Vectoralism]], [[PKM]]s and the [[IndieWeb]].


### Notions

-   [[Sharing information systems is praxis for the hacker class]]
    -   is it though?  a little bit grandiose.
    -   is anti-drm etc also praxis?


## The last thing we need is more tech growth

[[New UK tech regulator to limit power of Google and Facebook]].

The last thing we need is more tech growth.

The framing of this is all wrong.  It's all about innovation, competition, consumer choice, growth.  It should be about [[liberation]], [[user freedom]], [[agency]] and [[sustainability]].  

-   what's wrong with tech growth?  Reference Hail the maintainers maybe.


## [[Reweirding the web]]


## [[Experiments in community (digital) gardening]]

