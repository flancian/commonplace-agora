# Declaration of Digital Autonomy

-   https://techautonomy.org/

From the Software Freedom Conservancy.  I like the focus on consent, ownership, right to change/repair, privacy.  Too end-user focused though, I'd want some mention of the producers of technology&#x2026; there shouldn't be any exploitation anywhere in the supply chain.

They don't really state what they mean by [[Autonomy]] - I'll take it as the definition of "freedom to act without being controlled by others or manipulated by covert influences".

Describes 4 top-level principles of 'ethical technology'.  

-   in service of the people who use it
-   informed consent
-   empowering individual and collective digital action
-   protect people's privacy and other rights by design

Like that it gives some specific points for each of them.


### in service of the people who use it

-   freedom from surveillance, data gathering, data sales, and vendor and file format lock-in


### informed consent

-   study and understand the technology
-   what information is being collected and stored


### empowering individual and collective digital action

-   ability to change what you are using
-   ability to replace software on a device they have purchased
-   ability to repair the technology


### protect people's privacy and other rights by design

-   privacy
-   open communication
-   safety to develop ideas without fear of monitoring, risk, or retribution


## Some additions I'd want

-   doesn't really give any definition of what is meant by digital, autonomy, and technology.  The title references 'digital autonomy', then 'ethical technology' is the focus in the body.  I guess they're saying ethical technology should provide digital autonomy?
-   very focused on the user - but technology must be free from violence and exploitation throughout the supply chain;
-   who is it aimed at&#x2026; who is we?

