# catalyst group

> This sort of organizing council, composed of ideological anarchists and libertarian socialists that has the goal of spurring on further radicalization and prefiguration, is called a [[catalyst group]]. It stands in contrast to the vanguard model of the authoritarians.
> 
> &#x2013; [Constructing the Revolution](https://www.thecommoner.org.uk/constructing-the-revolution/) 

<!--quoteend-->

> The catalyst group is merely an Anarchist-Communist federation of affinity groups in action. The catalyst group, or revolutionary anarchist federation, would meet on a regular basis or only when necessary, depending on the wishes of the membership and the urgency of social conditions. It would be made up of representatives from the affinity group (or the affinity group itself), with full voting rights, privileges, and responsibilities. It would both set policies and future actions to be performed. It would produce both Anarchist-Communist theory and social practice. It believes in the class struggle and the necessity to overthrow Capitalist rule. It organizes in the communities and workplaces. It is democratic and has no authority figures like a party boss or central committee. 
> 
> &#x2013; [[Anarchist vs. Marxist-Leninist Thought on the Organization of Society]]

