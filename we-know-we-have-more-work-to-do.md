# We Know We Have More Work To Do

> We Know We Have More Work to Do (let’s call it W.K.W.H.M.W.T.D. for short) is the definitive utterance of the social media era, trotted out by executives whenever their companies come in for a public shaming.
> 
> &#x2013; [Opinion | Facebook Can’t Be Reformed - The New York Times](https://www.nytimes.com/2020/07/01/opinion/facebook-zuckerberg.html) 

<!--quoteend-->

> The phrase is both a promise and a deflection. It’s a plea for unearned trust — give us time, we are working toward progress. And it cuts off meaningful criticism — yes, we know this isn’t enough, but more is coming.
> 
> &#x2013; [Opinion | Facebook Can’t Be Reformed - The New York Times](https://www.nytimes.com/2020/07/01/opinion/facebook-zuckerberg.html) 

[[Facebook]].

