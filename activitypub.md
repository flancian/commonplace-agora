# ActivityPub



## Criticisms

> My big issues with ActivityPub is that the protocol is very big and not very easy to decompose.
> 
> &#x2013; indieweb chat

<!--quoteend-->

> Unfortunately, we have come to realize that using ActivityPub is considerably harder than we expected:
> 
> -   Using JSON-LD as an RDF serialization is very complicated. It requires the usage of algorithms (e.g. the Expansion Algorithm or the Framing Algorithm) that are incomprehensible and just pure madness. JSON-LD maybe was really just not intended to be an RDF serialization and trying to use it as such is painful.
> -   There are practically no implementations of the ActivityPub Client-to-Server protocol (C2S). This made developing and testing the client and server more time-consuming as we had to develop the protocol in lockstep on client and server. At the end we were still only compatible with our own software.
> -   ActivityPub is not a complete specification and many additional protocols need to be implemented (e.g. WebFinger) in specific ways in order to be compatible with existing servers.
>     
>     &#x2013; [openEngiadina: From ActivityPub to XMPP — inqlab](https://inqlab.net/2021-11-12-openengiadina-from-activitypub-to-xmpp.html)

