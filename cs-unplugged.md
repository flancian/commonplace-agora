# CS Unplugged

URL
: https://csunplugged.org/en/

Summary
: "CS Unplugged is a collection of free teaching material that teaches Computer Science through engaging games and puzzles that use cards, string, crayons and lots of running around."

