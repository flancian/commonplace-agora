# 2020-10-23



## [[Outlining vs writing]]

This dude is a bit.. overly passionate about why he thinks [[Obsidian]] is better than [[Roam]].

-   https://www.youtube.com/watch?v=_x54XJrECvk

I don't have a horse in that race.  But his points on outlining vs writing, and databasing vs writing, are interesting.

I don't think he actually said in what way Obsidian is better for writing.  But he says Roam is good for 'connector databasers', and Obsidian is good for 'connector writers'.

It's been on my mind for a while as to how I can best use org-roam for writing.

