# The government should invest in the NHS, not privatise it

[[NHS]].  [[Privatisation]].

[The Guardian view on NHS privatisation: the wrong treatment | Editorial | The&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/21/the-guardian-view-on-nhs-privatisation-the-wrong-treatment)


## Because

-   [[For-profit healthcare systems are a piece-of-shit]]


## But

-   I am still interested in commons-based / municipal forms of healthcare, as opposed to state health provision.


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Yes definitely]]

