# Viable system model

> a general model that he believed balanced centralized and decentralized forms of control in organizations.
> 
> &#x2013; [[Cybernetic Revolutionaries]]

A mixture of horizontal autonomy with channels for vertical communication and stabilisation.

> It offered a balance between centralized and decentralized control that prevented both the tyranny of authoritarianism and the chaos of total freedom.
> 
> &#x2013; [[Cybernetic Revolutionaries]]

At first blush there feels like some overlap between the Viable System Model and Elinor Ostrom's Institutional Analysis and Development framework.

In that they both approach structures from a multi-level conceptual map, with units acting autonomously at each level but communicating between them. The polycentrism thing.

Would be interesting to compare and contrast them. 

[[Stafford Beer]].

> He explains that System Three is concerned with the ‘inside and now’ of the organisation and System Four with the ‘outside and then’ ([1979] 1994). This mirrors how strategy is defined by scholars of organisation theory (for example, Carter et al., 2008),
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> System Five, where overarching goals and political strategy is developed, is something that ought to be rooted in everyone involved in the system.
> 
> &#x2013; [[Anarchist Cybernetics]]

