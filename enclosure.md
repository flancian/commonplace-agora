# Enclosure

> Enclosure is thus a profound act of dispossession and cultural disruption that forces people into both market dependency and market frames of thought.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The process of enclosure is generally driven by investors and corporations, often in collusion with the nation-state, to commodify shared land, water, forests, genes, creative works, and much else.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Enclosure is the opposite of commoning in that it separates what commoning otherwise connects — people and land, you and me, present and future generations, technical infrastructures and their governance, conservation areas and the people who have stewarded them for generations.
> 
> &#x2013; [[Free, Fair and Alive]]

