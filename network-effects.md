# Network effects

> Popular platforms tend to get even more popular over time, network effects kicked in as clients choose those platforms that expose them to a wider audience. 
> 
> &#x2013; [[Seeding the Wild]]

