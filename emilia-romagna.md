# Emilia Romagna

> in Emilia Romagna in Italy co-operative enterprises generate close to 40% of GDP.
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]

