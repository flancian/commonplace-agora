# We are Commoners

URL
: https://craftspace.co.uk/wearecommoners/

> This exhibition invites you to become or recognise yourself as a ‘Commoner’. Featuring UK and international artists, the projects exhibited in this exhibition represent ideas and resources to inspire acts of commoning.

