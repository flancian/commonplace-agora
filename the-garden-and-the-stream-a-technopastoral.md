# The Garden and the Stream: A Technopastoral

URL
: https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/

Author
: [[Mike Caulfield]]

Date published
: 2015-10-17

These are quite nice metaphors for the [[The blog and wiki combo]].  From Mike Caulfield's article: [The Garden and the Stream: A Technopastoral](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/).


## [[the Garden]]

> building out a network of often conflicting information into a web that can generate insights, iterating it, allowing that to grow into something bigger than a single event, a single narrative, or single meaning.
> &#x2013; [The Garden and the Stream: A Technopastoral ](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/) 


## [[the Stream]]

> the Stream replaces topology with serialization. Rather than imagine a timeless world of connection and multiple paths, the Stream presents us with a single, time ordered path with our experience (and only our experience) at the center. 
> 
> &#x2013; [The Garden and the Stream: A Technopastoral ](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/) 

![[images/the-stream.jpg]]


### Scale

But is he talking about the big stream of everything?  And a big garden of interconnected sites?  Or individual streams and individual gardens.

