# molnupiravir

-   oral antiviral pill for Covid
-   a pill that can be taken twice daily at home
-   due for delivery from mid-November
-   priority to elderly Covid patients and those with e.g. weakened immune systems
-   a clinical trial in the US showed a five-day course of the pills halved the risk of hospitalisation or death for at-risk patients

[[Coronavirus]]

-   [UK first to approve oral antiviral molnupiravir to treat Covid | Coronavirus &#x2026;](https://www.theguardian.com/world/2021/nov/04/uk-is-first-to-approve-oral-antiviral-pill-molnupiravir-to-treat-covid)

