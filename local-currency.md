# local currency

> For example, in extremely poor neighborhoods in Kenya, the [[Bangla Pesa]] and [[Lida Pesa]] are neighborhood-owned and -controlled currencies, part of the larger [[Sarafu-Credit]] system. The currencies enable members to capture and recirculate value created within the community while preventing the outside economy from siphoning it away. Such systems are complementary to conventional (fiat) money and serve as building blocks for a commons-based economy.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Researcher Grzegorz Sobiecki estimates there are more than 6,000 alternative currencies worldwide.
> 
> &#x2013; [[Free, Fair and Alive]]

