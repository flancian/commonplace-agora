# Building alternative social media

[[Alternative social media]].


## As per Anarchist Cybernetics

There's a chapter called exactly this, "Building Alternative Social Media" in [[Anarchist Cybernetics]] on this.

With a list of desirable features:

-   Forums for conversation
-   Direct messaging
-   Sharing content with meme/viral potential
-   News feed with curated and user-generated content
-   Public relations function
-   Linking alternative and mainstream platforms
-   Central resource hub
-   Noise and overload filters
-   Overuse addiction warning system
-   General assembly function
-   Pad function
-   Memory / narrative
-   File sharing and knowledge exchange
-   Procrastination
-   Online decision making embedded in offline structures
-   Sections open to non-members
-   Architecture for [[collective autonomy]]
-   Flexible engagement options
-   Responsibility and commitment
-   Privacy, security and data
-   Easy backup of information

