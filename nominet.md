# Nominet

> Nominet UK is the overall registry for the .uk domain name. Nominet directly manages registrations directly under .uk, including second level domains .co.uk .org.uk .me.uk and .ltd.uk. Nominet also manages the .wales and .cymru domains.
> 
> &#x2013; [Nominet UK - Wikipedia](https://en.wikipedia.org/wiki/Nominet_UK)

-   [Pressure builds on Nominet as members demand to know leadership's contingency&#x2026;](https://www.theregister.com/2021/03/02/nominet_board_vote/)

> The .UK namespace is critical infrastructure; vital to the national interest, commerce and the wider Internet community. Nominet is an incredible organisation that can proudly boast superb technical capabilities, a deep and passionate community and an altruistic spirit.
> 
> For the last 25 years Nominet has managed itself and we members must demonstrate to industry and government that we have the means to continue to do so. If we do not, Nominet could be the target of Government intervention. 
> 
> -   [Public Benefit .UK](https://publicbenefit.uk/)

