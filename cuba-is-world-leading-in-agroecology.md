# Cuba is world-leading in agroecology

[[Cuba]] is world-leading in [[Agroecology]].


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

Source (for me)
: [[Salvatore Engel-Di Mauro]] [[Socialist States &amp; the Environment w/ Salvatore Engel-Di Mauro]]

Don't see why not, but I would like to fact check a bit before espousing.  I also don't **really** know what agroecology is.

