# Commons

[[I like commons]].

In a nutshell: 'a commons' is a shared thing between a bunch of people that they actively maintain together.

The idea is that they are "beyond market and state".

Commons can be found in all kinds of walks of life - the environment (grazing lands, fisheries, community forests), culture, digital realm, knowledge commons.

There's a lot to unpack.  My favourite book on commons and commoning is [[Free, Fair and Alive]].


## What is a commons?

Also what's the difference between 'the Commons' and 'a commons'?

> The Commons is a means of provisioning and governance that generally doesn't need the permission of legislatures or courts to move forward.
> 
> &#x2013; [[David Bollier]], [[Stir to Action]] Issue 30

<!--quoteend-->

> The commons are cared for by the those that directly inhabit and gain from its wealth.
> 
> &#x2013; [[Seeding the Wild]]

<!--quoteend-->

> Despite vivid differences among commons focused on natural resources, digital systems, and social mutuality, they all share structural and social similarities.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> So instead of conceiving of commons as closed systems of common property managed by a “club,” it is more productive to see them as social organisms who, thanks to their [[semi-permeable membrane]]s, can interact with larger forces of life — communities, ecosystems, other commons.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The commons is not simply about “sharing,” as it happens in countless areas of life. It is about sharing and bringing into being durable social systems for producing shareable things and activities.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Commons are living social systems through which people address their shared problems in self-organized ways.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The commons is a robust class of self-organized social practices for meeting needs in fair, inclusive ways.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Each commons depends on social processes, the sharing of knowledge, and physical resources. Each shares challenges in bringing together the social, the political (governance), and the economic (provisioning) into an integrated whole.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The elemental human impulse that we are born with — to help others, to improve existing practices — ripens into a stable social form with countless variations: a commons.
> 
> &#x2013; [[Free, Fair and Alive]]


### How big is a commons?

> In a commons,  the resource can be small and serve a tiny group (the family refrigerator), it can  be  community-level  (sidewalks,  playgrounds,  libraries,  and  so  on), or it can extend to international and global levels (deep seas, the atmosphere, the Internet, and scientific knowledge).
> 
> &#x2013; [[Understanding Knowledge as a Commons]]

<!--quoteend-->

> The commons can be well bounded  (a  community  park  or  library);  transboundary  (the  Danube River,  migrating  wildlife,  the  Internet);  or  without  clear  boundaries (knowledge, the ozone layer).
> 
> &#x2013; [[Understanding Knowledge as a Commons]]


## Why?

> the commons is not just about small-scale projects for improving everyday life. It is a germinal vision for reimagining our future together and reinventing social organization, economics, infrastructure, politics, and state power itself. 
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The commons is a social form that enables people to enjoy freedom without repressing others, enact fairness without bureaucratic control, foster togetherness without compulsion, and assert sovereignty without nationalism.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> A commons &#x2026; gives community life a clear focus. It depends on democracy in its truest form. It destroys inequality. It provides an incentive to protect the living world. It creates, in sum, a politics of belonging.”
> 
> &#x2013; [[Free, Fair and Alive]]


## Politics of it

> The world of commoning represents a profound challenge to capitalism because it is based on a very different ontology.
> 
> &#x2013; [[Free, Fair and Alive]]


## Difficulties for commons

> Potential  problems  in  the  use,  governance,  and  sustainability  of  a commons  can  be  caused  by  some  characteristic  human  behaviors  that lead to social dilemmas such as competition for use, free riding, and over- harvesting. Typical threats to knowledge commons are commodification or enclosure, pollution and degradation, and nonsustainability.
> 
> &#x2013; [[Understanding Knowledge as a Commons]]


## Related

-   [[Elinor Ostrom]]
-   [[Commons-based peer production]]
-   [[a re-commonification of welfare systems]]

