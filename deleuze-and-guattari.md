# Deleuze and Guattari

![[images/deleuze-guatarri.png]]

> Deleuze and Guattari were both resolutely anti-individualist: whether in the realm of politics, psychotherapy or philosophy, they strived to show that the individual was a deception, summoned up to obscure the nature of reality.
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 

<!--quoteend-->

> a progressive, Marxist-inspired, anti-capitalist politics of joy
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 

Sounds pretty good to me.

> In the political arena, Guattari claimed that the ‘centralist disease of communist parties is due less to the ill intentions of their leaders than to the false relationships they establish with mass movements’. His problem was never a particular person – be they schizophrenic or Stalinist – but the process by which groups break up into discrete units, detached from one another, and from their own lives.
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 

<!--quoteend-->

> contrary to the assumptions of most philosophers, thought isn’t representational – which is to say, it doesn’t function by making pictures of the world, which can be judged as true or false depending on their degree of accuracy. By contrast, said Deleuze, thought is creative, and always connected to that which it thinks about.
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 

<!--quoteend-->

> thought is not grounded in identity; rather, it is generated out of difference. 
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 

<!--quoteend-->

> Why do collaborations always collapse into hierarchies, he asked himself? Why does the group get atomised, rather than retaining a unified voice?
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 


## See also

-   A City is not a Tree - [Thread by @NajlaAlariefy on Thread Reader App – Thread Reader App](https://threadreaderapp.com/thread/1315329838559965191.html)  <&#x2013; feels kind of D&G

