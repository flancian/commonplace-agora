# Resist the feudal internet

Via [@pfrazee](https://twitter.com/pfrazee)'s [article](https://infocivics.com/) on [[information civics]], came across this old [article of Bruce Schneier](http://en.collaboratory.de/w/Power_in_the_Age_of_the_Feudal_Internet)'s on what he calls the [[feudal internet]].  

In his analogy, we're the peasants who have traded in freedom for some convenience and protection.

> Users pledge allegiance to more powerful companies who, in turn, promise to protect them from both sysadmin duties and security threats.

He sees the two big power centres of the feudal lords as **data** and **devices**.

> On the corporate side, power is consolidating around both vendor-managed user devices and large personal-data aggregators. 

We no longer have control of our data:

> Our e-mail, photos, calendar, address book, messages, and documents are on servers belonging to Google, Apple, Microsoft, Facebook, and so on. 

I see the [[IndieWeb]], [[Beaker]], etc as means of resisting this.

And we're no longer in control of our devices:

> And second, the rise of vendor-managed platforms means that we no longer have control of our computing devices. We’re increasingly accessing our data using iPhones, iPads, Android phones, Kindles, ChromeBooks, and so on. 

I see the [[right to repair]] as a means of resisting this.  Allowing us to do what we wish with our own devices - including putting whatever software on them that we want.

One big omission from the article I find is that Schneier focuses on the disbenefits to the **users** of these devices and platforms - the [[manufactured iSlaves]], in Jack Qiu's terminology.   He doesn't mention (at least in this particular article) those exploited in the creation and upkeep of these - the [[manufacturing iSlaves]].  That's just as big, if not bigger, a reason for challenging these power structures.

