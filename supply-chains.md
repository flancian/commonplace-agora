# supply chains

-   [Restart Radio: Global supply chains and The Virus - The Restart Project](https://therestartproject.org/podcast/global-supply-chains/)

At times when supply chains fail, [[decentralised manufacture]] (and repair) can help with resilience.

It might not be possible to fully decentralise manufacture of some things.  Decentralised [[repair]] should also be enabled, to allow people to keep things going for longer locally.

-&#x2014;

