# Eco-anxiety

> the chronic fear of environmental doom
> 
> &#x2013; [‘Eco-anxiety’: fear of environmental doom weighs on young people](https://www.theguardian.com/society/2021/oct/06/eco-anxiety-fear-of-environmental-doom-weighs-on-young-people)

