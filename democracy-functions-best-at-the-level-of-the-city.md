# Democracy functions best at the level of the city

[[Democracy]] functions best at the level of the [[city]].

Seem to have not noted where this was mentioned.  I feel like it was [[James Muldoon]] on a podcast.

I could buy this.  I like [[municipalism]].  Don't know what the evidence is to back it up though.


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

