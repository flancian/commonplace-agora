# The Social Life of Commoning

Part of the [[Triad of Commoning]].


## Patterns

-   [[Cultivate Shared Purpose and Values]]
-   [[Ritualize Togetherness]]
-   Contribute Freely
-   Practice Gentle Reciprocity
-   Trust Situated Knowing
-   [[Deepen Communion with Nature]]
-   Preserve Relationships in Addressing Conflicts
-   Reflect on Your Peer Governance

