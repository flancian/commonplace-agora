# Review: Future Histories

There's a lot to chew on in [[Future Histories]]. Thematically it is right up my street, in that it is linking leftist ideas from history to modern issues around digital technology and [[technology capitalism]].  It is ultimately about how [[technology should be liberatory]], while warning against [[techno-utopianism]].

> As the planet slides further toward a potential future of catastrophic climate change, and as society glorifies billionaires while billions languish in poverty, digital technology could be a tool for arresting capitalism’s death drive and radically transforming the prospects of humanity. But this requires that we politically organize to demand something different.

![[images/future-histories.jpg]]

[[Fanon]] and his work on [[colonialism]] are used as a frame for [[digital self-determination]].  The historial commons is linked to the [[digital commons]].  [[Thomas Paine]] is a jumping off point for [[universal basic income]] and [[services]].  And lots of other interesting juxtapositions.

It's full of ideas and statements that I agree with.  It's so choc full of stuff that I'm not sure that I've come away from it with a coherent idea of what is to be done - it's more of a manifesto than a handbook.  Each chapter does have broad strokes of ideas, just more long-term legislative or policy demands than immediate opportunities for praxis. But definitely good jumping off points.  For example, [[decentralisation]], [[libre software]] and [[IndieWeb]] adjacent ideas (e.g. [[Solid]]) are mentioned for digital self-determination, although you'll be left to your own devices as to how you do something practical with those ideas.

Anyway, it's something I will definitely return to when I circle round to particular ideas again.

