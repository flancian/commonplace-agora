# How does linking digital gardens facilitate a richer commons?

> This shift from the standard wiki to a form of writing based on “one person, one wiki, in a federated environment,” may sound like a step backward from the Wikipedia style of open collaboration. But in fact the effect is quite the opposite: **giving online platforms to individual voices while bringing them together into a shared neighborhood of wikis results in a richer, more robust commons**.
> 
> &#x2013; [[A Platform Designed for Collaboration: Federated Wiki]]

Why?

I guess first of all, what is the commons that is being produced?

A commons is a combination of social, political (governance) and economic (provisioning) factors.
A collection of digital gardens are a knowledge commons.

But what is it's value? There is an assumption that the content of the gardens together produces something of value.  I don't think this can just be assumed.  If you've just a bunch of individual gardens linked together with no purpose, there's not necessarily a benefit.

I guess the assumption is that there's some intention behind the conjoined wikis - i.e. they are all working together on some topic. So yeah one benefit comes from if it's a group of gardens with a shared purpose, I think.

But in the absence of purpose I think another benefit is that it faciliates going on a [[dérive]] drift and learning about unexpected new things.

