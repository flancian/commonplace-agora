# one hour for the revolution

A term I first saw [[Flancian]] use.

I like it.  I've had in my todo list for a while a recurring [[praxis]] task.  This feels kind of the same thing.  Focused time on the revolution.

I'm sure there's plenty of counterarguments.  e.g. what is one doing outside of that hour?  contributing to status quo?  I guess is revolutionary practice a constant way of life, or a focused task.

[[Pomodoro praxis]]. [[Praxis for the hacker class]].

