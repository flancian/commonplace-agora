# Cultural hegemony

> When a conception permeates a thought collective strongly enough, so that it penetrates as far as everyday life and idiom and has become a viewpoint in the literal sense of the word, any contradiction appears unthinkable and unimaginable.”
> 
> &#x2013; [[Free, Fair and Alive]]

