# The Quantum Thief

I absolutely loved this book.  So full of ideas and vivid imagery.

Can't remember when I first read it, 2017 sometime I think.

Mum recommended it to me.

[[Gevulot]].

> Sending brain-to-brain messages directly through a quantum teleportation channel seems like a dirty, invasive way to communicate compared to [[Oubliette]] [[co-remembering]]. The latter is much more subtle: embedding messages in the recipient’s [[exomemory]] so that information is recalled rather than received. 
> 
> &#x2013; [[The Quantum Thief]]

Garden vs stream? 

> Gevulot. Of course. I am an idiot. There is a boundary in her memories, between those which are local and exo

<!--quoteend-->

> The Watch is meaningless on its own, without the public keys – gevulot – inside the brain

<!--quoteend-->

> They also do something called co-remembering, sharing memories with others just by sharing the appropriate key with them


## <span class="timestamp-wrapper"><span class="timestamp">[2021-12-15 Wed]</span></span>

Reading this again.  Just as good as last time.

For example - it starts in a prison that puts the inmates through the [[iterated prisoner's dilemma]], until they learn to cooperate.

I still really like all the [[exomemory]], [[gevulot]] stuff.  And the notion of Time as a currency.

