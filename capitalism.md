# Capitalism



## What is it

> Capitalism as a way of organizing economic activity has three critical components: private ownership of capital; production for the market for the purpose of making profits; and employment of workers who do not own the means of production
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> Capitalism is not only about economy. It is about resources, it is about nature, it is about how we organize and live our life. Capitalism is colonization and centralization, is monopoly and monoculture, is domination over life and nature. To implement this domination, capitalism has learned to hide itself under different masks, and today is dressed up with different costume.
> 
> &#x2013; [About the climate strike and the dark sides of the “green new deal”](https://makerojavagreenagain.org/2019/09/19/about-the-climate-strike-and-the-dark-sides-of-the-green-new-deal/)

Other non-capitalist forms of economy exist in pockets around capitalism.  So not **everything** is capitalist, but it hegemonic.

> Existing economic systems combine capitalism with a whole host of other ways of organizing the production and distribution of goods and services: directly by states; within the intimate relations of families to meet the needs of its members; through community-based networks and organizations; by cooperatives owned and governed democratically by their members; though nonprofit market-oriented organizations; through peer-to-peer networks engaged in collaborative production processes; and many other possibilities.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> We call such a complex economic system “capitalist” when capitalist drives are dominant in determining the economic conditions of life and access to livelihood for most people. That dominance is immensely destructive.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> with ever increasing concentrations of wealth in fewer and fewer hands, with capitalism roaming the globe in search of profits, with a deepening contradiction between the colossal growth of production and the failure to distribute its fruits justly.
> 
> &#x2013; [Howard Zinn on How Karl Marx Predicted Our World Today](https://inthesetimes.com/article/21125/karl_marx_howard_zinn_birthday_capitalism_200)

<!--quoteend-->

> Nevertheless, as Jacobin editor Bhaskar Sunkara wrote: “The core of the system he described is little changed. Capitalism is crisis-prone, is built on domination and exploitation, and for all its micro-rationality has produced macro-irrationalities in the form of social and environmental destruction.
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> At its core, capitalism was defined by Marx as a social relation of production. He meant that profits are not the result of good accounting or the inventive ideas of the superrich, but are instead the outcome of an exploitative relationship between two classes of people: bosses and workers
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Rather than a peasantry violently coerced to turn over goods to their lords, capitalism created a new underclass of wageworkers—a class of people theoretically free to work where and how they pleased, but who would in practice be compelled—by economic necessity—to produce a surplus for someone else nonetheless
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> capitalism is not simply an economic or a political structure, but a system of social relationships, whose foundation is the expropriation of masses of people from the land
> 
> &#x2013; [[A People's Guide to Capitalism]]


## Where it came from

Feudalism morphed into capitalism.  AFAIU it was a long process (decades/centuries).  Not a global rupture out of nowhere.  Started in England, then was exported with colonialism (and maybe also started of its own accord elsewhere?)

> Classical economists like [[Adam Smith]] argued that capital came to be through a gradually evolving division of labor, where some people became traders, and some of these traders would eventually—through thriftiness or hard work—save enough wealth to build factories and employ workers
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> This process may have been punctuated by political upheavals and even revolutions, but rather than constituting a rupture in economic structures, these political events served more to ratify and rationalize changes that had already taken place within the socioeconomic structure
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> Although capitalism became a dominant society only in the past few centuries, it long existed on the periphery of earlier societies: in a largely commercial form, structured around trade between cities and empires; in a craft form throughout the European Middle Ages; in a hugely industrial form in our own time; and if we are to believe recent seers, in an informational form in the coming period.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> The division of society into haves and have-nots did not gently come to pass, and certainly not through the frugalness and intelligence of a small elite. It was the outcome of a violent upheaval, which forced large swaths of the population from their lands and traditional means of self-sufficiency
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> The violence, coercion, legislation, and upheavals necessary for the birth of this new system evince just how unnatural and vicious the road to capitalism was
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> at capitalism’s dawn, the rising bourgeoisie depended heavily on the power of the state to enforce its collective will
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> So long as families could produce food and clothing for themselves, they did not have to work for a wage. Once the vast majority of people lost access to their lands, the organizing principles of society would change.
> 
> &#x2013; [[A People's Guide to Capitalism]]


### [[Primitive accumulation]]


## Why England?

Theory that the use of money for rent as tribute in England was one of reasons capitalism started there. Plenty of other places had a tribute system, but was it the use of money and rent relations that kicked off capitalism?

> England, where capitalism gained its first foothold, it did so on the basis of what’s referred to as the “[[Enclosure Movement]].” Millions of acres of common land were violently confiscated and turned into privately-owned plots during several centuries. Traditional rights to use common land for farming or grazing livestock were revoked, land was fenced in (enclosed) and restricted to private owners—whether through payment, theft, or law


## The problems it causes

> Capitalism is an inequality-enhancing machine as well as a growth machine.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

-   [[capital strike]]

> free market [&#x2026;] relentless drive to colonize every aspect of human existence, glossing over its incapacity to effectively manage common resources and its structural tendency to externalize a range of economic, social and environmental costs. 
> 
> &#x2013; [[Future Histories]] 


### Externalities

Moore's Law, for example, was fuelled by underpaid workers working with toxic solvents.  As the transistors got smaller, the solvents got more toxic.
(Note: haven't verified this, just heard it on the radio.  Could do with a proper source&#x2026;.
)


## In crisis

Capitalism keeps on fucking up, with crashes and depressions.

-   bit more nuanced discussion: [Glossary of Terms: Crisis](https://www.marxists.org/glossary/terms/c/r.htm#crisis-of-capitalism)

-   see [Crisis theory](https://en.wikipedia.org/wiki/Crisis_theory)


## Anti-capitalism

-   [[Logics of resistance]]


## Post-capitalism

What exactly is post-capitalism?  Guess I'll have to read Paul Mason's book on it.  But it seems like an odd phrasing.  Value-neutral.  If it's just meaing 'whatever comes after capitalism', then what says that that is necessarily going to be better?  I think we should apply some indication of what we expect to be after capitalism; if not, what's to stop it becoming something else in the vacuum?


## [[Neoliberalism]]


## How will it end

> capitalism is not an eternal system embedded in our nature. It has a history and an origin, and therefore, it can have an end
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> The power of the few to extract labor and profits from the many is based on a relationship of economic dependence, which is historically conditioned
> 
> &#x2013; [[A People's Guide to Capitalism]]


## Misc

> We can foresee a time when the proletarian, whatever the color of his or her collar or place on the assembly line, will be completely replaced by automated and even miniaturized means of production that are operated by a few white-coated manipulators of machines and by computers.
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> Capitalism, in effect, has generalized its threats to humanity, particularly with climatic changes that may alter the very face of the planet, oligarchical institutions of a global scope, and rampant urbanization that radically corrodes the civic life basic to grassroots politics.
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> The “prevailing life-motif” of modern capitalism and the liberal state, writes Greek social critic Andreas Karitzis: promotes the idea that a good life is essentially an individual achievement. Society and nature are just backdrops, a wallpaper for our egos, the contingent context in which our solitary selves will evolve pursuing individual goals. The individual owes nothing to no one, lacks a sense of respect for the previous generations or responsibility to future ones — and indifference is the proper attitude regarding present social problems and conditions.
> 
> &#x2013; [[Free, Fair and Alive]]

