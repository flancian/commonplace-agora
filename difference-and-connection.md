# Difference and connection

[Part five of the Deleuze & Guatarri Philosophize This! podcast](http://philosophizethis.org/deleuze-difference/) is awesome. 

They talk about this idea of becoming rather than being.  We are better in a constant process of becoming.  And that the way to facilitate becoming is through availability of links and connections, access to connections through the [[rhizome]] to discover new things.

They also make the link between the connected nature of the rhizome and the connected nature of the city. If you segregate parts of the city, say this is the business district, this is the residential district, this is the shopping district, entertainment district etc. Then, it doesn't work, the city doesn't function. As per [[Jane Jacobs]].

Cities function better when everything's mixed together, when diverse connections are easily made. And I think this applies to the online city, the web. We have Facebook and Twitter, huge districts of planned homogeneity locking in and blocking off certain paths. 

Constructing environments that allows for those connections to be made dovetails so nicely into some of the stuff I'm interested in with regards to connections on the web.  It would be better to have smaller communities of intermingled activities. The [[IndieWeb]], the [[fediverse]], the [[weird web]], allowing unfettered connections between diverse entities.  Perhaps we could do better at joining them up, but at least they're there to be joined.

