# complexity science

> Insights from complexity science help move beyond a Newtonian worldview of cause and effect to one that is holistic, nonlinear, and interactive.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Complexity science offers a more coherent way of explaining how functional design can emerge without a designer. Design happens as adaptive agents (such as commoners) interact with each other. The self-organization of agents — what we call “[[peer organization]]” in a commons — gives rise incrementally to complex organizational systems. There is no master blueprint or top-down, expert-driven knowledge behind the process. It emerges from agents responding to their own local, bounded circumstances.
> 
> &#x2013; [[Free, Fair and Alive]]


## See

-   [[complex systems]]
-   [[Evolutionary and adaptive systems]]

