# Prefiguration

Means embodying or reflecting ends.

> prefiguration refers to the means or methods of political action mirroring its ends or goals and thus reflecting the values that underpin them.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> prefiguration is a practice, something that people do, and not just an abstract political concept.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> methods and means cannot be separated from the ultimate aim. The means deployed become, through individual habit and social practice, part and parcel of the final purpose; they influence it, modify it, and presently the aims and means become identical.
> 
> &#x2013; [[Emma Goldman]]

<!--quoteend-->

> We must therefore strive to make this organisation as close as possible to our ideal. How could we expect an egalitarian and free society to emerge out of an authoritarian organisation! It is impossible. The International, embryo of the future human society, must be, from now on, the faithful image of our principles of liberty and federation, and must reject from within any principle tending toward authority, toward dictatorship.
> 
> &#x2013; The [[Jura Federation]]

<!--quoteend-->

> It is often summed up by reference to the [[Industrial Workers of the World]] slogan ‘building a new world in the shell of the old’ [[[Build the new world in the shell of the old]]] and its origins are to be found in the anarchist tradition from the late 19th century onwards.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> practising prefiguration has meant always trying to make the process we use to achieve our immediate goals an embodiment of our ultimate goals, so that there is no distinction between how we fight and what we fight for, at least not where the ultimate goals of a radically different society is concerned.
> 
> &#x2013; [[Anarchist Cybernetics]]

-   Prefiguration alone makes you a pocket in isolation, just or doing charity work.  It needs some structure and greater organising to go with.
-   But just structure and planning without prefiguration means you lose sight of where you're going.

