# the topology of information networks

> When the internet first came along it looked like an immensely liberating tool, poised to rebalance the power brought about by the press and consolidation of media ownership. People thought the internet would finally evolve the topology of information networks: from one-to-many to many-to-many.
> 
> &#x2013; [[Seeding the Wild]]

