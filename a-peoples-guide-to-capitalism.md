# A People's Guide to Capitalism

A
: [[book]]

URL
: https://www.haymarketbooks.org/books/1481-a-people-s-guide-to-capitalism

Author
: [[Hadas Thier]]

> Their quantitative exchange relation is at first determined purely by chance. They become exchangeable through the mutual desire of their owners to alienate them. In the meantime, the need for others’ objects of utility gradually establishes itself. The constant repetition of exchange makes it a normal social process. In the course of time, therefore, at least some part of the products must be produced especially for the purpose of exchange

