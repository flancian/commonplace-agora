# Ray Ison, "Systems Practice: How to Act In Situations of Uncertainty and Complexity in a Climate-Change World"

URL
: https://newbooksnetwork.com/systems-practice-how-to-act

Featuring
: [[Ray Ison]]

Topics
: [[Systems thinking]]

Interview based around the book [[Systems Practice: How to Act]].

The juggler isophor for applying systems thinking.

Despite it being about **practice**, felt pretty philosophical to me - didn't grasp it all on this first pass.

Makes a distinction between the terms [[systemic]] and [[systematic]].  If I recall correctly, systemic being more about the relationships, and systematic being&#x2026; I forgot.  But Ray Ison's preference was for the systemic.

