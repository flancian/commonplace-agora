# Coronavirus

The best source for daily updates so far has been the [[Guardian]].

Then, shows where you hear from members of the public, like Any Answers.

[[This week]], schools closed to most students, and that's causing a lot of problems.   There's anger about self-employed being left out of financial support measures.  Apparently lots of panic buying.  Public transport services are going to be reduced.

> Experts across the board agree that social distancing is a necessary aspect of the pandemic response, but this is only sustainable long term if people have the social safety nets available that can help incentivize staying home. 
> 
> &#x2013; [Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance](https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711) 


## From the first wave


### Social distancing


#### Guidance on social distancing for everyone in the UK - GOV.UK

> Social distancing measures are steps you can take to reduce social interaction between people. This will help reduce the transmission of coronavirus (COVID-19).
> 
> They are to:
> 
> Avoid contact with someone who is displaying symptoms of coronavirus (COVID-19). These symptoms include high temperature and/or new and continuous cough
> Avoid non-essential use of public transport when possible
> Work from home, where possible. Your employer should support you to do this. Please refer to employer guidance for more information
> Avoid large and small gatherings in public spaces, noting that pubs, restaurants, leisure centres and similar venues are currently shut as infections spread easily in closed spaces where people gather together.
> Avoid gatherings with friends and family. Keep in touch using remote technology such as phone, internet, and social media
> Use telephone or online services to contact your GP or other essential services
> 
> Everyone should be trying to follow these measures as much as is practicable.

<!--quoteend-->

> You can also go for a walk or exercise outdoors if you stay more than 2 metres from others


### When will the outbreak end and life get back to normal?

> the current strategy of shutting down large parts of society is not sustainable in the long-term. The social and economic damage would be catastrophic

<!--quoteend-->

> The best guess is a vaccine could still be 12 to 18-months away if everything goes smoothly. That is a long time to wait when facing unprecedented social restrictions during peacetime. 

<!--quoteend-->

> There are essentially three ways out of this mess.
> 
> vaccination
> enough people develop immunity through infection
> or permanently change our behaviour/society 

<!--quoteend-->

> The UK's short-term strategy is to drive down cases as much as possible to prevent hospitals being overwhelmed - when you run out of intensive care beds then deaths spike.


## 2021


### October

> Britain is heading into winter with the number of Covid cases remaining at a worryingly high level. At the same time, the nation’s vaccination programme appears to have stalled.
> 
> &#x2013; [UK might not be over the worst, scientists warn, as Covid case numbers stay h&#x2026;](https://www.theguardian.com/world/2021/oct/03/we-might-not-be-over-the-worst-scientists-warn-as-covid-case-numbers-stay-high)

<!--quoteend-->

> As the weather gets colder, more and more people are likely to socialise in restaurants, bars and cinemas rather than in parks or gardens with the result that transmission rates of Covid-19 are likely to rise.
> 
> &#x2013; [UK might not be over the worst, scientists warn, as Covid case numbers stay h&#x2026;](https://www.theguardian.com/world/2021/oct/03/we-might-not-be-over-the-worst-scientists-warn-as-covid-case-numbers-stay-high)

<!--quoteend-->

> At the same time, employees are being encouraged to return to their workplaces, which will also drive up infections. At present, new Covid cases are being reported at a rate of about 35,000 a day – though Britain’s vaccination programme has kept hospitalisations to below the 7,000 level with fewer than 200 deaths occurring every day. These figures have remained fairly stable for the past few weeks.
> 
> &#x2013; [UK might not be over the worst, scientists warn, as Covid case numbers stay h&#x2026;](https://www.theguardian.com/world/2021/oct/03/we-might-not-be-over-the-worst-scientists-warn-as-covid-case-numbers-stay-high)

<!--quoteend-->

> The unthinkable scale of the tragedy is the result of a capitalist perfect storm.
> 
> First, the increasing number of novel viruses is linked to the rise of factory farming, city encroachment on wildlife, and an industrial model of livestock production. Second, budget cuts and a systematic undermining of health care systems across the world—at varying levels of crisis—have left countries incapable of handling a public health emergency. Finally, as coronavirus rips through our communities, the dark reality of class inequality is laid bare: who will be most vulnerable to infection, who will receive treatment, and who will be left to die? Millions of frontline workers—from nurses to grocery clerks to delivery persons to the homeless largely unprotected and unable to stay home—will bear the brunt of the death toll.
> 
> &#x2013; [[A People's Guide to Capitalism]]

