# General Intellect Unit - The Cybernetic Brain, Part 1: Ontological Theatre

URL
: http://generalintellectunit.net/e/018-the-cybernetic-brain-part-1-ontological-theatre/

Publisher
: [[General Intellect Unit]]

About the first couple of chapters from [[The Cybernetic Brain]] book by Andrew Pickering.

Re-emphasises to me that there are/were various strands of cybernetics.  Geographically US, Russian, British.

People like [[Stafford Beer]], [[Gordon Pask]], [[Ross Ashby]] are the 'British' strand.  With the starting point being the brain, the embodied brain, and how it facilitates adaptation to an environment.

