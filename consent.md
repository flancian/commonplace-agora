# Consent

> Consent — as opposed to agreement — is defined by the absence of reasonable objections.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> To give my consent to a proposal does not necessarily mean that the proposal is my first choice.
> 
> &#x2013; [[Free, Fair and Alive]]

