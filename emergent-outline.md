# emergent outline

Same as/similar to [[speculative outline]].

> Next to actual output, I pull together Notions, and sometimes Notes in what I call ’emergent outlines’ (Söhnke Ahrens in his book about Zettelkasten calls them speculative outlines, I like emergence better than speculation as a term). These are brief lists to which I add Notions that I think together flow into a story. As I use transclusion I can read them using the underlying Notions. Emergent outlines are a lightweight and bottom-up way to write more, that has a much lower threshold than thinking up a writing project and sitting down trying to write it out.
> 
> &#x2013; [100 Days in Obsidian Pt 4: Writing Notes – Interdependent Thoughts](https://www.zylstra.org/blog/2020/11/100-days-in-obsidian-pt-4-writing-notes/) 

Here are mine: [[Emergent outlines]].

