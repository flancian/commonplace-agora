# Hypercore Protocol

What once was dat.

> The Dat protocol has been renamed to the Hypercore protocol, to reflect the fact it has found many uses beyond the Dat project it originated in. While invented for sharing scientific data sets, Hypercore serves equally well as a foundation for many decentralised projects, from chat apps to web browsers.
> 
> &#x2013; [Redecentralize Digest — May 2020 — Redecentralize.org](https://redecentralize.org/redigest/2020/05)

