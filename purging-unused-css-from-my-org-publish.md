# Purging unused CSS from my org-publish

I added [Cabin](https://withcabin.com) recently to my site (discovered [via](https://branch.climateaction.tech/2020/10/15/designing-branch-sustainable-interaction-design-principles/) Branch magazine).  One thing it quickly showed me was an excess of MBs per request on my digital garden.  Which, given it is a static site, is not good.

A quick look showed that the culprit was Tailwind.  Or to be fairer - my misuse of Tailwind.

I'd added it in fairly quickly a few weeks back to match the styles of my garden to my stream.  What I hadn't done was do [the steps you need to do](https://tailwindcss.com/docs/controlling-file-size) to keep the filesize down.  I'd also left in a reference to the full tailwind css (minified, but still massive) in my head section, from when I'd been prototyping.

Usually you'd point the tailwind purge config at template files, but as I don't have those, I've pointed it at some representative rendered html pages on my local publish:

```js
purge: [
    '/var/www/html/commonplace/index.html',
    '/var/www/html/commonplace/clougha-hike.html',
    '/var/www/html/commonplace/the-garden-and-the-stream.html',
    '/var/www/html/commonplace/20200821142536-how_i_publish_my_wiki_with_org_publish.html'
],
```

A better way migh be to have just one page which includes an example of every element I might be using, and point the purge at that.

After sorting all that, it's now down from about 4Mb of CSS being pulled in (holy crap!), to about 22Kb.

