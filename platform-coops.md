# Platform coops

[[Platform]] [[Cooperatives]].

> Platform co-operatives adopt the democratic ownership and governance structure of workers' co-operatives and utilise a digital platform for the sale of goods or services.

<!--quoteend-->

> Platform cooperatives are an alternative to venture capital-funded and centralized platforms, putting stakeholders before shareholders.
> 
> &#x2013; [Platform Cooperativism Consortium](https://platform.coop/)

<!--quoteend-->

> Platform co-ops are based on principles including:
> 
> -   Broad-based ownership of the platform, in which workers control the technological features, production processes, algorithms, data, and job structures of the online platform;
> -   Democratic governance, in which all stakeholders who own the platform collectively govern the platform;
> -   Co-design of the platform, in which all stakeholders are included in the design and creation of the platform ensuring that software grows out of their needs, capacities, and aspirations;
> -   An aspiration to open source development and open data, in which new platform co-ops can lay the algorithmic foundations for other co-ops.
> 
> &#x2013; [Platform Cooperativism Consortium](https://platform.coop/)

<!--quoteend-->

> Platform cooperatives are businesses that use a website, mobile app, or protocol to sell goods or services. 📱
> They rely on democratic decision-making and shared ownership of the platform by workers and users.👥
> 
> &#x2013; [[Preston Cooperative Development Network]]

<!--quoteend-->

> -   offer a near-future, alternative to platform capitalism based on cooperative principles such as democratic ownership and governance.
> -   introduce economic fairness, training, and democratic participation in the running of online businesses.
> -   give stakeholders a say in what happens on the platforms.
>     
>     &#x2013; [[Preston Cooperative Development Network]]

