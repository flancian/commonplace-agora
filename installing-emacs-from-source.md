# Installing Emacs from source



## Resources

-   [EmacsWiki: Emacs Snapshot And Debian](https://www.emacswiki.org/emacs/EmacsSnapshotAndDebian)


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-21 Sun]</span></span>

I want to upgrade to [[Emacs]] 27.  I'm currently on 26.3 on Mint.

I could get it from a PPA, but a couple suggestions [here](https://www.reddit.com/r/emacs/comments/hkoksm/best_way_to_get_emacs_27_on_ubuntu_well_actually/ ) suggest that building it from source is fairly quick and straightforward.  And possibly Emacs runs quicker if built for your own machine.  So I might do that.

```bash
  git clone --depth=1 --single-branch 
--branch emacs-27 https://github.com/emacs-mirror/emacs.git

cd emacs/

sudo apt install -y autoconf make gcc texinfo libgtk-3-dev libxpm-dev \
     libjpeg-dev libgif-dev libtiff5-dev libgnutls28-dev libncurses5-dev \
     libjansson-dev libharfbuzz-dev libharfbuzz-bin libmagickwand-dev libxaw7-dev

./autogen.sh

./configure --with-json --with-modules --with-harfbuzz --with-compress-install \
	    --with-threads --with-included-regex --with-x-toolkit=lucid --with-zlib --without-sound \
	    --without-xpm --with-jpeg --without-tiff --without-gif --with-png \
	    --without-rsvg --with-imagemagick  --without-toolkit-scroll-bars \
	    --without-gpm --without-dbus --without-makeinfo --without-pop \
	    --without-mailutils --without-gsettings --without-pop
make
sudo make install
```


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-27 Sat]</span></span>

When compiling, you need to set compiler flags.  So I kind of want to know what the current version I'm using was compiled with, so I can reuse those again.

You can get this by inspecting `system-configuration-options` (info [here](https://emacs.stackexchange.com/a/43631)).

For me it was

> "&#x2013;build=x86<sub>64</sub>-linux-gnu &#x2013;prefix=/usr '&#x2013;includedir=\\({prefix}/include' '--mandir=\\){prefix}/share/man' '&#x2013;infodir=\\({prefix}/share/info' --sysconfdir=/etc --localstatedir=/var --disable-silent-rules '--libdir=\\){prefix}/lib/x86<sub>64</sub>-linux-gnu' '&#x2013;libexecdir=${prefix}/lib/x86<sub>64</sub>-linux-gnu' &#x2013;disable-maintainer-mode &#x2013;disable-dependency-tracking &#x2013;prefix=/usr &#x2013;sharedstatedir=/var/lib &#x2013;program-suffix=26 &#x2013;with-modules &#x2013;with-file-notification=inotify &#x2013;with-mailutils &#x2013;with-x=yes &#x2013;with-x-toolkit=gtk3 &#x2013;with-xwidgets &#x2013;with-lcms2 'CFLAGS=-g -O2 -fdebug-prefix-map=/build/emacs26-TP6iDo/emacs26-26.3~1.git96dd019=. -fstack-protector-strong -Wformat -Werror=format-security -no-pie' 'CPPFLAGS=-Wdate-time -D<sub>FORTIFY</sub><sub>SOURCE</sub>=2' 'LDFLAGS=-Wl,-Bsymbolic-functions -Wl,-z,relro -no-pie'"

All of that looks fine, although -fdebug-prefix-map is pointing at an emacs26 specific directory.  I notice also that the recommended one from reddit has &#x2013;with-harfbuzz, which I think is new for 27.

Someone else recommends the following config options:

```bash
./configure --with-sound=alsa --without-gconf --with-mailutils --with-json --with-modules --prefix=/usr/local
```

> don't skip the json and modules flags [&#x2026;] . If you do, you'll be wondering why LSP is slow as balls and why vterm doesn't work.

Bit of a faff, really, figuring out which configuration flags are needed.  I wonder what they all mean.  And presumably there are some defaults if you don't select them all?

This article has a wealth of info here: [What's New in Emacs 27.1? - Mastering Emacs](https://www.masteringemacs.org/article/whats-new-in-emacs-27-1).  It gives some info as to what is default and what isn't.

I think I'll just keep it simple and see what happens.  There's a part of me that thinks I should just get it from the PPA - that would definitely be simpler - but I think I'll enjoy compiling it from source.  Takes me back to the early days of my Linux usage. 

```nil
"
'--includedir=${prefix}/include' 
'--mandir=${prefix}/share/man' 
'--infodir=${prefix}/share/info' --sysconfdir=/etc 
--localstatedir=/var --disable-silent-rules 
'--libdir=${prefix}/lib/x86_64-linux-gnu' 
'--libexecdir=${prefix}/lib/x86_64-linux-gnu' 
--disable-maintainer-mode 
--disable-dependency-tracking --prefix=/usr 
--sharedstatedir=/var/lib
--with-file-notification=inotify 
--with-mailutils 
--with-x=yes 
--with-x-toolkit=gtk3 
--with-xwidgets
"
```

> configure: error: xwidgets requested but WebKitGTK+ not found.

OK, installed `libwebkit2gtk-4.0-dev`.

> configure: WARNING: unrecognized options: &#x2013;disable-maintainer-mode, &#x2013;disable-dependency-tracking

OK, removed them from the config.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-27 Sat]</span></span>

OK!  All seemed to compile fine and startup with no problems.  Packages were all pulled in.

On 27, had to change the time-stamp-format.

> The following obsolescent time-stamp-format construct(s) were found:
> 
> "%04y" &#x2013; use %Y

Fair enough.

