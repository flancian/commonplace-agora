# Digital regulation

-   GDPR was big success in raising awareness of issues
    -   e.g. all the opt-in banners might be annoying, but at least it surfaces the issue of collection
-   ePrivacy
    -   EU is good, however volume of regulation should not be the yardstick as to whether a country is good
        -   EU also has some back things like the copyright directive
    -   what is an effective dissuasive technique?
        -   e.g. FBs stock price went up after $5 billion fine
    -   consumer choice can be powerful
        -   be we obviously need to have choice available in order for people to move somewhere
    -   Digital Services Act

