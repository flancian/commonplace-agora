# Part pairing

> Part pairing - also known as "serialisation" - is an increasingly common practice used by manufacturers of smartphones and other electronic products to control who can and can't perform certain types of repairs.
> 
> &#x2013; repair.eu newsletter July 2021

<!--quoteend-->

> Here is how it works: some parts have a unique serial number, which is paired to an individual unit of a device using software. If any of these parts need replacing during a repair, they will not be accepted unless remotely paired to the device again via software by the manufacturer. This approach could create major barriers to independent and self repair.
> 
> &#x2013; repair.eu newsletter July 2021

