# Against Digital Colonialism

An
: [[article]]

Author
: [[Renata Avila]]

[[Digital colonialism]].
[[Digital sovereignty]].

https://autonomy.work/wp-content/uploads/2020/09/Avila.pdf

This is a very good read.  It strengthens my belief in the need for [[Libre software]], [[small tech]], [[Municipal FOSS]], etc.


## Overview

> This paper examines this process of how dominant countries within a global system benefit from the digitisation of poor and middle-income countries in what appears to be a new form of [[colonialism]]. 

<!--quoteend-->

> With almost half of humanity without access to basic forms of connectivity and entire countries with a pending digitisation process, new patterns of domination have just begun to emerge. This paper identifies these problems and suggests a technical and regulatory path to neutralise and reverse them in order to secure a future of digital autonomy, democracy, sovereignty and dignity.

<!--quoteend-->

> Designing policies against digital colonialism and a digital transformation based in sovereignty and dignity is possible. Indeed, by taking back our education system, public infrastructure and combining the power of the many, a new digital transformation based on sovereignty and cooperation can be enacted. It would be more creative, participatory and public interest oriented, with citizens and institutions creating the technology they need to serve, instead of serving the data needs of quasi monopolies.


## Quotes

See backlinks too.

> Children all over the world are passively learning technologies they cannot improve, adapt or build upon. This stagnates digital innovation. Instead of building blocks, the children of today are provided with locked digital black boxes they have to accept as they are.

<!--quoteend-->

> This also leads to a rapid de facto privatisation of education infrastructure. It is not only the individual students that are shaped by the dominant tech firms. It is also educational data that are now in the hands of these firms, allowing them to develop further commercial products instead of facilitating an ‘education data commons’ that would help countries develop public interest digital services.

<!--quoteend-->

> The technology sector is rapidly moving to provide the infrastructure of oppression and control, especially of the most vulnerable, often disguised as donations. One company engaged in such practices is Palantir, which provided predictive policing systems for six years, ‘free of charge,’ to New Orleans.

<!--quoteend-->

> Increasingly, there is also a merger of political power and tech power in the US, which is then extrapolated to the rest of the world.

<!--quoteend-->

> For many precarious and debt-fuelled governments, it is difficult to reject offers of ‘free’ digital infrastructure and services.

<!--quoteend-->

> Trade agreements today are the primary source of rulemaking at the global level, encompassing an expansive list of issues. Because of this, they have become the preferred vehicle for an accelerated process of digital colonisation.

<!--quoteend-->

> Trade agreements cover a wide array of subjects that extend far beyond traditional trade matters. They have been useful tools for global corporations to dilute or eliminate government policies protecting local industries, minimise regulatory costs, challenge domestic consumer protections, weaken the leverage of local producers and maximise corporate profits at the expense of citizens’ rights.

In brief, digital trade agreements are the modern vehicle to consolidate digital colonisation.

> Multinational companies and governments together promote a utopian vision of the future in which technology will be a driver of exponential positive leaps for the global poor.

<!--quoteend-->

> Disguised as pathways to development that promise a digital future of prosperity, such plurilateral and polylateral alliances are increasingly jeopardising the future of digital sovereignty.

<!--quoteend-->

> For example, accepting the proposed terms by tech corporations in a global treaty could prevent city governments from deciding to hire local cloud providers to manage their public data commons. 

