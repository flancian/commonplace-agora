# Apple Self Service Repair

-   [Apple announces Self Service Repair - Apple](https://www.apple.com/newsroom/2021/11/apple-announces-self-service-repair/)
-   [Everyone Is a Genius: Apple Will Offer Parts and Tools for DIY Repairs | iFix&#x2026;](https://www.ifixit.com/News/55370/apple-diy-repair-program-parts-tools-guides-software)
-   [Too good to be true? Apple announces giving access to (some) spare parts and &#x2026;](https://repair.eu/news/too-good-to-be-true-apple-announces-giving-access-to-some-spare-parts-and-repair-information-to-consumers/)

Announced 17th November 2021.

-   generally regarded as a bit of a PR stunt, however:
    -   good news, because previously Apple were campaigning heavily against right to repair
        -   even [[Apple]] can change its tune

-   lots of unknowns
    -   how costly will the parts be?
    -   will they continue to use software locks and preventing use of 3rd party spare parts?

