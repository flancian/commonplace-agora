# Information strategies



## What's an information strategy?

AKA infostrat.  AKA personal knowledge management?

> What is my strategy to comb through the gigs and gigs of input I can plug myself into on the Web?
> 
> ---**[[Kicks Condor]]** ([Infostrats](https://www.kickscondor.com/infostrats/))

<!--quoteend-->

> deciding what and how to bookmark or archive stuff, sorting through conflicting news stories and accusations, and alternating "periods of discovery with periods of digesting and consolidating"
> 
> ---**Kicks Condor** (quoting Ton I think?) ([Infostrats](https://www.kickscondor.com/infostrats/))

Here's my thoughts on what are some constituents parts of what go into an information strategy:

-   discovery (finding out about interesting things)
-   writing & reflection ([[producing not just consuming]], posting publicly about things I’ve read or seen, and having to think about it before I do)
-   discourse & learning (getting others’ perspective on things, having my horizons expanded and my views challenged)
-   relationships: I think the ‘social’ part of social media should mean forming long-lasting bonds with people, not just being ephemeral blips on each others’ radars

(see [Working on an indie information strategy](https://doubleloop.net/2019/10/04/working-on-an-indie-information-strategy/) for a bit more)

Came across Harold Jarche [via](https://www.zylstra.org/blog/2019/06/the-blog-and-wiki-combo/) [[Ton]], he teaches about [[Personal Knowledge Management]], and talks about [seek, sense, and share](http://jarche.com/2014/02/the-seek-sense-share-framework/).  They map pretty closely to what I put above and are all alliterative, awesome.


## Discovery

-   [My Discovery Strategy, v0.2](https://doubleloop.net/2019/10/05/discovery-strategy/)
-   Perhaps peaks and troughs of online consumption vs production is alright.  I don't like consumption binges to be honest though.

Kicks is working on a tool called [[Fraidycat]] which looks to be a particular approach to a discovery / info strategy.

> Part of the idea here is to move past the cluttered news feed (which is itself just a permutation of the e-mail inbox) where you have to look through ALL the posts for EVERYONE one-by-one. As if they were all personal messages to you requiring your immediate attention.
> 
> &#x2013; Kicks Condor

<!--quoteend-->

> adds the ability to assign "importance" to someone you are following - allowing you to track them without needing to be aware of them every second. 
> 
> &#x2013; [Show HN: Fraidycat | Hacker News](https://news.ycombinator.com/item?id=22545878) 

I find this interesting - it kind of suggests there is utility in a curated feed.  You see some pushback against the curated timelines of the big silos - and rightly so, because they're curating the timeline for their own ends, not yours.  But there's a tendency to then go to the other poll, and say just give me an entirely chronological, unfiltered feed.  I don't think that's ideal either, at least not if you're following a lot of people.  Two options to get around info overload are: use a filtering strategy where you hand curate such that you prioritise seeing certain people's posts first (e.g. Ton's strategy), albeit still in strict chronology;  or this kind of algorithmic approach where something determines the order of what you see (which I think is maybe the fraidycat approach, although need to read more).   Maybe you could combine both.

But yeah - the 'just show me it chronological' is not a great argument to my mind.  It **might** be beneficial to have some heuristics of what you see - BUT the main criteria being that you determine the heuristic.

For example, I would like something that strongly favours things posted by my friends first, but does occasionally pull something in from something further afield, slightly outside of my bubble.

I have a memory of Seb talking about this at IndieWebCamp Utrecht - I think he was working on his own personal algorithm.


## Discourse / learning / interaction

-   I don't like likes.  Less likes, more comments.  Comments are a richer interaction.
    -   requires less feeds to begin with, I think
    -   there may be some small uses cases for actual likes
    -   I wrote a bit about this on [[screen capitalism]]


## Time well spent

I uninstalled Tusky.  It's a great libre app for Mastodon.  But after a morning spent losing about an hour (or more!) of time scrolling through the timelines, before even getting out of bed, I figured it's something I don't need on my phone.  Keep the firehose timeline at arms length.  If I want to for some reason just scroll through everything on Mastodon, I'll go to a website and login.    

Counterpoint: I found out some really interesting articles and points of view during that hour of scrolling&#x2026;

Point: I still can discover those things, the point is to do that only at certain times, it's not something I need to hand, to reflexively dip in to at any moment without thinking.


## [[Personal wikis]]


## My posts about it

-   [Introduced to infostrats](https://doubleloop.net/2019/06/15/introduced-to-infostrats/)
-   [Working on an information strategy](https://doubleloop.net/2019/10/04/working-on-an-indie-information-strategy/)
-   [My Discovery Strategy, v0.2](https://doubleloop.net/2019/10/05/discovery-strategy/)
-   [Digital commonplace books, wikis and blogs](https://doubleloop.net/2019/10/31/digital-commonplace-books-wikis-and-blogs/)


## Useful links

-   [Information strategies overview](https://www.zylstra.org/blog/information-strategies-overview/)
-   [Hypertexting](https://www.kickscondor.com/hypertexting/)

