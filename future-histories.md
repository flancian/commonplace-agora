# Future Histories

-   https://www.versobooks.com/books/2960-future-histories
-   by [[Lizzie O'Shea]]

![[images/future-histories.jpg]]

> What, she asks, can the [[Paris Commune]] tell us about earlier experiments in sharing resources—like the Internet—in common? Can debates over digital access be guided by Tom Paine’s theories of democratic economic redistribution? And how is Elon Musk not a visionary but a throwback to Victorian-era utopians?

Right up my street, in that it is linking fairly leftist ideas from history to modern issues with digital technology.


## [[Review: Future Histories]]


## Thoughts along the way

It’s really nicely written, and weaves together current social, political and economic technological quandaries with a reading of relevant ideas from history. I really like the historical perspective – it gives a nice handle with which to grapple with these problems.

Like a lot of books I’ve read lately though, so far it’s heavy on the diagnosis, and light on the actual treatment.  But I’m only at the beginning so I hope it will flesh out with some concrete action as I go along.

> We need social movements that collaborate—in workplaces, schools, community spaces and the streets—to demand that the development of technology be brought under more democratic forms of power rather than corporations or the state.

True enough.  Although I am unaware of what form it would take. Who is in these social movements? To whom are the demands made? What are they exactly?

> As the planet slides further toward a potential future of catastrophic climate change, and as society glorifies billionaires while billions languish in poverty, digital technology could be a tool for arresting capitalism’s death drive and radically transforming the prospects of humanity. But this requires that we politically organize to demand something different.

Totally agree with the sentiment. But who is we? What organizational form should we take? What is the demand we should be making?

It got a bit wordy at times - repeating the same message over and again.

Each chapter has a few ideas for what to do. More broad strokes of legislative or policy demands, less immediate opportunities for praxis. But good jumping off points.


## Themes


### [[Technology capitalism]]


### [[Biased data sets]]


### [[Keystroke patterns as biometric profiling]]


### [[The police]]


### [[Digital urban planning]]


### [[UBI]], [[UBS]], and a [[job guarantee]]


### [[Autonomy]] and [[digital self-determination]]


### Indigenous culture and ownership and governance


### The [[commons]] / [[digital commons]]


## Raw highlights

Fleeting notes that I highlighted in the e-book as I went along - should be turned into something else.

ork out the logistics of a future perfect society. But problems arise because these visions of the future are often detached in causal terms from the troubles of the present.

themselves to the job of shaping such societies. Utopianism can abridge our capacity to imagine what true emancipation might look like.

at West had left behind in the previous century. If we fail to transform the structures that give rise to social and economic inequality, advances in all fields of human endeavor will remain beholden to these problems—and so too will the imagined solutions.

Technological
Note: Means of conndction: this includes facebook hoovering up events. Part of article has to analyse if online connection is good at all. But feference some of the intercept article stuff about reidsnce etc

technological utopia was understood as the natural progression of capitalist developme

Just as the capitalists dictated to workers in the industrial revolution, so too would engineers preside over a society that treated people as a kind of machinery.

time in utopia, it seems, was not actually free. This kind of attitude fits with the prevailing views at the turn of the century, when industriousness was highly valued and respected. (Indeed, Bertrand Russell penned an entire dissenting essay in 1932 called In Praise of 

These utopias often put on display an imagined solution to unequal economic distribution and social disharmony that was just more of the same: more capitalism, technologically optimized. 

The entire premise of Gillette’s utopia was that society be run as a giant corporation.
Note: 84k!

The nation … organized as the one great business corporation in which all other corporations were 

In the digital age, we are witnessing an ideological revitalization of the idea that utopia can be achieved by building more extreme versions of technology capitalism. It is a revival that imperils the ideals of democracy and equality. The

I don’t know about you people, but I don’t want to live in a world where someone else makes the world a better place better than we 

This is the world that Richard Barbrook and Andy Cameron portrayed in their influential essay “The Californian Ideology,”

technological utopianism blinds us to the ways in which it is possible to build a new world from political materials available in the present.

By far the most interesting ideas about how to build alternative futures have instead come from below

. By 1871, the people of Paris were thoroughly sick of bearing the costs of imperial wars, which disproportionately fell to the poor and working people of 

they turned Paris into an autonomously organized society, a commune that experimented with alternative ways of structuring social and political life based on collaboration and cooperation. These

Paris is a true paradise … All social groups have established themselves as federations and are masters of their own fate.”

The Paris Commune showed how cities and communities can be rebuilt in radical new ways, not by “innovating” their way out of social problems, but by empowering people to make decisions collectively. Power

The virulence of Versailles’s violent repression of the Commune reflected the enormity of its perceived threat to existing gender, class, and religious hierarchies

The Commune tells us that it is possible to think of other ways of organizing, that alternative practices and principles for running society can spring forth, quite quickly, from the existing fabric of relationships and communities.

They are underpinned by a rejection of the idea of singular ownership and by a sense of the importance of collectively determining the basic standards that 

But the people who make technology and the many more who use technology do often have a better grasp of what kind of alternative futures are possible if they are given the chance to put it into practice. What technology capitalists therefore produce, above all, are their own gravediggers. T

, whatever the horrors of feudalism, it allowed those who labored to see what they themselves produced, to understand their value in terms of output 

How technology is developed and in whose interests it is deployed is a function of politics. The call to arms of the Luddites can be heard a full two centuries later, demanding that we think carefully about the relationship between technology and labor. 

This experience of exploitation gives rise to a separation or distancing of the worker from the product of her labor. Labor power becomes something to be sold in the market for sustenance, confined to dull and repetitive tasks, distant from an authentic sense of self. 

the history of software development. In it, we can see how labor can be both unalienated and productive, and how it can be limited by the imposition of a profit motive. 

Many African American women too did groundbreaking work at NASA, including Katherine Johnson, Dorothy Vaughan and Mary Jackson, whose experiences have been documented in the book (and film) Hidden Figures.

But it was, at its core, the beginning of a movement that aimed to dismantle the alienation associated with proprietary software production and use. In resisting the idea of closed, proprietary software by means of open, liberated software, these hackers shared a common objective with the Luddites over two centuries earlier. 

”: it serves to protect free software from proprietary expropriation but also to build and expand the digital commons by ensuring that future programs using free software are also publicly available. In short, the GPL takes the traditional view of copyright and inverts 

It serves as a living, breathing refutation of Bill Gates’s haughty assumption that creativity and ingenuity are motivated by the prospect of material gain.

Agile is about placing importance on engaging workers (or resisting alienation); it values collaboration (rather than 

gateways to a more radical politics”

. They want troubleshooting to be something that only a Genius Bar can do. They cultivate a sense of helplessness. 

Open source software does this because it is serving the people looking at and using the code, not the owners of a corporation, in a manner that is 

This will require us to cultivate a diverse and inclusive workforce making open source software whose contributions will not be exploited but properly valued and

. A universal basic income is a regular payment made to all citizens without qualification. It is a redistributive measure that aims to give everyone a substantively equal share in the productive output of society. 

economic units whose moral worth is measured by what they produce for the market. Every person is entitled to a share in the wealth that is created collectively in the world, as part of recognizing his or her 

mafia capitalism: an offer too good for poor people to refuse. 

Anti-colonialism has been economically catastrophic for the Indian people for decades. Why stop now?”

. Facebook’s objective is to make people stay on the site, and its algorithms are designed to use our emotions to do 

Facebook does more than just tinker around with content, it actively manipulates. 

Emotional states can be transferred to others via emotional contagion,

These results indicate that emotions expressed by others on Facebook influence our own emotions, constituting experimental evidence for massive-scale contagion via social 

The fragmentation of online communities and polarization of public discourse is an outcome of Facebook’s desire to monetize the web through optimally crafted audiences for advertising

think more broadly about how networked technology could facilitate more meaningful forms of public participation. A

Digital technology makes it possible to connect across space, class and culture and build resilient and sophisticated communities, without the usual institutional gatekeepers standing in the way. 

the “concentration of economic power also consolidates political power.” 

To the extent that technology platforms provide services for public benefit—such as communication, logistics or server hosting, to name a few—they should be held to a standard of nondiscrimination in terms of service delivery 

democratic society requires not only individual rights but also a shared sense of what it is to be a member of the public. This creates a forum for collectively exchanging ideas and experiences. Participatory democracy is an essential part of justice

The Cotton Mills Act passed in 1819 prohibited children under the age of nine from working in cotton mills and limited the work of people under sixteen to twelve hours 

the twenty-first century has meant that work has broken out of its “temporal and spatial confinements.” People in many parts of the world are almost continuously on the job, in the office, on the road or at home, constantly connected to devices.

impacts on the environment are often tragically redundant.        

digital technology has transformed creativity, communications, entertainment and work practices, but one key aspect of our lives that has languished under the influence of new technology is our pay. 

Entrepreneurs have a happy knack of finding new ways to make money, capture more market share, and monetize what has previously been unexploited, off-limits or considered unnecessary.

The future of technology capitalism is thus accumulative: for the system to continue and grow, entrepreneurs will attempt to commodify more of our lives. 

The process of automating work, when carried out under capitalism, results increasingly in disparity in income and greater pressure on people to work harder and longer.

Nick Srnicek and Alex Williams emphasize the importance of making full automation “a political demand, rather than assuming it will come about from economic necessity.”

We need to reverse the idea that efficiencies gained via automation can be the pretext for greater exploitation by employers.

Keynes predicted a three-hour workday, or a fifteen-hour week, to spread as thinly as possible the collective task of creating 

Automation under capitalism is not oriented toward living “wisely and agreeably and well.” Instead it has created a deep, abiding moral order that binds merit to hard work. This

We have been trained too long to strive and not to enjoy,

dismantle the culture of long hours and allow working people to learn new things, contribute to public policy discussions, and become greater contributors in their communities and homes.

worker cooperatives are enterprises in which workers own the bulk of the firm collectively and management decisions are made with the democratic participation of the worker-owners. S

We should be wary of the prescriptive approaches to policy-making that underpin a job guarantee.

We need the liberty offered by a basic income, the sustainability promised by the organization of a job guarantee, and the protection of dignity offered by centrally planned essential services. It

collective social and political life that prioritizes autonomy and is not subject to scrutiny by those who seek to profit from 

Technologists need to build bridges with everyday people and help popularize these tools, with empathy and humility.

The left’s job is to build communities and organizations where people can both learn and teach tactics for digital self-defense. 

allowing people to communicate, read, organize and come up with better ways of doing things, sharing experiences across borders, without scrutiny or engineering, a kind of cyberpunk 

> "The Algerian combatant is not only up in arms against the torturing parachutists,” wrote Fanon. “Most of the time he has to face problems of building, of organizing, of inventing the new society that must come into being.”

