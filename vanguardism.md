# Vanguardism

Supporting the idea of a professional revolutionary leadership.

See [[What is to be done?]]

> Rosa Luxemburg recoiled at the rigidity of Lenin’s vanguard, one molded by the discipline of the factory, army, and bureaucracy. 
> 
> &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]


## Resources

https://www.reddit.com/r/Anarchy101/comments/3zms3h/whats_vanguardismrevolutionary_vanguards_and_why/ 

