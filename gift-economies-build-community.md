# gift economies build community

First came across this claim in the Wikipedia article, which links out to [[The Stubborn Vitality of the Gift Economy]].


## Epistemic status

Type
: [[claim]]

Confidence
: 3

Gut feeling
: 6

