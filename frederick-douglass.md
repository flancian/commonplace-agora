# Frederick Douglass

> an American social reformer, [[abolitionist]], orator, writer, and statesman. After escaping from slavery in Maryland, he became a national leader of the abolitionist movement in Massachusetts and New York, becoming famous for his oratory and incisive antislavery writings.
> 
> &#x2013; [Frederick Douglass - Wikipedia](https://en.wikipedia.org/wiki/Frederick_Douglass) 

"Education makes a man unfit to be a slave"

