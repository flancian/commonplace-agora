# Digital colonialism

[[Digital]] [[colonialism]].

The domination of countries/cultures through the control of critical digital infrastructures (software, hardware, networking, data).  A form of [[neocolonialism]].

Often through tech companies rather than governments themselves.  Maybe a combo of the two.

United States and China leading the way on this.

Aside: [[Is the phrase 'digital colonialism' problematic?]]

See [[Digital self-governance]] for ideas on how to counter.

> referring to the deployment of imperial power over a vast number of people, which takes the form of rules, designs, languages, cultures and belief systems serving the interests of dominant powers. In the past, empires expanded their power through the control of critical assets, from trade routes to precious metals. Today, it is not states but technology empires that dominate the world through the control of critical digital infrastructures, data and the ownership of computational power.
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> By collecting the personal data of citizens on a scale unprecedented in human history, companies can serve as conduits of misinformation campaigns that can alter the flow of global geopolitics and even change the outcome of elections
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> this structural form of domination is exercised through the centralised ownership and control of the three core pillars of the digital ecosystem: software, hardware, and network connectivity, which vests the United States with immense political, economic, and social power. As such, GAFAM (Google/ Alphabet, Amazon, Facebook, Apple, and Microsoft) and other corporate giants, as well as state intelligence agencies like the National Security Agency (NSA), are the new imperialists in the international community. Assimilation into the tech products, models, and ideologies of foreign powers – led by the United States – constitutes a twenty-first century form of colonisation.
> 
> &#x2013; [[Digital Colonialism: US Empire and the New Imperialism in the Global South]]

<!--quoteend-->

> the phenomenon is not exclusively led by the United States. China is increasingly following the Silicon Valley pattern of behaviour and playing a similar role in digital colonialism. 
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> The modus operandi of tech companies with poor nations resembles a continuation of former colonial relationships, this time through technology. They offer deals that appear shiny but are ultimately extractive and deprive emerging economies of a digital future they can govern. 
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> Early manifestations of this process can be seen in the ‘free’ provision of critical infrastructure – from cables to connectivity – to large populations. This process led to the silent privatisation of the digital infrastructure of entire nations.
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> The fact that an entire nation delegates its digital services to a company based in Silicon Valley is alarming. The company is then in a position to handle not only highly sensitive government documents, but also is in possession of critical information relating to the entire country.
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> Digital colonialism is also occurring in the classroom. An entire generation is being prepared as a potential workforce for tech giants.
> 
> &#x2013; [[Against Digital Colonialism]]

<!--quoteend-->

> ‘the importance of technology choices for schools cannot be overstated: the specific technologies deployed will forge path dependencies by shaping the habits, preferences and knowledge base of the first tech generation from childhood. Education offers the ultimate breeding ground for Big Tech imperialism; product placement in schools can be used to capture emerging markets and tighten the stranglehold of Big Tech products, brands, models and ideology in the Global South.’
> 
> -   [[Digital Colonialism: US Empire and the New Imperialism in the Global South]]


## Resources

-   [[Digital Colonialism: US Empire and the New Imperialism in the Global South]]
-   [[Against Digital Colonialism]]
-   [[Governable Stacks against Digital Colonialism]]

