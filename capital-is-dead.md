# Capital is Dead

Expansion and update of ideas from [[A Hacker Manifesto]] e.g. [[Vectoralist class]] and [[Hacker class]].

![[images/capital-is-dead.jpg]]


## Notes

As of 31st October 2020, I've been reading it for a few nights, and enjoying it a lot - much more readable at a first pass, when new to the ideas, than [[A Hacker Manifesto]].

From my totally superficial understanding so far, it's about the idea that there is a new class layered on top of the capitalist class.  Who don't necessarily control the means of production, but that control the vectors of information.

And the hacker class being those that produce information.  Which is being appropriated by the vectoralists.

I feel an affinity to the notion of a [[Hacker class]], of which I am perhaps a member.  I certainly feel antagonism towards the [[Vectoralist class]], and hope for flows of information not controlled by a few.


## Quotes

> You make the information, but like some kind of info-prole, you don't own the information you produce or the means of realizing its value.

<!--quoteend-->

> They gave rise to a strange kind of political economy, one based not only on a scarcity of things but also on an excess of information.

<!--quoteend-->

> The dominant ruling class of our time owns and controls information.

<!--quoteend-->

> It takes about as much infrastructure to organize the information as it does to organize the distribution of the physical stuff that ends up on the shelves

<!--quoteend-->

> There's a world of everyday life the meat grinder doesn't describe from which a surplus is extracted for another's benefit in other ways. You can be someone other than a tenant farmer or an industrial worker and still not be a capitalist or even petit bourgeois.

<!--quoteend-->

> For years I was one of what the so-called alt-right calls a "cul- tural Marxist," interested mostly in what happens in the political and cultural superstructures of modern society, rather than in the technical and economic base. However, trying to understand culture will lead you to understanding media, which will lead you to try to figure out some things about technology. Then it turns out that the genteel forms of Western Marxist thinking taught in universities for several generations now are not good at understanding how the forces of production actually work. That requires some actual technical knowledge and experience, or at least a willingness to concede that others may know about such things and to learn from them. The production of counterhegemonic knowledge can really only be comradely and collaborative.

YES!!!  That's really key for me.  I feel like I'm somewhere in the middle of this theoretical knowledge and technical knowledge.  I'm not going to be the foremost expert in either, but can perhaps work on joining the two.   I hope I can make some kind of contribution there.


## Summaries

My basic summaries.


### Chapter 1: The Sublime Language of My Century

Marxism should not be taken as religion. We need to reread it based on current material conditions. The left sometimes makes it appear as if capitalism is the eternal form, until it transitions to socialism.  But material conditions have changed since the end of the 19th century, so perhaps some of Marxist theory needs to change.

I like the use of  [[détournement]] as a means to break doctrine.

This all makes sense to me, but wondering about how something like e.g. Red Menace would respond to the idea of Marxism being doctrinaire.

I like the references to [[The Matrix]], [[Ancillary Justice]].  Also the Dollhouse, Get Out, and a Cuban sci-fi book called [[The Year 200]] that I would now like to read.

> In our own times this old story was adapted into the belief system of the so-called tech industry, as a part of what Richard Barbrook calls [[the Californian Ideology]]. Into it can be folded certain other variations, about the "[[fourth industrial revolution]]," for example.


## Chapter 2: Capitalism &#x2013; or worse?

I guess Wark's argument is that the forces of production have changed considerably since Marx, being much more information-based than physical now.  And if you analyse the current forces of production, you see that it isn't really capitalism any more (it's something worse).

I guess it hinges on whether you want to call the current situation capitalism with some modifier (neoliberal, late-stage, surveillance, techno, etc) or  look at it as a whole new thing.  I think she makes a pretty compelling argument why we shouldn't just assume that capitalism will continue in perpetuity until we have communism&#x2026; but I'm definitely not a scholar of these things so someone with a bit more theory might be able to counter this idea a bit more.


## Criticism

> I like the point about acknowledging changes in capitalism but I think she overstates the power/freedom to move of the new class. The rise of logistics (which is part of material production) and the very physical fixed-capital-intensive nature of the new economy that Kim Moody talks about in On New Terrain needs to be taken into account, I think.
> 
> &#x2013; [Matt Noyes](https://social.coop/@Matt_Noyes/105091032101011760)

<!--quoteend-->

> I think there have definitely been critical reviews of how she positions it, and obviously there’s a whole discussion growing about [[neofeudalism]] now too and whether we’re actually seeing some kind of formation like that. Maybe it’s my own bias, but I think I’d still say we’re in another stage of capitalism, at least for now.
> 
> &#x2013; Paris Marx (on [[TWSU]] Discord)


## Links

-   [Capital Is Dead: Is This Something Worse? - YouTube](https://www.youtube.com/watch?v=9wBZVEnqocI)
-   https://invidious.tube/watch?v=eiV0wS_in-4

