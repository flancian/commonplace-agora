# Emacs Freex

Emacs Freex looks like a very interesting (but perhaps no longer maintained?) project - described as a "[[personal wiki]] on steroids".  

Looks like it had [[Transclusion]], autolinking.  Worked with another old project called Muse for publishing.

Looks like it worked in a fairly similar way to [[org-roam]] - a DB layer over text files.

https://github.com/gregdetre/emacs-freex/wiki/Introduction

