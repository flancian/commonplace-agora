# Emacs from scratch



## Good idea


## Bad idea

> A week ago I decided to cancel Doom Emacs and go back to building Emacs from Scratch, and once again I was reminded what a terrible idea that is.
> 
> Seriously, stock Emacs, even with a leg up from Nano Emacs, gets so many things “wrong” that I could spend the rest of my life fixing things and still wanting more. I thought building from scratch would help me avoid Configuration Fatigue. Wow, was I wrong.
> 
> -   [Doom Emacs from scratch - Jack Baty's Weblog](https://www.baty.blog/2021/doom-emacs-from-scratch)


## Bookmarks

-   [Doom Emacs from scratch - Jack Baty's Weblog](https://www.baty.blog/2021/doom-emacs-from-scratch)

([[Emacs]])

