# Assemblyism

> Assemblyism’, as he calls it, ‘mistook consensus assemblies, which had emerged from quite specific circumstances and inheritances, for a new universal model of democracy, which at the very least prefigured the postcapitalist society to come’
> 
> &#x2013; [[Anarchist Cybernetics]] 

