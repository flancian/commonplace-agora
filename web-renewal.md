# web renewal

> I get a sense that the reboot is coming. Not a sudden backlash against Facebook or other giant. More a rolling recognition of the loss that Dash talks about. A subtle but increasing desire to take back control over our own online lives.
> 
> -   [[Phil Jones]]: http://thoughtstorms.info/view/WebRenewal


## Twin pages

-   [[Phil Jones]]: http://thoughtstorms.info/view/WebRenewal

