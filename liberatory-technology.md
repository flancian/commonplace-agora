# Liberatory technology

As a statement: [[Technology should be liberatory]].

So what is liberatory technology?

I think the specific phrase was coined by [[Murray Bookchin]].  Certainly espoused by Bookchin.  For example in [[Towards a Liberatory Technology]]. 

I suppose a big piece of the definition is, what is your definition of [[liberation]]?

From reading Towards a Liberatory Technology, the vibe I get is:

-   technology should reduce toil
-   technology should function in harmony with nature
-   technology should facilitate community and reduce [[alienation]]

I'm down with all of that.  

I mean in some senses that makes me think of [[doughnut economics]] - helping to meet social foundation while staying within ecological boundaries.  A liberatory technology would be one that keeps us in the doughnut.

Liberatory technology as per Bookchin, I think, has the extra additional proviso of small scale, localised, and decentralised.  I'm fine with that too - but I imagine it's probably the area that would have the most contention.  As in - reducing toil, not wrecking the planet, I imagine most people can get on board with that. Whether you can succeed best with decentralisation or state coordination you're going to get discussion.

> Technology would be used for the purpose of eliminating toil and drudgery, and labor-saving techniques would be applied so as to minimize the amount of necessary work for everyone.
> 
> &#x2013; [Communalism Pamplet](https://www.communalismpamphlet.net/index.html#liberatory-technology)

Liberatory technology would facilitate [[Unemployment for all]], you (a [[Dadaist]]) might say.


## Examples

Bookchin discusses alternative and [[renewable energy]] a lot in [[Towards a Liberatory Technology]].  As well as technologies that facilitate local, decentralised agriculture.

The [[Communalism Pamphlet]] goes in to a bit of detail on ways technology could be liberatory with regards to:

-   manufacturing
-   mining
-   agriculture
-   energy


## See also

-   Compare with things like [[appropriate technology]], tech for good, [[Convivial Tools]], etc.
-   [Fields, Factories and Workshops | The Anarchist Library](https://theanarchistlibrary.org/library/petr-kropotkin-fields-factories-and-workshops-or-industry-combined-with-agriculture-and-brain-w)
-   [Towards Liberatory Technology | Black Star Tech Group](https://blackstartech.wordpress.com/2015/10/28/liberatory-technology/)
-   [[fully automated luxury communism]]

