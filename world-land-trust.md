# World Land Trust

> The World Land Trust (WLT) is an international conservation charity, which protects the world's most biologically important and threatened habitats acre by acre.

