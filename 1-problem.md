# +1 problem

[[I don't like likes]].  But without reactions, you used to get the +1 problem.

> I remember it back in the day of forum software. Like scrolling through hundreds of messages that were all one words like >heart< and >love< and it made discussions impossible to follow if something got popular.
> 
> &#x2013; [comment by wakest](https://social.wake.st/@liaizon/103127273354956343)

