# Roam

> A [[note-taking]] tool for [[networked thought]].
> 
> &#x2013; https://roamresearch.com/

I stumbled across [[org-roam]] (emulating Roam in Emacs), while looking for a way of improving my flow of working on my wiki, and am loving it so far.

> Roam attempts to implement a near-full conception of [[hypertext]] as originally conceived by visionaries like [[Vannevar Bush]] and [[Ted Nelson]].
> 
> &#x2013; [A Text Renaissance](https://www.ribbonfarm.com/2020/02/24/a-text-renaissance/) 

<!--quoteend-->

> It implements a few key features of 1980s vintage hypertext visions — block-level addressability, [[transclusion]] (changes in referenced blocks being “transfer-included” wherever they are cited), and bidirectional linking — that utterly transform the writing experience at the finger-tips level.  
> 
> &#x2013; [A Text Renaissance](https://www.ribbonfarm.com/2020/02/24/a-text-renaissance/) 

<!--quoteend-->

> You end up organizing high-level structure as you work at fleshing out low-level chunks of information, because the UX collapses high and low-level thinking into a single behavior.
> 
> &#x2013; [A Text Renaissance](https://www.ribbonfarm.com/2020/02/24/a-text-renaissance/) 

<!--quoteend-->

> It can also do note-taking, workflows (like kinda-sorta competitor Notion), and wiki-like knowledge management, but those use cases are not as interesting to me. Conspiracy theories and extended universes, in the best senses of those terms — escaped reality construction might be the general category — is what Roam wants to be about. 
> 
> &#x2013; [A Text Renaissance](https://www.ribbonfarm.com/2020/02/24/a-text-renaissance/) 

Written in Clojure.

You write things, and create relationships between them.
Don't have to worry about meticulously putting things into a hierarchy. Using back links, and link tags get around this.
You can use it for knowledge management, as a task manager, a CRM and a tool for writing.

Writing a block once and then just referencing that block elsewhere is nice.

