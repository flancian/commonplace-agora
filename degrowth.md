# Degrowth



## Articles

-   [Economic Planning and Degrowth: How Socialism Survives the 21st Century](https://newsocialist.org.uk/economic-planning-and-degrowth/)
    -   Degrowth
    -   Through central economic planning
    -   Using the tech of e.g. Amazon but for managing transition and living within means
    -   Didn't get so much from the article. More concrete actions would have been good.

-   [Center for a Stateless Society » We Are All Degrowthers. We Are All Ecomodernists](https://c4ss.org/content/52500)

