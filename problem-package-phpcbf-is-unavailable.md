# Problem: Package phpcbf is unavailable

When starting [[spacemacs]] I was getting the following errors:

> Package php-auto-yasnippets is unavailable. Is the package name misspelled?

<!--quoteend-->

> Package phpcbf is unavailable. Is the package name misspelled?

It's discussed here: https://github.com/syl20bnr/spacemacs/issues/14381

They seem to have been removed from Melpa.

I can stop the errors by adding the package names to `dotspacemacs-excluded-packages` in .spacemacs.

I don't know if it will cause any problems with PHP - we'll see.  Someone there suggests moving to lsp backend for PHP - I could try that at some point.

