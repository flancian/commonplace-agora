# Unitierra

> Unitierra — Universidad de la Tierra en Oaxaca — in Oaxaca, Mexico. It is a “de-institutionalized university” founded by commoners for commoners that rejects formal roles and hierarchy.
> 
> &#x2013; [[Free, Fair and Alive]]

