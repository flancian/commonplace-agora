# Frantz Fanon

> the writer and psychiatrist born in the French colony of Martinique, is perhaps best known for his contribution to our understanding of race and colonialism
> 
> &#x2013; [[Frantz Fanon Against Facebook: How to Decolonize Your Digital-Mind]]

<!--quoteend-->

> Fanon provided an explanation of how colonialism works – how both the colonizer and the person being colonized take on roles and practices that make colonialism seem inevitable and unbeatable.
> 
> &#x2013; [[Frantz Fanon Against Facebook: How to Decolonize Your Digital-Mind]]

<!--quoteend-->

> The pivotal experience for Fanon was his involvement in the Algerian war of independence
> 
> &#x2013; [[Frantz Fanon Against Facebook: How to Decolonize Your Digital-Mind]]

