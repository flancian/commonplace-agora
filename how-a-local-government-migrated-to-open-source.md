# How a local government migrated to open source

URL
: https://opensource.com/article/20/8/linux-government

> Eyüpsultan Municipality in Turkey gained flexibility, cost savings, performance, and independence by transitioning its entire workforce to GNU Linux and other open source software.

<!--quoteend-->

> I believe that regular training during our open source migration project was the single most significant reason for its success.

<!--quoteend-->

> Providing training throughout the migration was one of our key strategies. We did not stop after delivering training; **we checked back with our users to verify that the training produced the expected results**. It took us a year to uncover all the things people struggled with after switching from Office to LibreOffice, but we welcomed these issues and helped to solve them whenever they arose.

<!--quoteend-->

> We didn't just do technical analysis at this stage; we did psychological analysis at the same time. By talking to people, we tried to find out their anxiety points. We learned that there was a common fear of an "encounter with the unknown."

