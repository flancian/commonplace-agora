# email

> Email today is now less a communications medium than a communications compile target. It’s a clearinghouse technology. It’s where conversations-of-record go, where identity verification happens, where service alerts accumulate, and perhaps most importantly for publishers, where push-delivered longform content goes by default. It is distributed and federated, near universal, and is not monopolized by a single provider. Now that a constellation of other product categories have hived off around it in the last two decades, it’s finally finding its own true nature.
> 
> &#x2013; [A Text Renaissance](https://www.ribbonfarm.com/2020/02/24/a-text-renaissance/) 

<!--quoteend-->

> Looking back after 50 years, it’s clear that Tomlinson’s neat idea has proved strikingly resilient in the face of services like Slack, Facebook, Google Hangouts, WhatsApp, Snapchat, Skype, et al. Amid an internet landscape filled with “creative disruption,” email has managed to withstand the forces of rapid change that have rendered other technologies obsolete, failed, or co-opted. 
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/) 

<!--quoteend-->

> This is largely because email is decentralized — its underlying protocols are neutral and universal. The fact that no single company or entity controls email has made the technology accessible to a vast number of people worldwide. 
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/) 

<!--quoteend-->

> The outward effect of email’s decentralization is what some call [[adversarial interoperability]]. 
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/) 

<!--quoteend-->

> relatively small competitors wouldn’t be able to grow without the ability to interoperate with a giant like Gmail.
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/) 

