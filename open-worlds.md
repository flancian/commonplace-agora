# Open worlds

A type of [[virtual world]].

> There are some criteria that have to be met: a free-roaming environment you can wander at will, the ability to pick and choose your objectives. However, open worlds tend to defy the simple definitions of other game genres. In a platformer, you jump. In a shooter, you shoot. But what makes an open-world game is the promise of freedom.
> 
> &#x2013; [Closed Worlds](https://logicmag.io/play/closed-worlds/) 

.


## Bookmarks

-   [Closed Worlds](https://logicmag.io/play/closed-worlds/)

