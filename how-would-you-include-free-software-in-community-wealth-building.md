# How would you include free software in community wealth building?

How would you include FOSS in [[Community wealth building]]?

Maybe hire local tech coops to work on implementing / improving FOSS for the council.  But with the rule that improvements are always contributed back to the upstream project.

Municipal sponsorship of FOSS could be a good financial model too.

Think of all the councils in the world, imagine if they all used something like [[LibreOffice]].  Some of their IT budget (i.e. public money) goes on using and improving libre software.  Maybe they work with local tech coops to work on the codebase.  Money gets invested locally, code improvements can be used globally.  That amount of distributed labour, the libre offerings would easily surpass the proprietary offerings.

Heh, this is totally mentioned in DiscCO!

> The development of [[commons-public partnerships]] as an alternative to the established public-private, following the Preston and Evergreen models, with DisCOs facilitating:
> 
> a. Municipal service provision for local economies.
> b. FLOSS-built and open licensed, common-pool civic knowledge resources.
> c. Practical workplace education on feminist economics, the commons, decentralized technology and the ethical market sectors offered by participating DisCOs.
> 
> &#x2013; [[The DisCO Elements]]


## What kinds of software does a municipality need?

-   [[citizen participation platform]]s
-   maybe a search engine that favours local business / news / organisations?  And doesn't track. e.g. a custom [[Searx]] of some kind

General civic tech stuff. A ton of resource management stuff presumably.  


### See commercial offerings&#x2026;

-   [CouncilWise](https://www.councilwise.com.au/)
    -   finance; asset management; property and rating; records management; citizen services
-   [AdvantEDGE](https://www.edgeitsystems.com/advantedge/) (management solutions for town and parish councils)
    -   Admin (agendas and minutes)
    -   Allotments
    -   Asset management
    -   Cemetery management
    -   Facilities
    -   Finance
    -   Inspections
    -   Planning
    -   Playgrounds
    -   Services


## Bookmarks

-   [How a local government migrated to open source | Opensource.com](https://opensource.com/article/20/8/linux-government)

