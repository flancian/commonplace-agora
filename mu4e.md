# mu4e



## Fixing line breaks issue with mu4e and protonmail bridge

-   Yeah, so they don't even reappear correct in mu4e, which is interesting.
-   They are very off in the mobile client.
-   Let's try sending it again with these new settings as suggested online.
-   Hmm, that's annoying.  With that setting, turn-off-auto-fill, but it still has new lines included at the point of sending?
-   So, is ProtonMail Bridge stripping out the format=flowed?  Seems like it.
-   Hmm, maybe that's not such a big deal.  e.g. https://fastmail.blog/2016/12/17/format-flowed/ .

In which case then.  What to do?  Remove the text flowing?

-   This **does** work, however, it'll cause a mess if protonmail is stripping off format=flowed.

https://vxlabs.com/2019/08/25/format-flowed-with-long-lines/

-   So this is an option for me sending HTML emails.

http://kitchingroup.cheme.cmu.edu/blog/2016/10/29/Sending-html-emails-from-org-mode-with-org-mime/#orgheadline2


## Clearing out the Trash folder

```nil
/usr/bin/mu find maildir:/trash AND date:40y..90d --sortfield=date --fields=l | xargs /bin/rm -f
```

with the caveat here  -[mu4e - empty trash folder in regular intervals : emacs](https://www.reddit.com/r/emacs/comments/eu7xxy/mu4e_empty_trash_folder_in_regular_intervals/fg2r654/?utm_source=reddit&utm_medium=web2x&context=3) 

