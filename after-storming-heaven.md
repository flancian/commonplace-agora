# After Storming Heaven

URL
: https://novaramedia.com/2021/03/19/after-storming-heaven/

Publisher
: [[Novara Media]]

> James Butler is joined by Laura Forster, a historian of the Commune, to trace its aftershocks: from the lives of exiled radicals in Soho and Fitzrovia, to the British socialists who took up the story of the Commune – both as an inspiration and a warning – through to Lenin dancing in the snow and the red thread linking the [[Paris Commune]] with the London County Council.

Mentioned a few times the [[municipalist]] nature of the politics of the Commune.

