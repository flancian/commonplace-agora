# enclosure of the commons

> between 1760 and 1870, about 7 million acres (about one sixth the area of England) were changed, by some 4,000 acts of parliament, from common land to enclosed land.
> 
> &#x2013; [What the battle for Freeman's Wood says about the future of our common land |&#x2026;](https://www.theguardian.com/cities/2016/feb/10/battle-for-freemans-wood-lancaster-common-land-locals-property-development) 

[[Commons]].

> In England, over the course of a few centuries, lands that were held and cared for by communities in common were expropriated, fenced in and enclosed as private property to be owned by a small elite. This process of transforming shared interdependent ecologies into separate, scarce and privately owned ‘things’ was encoded into property law and violently exported across the world in a continuous and unceasing process of colonisation.
> 
> &#x2013; [[Seeding the Wild]]

