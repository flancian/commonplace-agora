# 19 broken laptops

I got 19 broken laptops from a friend at the makerspace.  He'd gotten a big batch of laptops off eBay for £120, from someone who used to work somewhere in IT and had accumulated them over time with the intention to fix them.  Eventually, unfixed, their partner had asked them to be rehomed&#x2026;

I thought I'd take the ones from my friend and use them as a bit of a learning experience.  I'll try and fix them, and in the process take some notes on it all.  Things like the disassembly, what's inside, what the problem is, what can I do with these old laptops?

For those that I fix that are still worth using, I'll give them away to someone.

For those that are completely end-of-life - I'll do some [[upcycling]] with them.

Here's the list:

-   [[Apple PowerBook G4]]
-   Acer	Extensa 5620Z	15”				Yes
-   Acer	Aspire 7720	17”				?
-   Sony	Vaio PCG-7N2L	15”				?
-   Apple	MacBook Pro A1150	15”				?
-   Apple	MacBook A1181	13”				?
-   Acer	Aspire 5338	15.6”				?
-   HP	Pavilion dv9500	17”				?
-   Toshiba	Satellite Pro C850-10N	15.6”				?	CD drive issue
-   Acer 	Aspire 5733	15.6”				?	Not all keys work
-   Dell	Inspiron 1564	15.6”				?	No RAM, does not power on, stupid RAM cover
-   [[HP	Stream 11]]
-   Acer	Aspire S3					?	Powers on but reports 6GB RAM, no hard drive, bottom cover off
-   [[Lenovo	Ideapad 100S]]
-   Samsung	RV511	15.6”				?
-   [[HP	655]]
-   Acer	Aspire V3-571G	15.6”				?	GPU issue
-   Targa	Traveller 1534	15.6”				?
-   Apple	PowerBook G4	15”				?	Cracked LCD it looks like

