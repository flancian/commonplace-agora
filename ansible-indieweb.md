# Ansible IndieWeb

Some kind of Ansible-scripted test best of IndieWeb sites, all spun up automatically and then tested interacting with each other.

Also!  Would be fun to turn that Ansible IndieWeb test bed into some kind of weird experiment of sites automated interacting with each other.  Just letting them go wild and see what happens.

