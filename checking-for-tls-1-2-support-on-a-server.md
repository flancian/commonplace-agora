# Checking for TLS 1.2 support on a server

[[Sysadmin]]

I was doing this to check that Mailgun would continue to work for some of our apps following their deprecation of TLS 1.2.

From what I glean from Mailgun's [blog post](https://www.mailgun.com/blog/tls-version-1-0-and-1-1-deprecation/), you need to check that wherever your app is that sends mail via Mailgun, that that server supports TLS 1.2.

You can do this by scanning it with \`nmap\`:

```shell
nmap --script ssl-enum-ciphers -p 443 domain.example
```

I guess it does a check on the [cipher suites](https://en.wikipedia.org/wiki/Cipher_suite) that the server is providing.

It will also tell you if any of the ciphers are vulnerable to attacks (and ergo that you need to update the server).

