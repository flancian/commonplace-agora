# Tragedy

> Why does tragedy give pleasure? Why do people who are neither wicked nor depraved enjoy watching plays about suffering or death? Is it because we see horrific matter controlled by majestic art? Or because tragedy actually reaches out to the dark side of human nature?
> 
> --[Why Does Tragedy Give Pleasure? - Oxford Scholarship](https://www.oxfordscholarship.com/view/10.1093/acprof:oso/9780198187660.001.0001/acprof-9780198187660) 

