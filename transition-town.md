# Transition town

I've known about transition towns for a while, but never really dug in that much.  [[Transition Town Tooting]] for example helped organise [[Restarters Tooting]] where I used to volunteer when I lived in London.

Most recently.. transition towns were mentioned in bolo'bolo.  

> Transition towns are emerging everywhere. Transition states, territories, provinces or regions could be the next step, up to a planetary transition “cooperative” of democratic states
> 
> &#x2013;  [[bolo'bolo]]

Hans Widmer obviously sees them as related to what he was espousing in bolo'bolo.

I've liked for a while the idea of local movements, e.g. [[Libertarian municipalism]].  I think transition towns are maybe a bit more about [[Social and Solidarity economy]] stuff, less about local politics?  But I bet there's a bit of an overlap.


## What is it

> Transition is a movement of communities coming together to reimagine and rebuild our world. 
> 
> &#x2013; [What is Transition? | Circular Model & Reconomy](https://transitionnetwork.org/about-the-movement/what-is-transition/)

<!--quoteend-->

> TT foregrounds the big twin threats as [[climate change]] and [[peak oil]], (the point when the maximum rate of global   production   is   reached   and   begins   its   terminal   decline.)   TT argues that these problems, can be tackled only if we develop robust community responses, forming local groups that grapple with issues like food, health, transport, energy, textiles, and waste and working out how they can become less fossil fuel dependent on a local level. 
> 
> &#x2013; [[The Rocky Road To a Real Transition]]

So it is a network of local, climate-conscious community groups.


## Critique/criticisms

> we want to ask: a transition to where, and from what? 
> 
> &#x2013; [[The Rocky Road To a Real Transition]]

<!--quoteend-->

> Much of the left-wing criticism of Transition Towns concerns their political naivety and the absence of a class analysis. From the right, this criticism can be framed as the decrying of the localist response to climate change as a patronizing and self-interested one by those who would happily spend a lifetime listening to Radio 4, sitting by the Aga.
> 
> &#x2013; [[How Could we Study Climate-Related Social Innovation? Applying Deleuzean Philosophy to the Transition Towns]]

<!--quoteend-->

> Transition’s refusal to engage in confrontational approaches to change [&#x2026;] has been a conscious decision from the outset [&#x2026;]. For me, Transition is something that sits alongside and complements the more oppositional protest culture, but is distinctly different from it. It is a different tool. It is designed in such a way as to come in under the radar.
> 
> &#x2013; [“The Rocky Road to a Real Transition”: A Review. » Transition Culture](https://web.archive.org/web/20080518012407/https://www.transitionculture.org/2008/05/15/the-rocky-road-to-a-real-transition-by-paul-chatterton-and-alice-cutler-a-review/) 

So I guess basically the criticisms are that TT is lacking in political consciousness and struggle.  Perhaps something that is just the preserve of the middle classes who don't really want to get their hands dirty?

Perhaps you could see it as [[Prefiguration]] though.

The authors of Rocky Road start off with this quote from [[Murray Bookchin]]:

> Any sound ecological perspective rests in great part on our social perspectives and interrelationships; hence to draw up an ecological agenda that has no room for social concerns is as obtuse as to draw up a social agenda that has no room for ecological concerns.

(Side note: where is that quote originally from?)

So perhaps they are levelling that TT is an ecological agenda with no social agenda.  Not sure that's true though.  It would seem to have plenty of social activity, it just isn't direct action.

