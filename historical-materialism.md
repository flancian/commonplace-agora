# Historical materialism

> The claim that history is the result of [[material conditions]] rather than ideas or the machinations of 'great men'
> 
> &#x2013; [The Fundamentals of Marxism: Historical Materialism, Dialectics, & Political Economy](https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy) 

<!--quoteend-->

> Historical materialism is, simply stated, the theory that human societies develop according to how the “forces of production” are ordered, and that the features of a society will, ultimately, relate back to the ordering of the forces of production. 
> 
> &#x2013;  [What It Means to Be a Marxist](https://jacobinmag.com/2018/12/marxism-socialism-class-struggle-materialism) 

In the episode of Rev Left in the resources, they talk about it in contrast to idealism.  And refer to it more in terms of material conditions, whereas the above quote is 'forces of production'.


## Resources

-   [The Fundamentals of Marxism: Historical Materialism, Dialectics, & Political Economy](https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy)


## Flashcard


### Front

What is historical materialism?


### Back

the theory that human societies develop according to how the “forces of production” are ordered, 

and that the features of a society will, ultimately, relate back to the ordering of the forces of production. 

