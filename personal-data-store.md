# Personal data store

You own your data, and you choose to let apps interact with it for your benefit.

See e.g. [[Solid]].

> It is possible to design data storage systems that are decentralized and give individuals control over their personal data, including its portability, and the ways in which it is shared with third parties.
> 
> &#x2013; [[Future Histories]] 


## Services

-   https://personium.io/en/index.html

