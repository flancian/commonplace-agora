# Why Does Agile Fail?

-   https://wakingrufus.neocities.org/fail-agile.html

Talks about [[Worker cooperatives]] and [[Viable system model]] as alternatives.

> How can we give software developers agency to be personally successful, generate value for their companies, AND develop software which interacts with our society in an ethical way?

Some great thoughts in this thread: https://mastodon.technology/@wakingrufus/104032275121191642

> The only way to fix the issues called out by the manifesto is to attack the root cause. You must address the structure of a for-profit corporation within the capitalist mode of production

