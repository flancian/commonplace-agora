# tit for tat with forgiveness

> "Tit for Tat with forgiveness" is sometimes superior. When the opponent defects, on the next move, the player sometimes cooperates anyway, with a small probability (around 1%-5%). This allows for occasional recovery from getting trapped in a cycle of defections. The exact probability depends on the line-up of opponents. "Tit for Tat with forgiveness" is best when miscommunication is introduced to the game — when one's move is incorrectly reported to the opponent. 
> 
> &#x2013; [Tit for tat | Psychology Wiki | Fandom](https://psychology.fandom.com/wiki/Tit_for_tat) 

[[tit for tat]]

