# Celebrate People's History: The Poster Book of Resistance and Revolution

A
: [[book]]

URL
: https://www.feministpress.org/books-a-m/celebrate-peoples-history-second-edition

A gift from my brother for Christmas 2020.

Over a hundred inspiring posters celebrating moments of resistance and revolution.

The posters: https://justseeds.org/project/cph/

