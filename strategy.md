# Strategy

> strategy is concerned with both what is happening in the moment inside the organisation and how best to regulate it to achieve set goals as well as what is happening outside in the external environment and with respect to the possible futures of the organisation.
> 
> &#x2013; [[Anarchist Cybernetics]]

