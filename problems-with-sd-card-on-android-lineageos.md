# Problems with SD Card on Android / LineageOS

Since moving to [[LineageOS]] ([[Installing LineageOS on Samsung Galaxy S5]]) I've had a bunch of problems with my SD Card.  Not sure if it's Lineage specific, or just Android 11 related.  But I was getting occasional messages like "SD Card Not Found", "This SD Card is corrupted", even though the card seems fine.

It was a pain because I started getting 'Failed to save photo' from Open Camera, files not showing up in the File Manager, etc.

I tried formatting it as internal storage as portable storage, problems with both.

Ultimately I've just removed the SD Card for now and 'Forgot' it from Android, so I'm back to the pretty bog-standard 16Gb storage for now.  Hoping to try out a separate SD Card soon to see if it is indeed a problem with the card.

If it isn't, it's either a problem with the reader on the phone, or something software related.

