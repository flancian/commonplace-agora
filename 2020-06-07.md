# 2020-06-07



## [[Editable software]]


## DiskUsage

The [DiskUsage](https://f-droid.org/en/packages/com.google.android.diskusage/) app is a handy little libre [[Android app]], available on F-Droid, for seeing where your storage space is being used, and freeing it up.


## Heysham promenade

![[photos/heysham-promenade.jpg]]

The promenade at [[Heysham]], looking out over [[Morecambe Bay]].  Cloudy blue sky, a flat sea, and concrete promenade, stacked on top of each other.  The tide was in.

