# conversations with myself

> Blogs as thinking out loud and conversations (also with oneself). Wiki as its accumulated residue.
> 
> ---**Ton Zylstra** ([The Blog and Wiki Combo](https://www.zylstra.org/blog/2019/06/the-blog-and-wiki-combo/))

A space for conversations with yourself are important. I tend to have my conversations with myself in my wiki more than on my blog.

