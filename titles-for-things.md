# Titles for things

Titles for things if I ever need to title something (songs, books, maybe).

-   **Beyond Fields**. Backstory: it's a place name in the Lake District.
-   **Buzzard from above**. Backstory: I saw a buzzard from above when hiking to Swirl How and it was great.
-   **Morecambe Meat Machine**.  Backstory: it was on the side of a butcher's van in Morecambe.
-   **If you lived here you'd be nearly home**.  Saw it on a sign somewhere for some shitty newbuild skyrise of flats.
-   **Black bamboo**.  I like bamboo.
-   **The Bee and the Buddleia**.  Saw a bee on a buddleia at a time I was feeling particularly happy in the Lakes.
-   **Dual Power** (https://en.wikipedia.org/wiki/Dual_power)

