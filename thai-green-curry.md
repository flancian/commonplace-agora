# Thai green curry

![[2021-04-03_10-47-49_MS0AHQu.png]]

> 1 aubergine: aubergine
> 
> 1 red chilli: red chilli
> 
> 1 lime: lime
> 
> 15g fresh root ginger: Ginger (100%) Keep the Ginger chilled.
> 
> 1 bag of roasted peanuts (25g): roasted peanut 96%, rapeseed oil. May contain sesame & nuts
> 
> 80g sugar snap peas: sugar snap peas
> 
> 80g Tenderstem broccoli: tenderstem broccoli(100%)
> 
> Dried kaffir lime leaves: dried kaffir lime leaves
> 
> 1 Thai green curry paste sachet (40g): garlic puree (32%), green chilli puree (23%), white onion puree (13%), lemongrass puree (13%), galangal, salt, lime leaves, coriander, rapeseed oil, water, citric acid, fennel, xantham gum, cayenne, spinach extract, potassium sorbate
> 
> 25g solid coconut cream: 100% coconut
> 
> 130g basmati rice: Basmati white rice
> 
> Vegetable stock mix sachet (11g): Dried glucose syrup, yeast extracts, salt, sugar, onion powder, carrot juice powder, tomato powder, rapeseed oil, herb (lovage), natural flavouring

