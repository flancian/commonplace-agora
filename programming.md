# Programming



## Timeline


### Early stuff

-   I have early memories coding with my Mum and my Grandad
-   typing in BASIC to the [[Amstrad]]
-   making the screen background flash different colours
-   making it print out "Hello, <name>" over and over
-   these were from the instruction manual I'm pretty sure

![[2020-03-21_22-11-58_serveimage.jpg]]

-   also I think we got these other books full of code from somewhere, where you just went through and typed a full program in line by line.  Often it didn't work, you had to go through line by line and find the error.
-   I also remember making a program with my Grandad on his business Amstrad PC.  It was something to do with the Blitz, a game of dropping bombs on a city.


### College

-   I learned Pascal at college in my Computing A-level


### The web

-   I remember teaching myself HTML around late 1990s it must've been, from [Webmonkey](https://en.wikipedia.org/wiki/Webmonkey).  I was listening to [[Tri Repetae]] by [[Autechre]] a lot while doing it.  Fun times.


### Now

I'd like to learn [[Lisp]].  We were doing a group learning via [[SICP]] for a while at https://evalapply.space, but that has gone dormant for now.

Perhaps [[Clojure]].  

