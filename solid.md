# Solid

Social linked data.

> As separate markets for data and apps emerge, Web development needs to adopt a new shape ◆ Most Web applications today follow the adage “your data for my services”. They motivate this deal from both a technical perspective (how could we provide services without your data?
> 
> &#x2013; [Paradigm shifts for the decentralized Web | Ruben Verborgh](https://ruben.verborgh.org/blog/2017/12/20/paradigm-shifts-for-the-decentralized-web/) 

Interesting article on the Solid (Social linked data) platform.  It describes a lot of the decentralisation concepts that are explored and implemented in the indieweb movement (surprised the article doesn’t mention indieweb, in fact, given the W3C link), but comes at it from a Linked Data angle.  The language around markets and competition doesn’t really appeal to my personal politics, but good to see the philosophy of moving away from centralised silos being explored in different ways.

I feel like Solid, [[ActivityPub]] with a generic server and C2S, and [[Indieweb]], are all kind of chipping away at the same thing. You have all your data in one place (either self-hosted or someone-else-hosted) and you decide which apps you want to let interact with it. 

Saturday lunchtime Tim BL gave a talk on Solid. People were queueing out the door to get in. I caught some of it on screen but not loads. When Tim talks about it it sounds pretty exciting and positive. If you go to the Inrupt website it sounds like corporate newspeak. I fear that venture capital will never truly want what is best for the world on general, just whatever lines the pockets of the investors.

\#mozfest #solid

&#x2013; [How solid is Tim’s plan to redecentralize the web? - Irina Bolychevsky - Medium](https://medium.com/@shevski/how-solid-is-tims-plan-to-redecentralize-the-web-b163ba78e835) 

I really like the [[Personal Data Store]] concept. You own your data, and you choose to let apps interact with it for your benefit. It’s pretty much what the IndieWeb is doing (though perhaps for the more limited subset of things that don’t need verified claims).

I don’t like the commercial nature of most PDS offerings (including Solid now).

Either way, some good general food for thought in this article.

