# Nature Matters: Systems thinking and experts

URL
: https://www.open.edu/openlearn/nature-environment/the-environment/nature-matters-systems-thinking-and-experts/content-section-0?active-tab=description-tab
    
    > This free course, Nature matters: Systems thinking and experts, **explores conceptual tools for assisting our thinking and deliberation on what matters**. The notion of 'framing' nature is introduced and three readings provide an understanding of **systems thinking for explicitly framing issues of environmental responsibility**.

