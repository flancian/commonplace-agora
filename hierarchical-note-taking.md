# Hierarchical note-taking

See e.g. [Dendron](https://www.dendron.so/).

Vs [[Non-hierarchical note-taking]].

See [[note-taking]].

> I see Dendron adds hierarchy by using naming conventions. That doesn't appeal to me at all, smacks of the Dewey decimal system, and ultimately everything ending up in '999miscellaneous', or the alt. usenet category. Whereas to me, as per Weinberger, everything **starts as** misc and its place in the scheme of things is emergent.
> 
> &#x2013; [Ton Zijlstra](https://m.tzyl.nl/@ton/105135732686588264) 


## Resources

-   [A Hierarchy First Approach to Note Taking - Kevin S Lin](https://www.kevinslin.com/notes/3dd58f62-fee5-4f93-b9f1-b0f0f59a9b64.html)

