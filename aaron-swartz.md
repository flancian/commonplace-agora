# Aaron Swartz

> Aaron Swartz was a computer programming prodigy and activist who played an instrumental role in the campaign for a free and open Internet and used technology to fight social, corporate and political injustices.
> 
> &#x2013;  Aaron Swartz | Internet Hall of Fame 

Huge activist for [[open access]] and internet freedom.

[[Guerilla Open Access Manifesto]].

Involved in:

-   [[RSS]]
-   [[Markdown]]
-   [[Creative Commons]]
-   [[Open Library]]
-   [[Reddit]]
-   [[SecureDrop]]

