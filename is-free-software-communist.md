# Is free software communist?

[Does this even make sense as a question?]

Depends on your definition of [[communism]] and your definition of [[free software]].

Free software at least fulfils the tenet "[[From each according to their ability, to each according to their need]]".

Multiple takes on this here: https://www.quora.com/Is-free-open-source-software-communist


## Epistemic status

Type
: [[question]]

