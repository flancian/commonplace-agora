# Tufte CSS

> Tufte CSS provides tools to style web articles using the ideas demonstrated by Edward Tufte’s books and handouts. Tufte’s style is known for its simplicity, extensive use of sidenotes, tight integration of graphics with text, and carefully chosen typography. 

-   [Tufte CSS](https://edwardtufte.github.io/tufte-css/)

