# To FILE

Fleeting notes that I **probably** want to make permanent in the wiki.  Keeping them here to avoiding clogging up rest of org&#x2026;


## oggcamp


### day 1

-   openbenches.org
    -   nice use of peer production
    -   pulled it together with a few simple technologies
    -   people like the leaderboard&#x2026;
-   mqtt
    -   what's it for?
        -   messaging queue
        -   streams of data from sensor devices?
    -   mosquitto
    -   used with IoT things?
    -   sceptical about overuse of IoT
    -   but liked the use of it around energy poverty
    -   and saving energy from fridges - detecting when seal was wasting energy (however the answer was replacement??)
    -   saving energy from detecting positions of animals in enclosures
-   I gave a talk on community repair
    -   it was international repair day
    -   talked about what community repair is
    -   talked about open data we publish
    -   global movement
    -   talked about right to repair
-   ubuntu snaps
    -   interesting, how does it compare/relate to nix/guix?
    -   sounds similar in some ways
    -   universal packaging system, allow for multiple versions side by side
    -   easy rollback
-   astralship
    -   really liked this
    -   a small alternative living space, mix of permaculture and technology
    -   comes from pirate politics to some degree
    -   liked the ship metaphors.. you don't go on workshops, you go on voyages
    -   idea is to enable deep flow states to try and solve some of current global problems
    -   the geodesic domes are also interesting
-   video downloads
-   panel discussion
    -   taking up linux more in schools: main problem is awareness of its existences
    -   computing curriculum is apparently currently rubbish
        -   boring
        -   would be better if it was project based
    -   microsoft - good or bad?
        -   for me, focused too much on whether its good or bad for open source
        -   didn't look at ethics of microsoft overall
            -   ICE, AI for fossil fuel yields


## your undivided attention


### the dictator's handbook

-   Dec of human rights as basis of content moderation
-   FreeBasics is colonialism


## Politics


### Refugee Crisis


#### No one puts their children in a dinghy unless the water is safer than the land     :quote:


### Energy Policy     :green:


#### Onshore wind and solar will be as cheap as or cheaper than gas by 2020


#### Increased role for renewewables, particularly due to improvements in battery storage


#### Environment Plan


##### Don't want to lose environmental protections from EU law


##### UK used to be known as the dirty man of Europe


### Inventing the future


#### Catalogue of sci-fi matching political ideas I'm interested in


### Emancipatory politics


#### Emancipatory politics must always destroy the appearance of a 'natural order', must reveal what is presented as necessary and inevitable to be a mere contingency, just as it must make what was previously deemed to be impossible seem attainable     :quote:


### Information goods are increasingly used as a way of enforcing purchase of market goods


### My politics

What are my values?  And from there, what are my politics?
I believe in the solidarity economy.
I believe in dual power as a means of transition.
I believe in communit organising and the avoidance of unnecessary hierarchy.
I believe in cooperativism and worker self-direction.
I would say that I am a social anarchist, but not sure of specific tendency.
I like Bookchin's writings, so perhaps a communalist.


### Definitions


#### What is civics?

Civics is the study of the theoretical, political and practical aspects of citizenship, as well as its rights and duties; the duties of citizens to each other as members of a political body and to the government."


#### What is the solidarity economy?

A solidarity economy is based on efforts that seek to increase the quality of life of a region or community through local business and not-for-profit endeavors. It mainly consists of activities organized to address and transform exploitation under capitalist economics and the large-corporation, large-shareholder-dominated economy, and can include diverse activities


## To investigate


### Thomas Spence commons


### Repowering london


### Briar app


### Population increases two people every second?


### 2050 Nigeria pop equal to China?


### phabricator


### Examples of open source following site deaths


### ordos, inner mongolia


## Politics


### Castells


### Tiqqun


### Shipping container vs dock workers


### Outrage on the internet


### If you took out tv signal would it affect anyone negatively?


### 3 aspects = automation?, surveillance and control?, commoditization


### Right look for converts, left look for traitors


## Marcuse: An essay on liberation

First of all saying that simply just the alienation of the working class, the proletariat was not enough.

Right now, it's not enough to bring about revolution, plenty of ways in which still alienated.  Cultural industry maybe. This is from the Frankfurt School, so probably something related to that. 

One was any kind of response has to be rhizomatic, you have a narrative that speaks to a number of different elements within the working class.

So, a big part of that kind of revolution is not it's not enough kind of depression and grief. That's something along the lines of changing people's sensibilities has to do with.

Yes, some of the awareness raising was through arts and creativity. This changing the biology people to think differently.

Finally, solidarity.


## Getting outside the social industry

What are some ways to get outside the social industry?  To break the pattern of railing against the spectacle just being part of the spectacle.  Like some of the stuff Adorno chats about as being outside the culture industry.  Maybe the cyberflaneur stuff.  That kind of stuff, draw on some of these movements and look for parallels. 

Let's say that the connection is axiomatic.  Everything else (likes, replies, etc) is spectacle unless proven otherwise. What new forms can we find that are completely outside of what has gone before? Avoid the formula of the social industry.


## TODO Return to group chats

Interesting. Mentioned on Joanne interview twsu. 


## TODO OECD

Juan podcast 


## TODO Global tech taxes

Juan podcast 


## TODO I don't want my free time to be labour for a big tech firm


## TODO How do you define value of big tech firms?


## TODO Social media addiction is attacking people's freedom of thought


## Leader of Haitian revolution coopted for venture capitalism


## TODO Solidarity across tech supply chains

Mentioned in grace blakely podcast 


## TODO Marx saw technology as emancipatory


## TODO Attwntion econony and the sociert of the spectscle


## TODO Cloud apps are a bit like supply chains

Global products, we have little agency.


## TODO Solidarity without class consciousness

That article says it can be dangerous. Parallel to libre software and use by big tech firms? 


## TODO BDC

Imagine if various States made interoperable platforms. With content from each.


## TODO Doesn't decentralization just mean you can't track fascists etc?

They have somewhere to hide themselves?  I got asked this question.


## TODO You needed access to a networked computer, and the knowledge–neither of those have ever been evenly distributed–but the technical architecture was incredibly exciting.


## Tools for support

-   into tools for thought, but they feel always quite individualistic.  what about tools for support?  community building? Microsolidarity?


## Micropublishing from org-mode

While I have my odd hybrid system, I would like to be able to use micropub to publish notes from my wikilog to my blog.  I'm using WP for that, for the dynamic bits that it gives me.  But I'm copying and pasting from my wiki to my blog.  I was doing it manual until it hurts, to see if it was worth it.  I think it is.  So now it would be good to automate it a little.


## GPT-3 and me

How to set up little GPT-3 prompts using my wiki as a corpus?


## TODO economic policy in response to Big Tech must go beyond the fascination with data. If hardware is important too, then opening up data is an ineffective idea at best and a counter-productive idea at worst. It could simply mean that the tech giants get access to even more free data – while everyone else trains their open data on Amazon’s servers. If we want to take back control over Big Tech, we need to pay attention to more than just data.


## TODO Existing arguments about how large tech companies freely use open-source software as a foundation to build their proprietary empires must also be supplemented with the ways in which free – and waged – labour are brought into the ambit of companies via things like open-source frameworks


## TODO monopolisation of AI is not just – or even primarily – a data issue. Monopolisation is driven as much by the barriers to entry posed by fixed capital, and the ‘virtuous cycles’ that compute and labour are generating for the AI providers.


## TODO phrase is both a promise and a deflection. It’s a plea for unearned trust — give us time, we are working toward progress. And it cuts off meaningful criticism — yes, we know this isn’t enough, but more is coming


## TODO The architecture of the social network — its algorithmic mandate of engagement over all else, the advantage it gives to divisive and emotionally manipulative content — will always produce more objectionable content at a dizzying scale.


## TODO “You see lots of people putting forth a hopeful idea of a new, humane social media platform to rescue us — one that respects privacy or is less algorithmically coercive,” Siva Vaidhyanathan, a professor of media studies at the University of Virginia, told me recently. “But if we’re being honest, what they’re really proposing at that point is not really social media anymore.”  In other words, the architecture is the problem


## TODO Stayin Alive on Twitter: "This week's book recommendation comes from @hrheingold. Learn the culture and consciousness behind the first PCs from one of the smartest #tech writers around. More book recommendations from our guests here via @goodreads: https:…

https://mobile.twitter.com/StayinPodcast/status/1280223754279403520


## TODO https://twitter.com/marinapla/status/1287963469103456257


## TODO https://twitter.com/RestartProject/status/1288371183654707201


## TODO To Tackle Obesity, Fight Inequality

Captains of the food industry would have you believe that this a result of poor personal choice alone. This is a libertarian falsehood. Obesity is a population health problem connected to social, economic and environmental factors. In higher-income countries, it is associated with lower socio-economic status, poverty and inequality. It does not exist in a vacuum.


## TODO https://twitter.com/tingeber/status/1290193855459991552


## TODO Tiera comun


## TODO Furness Line

One of the most scenic lines on the UK that I've been on&#x2026;


## TODO Superintelligence

If you think that a superintelligencee would mean total domination and subordination, that shows what you view to be intelligent behaviour.


## TODO Economic Networks — Write.as

https://write.as/economic-networks/


## TODO Freecycle is a nonmarket platform coop


## TODO Frameworks

But once you are given a tool that operates effortlessly — but only in a certain way — every choice that deviates from the standard represents a major cost.


## TODO https://twitter.com/julian0liver/status/1304680902929166336


## TODO https://twitter.com/tingeber/status/1305050369681227778


## TODO http://www.mindorg.com/hypertext/Echt93.htm


## TODO https://twitter.com/dmytri/status/1311271926451654658


## TODO https://twitter.com/julian0liver/status/1312164995149361152


## TODO Schools in England told not to use material from anti-capitalist groups | Education | The Guardian

https://www.theguardian.com/education/2020/sep/27/uk-schools-told-not-to-use-anti-capitalist-material-in-teaching


## TODO Ethics and agency

Agency and subjection
Four /3 software freedoms
Change from kantian to agency based framework


## TODO https://twitter.com/R2REurope/status/1313021379088994304


## TODO https://twitter.com/R2REurope/status/1313408296552980480


## TODO Tethered economy https://twitter.com/RestartProject/status/1314137772945481728


## TODO https://twitter.com/RestartProject/status/1317546759367856128


## TODO Connected thought

In short, increasing the rate of innovation can be achieved by fostering an environment with enough sociability (so we can collaborate and combine ideas), enough transmission fidelity (so we don’t keep on re-inventing the wheel), and enough transmission variance (so we can incrementally improve the wheel instead).


## TODO Interconnected zettelkasten

And threaded twitter (or threaded mastodon?)


## TODO Share via

Wells was also a prolific essayist and committed socialist who believed passionately that new information technologies would one day usher in an era of social equality and world peace.


## TODO https://twitter.com/RestartProject/status/1276172562419060739


## TODO https://twitter.com/RestartProject/status/1276158155576299521


## TODO https://twitter.com/RestartProject/status/1276157958636933120


## TODO https://twitter.com/RestartProject/status/1276150550225190914


## TODO The law, sometimes heralded as “ the ‘Magna Carta’ of the internet,” was passed in 1996 to provide websites with incentive to delete pornography, but it has since evolved. It is now effectively a shield websites use to protect them from responsibility for all sorts of activity on their platforms, from illegal gun sales to discriminatory ads.  


## TODO Citron has worked for nearly two decades to find legal and social strategies to combat the cyber harassment and invasions of sexual privacy that women, sexual minorities, and people of color disproportionately experience online


## TODO iSlavery” is “a planetary system of domination, exploitation, and alienation…epitomized by the material and immaterial structures of capital accumulation”


## TODO Technology does not guarantee progress. It is, instead, often abused to cause regress


## TODO IndieWeb seems like one of most active ways to.be political in tech? Or at least **an** active way.. Right to repair too.


## TODO I decided to hand-code everything in plain HTML and CSS, manually link all the pages and even hand-write the RSS feed. And to be honest, I haven’t had this much fun making a website since when I first started playing around with Microsoft Frontpage and Adobe Photoshop 4.0 in the late 90s and early 2000s.


## TODO It was all terribly exciting. Unlike traditional media, you could now speak back and participate. It was the first interplanetary communication system where anyone, anywhere in the world, could make a page and share their thoughts and ideas with the world.


## TODO But if you wanted your web page to be "on the web", where would you put it?  You needed a web host of some sort to store the pages and share a public address so other people could visit. It would ideally also be free so you could try things for fun without having to think about it too much.

Beaker makes this.change?


## TODO One way of doing so was by browsing directories, like the Geocities neighbourhoods: lists of websites often arranged by categories and sub-categories. In fact, most search engines were also directories, or portals as some were called back then.

Cyberflaneur ism?


## TODO https://blog.cjeller.site/correspondence?pk_campaign=rss-feed


## TODO Gated community more than a walled garden

Emphasis on the gated, less on the community&#x2026;

With the same hostility to the outsider. .

Interesting how architrctural metapjors come up so much. Jane Jacobs.


## “The system had to have one other fundamental property: It had to be completely decentralized. That would be the only way a new person somewhere could start to use it without asking for access from anyone else.” (Berners-Lee and Fischetti 1999, p. 16).


## Six experiments in social housing     :wiki:writing:

-   https://www.vam.ac.uk/event/e6xQ4gL3/a-home-for-all-six-experiments-in-social-housing


## https://ournetworks.ca/recorded-talks/     :wiki:


## https://reclaimfutures.org/     :wiki:


## TODO One of the hardest things about recycling is that you are not sure how [the manufacturers] made it,” says Kirkman.     :wiki:


## TODO Starting from March 2021, manufacturers selling certain household appliances will have to ensure that spare parts are available for a number of years after their product has launched; that their items can be easily disassembled (and so use screws not glue); and that they provide access to technical information to repair professionals.      :wiki:


## TODO Buyerarchy of needs     :wiki:


## TODO A movie should give us something     :wiki:


## Postmodernism, or, The Cultural Logic of Late Capitalism

https://www.listennotes.com/e/p/1a2fd2117bea476895d55bea024544c8.mp3 [00:57:24]

Chronological timelines and remembering the past, collapsing of time 


## Episode #116 &#x2026; Structuralism and Mythology pt. 1

https://www.listennotes.com/e/p/577501f5c1544f7cb2f06549350c7823.mp3 [00:10:34]

Barthes mythology mass media


## Structuralism and Mythology pt. 2

https://www.listennotes.com/e/p/da931452999d44c18a55032326a86f34.mp3 [00:04:18]

Social media timelines we.make. for ourselves are a mythology?


## Facebook ad boycott

When you look at the list of names of companies it is kind of depressing. And you partly think.- how can something instigated by these kinds of groups actually be good.

I guess if they rrpedent the demands of citizens, then good. But not convinced.


## Four ways to change the world Lessig


## https://generativeartistry.com


## https://mastodon.host/@BartG95/105249920879932653

BartG95@mastodon.host - "Search engines should be the business of libraries, not companies."


## http://a9.io/glue-comic/


## Unflattening — Nick Sousanis | Harvard University Press


## TODO https://twitter.com/julian0liver/status/1264731014011793413


## TODO Share via

The age of low-tech: towards a technically sustainable civilisation


## TODO https://chaos.social/@bleeptrack/105277621088219824

bleeptrack@chaos.social - RT @blinry@twitter.com

One of the weird things I do is curating the "Glitch Gallery", which collects accidental artworks, produced by broken or buggy programs! <3

I'm especially amazed by @ManuelaXibanya@twitter.com's submission today – she accidentally applied a text material to a tree! https://glitchgallery.org/a-poem-as-lovely/​

🐦🔗: https://twitter.com/blinry/status/1331999523653242880​


## TODO https://graz.social/@publicvoit/105277729470353565

Tethered economy

publicvoit@graz.social - Expressions of bad consumer choices. 🙄 

RT @Carnage4Life@twitter.com

Welcome to the future

🐦🔗: https://twitter.com/Carnage4Life/status/1331707774334427137​


## TODO Emacs Chat: Conversations about an awesome text editor –

https://sachachua.com/blog/emacs-chat/


## TODO https://mobile.twitter.com/Roamfu/status/1331201193100525569


## TODO https://mobile.twitter.com/kcorazo/status/1252669453961031680


## TODO https://www.roambrain.com/in-search-of-the-literature-x-ray/


## TODO Indie web is interconnected zettelkasten


## TODO European Parliament Votes for Right to Repair - iFixit

https://www.ifixit.com/News/47111/european-parliament-votes-for-right-to-repair


## TODO Death via ipad

https://twitter.com/roto_tudor/status/1334534101265682434


## TODO GIU designing freedom


## TODO Tech workers have unique access to current means of production


## TODO Less is more hickel


## TODO LikeWar: The Weaponization of Social Media


## TODO https://cydharrell.com/book/table-of-contents/


## http://meetingexpectations.surge.sh/#/


## TODO Refugees and coronavirus

The coronavirus pandemic that threatens to overwhelm the camps will have “catastrophic consequences for the refugees, Greek inhabitants and the rest of European society,” says the petition, launched last week by Dutch medical professors and public health experts.

It is an illusion to think that a COVID-19 outbreak in these camps could be kept under control: 40,000 people are living on a few square kilometres, and there are only a handful of doctors present. Many children and adults are already ravaged by physical and mental traumas.

If Europe looks away now, this situation could escalate to become a medical disaster, which would represent a serious violation of the norms and values of European healthcare. It is our duty to prevent this from happening.
https://www.theguardian.com/world/live/2020/mar/28/coronavirus-live-news-cases-in-italy-overtake-china-us-infections-pass-100000


## TODO Police and prison

Recent story where someone was promoting longer sentence lengths for murder of emergency workers?


## TODO How Facebook changedd

“We do not and will not use cookies to collect private information from any user,” vowed an early privacy policy.


## TODO Share via

Mandating interoperability, for example, would make it easier for new entrants to attract users. “We really want Facebook to have to compete based on the quality of their product,” she said.


## TODO http://hangaroundtheweb.com/2019/07/mind-maps-in-spacemacs/


## TODO Reading involves knowing what to skip

https://rosano.hmm.garden/01et5dvwp93keqfk30kb4da392


## TODO media.ccc.de - Funkwhale and the Importance of Decentralized Podcasting

https://media.ccc.de/v/rc3-520410-funkwhale_and_the_importance_of_decentralized_podcasting

Really don't want a Facebook of podcasts. Listennotes is a bit like that.


## TODO media.ccc.de - Building Blocks of Decentralization

https://media.ccc.de/v/rc3-11400-building_blocks_of_decentralization


## TODO design-system.service.gov.uk


## Anbox

run android on linux


## carbon divestment


## microrebellion / micropraxis

social account posting micro acts of dissent, revolution and rebellion. Maybe a mastadon bot? Could use book of dissent for some quotes.


## community technology - karl hess


## [Alan Kay - Quora](https://www.quora.com/profile/Alan-Kay-11)


## [New breed of local food halls in UK towns offer grub and a hub | Commercial p&#x2026;](https://www.theguardian.com/business/2021/feb/21/new-breed-of-local-food-halls-offers-grub-and-a-hub?CMP=Share_AndroidApp_Other)


## nb https://xwmx.github.io/nb/

command line note taking tool


## org-babel indentation

https://github.com/syl20bnr/spacemacs/issues/13255


## org mode / syncthing encryption

https://www.reddit.com/r/emacs/comments/mdjtyq/how_do_you_sync_your_org_files_securely/


## Appropriation of nature (and terminology) by technology

-   e.g. Apple
-   e.g. cloud
-   is there a danger of the left doing it too?
    -   e.g. [[Seeding the Wild]] &#x2026; great article, but leans on nature.  Is it OK?
-   is it probably OK as analogy
-   any time we reappropriate
    -   what are we hiding or being blind to?


## TODO Munipal ownership Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 41. SMOOTH CO-OPERATORS: the power of co-ops to transform society https://play.acast.com/s/reasonstobecheerful/episode41.smoothco-operators-thepowerofco-opstotransformsociety [00:24:50]


## TODO Benefits of coops Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 41. SMOOTH CO-OPERATORS: the power of co-ops to transform society https://play.acast.com/s/reasonstobecheerful/episode41.smoothco-operators-thepowerofco-opstotransformsociety [00:32:11]


## TODO Transition to coop economy Reasons to be Cheerful with Ed Miliband and Geoff Lloyd: 41. SMOOTH CO-OPERATORS: the power of co-ops to transform society https://play.acast.com/s/reasonstobecheerful/episode41.smoothco-operators-thepowerofco-opstotransformsociety [00:36:16]


## TODO Permanent job losses from Covid-19 are estimated to hit approximately a million by the end of 2022 and about 2m in the next decade


## TODO the immediate shock of the Covid recession could be offset by employment in low-CO2 sectors


## TODO Transparency International UK said its analysis indicated “apparent systemic biases in the award of PPE contracts that favoured those with political connections to the party of government in Westminster”, contrary to denials by civil servants and Conservative ministers


## TODO Wales to launch pilot universal basic income scheme     :wikify:

https://www.theguardian.com/society/2021/may/14/wales-to-launch-universal-basic-income-pilot-scheme?CMP=Share_AndroidApp_Other


## TODO The Guardian view on taking back the buses: a route to recovery     :wikify:

https://www.theguardian.com/commentisfree/2021/may/14/the-guardian-view-on-taking-back-the-buses-a-route-to-recovery?CMP=Share_AndroidApp_Other


## The Technology and the Society, Raymond Williams


## TODO Zak ove moko jumbie


## TODO Ian Cheng emissiaries


## TODO Betty davis     :research:


## Exarchia


## Hackerfarm, Tokyo


## Theory of change https://www.nesta.org.uk/sites/default/files/theory_of_change_guidance_for_applicants_.pdf


## Transition design http://transitiondesign.net/


## Beautiful trouble


## St Paul principles


## Autonomists like Negri


## Mouffe, deliberative democracy or agonistic pluralism


## Medium of exchange vs store of value


## M15 indignados


## TODO savesomegreen.co.uk


## TODO Securityheaders.com


## fieldready.org - engineers without borders


## Yabo farmers     :research:


## Signlfm podcasts


## Opportunity tree


## Assumptions buffet


## How does freegle image analysis work?


## Progressive WordPress apps


## African fractals


## Second hand market smartphones


## London renewable power


## Chile solar power


## Grenoble and sao Paulo ban advertising


## Food citizenship


## Anarchist Cybernetics is a bit weak on issues around cdm and exclusion, raises them but doesn't go in depth


## Shannon Weaver is good name for protagonist in book


## [An Introduction to Agent-Based Modeling: Modeling Natural, Social, and Engine&#x2026;](https://www.amazon.com/gp/product/0262731894/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=compleexplor-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=0262731894&linkId=def1b699f8a5d0473b7a759715fe457a)


## https://hal.archives-ouvertes.fr/hal-00876569/file/iccsa13-416-Online_Analysis_and_Visualization_of_agent_based_models.pdf


## https://drum.lib.umd.edu/bitstream/handle/1903/19050/Ariyaratne_umd_0117N_17609.pdf?sequence=1&isAllowed=y


## https://ccl.northwestern.edu/2017/Abbott.pdf


## Building in the Open. Around the beginning of this year, I… | by Paul Frazee | Jun, 2021 | Medium

https://paulfrazee.medium.com/building-in-the-open-70ac9dccf1aa


## Saying we demand X should be also saying what do we need to do to get X nunes podcast


## Tweet from 🌌🌵🛸Bret🏜👨‍👩‍👧🚙 (@bcomnes)

https://twitter.com/bcomnes/status/1409514648198692869?s=20


## TODO Corralling - how do you decide which pages should and shouldn't be included? Maybe everything could be included, but the more people contribute the more the pages have weight?


## TODO Isn’t it good, Swedish plywood: the miraculous eco-town with a 20-storey wooden skyscraper

https://www.theguardian.com/artanddesign/2021/oct/14/skelleftea-swedish-plywood-eco-town-20-storey-wooden-skyscraper-worlds-tallest?CMP=Share_AndroidApp_Other


## TODO Britain’s early handling of the coronavirus pandemic was one of the worst public health failures in UK history, with ministers and scientists taking a “fatalistic” approach that exacerbated the death toll, a landmark inquiry has found.  “Groupthink”, evidence of British exceptionalism and a deliberately “slow and gradualist” approach meant the UK fared “significantly worse” than other countries, according to the 151-page “Coronavirus: lessons learned to date” report led by two former Conservative ministers


## TODO TiddlyWiki on Twitter: "A demo of dragging tiddlers from one wiki another on an iPhone running iOS 15 https://t.co/C8qD4GWoSK" / Twitter

https://mobile.twitter.com/TiddlyWiki/status/1447576045289345026


## TODO "My big issues with ActivityPub is that the protocol is very big and not very easy to decompose."


## TODO https://social.coop/@mike_hales/107093275450219170

mike<sub>hales</sub> - @nicksellen 
I guess I feel that 'anything goes' openness of exchange is probably not productive, and some moderately sharp focus is called for, to justify infrastructure effort. A contradiction! But there's a difference between a movement organisation and a free market in conversations loosely organised by 'shared values'. Social media can do THAT (at a cost).

OK, maybe something of the vanguardist revolutionary speaking here 🙄 O dear, I better examine my soul 
@bhaugen @organizingInFedi​

Communities of praxis.


## TODO Indieweb metaverse

https://youtu.be/PvUenrIIECU


## TODO MediArXiv Preprints | Admins, Mods, and Benevolent Dictators for Life: The Implicit Feudalism of Online Communities

https://mediarxiv.org/sf432/


## TODO IndieWeb Thoughts: POSSE ⁑ Wesley’s Notebook

https://notebook.wesleyac.com/indieweb-thoughts-posse/


## TODO Our society is troubled. Beware those who blame it all on big tech

I agree with this mostly. Big tech has huge problems, but you can't lay blame for all of societies ills at its door. https://www.theguardian.com/commentisfree/2021/oct/24/society-blame-big-tech-online-regulation?CMP=Share_AndroidApp_Other


## TODO Isn’t it good, Swedish plywood: the miraculous eco-town with a 20-storey wooden skyscraper

https://www.theguardian.com/artanddesign/2021/oct/14/skelleftea-swedish-plywood-eco-town-20-storey-wooden-skyscraper-worlds-tallest?CMP=Share_AndroidApp_Other


## TODO Austerity in England linked to more than 50,000 extra deaths in five years

https://www.theguardian.com/society/2021/oct/14/austerity-in-england-linked-to-more-than-50000-extra-deaths-in-five-years?CMP=Share_AndroidApp_Other


## TODO https://social.coop/@mike_hales/107093275450219170

mike<sub>hales</sub> - @nicksellen 
I guess I feel that 'anything goes' openness of exchange is probably not productive, and some moderately sharp focus is called for, to justify infrastructure effort. A contradiction! But there's a difference between a movement organisation and a free market in conversations loosely organised by 'shared values'. Social media can do THAT (at a cost).

OK, maybe something of the vanguardist revolutionary speaking here 🙄 O dear, I better examine my soul 
@bhaugen @organizingInFedi​

Communities of praxis.


## TODO TiddlyWiki on Twitter: "A demo of dragging tiddlers from one wiki another on an iPhone running iOS 15 https://t.co/C8qD4GWoSK" / Twitter

https://mobile.twitter.com/TiddlyWiki/status/1447576045289345026


## TODO MediArXiv Preprints | Admins, Mods, and Benevolent Dictators for Life: The Implicit Feudalism of Online Communities

https://mediarxiv.org/sf432/


## TODO IndieWeb Thoughts: POSSE ⁑ Wesley’s Notebook

https://notebook.wesleyac.com/indieweb-thoughts-posse/


## TODO Indieweb metaverse

https://youtu.be/PvUenrIIECU


## TODO Digital gardening is more like care and maintenance. Less about quickness and performance. Part of a slower Internet


## TODO "https://youtu.be/EbTleMei3Is is one of the better NFT-related videos I've seen"


## TODO Sloan

https://platforms.fyi/


## TODO The big idea: Is democracy up to the task of climate change?

https://www.theguardian.com/books/2021/nov/01/the-big-idea-is-democracy-up-to-the-task-of-climate-change?CMP=Share_AndroidApp_Other


## TODO Britain’s early handling of the coronavirus pandemic was one of the worst public health failures in UK history, with ministers and scientists taking a “fatalistic” approach that exacerbated the death toll, a landmark inquiry has found.  “Groupthink”, evidence of British exceptionalism and a deliberately “slow and gradualist” approach meant the UK fared “significantly worse” than other countries, according to the 151-page “Coronavirus: lessons learned to date” report led by two former Conservative ministers


## TODO web 3.0

https://matrix-client.matrix.org/_matrix/media/r0/download/matrix.org/2021-11-01_XUtxcpsPVjQociNg

-   [Web3 is not decentralization - Articles - InvisibleUp](https://invisibleup.com/articles/38/)

