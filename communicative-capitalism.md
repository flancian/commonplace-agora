# Communicative capitalism

[[Jodi Dean]].

> Communicative capitalism refers to the form of late capitalism in which values heralded as central to democracy materialize in networked communications technologies. 
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> Ideals of access, inclusion, discussion and participation are realized through expansions, intensifications and interconnections of global telecommunications. In communicative capitalism, capitalist productivity derives from its expropriation and exploitation of communicative processes.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

<!--quoteend-->

> This does not mean that information technologies have replaced manufacturing; in fact, they drive a wide variety of mining, chemical, and biotechnological industries. Nor does it mean that networked computing has enhanced productivity outside the production of networked computing itself. Rather, it means that capitalism has subsumed communication such that communication does not provide a critical outside.
> 
> &#x2013; [[Communicative Capitalism and Class Struggle]]

