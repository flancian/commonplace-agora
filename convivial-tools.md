# Convivial tools

Described by [[Ivan Illich]] in his book [[Tools for Conviviality]].

My initial thoughts: sounds good.

The most notable critique I have come across so far is Bookchin (via Evgeny Morozov): "It didn’t make sense to speak of “convivial tools,” he argued, without taking a close look at the political and social structures in which they were embedded"

In Free, Fair and Alive they extend the notion to technologies, infrastructures and processes for provisioning.


## Agency and interdependence

In FFA they suggest that convivial tools are about that sweet sweet mix of individual agency with communal interdependence.

> Using convivial tools is about **enhancing our individual freedom while enriching our relationships and interdependence**
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> In Tools for Conviviality, Ivan Illich describes **a kind of tool that is shared in common, and which expands personal creative freedom and communal interdependence**. 
> 
> &#x2013; [[Building a Second Subconscious]]


## Openness and access

> A tool is convivial if people have access to the design and knowledge needed to create it; if it allows creative adpatation to one's own circumstances and if it is appropriate in the specific local context.  (Are suitable materials and skills available?  Is it compatible with the local landscape and culture?)
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> In our times, open source tools and technologies are convivial tools with great potential for Provisioning through Commons because users can determine how they will be used.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> convivial tools are open-ended systems that anyone can use and adapt for their own purposes
> 
> &#x2013; [[Free, Fair and Alive]]


## Characteristics of convivial tools

> The characteristics of convivial tools are those which enhance the user's capability to work with independent efficiency. A fairly comprehensive list might include the following characteristics:
> 
> -   Usability
> -   Intuitive Use
> -   Reliability
> -   Reparability
> -   Durability
> -   Ergonomics
> -   Simplicity
> -   Robustness
> -   Open access
> -   Modularity
> -   Recyclability
> -   Environmental friendliness
> -   Social friendliness
> -   Promotion of autonomy
> -   Full service
> -   Eutrapelia
> 
> &#x2013; [1 The Characteristics of Convivial Tools - Convivial Tools](http://conviviality.ouvaton.org/article.php3?id_article=2):

See the [[Matrix of Convivial Technology]].


## Examples of convivial tools

-   [[Permaculture]] (from FFA)
-   [[Agroecology]] (from FFA)
-   [[Libre software]] (from FFA)

> Bicycles, libraries, and sewing machines can all be convivial tools.
> 
> &#x2013; [[Building a Second Subconscious]]

-   bicycles
-   libraries
-   sewing machines


## Convivial tools and commoning

[[Use Convivial Tools]] is one of the patterns for commoning in [[Free, Fair and Alive]].  In the [[Provisioning Through Commons]] group of patterns.


## Criticisms

The most notable critique I have come across so far is from [[Murray Bookchin]] (via [[Evgeny Morozov]]):

> It didn’t make sense to speak of “convivial tools,” he argued, without taking a close look at the political and social structures in which they were embedded

See [[Comparing liberatory technology and convivial tools]].


## Comments

> He didn't mean tools in the current software sense: discrete apps that perform defined tech functions. He meant something more like 'institutions' - public or popular healthcare, public or popular education, civic decision making, etc etc. Such practices are typically carried out thro some alliance of the State and professionalised elites. His critique thus has a similar basis to Bookchin's
> 
> &#x2013; https://social.coop/@mike_hales/107277225698520917

<!--quoteend-->

> @mike<sub>hales</sub> I read the book, and actually understood it that it **could** apply to tools as simple as a hammer (and therefore software tools too) and that we can **also** look at institutions as a form of "tool" too. This is nice in that we can apply a more coherent philosophy across the whole range.
> 
> &#x2013; https://social.coop/@nicksellen/107279966680322155

<!--quoteend-->

> Yes I think that's right Nick. the concept can stretch over that scope. I think the important thing not to lose from Illich is that social relations, power relations, class relations are part of the construction and positioning and mobilising of a tool, small-and artefactual or massive-and-institutional?
> 
> &#x2013; https://social.coop/@mike_hales/107280956585380392

<!--quoteend-->

> Tools should not attempt to control humans by prescribing narrow ways of doing things. Software should not be burdened with encryption and barriers to repair. Convivial tools are designed to unleash personal creativity and autonomy.
> 
> &#x2013; [[Free, Fair and Alive]]

