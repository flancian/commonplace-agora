# Platform Socialism and Web3

A
: [[podcast]]

URL
: https://theblockchainsocialist.com/platform-socialism-and-web3/

Series
: [[The Blockchain Socialist]]

Featuring
: [[James Muldoon]]

Great stuff (the platform socialism more of interest to me than the web3, but the chat around that is interesting too.)

[[Platform socialism]].

Mentiones problems with the [[Center for Humane Technology]] approach to [[big tech]].  (What he calls the techno-humanism approach I think?)  Also the problems with the anti-monopoly approach to big tech.

[[Community washing]] - handy phrase for the bullshit Uber, Airbnb do/did around pretending they facilitate community relations.

The left needs to be better at harnessing tech for inventing the future.  (I reckon [[solarpunk]] does this a bit).

And to avoid the classic trope of critiquing but not inventing.

Likes [[Guild socialism]]. Like [[G. D. H. Cole]].  Critical of [[Fabian Society]] (as too statist I believe?).  See Fediverse as related to guild socialism.

Muldoon remains skeptical of the [[DAO]] space.  Though the message of interesting experiments in governance comes up again.

Politics first, technology second.  Avoid [[techno-solutionism]].

Flexibility does not have to mean precarity.

Apparently [[Hannah Arendt]] favoured a federal model of political organising.

Favours platforms that are public goods and commons.

