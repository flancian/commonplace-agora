# Cloud software has benefits

> Cloud software is in many regards superior to “old-fashioned” software: it offers collaborative, always-up-to-date applications, accessible from anywhere in the world. We no longer worry about what software version we are running, or what machine a file lives on.  
> 
> &#x2013; [Local-first software: You own your data, in spite of the cloud](https://www.inkandswitch.com/local-first.html) 

BUT [[Cloud apps are problematic]]. 

