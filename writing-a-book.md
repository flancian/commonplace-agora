# writing a book

[[Writing]].

Nadia Eghbal, someone who obviously loves writing, [writes about writing a book](https://nayafia.substack.com/p/20-mannequins).  It sounds less&#x2026; enjoyable than you might think.

> What I hated most about this past year was feeling unable to seriously think about anything besides this one thing. Everything I read or talked about was in service to the thing. There was nothing but the thing.

<!--quoteend-->

> Afterwards, I expected to feel a satisfying sense of completion, but mostly I just felt relieved. I didn't think of it as having finished a manuscript so much as having expelled a virus from my body. 

