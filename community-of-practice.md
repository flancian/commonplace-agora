# Community of practice

> a group of people who "share a concern or a passion for something they do and learn how to do it better as they interact regularly".
> 
> &#x2013; [Community of practice - Wikipedia](https://en.wikipedia.org/wiki/Community_of_practice) 

I like the stuff on moving from network to community of practice in [[Margaret Wheatley]] and Deborah Frieze's paper - https://www.margaretwheatley.com/articles/

