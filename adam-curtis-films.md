# Adam Curtis films

I've definitely seen The Power of Nightmares.  And [[Century of the Self]].  I watched [[Machines of Loving Grace]] more recently.  

I think I started both Bitter Lake and Hypernormalisation but I genuinely can't remember what they're about.. I wonder if I finished them?  

I think some of his early programs are great, he pretty much created his own genre, but I started to think of them less as documentaries and more as visual essays, in that they are very much his own opinion, woven together very compellingly.  (Maybe that's what all documentaries are though I guess?)  

The parody of Adam Curtis is a bit bitchy, but also bang on the nose: https://www.youtube.com/watch?v=x1bX3F7uTrg

They have good soundtracks, can't deny that.
