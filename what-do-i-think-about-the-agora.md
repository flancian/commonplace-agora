# What do I think about the Agora?

[[Agora]] as in https://anagora.org

tl;dr: It's rad.


## Longer version

I like the connecting of [[digital garden]]s.  To build a [[knowledge commons]] together while retaining [[agency]] and [[autonomy]].

I'm inspired by the way in which FedWiki is described in the section in [[Free, Fair and Alive]] on  [[A Platform Designed for Collaboration: Federated Wiki]].  That is, [[Combining multiple personal wikis provisions a better commons than one central wiki]].

I feel Agora achieves this same principle, but in a different way. ([[What are the similarities and differences between Agora and FedWiki?]])  It appears to allow for a greater plurality of **ways of making** personal wikis (as long as you can get your notes in to markdown format in a git repo, you're good), resulting, I think, in even wider scope for provisioning the commons.

That said, I would still like to see explorations in [[peer-to-peer]] connection of gardens - something perhaps [[IndieWeb]] and webmention based.

But even if that were so, I think there is an important place for an aggregator such as Agora.  You can see Agora as an easy[^fn:1] way to get your garden on the web, for those without the time or inclination to learn how to publish their own website, while still retaining complete ownership of the data.  And it also connects you to other people out-of-the-box - you see what people have written on topics similar to you.  In some sense I see Agora as doing for indie gardens what [[micro.blog]] does for indie streams.

I think Agora could position ourselves as a left-leaning tool for [[collective knowledge management]] for [[communities of practice]] (or perhaps [[communities of praxis]]?) There's plenty of VC-backed companies doing the 'multiplayer' knowledge thing but they will never be [[liberatory technology]] if capital, growth and profit is their foundation. 


## <span class="timestamp-wrapper"><span class="timestamp">[2021-11-28 Sun]</span></span>

I like [[collective knowledge management]] tools such as [[Anagora]], as when looking at a topic it gives me a subjective view of what my friends/community of practice think about it.  As a fallback after that I can see what [[Wikipedia]] attempts to shake out as the objective view.

It would be good for other [[Agora]] instances to come online, to be able to quickly get a peek at multiple different groups' subjective view.

I wonder if one may in some sense see it as similar to [[liquid democracy]].  On any topic, I could choose who I am most interested to see definitions from.  I 'delegate' the definition to them, if I've not had chance to do my own yet.  I would probably start in my local instance of Agora.  Then perhaps my instance federates with others.  At the end of it all, if noone has defined it yet, see what Wikipedia says.

(also on matrix [here](https://matrix.to/#/!WhilafaLxfJNoigHCj:matrix.org/$VOXFdebtMOD4wrEZlZp6I_zquOPdNo82fC3eCZSlUpc?via=matrix.org&via=fairydust.space&via=asra.gr))


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Well, easy-ish. You can make it in a program such as Obsidian, Foam, logseq, org, or your favourite text editor of markdown, and you need to get it in to a public git repository.
