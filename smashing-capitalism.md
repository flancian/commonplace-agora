# Smashing capitalism

> This is the classic revolutionary road to socialism. It assumes a seizure of power by a cadre of radicals, typically achieved by violent means, but also potentially through elections. Its defining element is not so much reliance on revolution, but what happens after — that it suppresses the counterrevolution by force and then rapidly builds new socialist institutions.
> 
> &#x2013; A Blueprint for Socialism in the Twenty-First Century


## Pros

> periodically there will be intense capitalist economic crises in which the system becomes vulnerable and ruptures become possible.
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> The idea that capitalism can be rendered a benign social order in which ordinary people can live flourishing, meaningful lives is ultimately an illusion because, at its core, capitalism is unreformable.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright


## Cons

> It is one thing to burn down old institutions; it is quite another to build emancipatory new institutions from the ashes.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> Give up the fantasy of smashing capitalism. Capitalism is not smashable, at least if you really want to construct an emancipatory future.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

Why not?   Idea seems to be that defending such a revolution would be necessarily violent.  Capitalism won't take it lying down.  See: [[To Posterity - Bertolt Brecht]]. 

A pro-smash article: [Goodbye Revolution? - Regeneration Magazine](https://regenerationmag.org/goodbye-revolution/) 

