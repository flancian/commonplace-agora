# post-structuralism

![[images/rhizome-network.png]]

Meanings are unstable, changing and contextual.

Decentres traditional authorities.

Post-structuralism sees 'reality' as being much more fragmented , diverse, tenuous and culture-specific than does structuralism.

Relates to  [[postmodernism]].  But there is interesting distinction in the definition of postmodernism?- of it not so much being an ideology of some kind, and more a description of the state of the world.  And people like [[Deleuze & Guattari]], [[Baudrillard]], are describing that state of the world and what it means, but they're not "postmodernists".  They're best described as post-structuralists ([Revolutionary Left Radio: Post-Structuralism, Postmodernism, and&#x2026; Metamodernism?](https://revolutionaryleftradio.libsyn.com/post-structuralism-postmodernism-and-metamodernism)).

Potentially a bit of a linkage between post-structuralism and [[anarchism]] and [[horizontalism]] ([Revolutionary Left Radio: Post-Structuralism, Postmodernism, and&#x2026; Metamodernism?](https://revolutionaryleftradio.libsyn.com/post-structuralism-postmodernism-and-metamodernism))

And maybe some tension between post-structuralism and [[Marxism]], in its presentation of a scientific approach to history ([Revolutionary Left Radio: Post-Structuralism, Postmodernism, and&#x2026; Metamodernism?](https://revolutionaryleftradio.libsyn.com/post-structuralism-postmodernism-and-metamodernism)).

> absence or refusal of a general political theory has led some critics to accuse the post-structuralists of a self-defeating normative relativism or outright nihilism.
> 
> &#x2013; [Is Post-Structuralist Political Theory Anarchist? | The Anarchist Library](https://theanarchistlibrary.org/library/todd-may-is-post-structuralist-political-theory-anarchist) 

<!--quoteend-->

> The critique of representation is a central theme of the post-structuralists; Deleuze once told Foucault “you were the first&#x2026;to teach us something absolutely fundamental: the indignity of speaking for others.”
> 
> &#x2013; [Is Post-Structuralist Political Theory Anarchist? | The Anarchist Library](https://theanarchistlibrary.org/library/todd-may-is-post-structuralist-political-theory-anarchist) 

