# Attention economy

When information is abundant, attention is the scarce resource.

That podcast that I listened to recently, from the Centre for Humane Tech.

Where they talked about all the problems inherent in social media and how it is quite intentionally capturing your attention.  The analysis of how distraction affects people was all good, but they completely failed to mention the profit motive or capitalism though, which was a bit surprising.

I particularly liked idea from it, which was that we could try to reduce our predictability.  Predictability being profitable to the companies when it comes to selling advertising or etc to others.  If they can predict our behaviour, that's more lucrative to someone who wants to influence behaviour.  I think they hinted that the Taylorised social actions were very good for predicting your behaviour.  For example, 300 likes on Facebook gives a very good clue as to what you're interested in in life.  And breaking our thoughts down into smaller and smaller nuggets makes them more machine processable.  Seems surprising but apparently that's what the research said.  

So I felt that by then not reducing our socialising to such minimal actions, we could reduce our predictability.  If I write a longer form blog post, it's hard to analyse.  If I reply to people, rather than clicking a button to provide a piece of metadata, that's less predictable and analysable.


## A counterpoint

-   https://twitter.com/vgr/status/1047925106423603200

