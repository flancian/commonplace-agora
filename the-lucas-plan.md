# The Lucas Plan

I really like the Lucas Plan.  An example of how reskilling of skilled workers could be possible and could all come from the workers themselves, not a top-down decree.  It is a positive example of how reskilling could work under the Green New Deal.

-   rampant Taylorism would make reorienting factories more difficult
    -   machines and people are so specialised to one product, they don't have general skills needed to be repurposed?


## overview

-   Workers plan from 1976 at Lucas Aerospace on how to avoid job losses by producing things useful to society
-   "the Combine argued that state support would be better put to developing products that society needed"
-   It included ideas of things the factory could make to reskill workers
-   these were all pretty compatible with current energy transition requirements
-   heat pumps, solar cell technology, wind turbines and fuel cell technology


## useful links

-   [The Plan](http://theplandocumentary.com/) film
-   [Story of the Lucas Plan](http://lucasplan.org.uk/story-of-the-lucas-plan/)

