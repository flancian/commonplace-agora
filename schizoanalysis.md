# schizoanalysis

> schizoanalysis: a narration of the history of desire, as a productive and impersonal world-creating force.
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 

<!--quoteend-->

> Enter the schizoanalyst, somewhere between a psychoanalyst and a political agitator. The role of this figure was to decode the unconscious processes of desire and identify their revolutionary potential.
> 
> &#x2013; [[A creative multiplicity: the philosophy of Deleuze and Guattari]] 

