# Marking up wiki pages with microformats

Why mark them up with microformats?

So others could parse out the relevant bits of the pages if they wanted (title, content) in order to bookmark / leave replies.

I would have thought h-entry, though from the description of h-entry (https://indieweb.org/h-entry) it seems to be more for blog posts / episodic / time-based content.

Tantek suggested in the IndieWeb chat that h-entry is fine for a wiki page: https://chat.indieweb.org/dev/2021-10-09#t1633797644753700

and linked to some really good discussion on IndieWeb wikis in general: https://indieweb.org/wiki-projects#Brainstorming

So yes I will for now mark my wiki pages up with:

-   h-entry
    -   p-name (for the title)
    -   e-content (for the content)
    -   dt-published and dt-updated (for planted and last tended in digital garden lingo)

