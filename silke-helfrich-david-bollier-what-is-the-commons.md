# Silke Helfrich & David Bollier: What Is The Commons?

URL
: https://voices-of-the-commons.sounder.fm/episode/silke-helfrich-david-bollier-what-is-the-common


Publisher
: [[Guerrilla Media Collective]]


Guests
: [[David Bollier]], [[Silke Helfrich]]


Topics
: [[Commons]]

Nice little podcast on the commons.  What is it, what are [[Patterns of Commoning]], how do you inculcate a commoner mindset.

