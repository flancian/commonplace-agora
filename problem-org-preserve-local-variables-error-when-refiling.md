# Problem: org-preserve-local-variables error when refiling

Whenever I get an `org-preserve-local-variables` error (usually when refiling an item, and usually after a spacemacs update), this is the way to solve it:

```bash
cd ~/.emacs.d/elpa
find org*/*.elc -print0 | xargs -0 rm
```

NOTE: where the elpa directory might be in a different location depending which version/branch you’re on.  e.g. `~/.emacs.d/elpa/26.3/develop/`.

And then run `M-x spacemacs/recompile-elpa`.

Basically it's removing all of the compiled org files and then compiling them again.

source: https://github.com/syl20bnr/spacemacs/issues/11801

