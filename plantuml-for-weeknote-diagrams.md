# PlantUML for weeknote diagrams

Works via inline source blocks in org-mode.  You can evaluate them and turn them into png/svg with org-babel evaluation.

I find the indentation of the source doesn't work too great in source blocks.

[Deployment diagrams](https://plantuml.com/deployment-diagram) seem like the most expressive diagram type for what I want.

It works pretty good, but embedding images is a bit limited.  https://crashedmind.github.io/PlantUMLHitchhikersGuide/PlantUMLSpriteLibraries/plantuml_sprites.html

I actually kind of like the style - it's just kind of goofy.

