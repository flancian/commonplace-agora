# Recipes

Keeping track of some recipes.

This could definitely **not** be public.  But it's handy to have access to it on my phone sometimes, so&#x2026;

-   [[Cheeseless pasta bake]]
-   [[Creamy vegetable vegan risotto]]
-   [[Cauliflower macaroni cheese]]
-   [[Spinach and mushroom risotto]]
-   [[Spanish omelette]]
-   [[Peanut butter cookies]]
-   [[Thai green curry]]

