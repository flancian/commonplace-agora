# GTD horizons of focus

-   [The 6 Horizons of Focus](https://gettingthingsdone.com/2011/01/the-6-horizons-of-focus/)

-   Ground: Calendar/actions
-   Horizon 1: Projects
-   Horizon 2: Areas of focus and accountability
-   Horizon 3: One- to two-year goals and objectives
-   Horizon 4: Three- to five-year vision
-   Horizon 5: Purpose and principles

Is this the same as altitudes?

> your commitments are reviewed at different levels ("altitudes") giving you higher and longer-term perspectives on your life. 
> 
> &#x2013; [How I use Emacs and Org-mode to implement GTD](http://members.optusnet.com.au/~charles57/GTD/gtd_workflow.html)

