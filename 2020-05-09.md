# 2020-05-09



## Digital psychogeography

Thanks to a couple of recent [[thinking prompts]] from [[Panda]] and [[Graham]], [[psychogeography]] was on my mind.  And lazily putting the word 'digital' in front of another always seems to be on my mind&#x2026;

&#x2026;So I have been thinking about the idea of [[digital psychogeography]], [[drifting]] through [[hypertext]].  Perhaps as a means of rediscovering lost history, and maybe as a way to break out of self- and algorithmically-prescribed [[filter bubbles]].

And well what do you know, there's already a [rich](https://www.theparisreview.org/blog/2013/10/17/in-praise-of-the-flaneur/) [vein](https://www.nytimes.com/2012/02/05/opinion/sunday/the-death-of-the-cyberflaneur.html) of [articles](https://www.theatlantic.com/technology/archive/2012/02/the-life-of-the-cyberfl-neur/252687/) out there on the idea of being a [[cyberflâneur]].

(When I think cyberflâneur, I think '[[Kicks Condor]]').


## Wiki links are my tags

For a brief period, I made a concerted effort to tag and categorise my posts.  It seemed like a means of picking out themes in my writing, maybe discovery for me and perhaps also for people visiting my site.

But it always felt a bit weird and disconnected for me - I've written something, now I have to think about where it fits in to some abstract notion of my content. And it's not fun.  It's just a thing that sits there staring at you in WordPress when you make a post.  Hey you, eat your greens, and tag your posts.  (If you don't give it a category, you get the 'Uncategorized' label of shame). So I kind of gave up on it again.  

But as I've started heavily hyperlinking stream posts to my wiki, I've realised that pretty much my [[wiki links are my tags]].   I'm just tagging as I'm linking things together.   And that's fun! (for me anyway&#x2026;). 

![[Wiki_links_are_my_tags/2020-05-09_11-18-18_screenshot.png]]

I've styled the wiki links diferent from other hyperlinks.  I quite like it personally, but part of me wonders if it's just visual noise.  I'd quite like if I could get WordPress to pick up those links and set them as tags, mainly just as a easy way to have them separate from the main body of the post.


## [[Being, having, appearing]]

> An earlier stage in the [[economy's domination of social life]] entailed an obvious downgrading of _being_ into _having_ that left its stamp on all human endeavour.  The present stage, in which social life is completely taken over by the accumulated products of the economy, entails a generalized shift from _having_ to appearing: all effective "having" must now derive its immediate prestige and its ultimate raison d'etre from appearances.
> 
> &#x2013; [[The Society of the Spectacle]]


## [[Enclosure of the gardens]]

I was thinking - [[digital gardens]] are an amazing, unique feature that the big [[silos]] don't really have.

The [[stream]] stuff, to some degree we're playing catchup.  But other than a photo album here, a pin there, an 'on this day' here, there's very little mainstream investment in letting you turn your stream into a commonplace books, your knowledge base over time.

There's big platforms that do note-taking things, of course, like Evernote, etc, but the big companies in charge of the streams, I thought, don't seem to see the link between what goes in your stream being the seeds to grow a garden over time.  You'd think they'd be all over that.

But&#x2026; of course, the silos do have a digital garden for everyone.  It's just it is [[enclosed]].  You don't have access to your own garden.  They are reaping the benefits of the seeds you are sowing, collecting, connecting your dots, and then selling your fruit. [[People farming]], as Aral Balkan would have it.


## All flow and no stock

> And the real magic trick is to put them both together. To keep the ball bouncing with your [[flow]]—to maintain that open channel of communication—while you work on some kick-ass [[stock]] in the background. Sacrifice neither. The hybrid strategy.
> 
> &#x2013; [Stock and flow / Snarkmarket](http://snarkmarket.com/2010/4890) 

Twitter and Facebook are all flow and no stock.


## [[Water from the stream]]

I water my [[garden]] to help it grow.  I get the water from the [[stream]].

