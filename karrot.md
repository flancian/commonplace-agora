# Karrot

Platform to facilitate food sharing (and more recently sharing of other things too I think).

https://karrot.world/


## How they make their software

-   They tried a [[design sprint]] approach recently - [We are designing governance features and we need your input!](https://community.foodsaving.world/t/we-are-designing-governance-features-and-we-need-your-input/529)
-   [Karrot: a case-study of a digital tool that supports grassroots-movements in saving and sharing resources](https://community.foodsaving.world/t/karrot-at-the-nordichi-conference/562)

