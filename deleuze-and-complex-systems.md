# Deleuze and complex systems

> Deleuze’s philosophy appears to fit neatly with interpretations of the world as complex systems, especially in ecology (Robinson and Tansey, 2006), in urban planning (Batty, 1969, 1994, 2005) and in economic spheres (Noell, 2007).
> 
> &#x2013; [[How Could we Study Climate-Related Social Innovation? Applying Deleuzean Philosophy to the Transition Towns]]
