# Policing bill

-   https://bills.parliament.uk/bills/2839

[[I strongly dislike the policing bill]].

> Tucked away in the government’s 300-page police, crime, sentencing and courts bill, are various clauses which will have serious implications for the right to protest. The bill seeks to quietly criminalise “serious annoyance”, increase police powers to restrict protests, and give the home secretary discretion over what types of protests are allowed.
> 
> &#x2013; [The police bill is not about law and order – it’s about state control | Joshu&#x2026;](https://www.theguardian.com/commentisfree/2021/aug/09/police-bill-not-law-order-state-control-erosion-freedom) 

<!--quoteend-->

> A new policing bill that will be debated this week risks deepening racial and gender disparities in the justice system while forcing professionals to betray the trust of vulnerable people, hundreds of experts and a report have warned.
> 
> &#x2013; [Policing bill will deepen racial and gender disparities, say experts | Police&#x2026;](https://www.theguardian.com/uk-news/2021/sep/13/policing-bill-will-deepen-racial-and-gender-disparities-say-experts) 

