# adversarial interoperability

> “That’s when you create a new product or service that plugs into the existing ones without the permission of the companies that make them,” writes Cory Doctorow, special advisor to the Electronic Frontier Foundation. “Think of third-party printer ink, alternative app stores, or independent repair shops that use compatible parts from rival manufacturers to fix your car or your phone or your tractor.” Without adversarial interoperability, users have limited [[agency]] and innovation is stifled.
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/) 

