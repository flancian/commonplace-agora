# Smart cities

-   Seoul city machine
    -   Saw it at the Centre for Chinese Contemporary Art
    -   Shows the tension between smart city looking after us vs controlling us
    -   https://vimeo.com/312092226


## Links

-   [Toronto swaps Google-backed, not-so-smart city plans for people-centred vision](https://www.theguardian.com/world/2021/mar/12/toronto-canada-quayside-urban-centre)

