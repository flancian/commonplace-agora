# Ubuntu 20.04 server setup at GreenHost

I decided to move my server from Digital Ocean to [[GreenHost]].  I'd been on cloudvault.me, but that disappeared all of a sudden&#x2026; so I quickly moved it to a droplet on Digital Ocean.  But I like GreenHost for their impeccable environmental creds and privacy ethos.

I've generally been using Ubuntu server, which GreenHost has available for their VPSes, so went with Ubuntu 20.04.  (The options are Debian 10, Ubuntu 18.04 and 20.04, and CentOS 7.)

A droplet at Digital Ocean is $5, 1 core, 1GB, 25GB, 1000GB transfer.
GreenHost: 1 core, 1GiB, 5GiB, 5.75 euro. (6.50 euro if you go 10GiB disk), 5TB data limit.

You can turn on server backups - not 100% sure how you restore at GreenHost yet.

Here's my notes on basic server setup for my two sites (my WordPress IndieWeb stream, and my static org-roam wiki): [[Ubuntu 20.04 basic server setup]]

