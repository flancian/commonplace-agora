# Fixing at Rhino Camp Refugee Settlement

A
: [[podcast]]

URL
: https://therestartproject.org/podcast/refugee-repair/

Series
: [[Restart Radio]]

Featuring
: [[Mathew Lubari]]

[[Repair]] in the [[Rhino Camp Refugee Settlement]], [[Uganda]]. 

Fixing a phone can be vital for connecting to family, friends, and other services.

Fixing a bicycle can help you get around.

Fixing a solar lantern can protect your from scorpion bites.

