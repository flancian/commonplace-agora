# Proprietary empires built on free software

> Existing arguments about how large tech companies freely use open-source software as a foundation to build their proprietary empires must also be supplemented with the ways in which free – and waged – labour are brought into the ambit of companies via things like open-source frameworks
> 
> &#x2013; [Data, Compute, Labour | Ada Lovelace Institute](https://www.adalovelaceinstitute.org/data-compute-labour/) 

