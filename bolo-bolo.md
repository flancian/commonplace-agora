# bolo bolo

A
: [[book]]

Author
: [[Hans Widmer]]

URL
: https://theanarchistlibrary.org/library/p-m-bolo-bolo


## What's it about

> bolo'bolo is a book about an anarchist utopia, the name of the utopia itself and the plural of that utopia's organizational unit—the bolo. &#x2026; Bolo\`bolo is also a plan for a transformation from our current state, the [[planetary work machine]], to another social organization mode based on local organization and a microclimate of cultures that form the unit of social cohesion.
> 
> &#x2013; [bolo'bolo](https://www.akpress.org/bolobolo30thanniversary.html) 

<!--quoteend-->

> The basic questions are still the same: how can we find a way of life that is really sustainable, ecologically and socially

<!--quoteend-->

> Ideas already presented in bolo’bolo are part of a larger common sense now. Degrowth, the commons, transition towns, cooperatives, climate justice, are all aspects of a global way out of capitalism

<!--quoteend-->

> [[Transition town]]s are emerging everywhere. Transition states, territories, provinces or regions could be the next step, up to a planetary transition “cooperative” of democratic states (asa

It seems to be in two parts - first diagnosis of the problem; second, a proposed solution.


## [[Planetary Work Machine]]

The description of the planetary work machine is fairly good.  I mean it's not going as in-depth as Marx's analysis of capital right, but it's a decent summary of present day economic woes.

> It is a Planetary Machine: it eats in Africa, digests in Asia, and shits in Europe. 

There's plenty of good stuff about the adapation and difficulty of changing capitalism. How capitalism subsumes attempts at anti-capitalism.

> We are all parts of the Planetary Work Machine — we are the machine. We represent it against each other. [&#x2026;] Those who try to get out of the Machine fulfill the function of picturesque “outsiders” (bums, hippies, yogis). **As long as the Machine exists, we’re inside it.** [&#x2026;] If you try to retreat to a “deserted” valley in order to live quietly on a bit of subsistence farming, you can be sure you’ll be found by a tax collector, somebody working for the local draft board, or by the police. With its tentacles, the Machine can reach virtually every place on this planet within just a few hours. Not even in the remotest parts of the Gobi Desert can you be assured of an unobserved shit.

**As long as the machine exists, we're inside it.**  Aye.  Looks like if favours a [[Revolution]] of some kind.  [[Smashing capitalism]] or [[Escaping capitalism]]?

> The Machine doesn’t even need anymore a special ruling class to maintain its power. Private capitalists, the bourgeoisie, aristocrats, all the chiefs are mere left-overs, without any decisive influence on the material execution of power. The machine can do without capitalists and owners, as the examples of the socialist states and state enterprises in the West demonstrate. These relatively rare fat cats are not the real problem. The truly oppressive organs of the Machine are all controlled by just other workers: cops, soldiers, bureaucrats. We’re always confronted with convenient metamorphoses of our own kind.

<!--quoteend-->

> lot of thus “privileged” A workers flee to the countryside, take refuge in sects, try to cheat the Machine with magic, hypnosis, heroin, oriental religions or other illusions of secret power. Desparately they try to get some structure, meaning, and sense back into their lives. But sooner or later the Machine catches its refugees and transforms exactly their forms of rebellion into a new impetus of its own development. “Sense” soon means business sense


## Revolution

> Let’s not fool ourselves. Even if we mobilize all our spirit of sacrifice, all of our courage, we can achieve not a thing. The Machine is perfectly equipped against political kamikazes, as the fate of the Red Army Faction, the Red Brigades, the Monteneros and others has shown. It can coexist with armed resistance, even transform that energy into a motor for its own perfection. Our attitude isn’t a moral problem, not for us, much less for the Machine

<!--quoteend-->

> The experiences of Eastern Europe show that the concept of armed revolutionary struggle is out-dated, ridiculous and unnecessary, at least in industrially advanced areas. Social mass sabotage is much more effective

[[Social mass sabotage]] rather than armed revolution.  What's social mass sabotage?


## A, B, C

Not 100% sure about these A, B and C classes of workers and 'deals'.  Will need to reread that.

> Social stratification is used for the maintenance of the whole system


## Work

> We try not to face that strange void, but in unoccupied moments between job and consuming, while we are waiting, we realize that time just isn’t ours

<!--quoteend-->

> In the morning we think of the evening, during the week we dream of the week-end, we sustain everyday life by planning the next vacation from it.

<!--quoteend-->

> the terrorism of waged work


## What's the solution?

Skimming through, the solution kind of looks like speculative fiction.  Not that that means it won't have good ideas.

I've not had chance to read the [[asa'pili]] stuff yet, that seems to be where the bulk of the ideas are.


## Misc

> If you dream alone, it’s just a dream. If you dream together, it’s a reality

<!--quoteend-->

> But many of these futures (or “futuribles”, as the French say) are not very appetizing: they stink of renunciation, moralism, new labors, toilsome rethinking, modesty and self-limitation. Of course there are limits, but why should they be limits of pleasure and adventure? Why are most alternativists only talking about new responsibilities and almost never about new possibilities
