# Project Cybersyn

Project Cybersyn was a system that utilised [[cybernetics]] as a part of socio-economic planning in [[socialist]] Chile.

> 1970s Chilean computer network for economic management
> 
> &#x2013; [[Cybernetic Revolutionaries]]

I find Cybersyn really interesting because it is an example of an attempt to use technology for leftist, liberatory ends.  I see it as an example of  [[liberatory technology]].

That said, I'm not massively knowledgeable about it.  I'm on and off reading through [[Eden Medina's book]] about it.  I had originally thought of it as an attempt at centralised planning, but it's more subtle than that I think.

> Some Cybersyn technologists also tried to use Project Cybersyn as a vehicle for increasing worker participation in economic management and proposed having workers collaborate with Chilean operations research scientists
> 
> &#x2013; [[Cybernetic Revolutionaries]]


### Change structure to change systemic behaviour

> [The] sensible course for the manager is not to try to change the system’s internal behaviour… but to change its structure – so that its natural systemic behavior becomes different.
> 
> &#x2013; [[Cybernetic Revolutionaries]]


### Technology for structural transformation

> Computers did not need to reinforce existing management hierarchies and procedures; instead, they could bring about structural transformation within a company and help it form new communications channels, generate and exchange information dynamically. …. [Beer’s] focus was not on creating more advanced machines but rather on using existing technologies to develop more advanced systems of organisation.’
> 
> &#x2013; [[Cybernetic Revolutionaries]]


### [[Viable System Model]]     :cybernetics:systems_science:


### Liberty Machine     :cybernetics:

‘a sociotechnical system that functioned as a disseminated network, not a hierarchy’

‘treated information, not authority, as the basis for action’

‘prevented top-down tynranny by creating a distributed network of shared information’.


## Critique

One critique is that it centralised control.  People point to the control room as a centralised point of control.

But I feel like that misses the point of the [[Viable system model]].

In [[Eden Medina, "Cybernetic Revolutionaries: Technology and Politics in Allende's Chile"]] Medina describes an interesting point where while [[Stafford Beer]] wanted it to be completely democratic, so workers in the factories were involved in defining the metrics for measuring the factory health, in practice this didn't always happen for various reasons.


## Misc

combined "political leaders, trade unionists, and technicians." Not citizens / people though?

