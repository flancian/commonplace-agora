# For big tech, user benefits are secondary to business model

> These architectures promise, and indeed often deliver, user benefits. But these benefits are secondary to the business model, best understood as a combination of surveillance and manipulation.
> 
> &#x2013; [The British Digital Cooperative: A New Model Public Sector Institution](https://thenextsystem.org/bdc) 

