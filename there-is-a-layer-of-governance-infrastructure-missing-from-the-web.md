# There is a layer of governance infrastructure missing from the web



## Resources

-   [Points of inspiration · Wiki · Media Enterprise Design Lab / CommunityRule · &#x2026;](https://gitlab.com/medlabboulder/communityrule/-/wikis/Points-of-inspiration)
-   [[Metagov]] [Govern your Collective with Metagov Gateway](https://blog.opencollective.com/metagov-gateway/)


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-05-31 Mon]</span></span>

Read [[Governing the Information Commons]].
Linking in [[Online Communities Are Still Catching Up to My Mother's Garden Club]].


### <span class="timestamp-wrapper"><span class="timestamp">[2021-05-17 Mon]</span></span>

-   [[Flancian]] asks a good question on the social.coop tech channel - why does [[social.coop]] not use [[Mastodon]] to coordinate [[governance]]?
    -   In my opinion because Mastodon is not very good for governance.  It only has polls and even that is fairly new.
    -   In fact I don't find Mastodon very good for even just conversation.
    -   That said, having two/more platforms to coordinate things is definitely a huge piece of friction to involving a community in governance.
    -   h's mockups from way back when, are interesting: https://social.coop/@h/1868600 https://social.coop/@h/1870490


### <span class="timestamp-wrapper"><span class="timestamp">[2021-05-09 Sun]</span></span>

Listened to [[Nathan Schneider on Cooperatives and Digital Governance]].

