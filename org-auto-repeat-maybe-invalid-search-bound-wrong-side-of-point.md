# org-auto-repeat-maybe: Invalid search bound (wrong side of point)

I've been getting this error for a while when trying to complete repeating tasks in org-agenda

> org-auto-repeat-maybe: Invalid search bound (wrong side of point)

Someone here with pretty much the same issue: https://www.reddit.com/r/orgmode/comments/nfrpc0/strange_bug_with_agenda_in_945_installed_with/

They couldn't really figure it out, but someone in the comments says it's been fine for them since installing org 9.5.

Running 

```nil
M-x org-version
```

I am on org 4.9.6.  So I should try getting up to org 9.5 and seeing if it's sorted.

Great, so during a packge update in spacemacs via

```nil
SPC f e U
```

I get "Package org-plus-contrib is unavailable"

&#x2026;

That was a pain, but at some point org-plus-contrib seems to have been split out into org and org-contrib.

I updated spacemacs to latest on develop branch, and also had to remove org packages from .emacs.d/elpa/27.2/develop and then restart.

Bit of a faff, but seems that the original error is sorted now.

