# clippings



## Clippings


### [Boris Johnson to warn UK: tougher lockdown may be necessary | World news | The Guardian](https://www.theguardian.com/politics/2020/mar/28/boris-johnson-coronavirus-outbreak-will-get-worse)     :website:

<span class="timestamp-wrapper"><span class="timestamp">[2020-03-29 Sun 11:03] </span></span> mass [[testing]] as used in South Korea and Germany is the fastest way to end the lockdown.

“The restaurants are open in South Korea. You can go shopping in Taiwan. Offices are open in Singapore,” Hunt wrote. “These countries learned the hard way how to deal with a pandemic after the deadly Sars virus. They now show us how we can emerge from lockdown.”

Germany has carried out four times as many tests as the UK and recorded only 342 deaths from the virus. “Where you find it, you can isolate and contain it,” Hunt wrote.


### [Boris Johnson to warn UK: tougher lockdown may be necessary | World news | The Guardian](https://www.theguardian.com/politics/2020/mar/28/boris-johnson-coronavirus-outbreak-will-get-worse)     :website:

<span class="timestamp-wrapper"><span class="timestamp">[2020-03-29 Sun 11:04] </span></span> Concerns remain over the number of [[ventilators]] and the speed at which more can be purchased. There are currently 8,000 available and another 8,000 said to be arriving in the next few weeks. Stefan Dräger, the head of ventilator manufacturer Drägerwerk, told German magazine _Der Spiegel_ that the number of intensive care beds in England per capita was lower than Italy and five times lower than in Germany. “The challenge in England will be greater than in Spain,” he warned.

