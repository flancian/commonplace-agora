# UK supply chain crisis 2021

Food and fuel mostly.


## Food

> Boris Johnson’s government has made a dramatic U-turn in an attempt to save Christmas – with a raft of extended emergency visas to help abate labour shortages that have led to empty shelves and petrol station queues.
> 
> &#x2013; [Emergency visa scheme extended in major U-turn by Boris Johnson | Supply chai&#x2026;](https://www.theguardian.com/business/2021/oct/01/overseas-food-and-fuel-drivers-to-get-visas-in-major-u-turn-by-boris-johnson)


## Fuel

Both natural gas and petrol.

[[UK fuel crisis 2021]].


## Pigs

> The slaughter of healthy pigs has begun on British farms, with farmers forced to kill animals to make space and ensure the continued welfare of their livestock, amid an ongoing shortage of workers at slaughterhouses.
> 
> Pig farmers have been warning for several weeks that labour shortages at abattoirs have led to a backlog of as many as 120,000 pigs left stranded on farms long after they should have gone to slaughter.
> 
> The meat industry is one of many sectors of the UK economy grappling with labour shortages linked to Brexit and the pandemic, while a lack of delivery workers and drivers has affected supply chains.
>     &#x2013; [Hundreds of healthy pigs culled amid UK shortage of abattoir workers | Supply&#x2026;](https://www.theguardian.com/business/2021/oct/05/pigs-culled-amid-uk-shortage-abattoir-workers) 

Grim.  This is unpleasant for so many reasons.  Makes me glad to be [[vegetarian]] and not feel part of this particular problem.


## Food banks

> Food banks have said they will have to shrink the size of the parcels they give to struggling families owing to declining stock levels caused by the HGV crisis, supply shortages and a collapse in public donations.
> 
> &#x2013; [Food banks warn of smaller parcels due to HGV supply shortages | Food banks |&#x2026;](https://www.theguardian.com/society/2021/oct/04/food-banks-warn-of-smaller-parcels-due-to-hgv-supply-shortages) 

<!--quoteend-->

> A combination of declining food bank stocks and an expected explosion in demand for charity support after universal credit is cut this week has led some to prepare emergency measures to eke out food supplies further, including making parcels smaller and offering less variety
> 
> &#x2013; [Food banks warn of smaller parcels due to HGV supply shortages | Food banks |&#x2026;](https://www.theguardian.com/society/2021/oct/04/food-banks-warn-of-smaller-parcels-due-to-hgv-supply-shortages) 

