# intellectual property

The enclosure of ideas built upon the commons of all prior thought.  

A ridiculous concept&#x2026;

-   Intellectual provenance, not intellectual property

