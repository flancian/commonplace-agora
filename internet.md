# Internet

I like hypertexting ([[blogging]] and [[wiki-ing]] mainly).  I prefer the [[IndieWeb]] and the [[Fediverse]] to [[screen capitalism]] and all of its attendant problems. I think about [[information strategies]] that move beyond the problems of the [[attention economy]].

I'm interested in [[decentralisation]] of the big platforms of the Internet and the web.

