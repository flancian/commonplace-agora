# 2020-07-30



## Distance is a function of the intensity of the relation

Re: [[assemblage thinking]] and [[actor-network theory]]:

> Both have a topological view of space, in which distance is a function of the intensity of a relation.

^ this makes me think of [[Ton]]'s idea of [[social distance]].


## Some constellations

I've been doing quite a lot of reading and not much writing the past week or so.  I think that's OK.  It's things I know I'm interested in, just don't know that much about yet.  I think the notes will coalesce into something eventually, with a bit of tending.  The note-taking I've been doing is a bit too much copy-paste, not enough writing in my own words, so I could probably do with revisiting them a bit.

I've noticed some [[constellations]] of interest forming around [[agency]], [[self-determination]], [[networked agency]], [[assemblage thinking]], [[actor-network theory]], the [[actor model]].  Which tie into my past interests in [[agent-based systems]], and technological and political distributed systems.  

Also a strand on meditation, [[appropriate technology]], [[Buddhist economics]], and [[Schumacher]], triggered by the Rev Left Radio episode on [Meditation, Materialism, and Marxism](https://revolutionaryleftradio.libsyn.com/meditation-materialism-and-marxism).  Panda previously recommended Buddhist economics to me.

