# The policing bill will undermine the right to protest at a critical moment in the fight to avoid climate breakdown

The [[Policing bill]] will undermine the [[right to protest]] at a critical moment in the fight to avoid [[climate breakdown]].

(I guess really here the actual claim is that the policing bill will undermine the right to protest.  The followup about it being a bad time to do that is an addendum? Maybe I should split that out?)


## Because

-   [[The policing bill increases the risk of peaceful demonstrators being criminalised]]
-   The bill includes powers for the home secretary to ban marches and demonstrations that might be “seriously disruptive” or too noisy; a criminal offence of obstructing infrastructure such as roads, railways, airports, oil refineries and printing presses; jail sentences for attaching or locking on to someone or something; bans on named individuals from demonstrating or going on the internet to encourage others; police stop-and-search powers to look for protest-related items; and up to 10 years’ prison for damaging memorials or statues


## Epistemic status

Type
: [[claim]]

Agreement vector
: [[Signs point to yes]]

Haven't looked at the policing bill a huge amount, but it does sound like what is contained within is designed to have a chilling effect on protest.

