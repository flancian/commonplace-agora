# Marxism

> the intervention to reappropriate [[surplus value]] through the seizure of the [[means of production]] and the capture of the state.
> 
> &#x2013; [Is Post-Structuralist Political Theory Anarchist? | The Anarchist Library](https://theanarchistlibrary.org/library/todd-may-is-post-structuralist-political-theory-anarchist) 

^ in a nutshell

> Capitalism brought about an enormous development in technology and production: “The bourgeoisie has created more massive and more colossal productive forces than have all preceding generations together.” But workers were now nothing more than commodities, their lives subject to the domination of the market. And as capitalism becomes more and more obviously inadequate to control its own enormous growth, the working class will become the instrument for its replacement.
> 
> &#x2013; [Howard Zinn on How Karl Marx Predicted Our World Today](https://inthesetimes.com/article/21125/karl_marx_howard_zinn_birthday_capitalism_200)

<!--quoteend-->

> As workers become “a ruling class,” representing the vast majority of the nation, they will sweep away the conditions for the existence of all classes, “and will therefore have abolished its own supremacy as a class.” The climactic sentence of the first part of the Manifesto is profoundly important, repudiating any notion of a police state, and insisting on the ultimate goal of individual freedom: “In place of the old bourgeois society, with its classes and class antagonisms, we shall have an association, in which the free development of each is the condition for the free development of all.”
> 
> &#x2013; [Howard Zinn on How Karl Marx Predicted Our World Today](https://inthesetimes.com/article/21125/karl_marx_howard_zinn_birthday_capitalism_200)

<!--quoteend-->

> Marx’s economic theory and political method of analysis are critical tools for activists today. Marxism is a means to understand and dismantle the world of the 1 percent, a world that exploits, disenfranchises, oppresses, and dispossesses the many for the sake of the few; a world that may not make it to the next century with our planet and our humanity intact. I wrote this book for those of us who are attempting to make sense of the world in order to change it.
> 
> &#x2013; [[A People's Guide to Capitalism]]


## Themes

> -   Modern work is alienated
> -   Modern work is insecure
> -   Workers get paid little while capitalists get rich
> -   Capitalism is very unstable
> -   Capitalism is bad for capitalists
> 
> [POLITICAL THEORY - Karl Marx - YouTube](https://www.youtube.com/watch?v=fSQgCy_iIcc)


## Misc

> We owe much to Marx’s attempt to provide us with a coherent and stimulating analysis of the commodity and commodity relations, to an activist philosophy, a systematic social theory, an objectively grounded or “scientific” concept of historical development, and a flexible political strategy. 
> 
> &#x2013; [[The Communalist Project]]


## See also

-   [[Alienation]]
-   [[Communist Manifesto]]
-   [[Marxist humanism]]
-   [[Revisionism]]
-   [[Caliban and the witch]]
-   [[Withering away of the state]]


## Criticism

> But for the most part, as we have seen, Marxism’s economic insights belonged to an era of emerging factory capitalism in the nineteenth century. Brilliant as a theory of the material preconditions for socialism, it did not address the ecological, civic, and subjective forces or the efficient causes that could impel humanity into a movement for revolutionary social change.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> By emphasizing the nation-state—including a “workers’ state”—as the locus of economic as well as political power, Marx (as well as libertarians) notoriously failed to demonstrate how workers could fully and directly control such a state without the mediation of an empowered bureaucracy and essentially statist (or equivalently, in the case of libertarians, governmental) institutions. As a result, the Marxists unavoidably saw the political realm, which it designated a workers’ state, as a repressive entity, ostensibly based on the interests of a single class: the proletariat.
> 
> &#x2013; [[The Communalist Project]]

