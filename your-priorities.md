# Your Priorities

> Your Priorities is a [[citizen engagement]] platform, a progressive web app, and a [[participatory social network]] that empowers groups of any size to speak with one voice and organize around ideas. 

