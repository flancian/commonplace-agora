# Wiki graphs

[[org-roam]] has a nice feature that lets you [graph the notes in your wiki](https://org-roam.readthedocs.io/en/develop/tour/#exporting-the-graph) and the links between them.  I just saw that there's a pull request to [produce that map for the current note](https://github.com/jethrokuan/org-roam/pull/398).  

When that lands, I'd like to try and hook up my [[publish]] step to add the note-specific graph to each published page.  That'd give a navigation path something like the one in [[FedWiki]]:

![[Wiki_graphs/2020-04-04_09-39-41_screenshot.png]]

Though I would still want my own curated [[paths]] in addition to this generated map o' everything.

-   [[Roam]] has graphs.
-   [TiddlyMap](http://tiddlymap.org/) can make maps of your tiddlers.

A type of [[sensemaking]] aid for surfacing patterns.

-   https://notes.andymatuschak.org/Approaches_for_visualizing_large_graphs

