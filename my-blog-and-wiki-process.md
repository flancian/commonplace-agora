# My blog and wiki process

I water my garden to help it grow.  I get the water from the stream.

Currently working out how I write and share and evolve my thoughts in my [[blog and wiki combo]].  At the moment I'm trying to start with the [[stream]], my [[flow]], and then incorporate that back into the [[garden]]/[[stock]].

Ton's [mentioned](https://www.zylstra.org/blog/2020/04/13907/) once having a script that [publishes blog content straight into wiki](https://www.zylstra.org/blog/2004/08/wikifying_the_b_1/), for later refinement.  I'm kind of doing this, just [manual til it hurts](https://indieweb.org/manual_until_it_hurts) for now.

-   completely random thoughts are usually captured into org-mode first ([[fleeting notes]]?)
    -   my org-mode todos are never published anywhere (and never will be), so the most half-bake fluff can go here
    -   occassionally some stuff I might just write directly to the blog or wiki, e.g. if I'm doing some [[wiki gardening]], or I'm just happy to post whatever it is I'm posting
-   the random thoughts, with a little bit of rounding off of the edges, will then be either put onto the blog or the wiki
    -   in theory, my process could/should be:
        -   post everything to the [[stream-first]], for discussion and feedback
        -   maybe once a week do a weekly review and put stuff back into the wiki, either new notes, or refinements of existing ones
    -   it isn't exactly this at the moment though
        -   I tend to write quite a lot in the wiki first
            -   maybe it's stuff I don't feel is worth sharing yet - it's where I have my [[conversations with myself]]
    -   the final output would be I'd say 'articles', would be the culmination of building something up in the wiki over time to the point it has become fleshed out enough for a long-form piece.
        -   It's the part of your garden that you might actively show people when they come round to visit, that you're most proud of.
        -   If they want to wander around and look at other stuff too, or see what's in your toolshed, then cool also.

Recently, I've enjoyed writing things in to the stream, with links from some of the things in the notes going to their page on the wiki.  Kind of like - these is very _latest_ thoughts on these topics (that I've thought about before), what do others think?  And those thoughts will get integrated in to the wiki fairly quickly.  I noticed [[Ton]] doing that quite often - the most recent post links back to many other posts for some context.  It shows a nice evolution of thoughts. 


## Process issues

-   I have been wikifying past stream posts on things              
    -   duplication of effort
    -   how would I avoid that duplication of effort going forwards?
    -   so that things from the stream end up in the wiki with minimal effort
    -   possibly using the org-roam timeline feature as the place to do stream posts would work for that
        -   one day&#x2026; I'd want the stream to be all be indiewebified
-   With the wiki gardening, as I update my thoughts&#x2026; I'm losing some of their history!!
    -   I can either write that history into the wiki post, or maybe if stream posts link to the wiki post and appear as backlinks, that'd work (although it would assume the wiki post already exists to be linked to)
-   I have one system for the stream, and one for the garden.  It's a bit 'manual til it hurts' at the moment combining the two - but not a big deal.  If I had a choice, I'd go for moving the stream to org-mode too.  But WordPress is so full featured for now with IndieWeb stuff, it'd take a long time to recreate all that in org-mode (there is Arcology though if only could get the source&#x2026;)


## Linking some things to months in the wiki

-   the stream is sometime referred to as the timeline, but I also quite like the idea of having a timeline in my wik, too - so I'll link certain things to dates when they happened (e.g. when I finished a particular book)
-   so it's not a stream, it's a timeline I can come back to and look at key events

