# IPCC Sixth Assessment Report

[[IPCC]].

> the sixth such report from the IPCC since 1988, has been eight years in the making, marshalling the work of hundreds of experts and peer-review studies.
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn) 

<!--quoteend-->

> It represents the world’s full knowledge to date of the physical basis of [[climate change]]
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn) 

<!--quoteend-->

> found that human activity was “unequivocally” the cause of rapid changes to the climate, including sea level rises, melting polar ice and glaciers, heatwaves, floods and droughts.
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn) 

<!--quoteend-->

> Global temperatures were likely to top 1.5C above pre-industrial levels in the next two decades
> 
> &#x2013; [Worst polluting countries must make drastic carbon cuts, says Cop26 chief | C&#x2026;](https://www.theguardian.com/environment/2021/aug/09/worst-polluting-countries-must-make-drastic-carbon-cuts-says-cop26-chief) 

<!--quoteend-->

> The gravity of the situation laid out in the report blows away blustering over the supposed costs of climate action. In any case, not acting will cost far more.
> 
> &#x2013; [IPCC report’s verdict on climate crimes of humanity: guilty as hell | Climate&#x2026;](https://www.theguardian.com/environment/2021/aug/09/ipcc-reports-verdict-on-climate-crimes-of-humanity-guilty-as-hell) 

<!--quoteend-->

> Human activity is changing the Earth’s climate in ways “unprecedented” in thousands or hundreds of thousands of years, with some of the changes now inevitable and “irreversible”, climate scientists have warned.
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn)

<!--quoteend-->

> Within the next two decades, temperatures are likely to rise by more than 1.5C above pre-industrial levels, breaching the ambition of the 2015 Paris climate agreement, and bringing widespread devastation and extreme weather.
> 
> &#x2013; [Major climate changes inevitable and irreversible – IPCC’s starkest warning y&#x2026;](https://www.theguardian.com/science/2021/aug/09/humans-have-caused-unprecedented-and-irreversible-change-to-climate-scientists-warn)

![[images/temperature-since-1AD.png]]

