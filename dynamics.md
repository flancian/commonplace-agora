# Dynamics

Dynamics is the science of how systems change over time.

How does behaviour unfold and how does it change over time.

e.g. planetary dynamics; fluid dynamics; electrical dynamics; climate dynamics; crowd dynamics; population dynamics; financial dynamics; group dynamics; dynamics of conflicts and dynamics of cooperation.

