# Toussaint Louverture

> As a revolutionary leader, Louverture's military and political acumen helped transform the fledgling slave rebellion into a revolutionary movement. Louverture is now known as the "Father of Haiti." 
> 
> &#x2013; [Toussaint Louverture - Wikipedia](https://en.wikipedia.org/wiki/Toussaint_Louverture) 

