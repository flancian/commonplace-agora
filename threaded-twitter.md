# Threaded Twitter

> What would be ideal, I think, is if all information could be represented as “cards”, and all cards could be easily threaded. Every book, every blogpost, every video, even songs, etc – all could be represented as “threaded cards”. Some cards more valuable than others.
> 
> &#x2013; [twitter threads solve the fragmentation problem - @visakanv's blog](http://www.visakanv.com/blog/threading/) 

Threaded twitter is interesting in that it's kind of like zettelkasten with a simple way to thread those cards together.  I don't really have a good story for threading my zettels in org-roam together.

