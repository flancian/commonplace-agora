# Creamy vegetable vegan risotto

Original source: [Creamy Vegetable Vegan Risotto | Minimalist Baker Recipes](https://minimalistbaker.com/creamy-vegetable-risotto-30-minutes/) 


## Ingredients

Something like:

-   2 cups vegetable stock
-   0.5 small bundle asparagus (ends trimmed or 1 small bundle broccolini, stalks trimmed)
-   0.5 medium red bell pepper (seeds + stem removed, thinly sliced)
-   ~0.13 tsp each sea salt and black pepper
-   0.38 cup thinly sliced onion
-   0.5 cup arborio rice
-   0.13 cup dry white wine (or sub more vegetable broth)
-   0.13 cup vegan parmesan cheese (plus more for serving)


## Method

1.  In a medium saucepan, heat vegetable broth over medium heat. Once simmering, reduce heat to low to keep warm.
2.  In the meantime, heat a large pan over medium heat. Once hot, add half of the water (or oil) and the asparagus (and/or broccolini) and the red bell pepper. Season with a pinch each salt and pepper and sauté until just tender and slightly browned - 3-4 minutes - stirring frequently. Cover to steam and speed cooking time. Remove from pan, uncover, and set aside.
3.  Heat another large rimmed pan over medium heat. Once hot, add remaining water (or oil) and shallot. Sauté for 1-2 minutes or until softened and very slightly browned.
4.  Add arborio rice and cook for 1 minute, stirring occasionally. Then add dry white wine (or more vegetable broth) and stir gently. Cook for 1-2 minutes or until the liquid is absorbed.
5.  Using a ladle, add warmed vegetable broth 1/2 cup (120 ml) at a time, stirring almost constantly, giving the risotto little breaks to come back to a simmer. The heat should be medium, and there should always be a slight simmer. You want the mixture to be cooking but not boiling or it will get gummy and cook too fast.
6.  Continue to add vegetable broth 1 ladle at a time, stirring to incorporate, until the rice is "al dente" - cooked through but not mushy. This whole process should only take 15-20 minutes (may take longer if making a larger batch).
7.  Once the rice is cooked through and al dente, remove from heat and season with salt and pepper to taste. Also add vegan parmesan cheese and most of the cooked vegetables, reserving a few for serving. Stir to coat (see photo).
8.  Taste and adjust flavor as needed, adding a pinch of salt and pepper to taste or more vegan parmesan to enhance the cheesiness.
9.  To serve, divide between serving bowls and top with remaining vegetables, additional vegan parmesan cheese, and a sprinkle of parsley (optional).
10. Best when fresh, though leftovers will keep covered in the refrigerator for 2-3 days. We haven't tried freezing this recipe.


## My notes

Can switch the veg up.
But it'll depend what I get in the veg box.
Onions, shredded carrots, brocolli could be one.


## Generic stovetop method for just the rice

Heat olive oil in a medium sauce pan over medium-low heat. Add onion and garlic, and cooker about 5 minutes, or until the onion is translucent. Stir in the rice and toast for about a minute or so, making sure the grains are coated.
Stir in 2 cups of broth, and cook until the liquid is nearly absorbed, stirring frequently. Continue adding the remaining stock, 1 cup at a time, stirring often and adding each addition when the liquid is nearly all absorbed. This should take about 25 minutes.

