# implicit feudalism

> a bias, both cultural and technical, for generating absolutist fiefdoms. 
> 
> &#x2013; [[Online Communities Are Still Catching Up to My Mother's Garden Club]]

<!--quoteend-->

> Implicit feudalism is how platforms nudge users to practice (and tolerate) nearly all-powerful admins, moderators, and “benevolent dictators for life.”
> 
> &#x2013; [[Online Communities Are Still Catching Up to My Mother's Garden Club]]

[[Feudalism]].


## Bookmarks

&#x2013; [Admins, Mods, and Benevolent Dictators for Life: The Implicit Feudalism of Online Communities](https://mediarxiv.org/sf432/)

