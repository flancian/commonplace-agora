# Flock

Flock is the name I'm giving my personal wiki / notes / stream / garden / flow / stock setup.  (My [[Personal learning environment]]?)  It's a Rube Goldberg device of various other systems, so it's handy to have a word to refer to it all with.

Flock as a portmanteau of flow and stock, and also because I like flocks of birds and [[agent-based systems]].  I like the idea of groups of people doing similar stuff to this, autonomously but connected, producing emergent phenomena.


## Parts

-   [[org-mode]]
-   [[org-roam]]
-   [[orgzly]]
-   [[org-publish]]
-   [[WordPress]] with [[IndieWeb]] plugins


## [[Fleeting notes]]

I take these with org-capture if I'm at my desktop, orgzly if I'm somewhere with my mobile.


## [[Permanent notes]]

Write them in org-roam.  Link them from my stream.


## Garden / stock

Same as permanent notes?  This is org-roam, anyway.


## Stream / flow

I write this in org-roam first with org-roam-daily.  Most stuff I'll then publish to my WordPress site, by publishing to html from org-roam and then copy/pasting into WordPress.

I do this as on WP I have feeds / webmentions set up, so I can get a bit of interactivity with my friends through this.  As well as POSSE, etc.  All the IndieWeb goodness.


## Articles

org-roam, but then publish on WordPress.


## Principles

-   [[Local-first]].  I don't want to be dependent on connectivity, at least not for the basics of capturing notes and writing.
-   


## Itches

-   [ ] Rather than copy/paste my notes from published org-roam to WordPress, push them via micropub.
    -   Would require the content to be converted to HTML first, otherwise it would still be marked up in org-mode.  [Micropub accepts html](https://www.w3.org/TR/micropub/#new-article-with-html).
    -   I could probably use org-html-export-as-html or org-html-convert-region-to-html (elpa/26.3/develop/org-plus-contrib-20201019/ox-html.el:3775)
-   [ ] Or, set up webmentions etc as some layer on top of published org-roam, so WP no longer needed
-   [ ] With my current setup, I can't really post from my phone. I mean I can post via micropub to WP, but that I have to PESOS back to my org-roam version.  So what I do is just take it as a note in orgzly, and post it later.  I'm kind of OK with that - I have no need for things to be instant.  That said, it could be nice to bring back micropub into the equation, just because I like it.
-   [ ] a good way for getting notes from koreader on my kobo into my garden
-   [ ] add a wordcloud
    -   e.g. https://www.idkrtm.com/creating-word-clouds-with-python/
-   [X] change the capture template to not include timestamp
-   [X] setup purge on tailwind ([[Purging unused CSS from my org-publish]])
-   [ ] reloading a page with stacked notes sometimes gives a 'page not redirecting correctly' error.
-   [ ] some links from subfolders (e.g. recent changes) don't work
-   [ ] the 'root this page' link doesn't from pages in a subfolder
-   [ ] Add some basic night mode styling
-   [ ] Use or take some inspiration from [[Tufte CSS]]


## Changelog

-   2021-02-21: [[Publishing org-roam via GitLab CI]]
-   2020-11-28: [[Made myself a logo with PlantUML]]
-   2020-11-21: [[Updating to org-roam 1.2.3]]
-   2020-10-24: [[Updating to org-roam 1.2.2]] (to enable wikilink autocompletion)
-   2020-10-24: [[Adding a 404 page with .htaccess]]
-   2020-10-19: [[Making a recent changes page on my wiki]]
-   &#x2026;
-   2020-07-20: [[Add Miller columns view]]
-   2020-03-08: [Started using org-roam](https://gitlab.com/ngm/commonplace/-/commit/62c1aa137a3d8a4914398386d0ff7c0ba96fe3af)
-   2019-10-05: [Started using org-mode and org-publish with intent](https://gitlab.com/ngm/commonplace/-/commit/1382224b55b4e6f6cb818bb02afe546982ecdfc4)

