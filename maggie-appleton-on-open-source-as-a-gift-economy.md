# Maggie Appleton on Open Source as a Gift Economy

A
: [[podcast]]

URL
: https://maintainersanonymous.com/gift/

Series
: [[Maintainers Anonymous]]

Featuring
: [[Maggie Appleton]] / Henry Zhu

Great discussion on [[gift economies]], [[Free software]].

Cool to learn about "the three types of reciprocity": Generalised, Balanced, and Negative, as related to gift economies.

Appreciated the discussion around embodied knowledge.  (a la [[Trust Situated Knowing]]).  And a little bit around the way in which metaphor and language frames our culture (a la [[The Patterning Instinct]])

[[Tools for Conviviality]] gets a mention too.

Like the discussion of Patreon as a [[market economy]] dressed up as a [[gift economy]].

This post goes along with the podcast: [[A Chat with Henry Zhu on OSS &amp; Gift Economies]] 

