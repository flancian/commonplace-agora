# Why is agency important?

> Many people currently feel deprived of agency or even powerless in the face of the fall-out of issues originating in systems or institutions over which they have no influence. [&#x2026;] In response to this feeling of being powerless or without any options to act, there is fertile ground for reactionary and populist movements, that promise a lot but are as always incapable of delivering at best and a downright con or powerplay at worst. 
> 
> &#x2013; [On Agency pt. 2: The Elements of Networked Agency – Interdependent Thoughts](https://www.zylstra.org/blog/2016/08/on-agency-pt-2/) 


## Flashcard


### Front

Why is agency important?


### Back

Without agency, people feel powerless and without options.  This is fertile ground for reactionary and populist movements.

