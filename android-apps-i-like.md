# Android apps I like

Probably only going to list ones available on F-Droid.

-   AnkiDroid
-   AntennaPod
-   DiskUsage: really nice app for seeing where your storage space is being used, and freeing it up.
-   Indigenous
-   Markor
-   NewPipe
-   Nextcloud
-   OpenLauncher
-   OsmAnd.  I found this pretty handy for offline routing when I was cycling in the Lake District.
-   RadioDroid
-   [[orgzly]]
-   [[Wallabag]]
-   [[AnySoftKeyboard]]
-   [[KeePassDX]]
-   [[Syncthing]]
-   ameixa
-   dimmer
-   fdroid
-   open camera
-   Simple Contacts
-   Simple File Manager
-   Simple Flashlight
-   Simple Gallery Pro
-   StreetComplete
-   Tor Browser
-   Termux
-   Tusky
-   Vanilla Music
-   VLC

