# Searx

-   https://searx.github.io/searx/

Meta search engine.  I have an instance installed via YunoHost.

I like the ! syntax to choose which search engine to use.  Reminds me a bit of [[yubnub]] from back in the day.

