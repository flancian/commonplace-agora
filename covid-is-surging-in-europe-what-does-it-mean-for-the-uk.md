# Covid is surging in Europe. What does it mean for the UK?

A
: podcast

URL
: https://www.theguardian.com/news/audio/2021/nov/19/covid-is-surging-in-europe-what-does-it-mean-for-the-uk

Series
: [[Today in Focus]]

Topics
: [[Coronavirus]] / [[United Kingdom]] / [[Europe]]
    
    > As the days get shorter and we huddle indoors, memories of 2020’s catastrophic winter are close at hand. Now a new surge of coronavirus cases is spreading across Europe. But as well as notes of caution, there are good reasons to hope that the UK will avoid the lows of last year – from lower hospitalisation rates to exciting treatments on the verge of approval. How optimistic should we be – and can we still go to Christmas parties?

Discuss possible reasons why cases are high in Europe right now.  It's Delta variant.  But possibly high cases are due to those countries only just getting Delta - UK has had it for a while. 

Interestingly mentioned UK has stalled a bit on vaccinations - uptake in young people is poor.

Mentioned some new drugs on the way that might help - [[molnupiravir]] being one.

What is Christmas in the UK going to be like?  Lockdown unlikely, but one thing we know of this government is they act late.  Push decisions back and back till things are fucked and they can't **not** do anything.

The science reporter from The Guardian said he was tentatively planning to go to a bunch of Christmas parties.  He would play it by ear when he saw each venue, but still - he felt being double-jabbed etc was good enough.  Interesting.

