# Weeknotes 2020-46

-   put together another [[5tracks]] of bandcamp - [[5tracks - week 46]].
-   I'm slowly rambling towards a regular writing practice of some kind.  Thinking about a regular [[digital gardening practice]] and going to try and follow along with the [[Collector to Creator]] course.
-   Reading more [[Capital is Dead]].  Digging it.  Felt a call to action on how those with technical knowledge can work with others to help understand (and hopefully overcome) [[Vectoralism]].  [[Praxis for the hacker class]]?
-   Read [[Informatics of the Oppressed]] article, also digging that.
-   ^ Bit of a theme there between the two about [[Redistribution of informational wealth]].  Want to explore this more.  Also how it may or may not link to [[Independent researcher]].
-   Thought briefly about [[Glitch and détournement]].
-   Starting playing [[Myst]].
-   Read a bit about [[Karrot]] (foodsharing software).  Built with [[Vue]].  [[Nick Sellen]] works on it.
-   In the absence of useful diagrams for now, I think I'll just do random PlantUML art OK then
    
    ```plantuml
    		 node what
    		 node is
        rectangle {
    		 node to
    		 node be
    		 node done
    	}
    		cloud "?" as q
    
    	    what -> is
    	    is --> to 
    	    to --> be
    	    be --> done
    	    done --> q
    	q -> what
    note right of to : praxis
    note top of is : theory
    ```

