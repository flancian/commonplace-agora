# Weeknotes 2020-45

-   got interested in [[Agora]]
    -   made a few tweaks to my PKM ([[Flock]]) based on that
        -   [[Placing my daily logs in a journal subfolder]]
        -   start using [[person]] tag
-   thought about my [[Music listening strategy]] and bought some tracks from [[Bandcamp]]
-   was very happy that Trump lost
-   watched a webinar on [[Independent researcher]]s and [[Tools for thought]].
-   started reading about [[Ton's PKM]]
-   discovered I have an [[Antilibrary of articles]]
-   thought a bit more about [[Vectoralism]]
-   played around with [[PlantUML for weeknote diagrams]].  Got somewhere with it&#x2026; but to be honest, might just be easier with LibreOffice :/
    
    ```plantuml
    node yak
    node shave
    yak --> shave
    ```

