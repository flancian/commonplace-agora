# Personal wiki tooling

To anyone else thinking about a tool - I've fizzled out before with a wiki because of endless tool hunting.  If that's happening to you, I'd honestly say just start with plain text (**maybe** markdown) - just make sure you can write your ideas somewhere.  Then progressively enhance.


## [[My personal wiki requirements]]


## [[My personal wiki setup]]


## [[Personal wiki software I've considered or have seen recommended]]


## NAQ

(never asked questions)


### Why not Roam, Notion, etc?

In general I try to avoid [[silos]] and prefer an [[IndieWeb]].  And I prefer to use [[libre software]].


## Resources

-   [Does anyone else keep their own knowledge wiki?](https://lobste.rs/s/ord0rg/does_anyone_else_keep_their_own_knowledge)
-   Some discussion of knowledge management in org-mode: [Serious knowledge management / concept networks with Org-mode? : emacs](https://www.reddit.com/r/emacs/comments/5h6weh/serious_knowledge_management_concept_networks/)

