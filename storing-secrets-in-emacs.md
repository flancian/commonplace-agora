# Storing secrets in Emacs

Best approach seems to be via ~/authinfo.gpg.

Step 1 is using authinfo.  It's a separate file for storing credentials that is only on your machine, so any config files you migh check in somewhere don't include them.  It's a [netrc](https://everything.curl.dev/usingcurl/netrc) file.

Step 2 is giving it a gpg extension.  Then emacs will automatically encrypt and decrypt it.

-   [Keeping Secrets in Emacs with GnuPG and Auth Sources - Mastering Emacs](https://www.masteringemacs.org/article/keeping-secrets-in-emacs-gnupg-auth-sources)
-   [emacs-fu: keeping your secrets secret](http://emacs-fu.blogspot.com/2011/02/keeping-your-secrets-secret.html)

