# Commoning represents a profound challenge to capitalism

A claim from [[Free, Fair and Alive]].

Because&#x2026;

-   "Commoning is based on a very different ontology."

Hmm&#x2026; I believe it, but need a bit more evidence than that.


## Epistemic status

Type
: [[claim]]

Gut feeling
: 6

Confidence
: 2

