# Taming capitalism



## Cons

> Defenders of the idea of revolutionary ruptures with capitalism have always claimed that taming capitalism was an illusion, a diversion from the task of building a political movement to overthrow capitalism.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

<!--quoteend-->

> capitalism is too powerful a system to destroy. Truly taming capitalism would require a level of sustained collective action that is unrealistic, and anyway, the system as a whole is too large and complex to control effectively. The powers-that-be are too strong to dislodge, and they will always coopt opposition and defend their privileges.
> 
> &#x2013; [How to Be an Anticapitalist Today](https://www.jacobinmag.com/2015/12/erik-olin-wright-real-utopias-anticapitalism-democracy/) , Erik Olin Wright

