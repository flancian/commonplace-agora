# Getting a phone

I usually try and do some kind of cross-reference of repairability (https://www.ifixit.com/smartphone-repairability?sort=score) and Lineage support (https://stats.lineageos.org/)

Last time I got a Samsung Galaxy S5, does the job, easy to replace the battery.  Can get for under a £100. 

https://uk.webuy.com/search?stext=samsung%20galaxy%20s5

https://wiki.lineageos.org/devices/klt

https://social.coop/@neil/106014888504117689

