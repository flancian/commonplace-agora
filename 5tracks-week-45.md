# 5tracks - week 45

[[5tracks]] from Bandcamp for you to and enjoy and support the artistes.

-   [Proswell - FFF](https://proswell.dsp.coffee/track/fff)
-   [Bogdan Raczynski - Boku Mo Wakaran 9](https://bogdanraczynski.bandcamp.com/track/boku-mo-wakaran-9)
-   [Venetian Snares - Katzesorge Part 1](https://venetiansnares.bandcamp.com/track/katzesorge-part-1)
-   [Lapalux - Rotted Arp (feat. Louisahhh)](https://lapalux.bandcamp.com/track/rotted-arp-feat-louisahhh)
-   [Datach'i - Saiph](https://datachi.bandcamp.com/track/saiph)

