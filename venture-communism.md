# Venture communism

An association of co-operatives that is the sole owner of all the assets of all constituent worker-owned firms.

> All land, buildings, capital etc. are rented by the co-ops from the venture commune. All members of co-ops are automatically members of the commune. Hence, the means of production are communally owned.
> 
> &#x2013; [Dmytri Kleiner’s Venture Communism – 🅳🅰🆁🅺 🅼🅰🆁🆇🅸🆂🅼](https://ianwrightsite.wordpress.com/2016/12/14/blog-post-title/) 

<!--quoteend-->

> Venture communism is precisely the kind of institutional proposal that satisfies the requirement of a political economy of socialism that is immediately a new kind of political practice. 
> 
> &#x2013; [Dmytri Kleiner’s Venture Communism – 🅳🅰🆁🅺 🅼🅰🆁🆇🅸🆂🅼](https://ianwrightsite.wordpress.com/2016/12/14/blog-post-title/) 

<!--quoteend-->

> a form of struggle against the continued expansion of property-based [[capitalism]], a model for worker self-organization inspired by the topology of [[peer-to-peer networks]] and the historical pastoral [[commons]]. 
> 
> &#x2013; [[The Telekommunist Manifesto]] 

<!--quoteend-->

> A venture commune is not bound to one physical location where it can be isolated and confined. Similar in topology to a peer-to-peer network, Telekommunisten intends to be [[decentralized]], with only minimal coordination required amongst its international community of producer-owners.
> 
> &#x2013; [[The Telekommunist Manifesto]] 

