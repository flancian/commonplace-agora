# T cell defences have proved far more robust against Covid

> [[T cell defences]] are crucial for preventing hospitalisations and may continue to hold up well against future variants
> 
> [What lies on the other side of the UK’s Omicron wave? | Omicron variant | The&#x2026;](https://www.theguardian.com/world/2022/jan/13/what-lies-on-the-other-side-of-the-uks-omicron-wave)

