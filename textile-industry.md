# Textile industry

The political history of the textile industry - it's fascinating and horrible lesson from history.  

Manchester and Lancashire are wrapped up in it.

Before the industrial revolution, people had spinning wheels and hand looms in their homes.  It was decentralised fabrication.

The industrial revolution brought in spinning frames, power looms, centralisation, mass production and a whole shitload of exploitation around the globe to go along with it.  

(It's not as if everyone was happy beforehand though.  People were exploited pre-industrialisation, too).

