# Being, having, appearing

> An earlier stage in the economy's domination of social life entailed an obvious downgrading of _being_ into _having_ that left its stamp on all human endeavour.  The present stage, in which social life is completely taken over by the accumulated products of the economy, entails a generalized shift from _having_ to [[appearing]]: all effective "having" must now derive its immediate prestige and its ultimate raison d'etre from appearances.
> 
> &#x2013; [[The Society of the Spectacle]]

