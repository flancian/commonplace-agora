# Households across the UK will see rising prices and stalling wages in 2022

> It’s being called “[[the year of the squeeze]]”: thanks to stagnating wages, rising costs of consumer goods, and increases in energy tariffs, plus changes in government measures, the cost of living is set to increase sharply in 2022. 


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

