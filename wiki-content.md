# Wiki content

What do you put in a wiki?  Whatever you want!  It's your wiki.  

For me, I feel it's a lot more than just a set of 'facts' that I've discovered about my particular interests.  It would then just be a really poorly maintained subset of Wikipedia.  I think the point is to make sure not to lose the personal and the [[personality]].  (That which Wikipedia deliberately avoids).

> Wisdom, not facts. We’re not just looking random pieces of information. What’s the point of that? Your commonplace book, over a lifetime (or even just several years), can accumulate a mass of true wisdom–that you can turn to in times of crisis, opportunity, depression or job.
> 
> &#x2013; [How and why to keep a commonplace book](https://ryanholiday.net/how-and-why-to-keep-a-commonplace-book/)

I agree with that, except to de-emphasise wisdom a _little_.  I don't want to feel a pressure that what I put in here has to be wise.  It's a place for percolation.  It might turn into wisdom eventually.

