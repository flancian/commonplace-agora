# Tarot for thought

> I’m more interested in closing feedback loops to generate creative divergence. More tarot than flash cards. Tarot for thought.
> 
> &#x2013; [Building a Second Subconscious](https://subconscious.substack.com/p/second-subconscious) 

[[Maya]] has an article related to [[Oblique Strategies]] and [[tarot for thought]] - [introducing randomness into chaos: culture, oblique strategies, and tarot for&#x2026;](https://lesser.occult.institute/introducing-randomness-into-chaos-culture-oblique-strategies-and-tarot) (shared by Maya in hypothesis annotation to my journal from yesterday, thanks Maya!)

> I find that random searching like this has a lot of power to create unexpected connections in my brain; it can almost be like consulting the [[I Ching]]. In my experience this kind of searching really works, and it produces interesting results.
> 
> &#x2013; [Tasting Notes with Robin Sloan - Superorganizers - Every](https://every.to/superorganizers/tasting-notes-with-robin-sloan-25629085) 

