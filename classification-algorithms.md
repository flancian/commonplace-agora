# Classification (algorithms)

Picking a category.


## examples

-   advertising, putting people into a particular category that might be receptive to certain ads
-   classifying content on e.g. YouTube
-   labelling holiday photos
-   optical character recognition (classifying handwriting into letters)

