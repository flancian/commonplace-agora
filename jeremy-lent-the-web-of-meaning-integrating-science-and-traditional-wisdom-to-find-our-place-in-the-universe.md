# Jeremy Lent, "The Web of Meaning: Integrating Science and Traditional Wisdom to Find our Place in the Universe"

A
: [[podcast]]

URL
: https://newbooksnetwork.com/category/science-technology/systems-and-cybernetics/

Featuring
: [[Jeremy Lent]]

Like it. Intersection of science and spirituality. Chi and li. Objects and relations.

