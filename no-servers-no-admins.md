# No Servers!  No Admins!

[[Counter-anti-disintermediation]].
[[Authority in networks]].

![[2020-07-11_22-50-24_screenshot.png]]

A less forceful view: [[Servers should provide a supporting role, not a central role]]. 

[[Servers should be background infrastructure]].

-   http://networkcultures.org/wp-content/uploads/sites/17/2015/12/kleiner_gottlieb_moneylab.pdf

