# Triad of Commoning

What they call the collection of their patterns for commoning in [[Free, Fair and Alive]].  They focus on social life, peer governance, and provisioning.

3 parts:

-   [[The Social Life of Commoning]]
-   [[Peer Governance]]
-   [[Provisioning Through Commons]]

Extra info and annotation and thoughts from [[Mike Hales]]: [Commoning - Three moments - handbook trial](https://meet-coop-1.gitbook.io/handbook-trial/1-intentions/commoning/commoning-three-moments)

Also called [[Spheres of commoning]].

