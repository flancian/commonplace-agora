# the Garden

For me, I see this as a metaphor for a [[personal wiki]] (generally in the sense of a [[personal knowledge management]] tool).


## History of the metaphor

> The Garden is an old metaphor associated with [[hypertext]]. Those familiar with the history will recognize this. The Garden of Forking Paths from the mid-20th century. The concept of the Wiki Gardener from the 1990s. Mark Bernstein’s 1998 essay Hypertext Gardens.
> 
> The Garden is the web as topology. The web as space. It’s the integrative web, the iterative web, the web as an arrangement and rearrangement of things to one another.
> 
> Things in the Garden don’t collapse to a single set of relations or canonical sequence, and that’s part of what we mean when we say “the web as topology” or the “web as space”. Every walk through the garden creates new paths, new meanings, and when we add things to the garden we add them in a way that allows many future, unpredicted relationships
> 
> &#x2013; [The Garden and the Stream: A Technopastoral ](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/) 

<!--quoteend-->

> building out a network of often conflicting information into a web that can generate insights, iterating it, allowing that to grow into something bigger than a single event, a single narrative, or single meaning.
> &#x2013; [The Garden and the Stream: A Technopastoral ](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/) 


## Practicalities

> What’s the difference between a digital garden, a note-taking app, and a blog? You can see the digital garden sitting between the two. It’s a place to share your evergreen notes—not raw notes you may have stored in your note-taking app, but not quite the level of polish you would expect on a blog. 
> 
> &#x2013; [How to build a digital garden with TiddlyWiki - Ness Labs](https://nesslabs.com/digital-garden-tiddlywiki) 

Here I would take 'blog' to mean blog articles, not [[stream]] per se.  Plenty of unpolished stuff on the stream.

> Over time, several posts in your digital garden may be combined to create longer essays to post on your blog, but it won’t necessarily be the case.
> 
> &#x2013; [How to build a digital garden with TiddlyWiki - Ness Labs](https://nesslabs.com/digital-garden-tiddlywiki) 

For me I see this as an important part of it - I definitely would like it to be the case.

> In order to help you go from note collector to original content creator, your digital garden should ideally make it as easy as possible to explore the connection between various seeds of ideas so new ones can sprout. You want to interlink your notes and create a knowledge web unique to your mind and areas of interest.
> 
> &#x2013; [How to build a digital garden with TiddlyWiki - Ness Labs](https://nesslabs.com/digital-garden-tiddlywiki) 

It should aid [[pattern recognition]] / finding [[constellations]].

-   tags: [[Hypertext]]

> &#x2026;in my view, "digital gardening" is a synomym for "personal knowledge gardening", where the term "knowledge gardening" was first coined as a synomym for Douglas Engelbart's grand concept of Dynamic Knowledge Repositories and Networked Improvement Communities.
> 
> &#x2013; Jack Park

