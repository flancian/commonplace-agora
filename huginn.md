# Huginn

Like Zapier or IFTTT, but libre.

> Huginn is a system for building agents that perform automated tasks for you online. They can read the web, watch for events, and take actions on your behalf. Huginn's Agents create and consume events, propagating them along a directed graph.
> 
> https://github.com/huginn/huginn

