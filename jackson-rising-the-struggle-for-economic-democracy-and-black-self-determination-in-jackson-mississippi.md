# Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi

-   tags: [[books]]

&#x2013; Ajamu Nangwaya, Kali Akuno	

Documenting the work of [[Cooperation Jackson]].

I didn't actually read every chapter yet.  But the first 4 or 5 were really good.  A handbook of activist tactics in pretty adversarial circumstances.

> We believe that the participatory, bottom-up democratic route to economic democracy and eco-socialist transformation wil be best secured through the anchor of worker self-organization, the guiding structures of coperatives and systems of mutual aid and communal solidarity, and the democratic ownership, control, and deployment of the ecologically friendly and labor liberating technologies of the fourth industrial revolution.

It's available to read online as of 2021: https://jacksonrising.pressbooks.com/

