# social insertion

> It stems from the belief that the oppressed are the most revolutionary sector of society, and that the seed of the future revolutionary transformation of society lies already in these classes and social groupings
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

<!--quoteend-->

> Social insertion means anarchist involvement in the daily fights of the oppressed and working classes
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

