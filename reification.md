# reification

> the process by which social relations are perceived as inherent attributes of the people involved in them, or attributes of some product of the relation, such as a traded commodity
> 
> -   https://en.wikipedia.org/wiki/Reification_(Marxism)

[[The map is not the territory]].

