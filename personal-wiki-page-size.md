# Personal wiki page size

I started my wiki with pretty long pages, lots of thoughts bunched together.  I didn't think that much about structure, as I just wanted somewhere to chuck my ideas, and it worked great.  After building up it up for about a month or so, though, I started feeling the need for something that makes it easier to link concepts together.

That tends to then lead you to towards things like [[zettelkasten]] and the philosophy of [[tiddlers]].  Breaking everything up into small chunks that can be linked together ('[[collecting the dots]]').

I like the way that [[TiddlyWiki]] and [[FedWiki]] do it.  [[Roam]] seems to be the latest hot new thing along those lines.  And I found [[org-roam]] has helped with this for my own setup. 

There is much to be said for the zettelkasten / tiddler approach.  But - also I think the long player is vital too.  The occassional [[connecting of the dots]] into longer-forms (AKA articles).  It's a type of path or a thread of your ideas, made sense of and hand-curated at a point in time by yourself, to share with others.  Sitting somewhere between the garden and the stream?  It's kind of an entry point into your garden that your share into the stream.

Lately, I've been hitting a rich seam of classic articles out there, 5 years old or more, that would have been lost in time if just in a stream, and replanted or paved over if part of the garden.

(And, side note, some of my wiki pages are still pretty long.)

> I often struggle with the assumed path of small elements to slightly more reworked content to articles. It smacks of the DIKW pyramid which has no theoretical or practical merit in my eyes. Starting from small crumbs doesn’t work for me as most thoughts are not crumbs but rather like [[Gestalt]]s. Not that stuff is born from my mind as a fully grown and armed Athena, but notes, ideas and thoughts are mostly not a single thing but a constellation of notions, examples, existing connections and intuited connections.
> 
> &#x2013; [On Wikis, Blogs and Note Taking – Interdependent Thoughts](https://www.zylstra.org/blog/2020/04/on-wikis-blogs-and-note-taking/)

Really interesting.  I really like this idea of patterns and Gestalts.  ("Gestalt psychologists emphasized that organisms perceive entire patterns or configurations, not merely individual components.")  For my wikiblog to help me learn and grow my thoughts, it definitely needs to help me see these Gestalts.  I do sometimes wonder about the merit of making every concept as small as possible. Perhaps in the right context, yes, but I don't feel that doing it dogmatically will be helpful to me.

