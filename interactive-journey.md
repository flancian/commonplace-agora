# interactive journey

YOU ARE IN A MAZE OF TWISTY LITTLE PASSAGES, ALL ALIKE.

It is my ([[exo]]?) brain.

Where do you want to go today?

![[files/sketches/noodlemaps.png]]

-   There is a factory to the North: [[Technology]].
-   You can hear people shouting to the East: [[Politics]].
-   There are hills to the South: [[Environment]].
-   Go West: [[Culture]]. We'll find our promised land.

If you get lost, try the  [[The Map]] or the [[cheatsheet]].

