# What could governance online look like?

> For example, the City Council there has established an open digital marketplace to make public procurement more accessible to local startups and small and medium sized firms.
> 
> &#x2013;  [The British Digital Cooperative: A New Model Public Sector Institution](https://thenextsystem.org/bdc#a-socialist-agenda-for-digital-technology) 

<!--quoteend-->

> If citizens’ assemblies are to become an ordinary feature of public life, they will also need a digital infrastructure to support their work and integrate their proceedings and recommendations into the wider field of publicity.
> 
> &#x2013;  [The British Digital Cooperative: A New Model Public Sector Institution](https://thenextsystem.org/bdc#a-socialist-agenda-for-digital-technology) 

