# Birth of the Commune

URL
: https://thenewinquiry.com/birth-of-the-commune/

Summary
: "The Paris Commune grew out of the radical debating clubs of the collapsing French Empire"
    
    > Let us begin instead with the popular reunions at the end of the Empire, the various associations and committees they spawned, and the “buzzing hives” that were the revolutionary clubs of the Siege.

> For it was the reunions and the clubs that created and instilled the idea—well before the fact—of a social commune.

<!--quoteend-->

> **Ambulatory orators helped revolutionary clubs to federate with each other**, in the now familiar structure shared by all of the embryonic organizations that preceded the Commune but which were in many ways indistinguishable from it. That structure, a kind of **decentralized federation of local, independent worker-based committees organized by arrondissement**, had been adopted by the Paris section of the International, some 50,000 members strong in the spring of 1870

