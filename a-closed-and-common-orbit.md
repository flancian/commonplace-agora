# A Closed and Common Orbit

by [[Becky Chambers]] 

I finished this recently.  I loved it.  There's something very special about Becky Chambers' sci-fi - it's much more touching and emotional than a lot that I've read.  I felt quite emotional at the ending of this one - it's so full of heart and compassion.

