# 2020-06-01



## Simulacra and Simulation

Listened to [Episode 124 – Simulacra and Simulation – Philosophize This!](http://philosophizethis.org/simulacra-and-simulation/).

Getting a bit of an initial handle on [[Simulacra and Simulation]].  

So&#x2026; [[postmodernism]] says everything is contextual.  There is no absolute truth, or grand narratives for people to follow ideologically. 

If we have no grand narratives for people to follow in a postmodern society, what might happen?  

One thing [[Baudrillard]] says is that rampant consumerism becomes a way of creating identity in the absence of it being provided from elsewhere.  

And that as everything is so mediated, the way most people experience the world becomes a simulation of the actual reality of things.   See [The Gulf War Did Not Take Place](https://en.wikipedia.org/wiki/The_Gulf_War_Did_Not_Take_Place).  I hope I’m not adding to this state of affairs by listening to a podcast about the book rather than reading the book.

