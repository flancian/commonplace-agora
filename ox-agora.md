# ox-agora

I wanted to push my garden into [[Agora]].  

In this I am hindered by using org-mode.  Agora, quite fairly, expects Markdown with [[wikilinks]].

Chatted briefly with [[Flancian]] about org-roam and Agora [in November 2020](https://social.coop/@neil/105146750811444646). It's in the Some Day section of the [agora plan](https://anagora.org/agora-plan) to support org-roam.

I figure in the interim, an org export backend could make sense.  Something that basically takes my org files, and exports them to Markdown+wikilinks.

Currently a work in progress - check the log below, and the latest version here: https://gitlab.com/ngm/ox-agora


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-28 Sun]</span></span>

There is of course [ox-md](https://github.com/emacsmirror/org/blob/master/lisp/ox-md.el).  And there is [ox-blackfriday](https://github.com/kaushalmodi/ox-hugo/blob/master/ox-blackfriday.el) on which ox-hugo builds, and then there is [ox-hugo](https://github.com/kaushalmodi/ox-hugo/blob/master/ox-hugo.el) itself.  All these export to some form of Markdown.

Do not forget, there is also [md-roam](https://github.com/nobiot/md-roam).

I don't think any of the oxen export to a wikilinks format.  md-roam supports org-roam's notion of wikilinks, but I suspect this is slightly different from the standard notion of wikilinks.

As a first experiment:

```Makefile
md: publish.el
	@echo "Publishing for agora..."
	emacs --batch --load publish-agora.el --funcall commonplace/publish-for-agora
```

```elisp
(require 'package)

(package-initialize)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "http://orgmode.org/elpa/")))

(package-refresh-contents)

(package-install 'htmlize)
(package-install 'org-roam)
(package-install 'ox-hugo)
(package-install 's)

(require 'ox-publish)
(require 'ox-hugo)
(require 'org-roam)
(require 's)

(defun commonplace/publish-for-agora ()
	(let ((current-prefix-arg 4))
    (call-interactively 'org-publish-all)))

(setq org-publish-project-alist
      `(("commonplace"
	 :components ("commonplace-notes"))
	("commonplace-notes"
	 :base-directory "/home/shared/commonplace"
	 :base-extension "org"
	 :publishing-directory "/tmp/agora"
	 :publishing-function org-blackfriday-publish-to-blackfriday
	 :recursive t
	 :headline-levels 4
	 :with-toc nil
	 :exclude "node_modules"
	 )))
```

This outputs a folder of Markdown files.  So far so good.

Some problems:

-   the `#+TITLE` option is not picked up
    -   the headlines or the org file start at #, but would need to be ##
    -   no wikilinks

I think probably the next step would be to create `ox-agora.el`, which will be a publish backend, and override the bits in there that I need.

Nice!  That works already.  I just copied ox-blackfriday.el, and did a test override of the link transcode function.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-28 Sun 16:44]</span></span>

OK!  Now overriding the link override properly.  It's OK when the link description is the name of the page being linked to.  Not working yet with external links - need to do a check on the link type.  Just stomps over most other link types at the moment.

Also need to prepend the title to the start of the file.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-28 Sun 17:54]</span></span>

Alrighty, got the title coming through now OK by using `inner-template`.

Have published the WIP export backend to https://gitlab.com/ngm/ox-agora.

And the WIP outfile files to  https://gitlab.com/ngm/commonplace-agora.

Due to the way my filenames used to be named, there will also be a problem with some of the filenames at the moment.  I wonder if I can add some kind of slugify method to the export.  Alternatively, I'll just finish off moving all of my files to the new naming convention, which I want to do anyway.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-28 Sun 20:50]</span></span>

Fixed (a bit) the external link clobbering.  In `org-agora-link`, I now check the type of the link, and only turn internal `file:` links into wikilinks.  Every else I use the raw URL of the link.

Will definitely need to refine this further.  e.g. image links.   And thinking about it, I'm probably also losing descriptions of the links.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-29 Mon 12:24]</span></span>

As far as I can tell, output filename name boils down to `org-export-output-file-name`. https://github.com/emacsmirror/org/blob/master/lisp/ox.el#L6417

And the only way to amend that is with an EXPORT<sub>FILE</sub><sub>NAME</sub> property in the file itself. I couldn't see a way to let the backend determine the filename.

I wonder if I could do something like inserting the EXPORT<sub>FILE</sub><sub>NAME</sub> property to each file via an `org-export-before-processing-hook`.  I would put that not in ox-agora, but in my publish-agora.el file, as it's not something you should force on every user of ox-agora.

I suppose the problem with that, is that files referencing that file won't know about it's changed name, will they?  So links would be broken?  Well no, not in this case, as other files are using wikilinks based on the title.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-29 Mon 23:15]</span></span>

Well, kind of pleased with myself.  I couldn't get the `org-export-before-processing-hook` approach to work.  I could inject `#+EXPORT_FILE_NAME` in to the buffer with it, but for whatever it wasn't subsequently being picked up or acknowledged.

So instead, I used `advice-add` to modify the returned value of `org-export-output-file-name`.   

```elisp
(defun commonplace/slugify-export-output-file-name (output-file)
  (let* ((title (get-title (buffer-file-name (buffer-base-buffer))))
	 (directory (file-name-directory output-file))
	 (slug (commonplace/slugify-title title)))
    (concat directory slug ".md")))

(advice-add 'org-export-output-file-name :filter-return #'commonplace/slugify-export-output-file-name)
```

I basically get the title from the active buffer and slugify it, and concat it to the publish directory (gotten from the `output-file` that is returned by `org-export-output-file-name`).  To be honest, it feels like a brutal hack, but I'm just pleased to have gotten it working. And I learned a bunch in the process.  I hadn't used `advice-add` before, for example. 


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-31 Wed 10:30]</span></span>

I have joined an [[Agora]]! https://anagora.org/@neil

A couple of notes:

-   I need to fix up my slugify method a bit.  I knew this already, but I see it more now that some of the slugs I generate don't match the slugs that Agora expects.   
    -   I think mainly punctuation is the issue.  I convert question marks, commas, apostrophes, etc, into dashes, but they would be better just removed.

<!--listend-->

-   It would be nice to autogenerate the agora markdown as part of my gitlab pipeline.  I'm doing it manually for now.  However, that's fine - manual until it hurts, and until I iron out a few wrinkles, I think.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-04-01 Thu 19:41]</span></span>

Pushing to a repo from within a gitlab pipeline doesn't seem completely straightforward:

-   [How do I push to a repo from within a gitlab CI pipeline? - Stack Overflow](https://stackoverflow.com/questions/51716044/how-do-i-push-to-a-repo-from-within-a-gitlab-ci-pipeline)
-   [Git push from inside a gitlab-runner - GitLab CI/CD - GitLab Forum](https://forum.gitlab.com/t/git-push-from-inside-a-gitlab-runner/30554) 
    
    Shame, but I don't think I'll spend too much time on it right now.

