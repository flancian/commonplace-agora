# Behaviour change or system change?

At the moment, my view is something along the lines of: 

-   Do what you can as an individual, to the best of your ability
-   Don't be overburdened by guilt if you can't do it all
-   Be a good example, but don't harangue people
-   Be very conscious that not everyone is in a position to do what you think is 'doing the right thing'
-   The system is pretty rigged against doing the right thing, so ultimately the system is the one that needs to change
-   Individual behaviour change is probably a part of bringing about system change

> Because it's no one person's individual responsibility\* and it is at best unfair, at worst impossible, to expect people to take the weight of the world on top of their immediate concerns; any real change will only happen at organisation, collective, corporate levels. 
> 
> It's small, and seems kind of futile in the grand scheme of things. Why even bother? What difference are fewer than 20 people going to make? [&#x2026;] But at the very least we can spread the message, the intent, the energy to our friends, family, and possibly our clients, who might spread it onwards.
> 
> &#x2013; Amy Guy, [Reflections prompted by #ClimateStrike](https://rhiaro.co.uk/2019/09/reflections-climate)

<!--quoteend-->

> Telling people to get out of their cars is counterproductive in parts of the country where decades of chronic underinvestment has left us without public transport.
> 
> Campaigns to tackle climate change need to link up with campaigns for better transport and fairer funding for it, particularly for buses, which are scandalously neglected in our national narrative but are the key mode of public transport for most of the country.
> 
> It is also counterproductive to disrupt the one holiday a family has, one they’ve saved for all year, instead of targeting the global corporations whose business models rely on frequent air travel and the governments who refuse to tax them for it.
> 
> &#x2013; Lisa Nandy,[ Yes, we need climate action; but it needs to be rooted in people’s daily reality](https://www.hopenothate.org.uk/yes-we-need-climate-action-but-it-needs-to-be-rooted-in-peoples-daily-reality/)


## Useful links

-   [We Can't Do It Ourselves](https://www.lowtechmagazine.com/2018/07/we-cant-do-it-ourselves.html) &#x2013; Kris De Decker
-   [Reflections prompted by #ClimateStrike](https://rhiaro.co.uk/2019/09/reflections-climate) &#x2013; Amy Guy
-   [Yes, we need climate action; but it needs to be rooted in people’s daily reality](https://www.hopenothate.org.uk/yes-we-need-climate-action-but-it-needs-to-be-rooted-in-peoples-daily-reality/) &#x2013; Lisa Nandy

