# my social media usage

-   My social media usage is something like this.
    -   Follow people wherever they are (including the big silos).
        -   This is with a [[social reader]].
    -   Write locally, in my [[personal wiki]], first.
        -   This is with [[org-roam]].
    -   Publish on my own site, my '[[digital garden]]'.  I for sure own the data this way.
        -   This is via [[Micropub]] (stream), and [[org-publish]] (garden).
    -   Syndicate things elsewhere, wherever the community best fits for my post.  But don't feed the big tech beasts.
        -   This is via [[Webmentions]], [[brid.gy]].
    -   Interact with people wherever they are, but still via my own site if possible.
        -   Some combo of [[social reader]], [[Micropub]], [[brid.gy]].
    -   At present, a combo of [[org-mode]], [[IndieWeb]], [[Fediverse]], [[Agora]] make this possible for me.

So many interesting people are still on the big platforms. (Stuck, I would say).  But that's fine.  Meet people where they are.  You'll more likely to pique people's interest towards another way of doing things if you interact with them and they find interest in what you say, then they see that you are doing it differently.  

