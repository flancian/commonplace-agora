# Governing complex economies and societies by consent is hard

[Attempting to ban protest is usually the mark of a repressive state. That’s n&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/23/priti-patel-police-and-crime-bill-banning-protest-britain)


## Because

&#x2026;


## So

&#x2026;?


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Yes definitely]]

Seems like a truism more than a claim to be honest.  An axiom for exploring other claims.

