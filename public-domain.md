# public domain

> The public domain comprises creative works to which copyright no longer applies, because it has expired, expressly been waived, or may be inapplicable (Dusollier, 2010).
> 
> &#x2013; [[dulongderosnay2020: Digital commons]]

<!--quoteend-->

> While the public domain is legally and conceptually separate from the digital commons, in practice, public domain works constitute an important source from which commoning practices can draw, all the more as public domain books and artworks are being digitised (Boyle, 2008; Dulong de Rosnay & De Martin, 2012). 
> 
> &#x2013; [[dulongderosnay2020: Digital commons]]

