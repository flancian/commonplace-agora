# Freegle

A non-market sharing platform.  Basically a way to give away or get stuff for free.

It is democratically governed.  

Freegle Ltd is a Registered Society.

> Each Freegle local group is run independently and is affiliated to Freegle Ltd, which provides central services (such as this website) to these groups and their volunteers. Freegle Ltd is a Registered Society (previously known as an Industrial and Provident Society for Community Benefit). Freegle Ltd is owned and governed by its members. Local and national volunteers are eligible for membership.
> 
> &#x2013; [Freegle - About Us](https://www.ilovefreegle.org/about) 

