# Assemblage thinking

Building on [[Deleuze & Guattari]].

> at their most basic, assemblages could thus be thought of as a collection of relations between heterogeneous entities to work together for some time. But they are more than this. Terms such as ‘contagions’, ‘epidemics’ and ‘the wind’ hint at the fluidity and ephemerality of assemblages and at their unpredictability, while ‘sympathy’ and ‘symbiosis’ suggest that there is a vital, affective quality to them

<!--quoteend-->

> The English term ‘assemblage’ is the translation of the French original agencement . It captures well that an assemblage/agencement consists of multiple, heterogeneous parts linked together to form a whole – that an assemblage is relational. But the translation risks losing some connotations of agencement , especially that of an arrangement that creates agency. For Deleuze and Guattari, there are thus no pre‐determined hierarchies, and there is no single organising principle behind assemblages (‘it is never filiations … these are not successions, lines of descent’), be it capital or military might.
> 
> &#x2013; Assemblages and Actor‐networks: Rethinking Socio‐material Power, Politics and Space - Müller - 2015

[[post-structuralism]].

