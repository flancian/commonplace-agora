# Levelling up

Tory nonsense.

-   [Want to ‘level up’ the UK? Just give places the power and money they need | J&#x2026;](https://www.theguardian.com/commentisfree/2021/sep/19/level-up-uk-regions-local-authority-funding)

-   [Boris Johnson now has the chance to make ‘levelling up’ mean something | Andy&#x2026;](https://www.theguardian.com/commentisfree/2021/oct/06/andy-burnham-boris-johnson-levelling-up-north-of-england)

"Real levelling-up means going local"
[Letters: real levelling-up means going local | Inequality | The Guardian](https://www.theguardian.com/theobserver/commentisfree/2022/jan/09/real-levelling-up-means-going-local-letters)

