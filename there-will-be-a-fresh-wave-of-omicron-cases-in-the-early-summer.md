# There will be a fresh wave of Omicron cases in the early summer

So sayeth Sage in The Guardian.

So sometime around June presumably.


## Because

-   People will resume social habits
-   Immunity will be waning


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

Source (for me)
: [[The Guardian]] / [[Sage]] / [Expect another Omicron wave in early summer, Sage says | Omicron variant | Th&#x2026;](https://www.theguardian.com/world/2022/jan/14/expect-another-covid-omicron-wave-in-early-summer-sage-says)

