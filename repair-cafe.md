# Repair Cafe

> The open workshops and repair cafes are places for community building, collective thinking, and learning. They [[Creatively Adapt & Renew]] countless objects that are considered waste, giving them a second life cycle.
> 
> &#x2013; [[Free, Fair and Alive]]

