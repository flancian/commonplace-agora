# 2020-07-05



## [[Monopolisation of AI is about more than data]]

> The monopolisation of AI is not just – or even primarily – a data issue. Monopolisation is driven as much by the barriers to entry posed by fixed capital, and the ‘virtuous cycles’ that compute and labour are generating for the AI providers.

Nick Srnicek talks about how imbalanced access to fixed capital and labour are as big issues as access to large datasets when it comes to the big tech monopolies.

> economic policy in response to Big Tech must go beyond the fascination with data. If hardware is important too, then opening up data is an ineffective idea at best and a counter-productive idea at worst.

I think the argument being that something like the EU’s data strategy focuses too much on the data itself, and neglects the hardware, capital and labour needed to do useful things with that data.

> It could simply mean that the tech giants get access to even more free data – while everyone else trains their open data on Amazon’s servers.

[Data, Compute, Labour | Ada Lovelace Institute](https://www.adalovelaceinstitute.org/data-compute-labour/).

phrase is both a promise and a deflection. It’s a plea for unearned trust — give us time, we are working toward progress. And it cuts off meaningful criticism — yes, we know this isn’t enough, but more is coming

The architecture of the social network — its algorithmic mandate of engagement over all else, the advantage it gives to divisive and emotionally manipulative content — will always produce more objectionable content at a dizzying scale.


## [[We Know We Have More Work To Do]]


## Does Facebook benefit from hate speech?

Facebook VP of Global Affairs and Communications, Nick Clegg:

> We don't benefit from hate speech&#x2026; we benefit from positive human connection.
> 
> &#x2013; [Nick Clegg on CNN](https://edition.cnn.com/2020/06/28/tech/nick-clegg-facebook-boycott-reliable/index.html)

OK Cleggy.  Not so sure about that.  You will only care about positive human connection when it makes you money.  I'd suggest that those two things are mutually exclusive.

> The architecture of the social network — its algorithmic mandate of engagement over all else, the advantage it gives to divisive and emotionally manipulative content — will always produce more objectionable content at a dizzying scale.
> 
> &#x2013; [Opinion | Facebook Can’t Be Reformed - The New York Times](https://www.nytimes.com/2020/07/01/opinion/facebook-zuckerberg.html) 


## Sister sites and twin pages

I like the ideas of [[sister sites]] and [[twin pages]] that I saw on [Bill Seitz's wiki](http://webseitz.fluxent.com/wiki/).  For some reason it makes me think of synaptic links , and the "neurons that fire together, wire together" from Hebbian learning.

