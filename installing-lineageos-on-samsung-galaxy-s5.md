# Installing LineageOS on Samsung Galaxy S5



## Why?

Getting long in the tooth now, stuck on Android 6.  Couldn't install Termux!


## Back stuff up

-   https://www.xda-developers.com/how-to-backup-android/
-   Use List My Apps app to export a list of installed apps.
    -   files are already on external storage or synced elsewhere.
    -   Signal
        -   https://support.signal.org/hc/en-us/articles/360007062012-New-Number-or-New-Phone
    -   Element
    -   Google Authenticator
    -   Tasker
        -   backup data
    -   AntennaPod
        -   list of podcasts as OPML
    -   Typing Hero
        -   export to csv


## adb

Need to get adb installed on to my laptop first.  I might have it already, but probably best to get the latest.

-   https://wiki.lineageos.org/adb_fastboot_guide.html


## Lineage

Then download and install Lineage.

-   https://wiki.lineageos.org/devices/klte/install

-   Install heimdall
-   Test heimdall
    -   need to switch to root to access /dev/usb
-   install recovery
    -   just go with the lineage one.
    -   When the status message in the top left of the devices’s display reports that the process is complete, you may proceed.
        -   this never happened, but everything was OK.

-   install lineage from recovery

-   install gapps from recovery

-   noticed interesting 'Set Warranty Bit: recovery' and 'Set Warranty Bit: kernel' when booting


## Google Apps

I need these on my phone sadly.  Do this at the same time as Lineage, pretty much.


## Post install setup

-   why do I have to format my SD card?  that's annoying.
-   install fdroid
-   install fennec
-   install open launcher
-   install ameixa
-   install wpgen, set background to black
-   install syncthing
-   sync org folder
-   install orgzly
-   install keepassdx
-   install antennapod, import opml
-   install tusky
-   install indigenous
-   install element
    
    things i forgot to backup
    
    -   orgzly searches
    -   openlauncher settings

