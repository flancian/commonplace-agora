# mind garden

> In French, "cultiver son jardin intérieur" means to tend to your internal garden—to take care of your mind. 
> 
> -   [You and your mind garden - Ness Labs](https://nesslabs.com/mind-garden)

Essentially about [[personal knowledge management]].  Heard about it from [[Anne-Laure]] (did she also come up with the term?).  Commonly manifested in a [[digital garden]].

> taking care of your mind involves cultivating your curiosity (the seeds), growing your knowledge (the trees), and producing new thoughts (the fruits).
> 
> &#x2013; [You and your mind garden - Ness Labs](https://nesslabs.com/mind-garden)

I like this alternative way of thinking about [[seek, sense, share]].

> each day tending to your “mind garden” is different: discovering a new learning strategy, having a eureka moment, connecting the dots between two authors, getting involved in a lively conversation with an expert
> 
> &#x2013; [You and your mind garden - Ness Labs](https://nesslabs.com/mind-garden)

<!--quoteend-->

> When consuming content, grow branches on your knowledge tree by taking notes.
> 
> &#x2013; [You and your mind garden - Ness Labs](https://nesslabs.com/mind-garden)

<!--quoteend-->

> Over time, you will find yourself going back to certain corners of your mind garden more often than others, and that’s alright.
> 
> &#x2013; [You and your mind garden - Ness Labs](https://nesslabs.com/mind-garden)

You will see parts of your interests form and coalesce.

> When you find some of these new combinations particularly interesting, share the seeds with fellow gardeners. Leverage the knowledge of fellow explorers. 
> 
> &#x2013; [You and your mind garden - Ness Labs](https://nesslabs.com/mind-garden)

[[Network learning]].

> Your [[note-taking app]], such as Roam or one of its open source alternatives for instance, can be used for collating snippets, ideas, and raw thinking. Use it to seed your garden and connect the dots.
> 
> &#x2013; [You and your mind garden - Ness Labs](https://nesslabs.com/mind-garden)

