# Vectoralist class

As per [[McKenzie Wark]], part of [[Vectoralism]] - information has empowered a new kind of ruling class - the vectoralist class.

> Information, like land or capital, becomes a form of property monopolised by a class of vectoralists, so named because they control the vectors along which information is abstracted, just as capitalists control the material means with which goods are produced, and pastoralists the land with which food is produced.
> 
> &#x2013; [Wark, A Hacker Manifesto](http://subsol.c3.hu/subsol_2/contributors0/warktext.html) 

The vectoralist class sits as a layer built on top of the [[capitalist]] class.

And are antagonists to the [[hacker class]].

