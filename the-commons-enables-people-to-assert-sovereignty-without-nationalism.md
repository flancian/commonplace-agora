# The commons enables people to assert sovereignty without nationalism

A claim from [[Free, Fair and Alive]].

[[Sovereignty]] ([[self-governance]]) without [[Nationlism]].


## Epistemic status

Type
: [[claim]]

Gut feeling
: 6

Confidence
: 4

