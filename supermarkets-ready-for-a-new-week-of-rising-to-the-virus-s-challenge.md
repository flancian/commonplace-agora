# Supermarkets ready for a new week of rising to the virus’s challenge

> The wave of stockpiling seen in recent weeks as the initial _hamsterkauf_ phase of the pandemic.

<!--quoteend-->

> next big test will come if there is a move to “phase two”, which Monteyne says is when high levels of staff absence start to affect the ability of the supermarkets to operate properly.

<!--quoteend-->

> People simply buy to fill their fridges, freezers, cellars and bedrooms.

<!--quoteend-->

> old-fashioned superstores, with their more flexible distribution models and click-and-collect services, have proved to be much more adaptable to the seismic shift in demand

