# Trying to install a custom ROM on Sony Xperia Z1 Compact

These are old notes.  tl;dr - the bootloader was unlockable.


## Custom ROM on phone


### phone notes

-   14.6.A.1.236


### Prereqs


#### Authenticator&#x2026;

-   make sure you have backup codes available!
-   or put it on another device first?
-   

Rooting phone

-   

-   https://forum.xda-developers.com/showthread.php?t=2631291&page=20
    -   is this particular version rootable easily?
-   recovery? unlocking bootloader? rooting?


### Bootloader

-   https://www.addictivetips.com/mobile/what-is-bootloader-and-how-to-unlock-bootloader-on-android-phones-complete-guide/
-   In literal terms, bootloader is code that is executed before any Operating System starts to run.
-   Every Android phone has a bootloader that instructs the operating system kernel to boot normally
-   A bootloader is usually locked on an Android device 
    -   because although it’s an open source OS, still the manufacturers want you to stick to their Android OS version specifically designed for the device.
    -   In order to apply this concept, manufacturers lock the bootloader.
    -   With a locked bootloader on Android devices, it is virtually impossible to flash a Custom ROM and forced attempts void warranty as well as usually end up in bricks.
    -   Therefore, the first step is to always unlock the bootloader.
    -   If you want root, you’d be wanting to flash Custom ROM post-root, 
        -   and if you want to install a Custom ROM, you’d need to have your stock bootloader unlocked.
        -   In simple terms Unlocking the bootloader allows you to install custom Firmware on your Android phone.
-   http://www.theandroidsoul.com/how-to-unlock-bootloader-on-sony-xperia-z1-compact/
-   instructions to unlock bootloader
-   20/003/2017 00:03:19 - INFO  - Loader : S1<sub>Root</sub><sub>6732</sub> - Version : MSM8974<sub>23</sub> / Boot version : S1<sub>Boot</sub><sub>MSM8974</sub><sub>Rhine1.3.1</sub><sub>LA1.04</sub><sub>16</sub> / Bootloader status : NOT<sub>ROOTABLE</sub>


### Bootloader unlock not allowed?

-   https://forum.xda-developers.com/sony-xperia-z1-compact/help/unlocking-bootloader-root-t3086617
-   backup TA partition?
-   https://forum.xda-developers.com/sony-xperia-z1-compact/help/unlock-bootloader-unlock-allowed-t2805530/page3


### Phone info number

-   dial **#\*#7378423#\*#**

Instructions here to get flashtool

-   tthis is to &#x2013; root the device (hopefully I don't need to unlock bootloader?)
-   http://www.flashtool.net/downloads_linux.php

I can't actually get latest most secure version of Android

-   https://talk.sonymobile.com/t5/Xperia-Z4-Tablet/Bootloader-unlock-allowed-No/td-p/1052106

