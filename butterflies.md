# Butterflies

> As well as forming a vital part of the food chain, butterflies are considered significant indicators of the health of the environment.
> 
> &#x2013; [Number of butterflies in the UK at a record low, survey finds](https://www.theguardian.com/environment/2021/oct/07/number-of-butterflies-in-the-uk-at-a-record-low-survey-finds) 

