# decentralized autonomous organisations

-   

-   built on smart contracts
-   do not exist on a specific server, instead encoded into the blockchain
-   problematic

> While on the positive side, DAOs could drastically lower the cost of horizontal social collaboration, cut the bureaucratic fat and automate tedious processes, they could also be deployed by the existing power structures to lock down the current hegemony into an inescapable, cybernetic one.
> 
> &#x2013; [[DisCO Manifesto]] 

<!--quoteend-->

> DAO projects have mostly prioritised focus on speculative assets, tokenization and grandiose promises of disruption, while reproducing many of the power dynamics of the normative systems they aim to disrupt and decentralise.
> 
> &#x2013; [Last Night A Distributed Cooperative Organization Saved My Life: A brief intr&#x2026;](https://hackernoon.com/last-night-a-distributed-cooperative-organization-saved-my-life-a-brief-introduction-to-discos-4u5cv2zmn) 

<!--quoteend-->

> [&#x2026;]there is a very real danger that the future of these decentralised autonomous organizations will reproduce and amplify existing inequalities and power dynamics.
> 
> &#x2013; [Last Night A Distributed Cooperative Organization Saved My Life: A brief intr&#x2026;](https://hackernoon.com/last-night-a-distributed-cooperative-organization-saved-my-life-a-brief-introduction-to-discos-4u5cv2zmn) 

