# Free culture

I want to support either free culture or independent creators.  So trying to do it in a way that means easy access to culture for those that can't afford it, but also in a way that artists and creators get fairly compensated for their work.  Cutting out the big corporations that skim off the hard work of others.

I have a [[Music listening strategy]].

> In part as a response to the increasingly aggressive assertion of copyright by the cultural industries suing customers for performing everyday acts online, and in part drawing inspiration from the [[free software]] movement, the free culture (or open content) movement began to take shape (Boyle, 1997; Lessig, 2001).
> 
> &#x2013; [[dulongderosnay2020: Digital commons]]

<!--quoteend-->

> The largest free culture project is [[Wikipedia]], an encyclopedia that is cooperatively written and financed by donations from the readers who are part of the community (Dobusch & Kapeller, 2018).
> 
> &#x2013; [[dulongderosnay2020: Digital commons]]

