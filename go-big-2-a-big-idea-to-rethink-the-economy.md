# GO BIG #2: A Big Idea to Rethink the Economy

URL
: https://www.cheerfulpodcast.com/rtbc-episodes/go-big-rethink-the-economy

Topics
: [[GDP]]

Publisher
: [[Reasons to be Cheerful]]

Featuring
: [[Katherine Trebeck]] /  [[Kate Raworth]] / [[Rokhsana Fiaz]]

Good discussion on the massive shortcomings of GDP as a metric for the prosperity of a country.

Also discussion with Kate Raworth on how Doughnut Economics has become quite big lately, and she has started a [[Doughnut Economics Action Lab]].

And they speak to Rokhsana Fiaz about what is happening in Newham in London, which sounds pretty rad - [[Community wealth building]], [[Citizens' assembly]], and [[Participatory budgeting]].

