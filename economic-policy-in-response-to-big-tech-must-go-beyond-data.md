# economic policy in response to Big Tech must go beyond data

Particularly in the context of AI.

> economic policy in response to Big Tech must go beyond the fascination with data. If hardware is important too, then opening up data is an ineffective idea at best and a counter-productive idea at worst. It could simply mean that the tech giants get access to even more free data – while everyone else trains their open data on Amazon’s servers. If we want to take back control over Big Tech, we need to pay attention to more than just data.
> 
> &#x2013; [Data, Compute, Labour | Ada Lovelace Institute](https://www.adalovelaceinstitute.org/data-compute-labour/) 

