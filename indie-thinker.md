# Indie thinker

> A new breed of influencers has emerged from the PKM market: indie thinkers, which some call thinkfluencers. From researchers to entrepreneurs and writers, these influencers rely on new dissemination tools to monetize their unique knowledge.
> 
> &#x2013; [The state of personal knowledge management - Ness Labs](https://nesslabs.com/the-state-of-personal-knowledge-management) 

^ I think the term thinkfluencer is pretty awful - hope that doesn't take off.  I guess I just have a negative reaction to the modern notion of "influencing" others' behaviour.  But I suppose in some sense also I do wish to influence others' behaviour through what I share.

> Producing work that makes other people think, and perhaps change their behavior, is the validation, and it’s enormously satisfying.
> 
> &#x2013; [Nadia Eghbal | The independent researcher](https://nadiaeghbal.com/independent-research) 

'Monetizing' I also don't like, but on the flipside in the current absence of universal security, I do think it's fair enough for people to be recompensed for what they've worked on.  Certainly don't want it to end up as some winner-takes-all profit-driven competition of _thinkfluencing_ bullshit though.  

I guess I just hope the idea is that the indie stands more for 'independent', less for 'individual'.   I like the idea that the institution of the university need not be the only route for research.

> There’s no reason that universities need to be the gatekeepers of exploring and developing new ideas.
> 
> -   [Nadia Eghbal | The independent researcher](https://nadiaeghbal.com/independent-research)

Perhaps even better would be if the indie stood for something like [[Ton]]'s /inter/dependent thoughts.


## Indie Research webinar

I watched the webinar [The Indie Researcher: Tools for thought and internet academia](https://www.stream.club/e/indie).  It was more discursive than practical to be honest, but a few takeaways that I like:

-   being an indie researcher is about being curious in public - not necessarily an expert in some domain with a position to defend (I think [[Anne-Laure]] said this)
-   being 'indie' doesn't mean you just do it all yourself - it's very much about discussion and community, too
-   it might free you to do some things that might not happen in an academic environment


## Some indie researchers

Just based on my opinion of what it means. Being curious in public, possibly financially supported by the crowd, regularly producing 'research' outputs. 

-   [[Anne-Laure]]
-   [[Nadia Eghbal]]
-   [[Chris Webber]]
-   [[Andy Matuschak]]


## Links

-   [[Nadia Eghbal]] has written about this:
    -   [Nadia Eghbal | The independent researcher](https://nadiaeghbal.com/independent-research)
    -   [Nadia Eghbal | Reimagining the PhD](https://nadiaeghbal.com/phd)
-   [Calling all indie thinkers (literally) - Other Life](https://otherlife.co/calling-all-indie-thinkers-literally/)
    -   note: after subscribing to his newsletter, I suspect (perhaps unfairly, TBD) that this dude is a bit alt-right.
-   [The personal PhD](https://davidklaing.com/personal-phd/)

