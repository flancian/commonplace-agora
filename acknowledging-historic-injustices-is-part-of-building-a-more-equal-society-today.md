# Acknowledging historic injustices is part of building a more equal society today

Articulated here: [The Guardian view on the ‘Colston Four’: taking racism down | Editorial | The&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/07/the-guardian-view-on-the-colston-four-taking-racism-down) 


## Because


## Epistemic status

Type
: [[claim]]

Acceptance level
: [[Most likely]]

I'm sure it must be, I just don't have any supporting evidence as of yet.

