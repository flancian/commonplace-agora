# Digital architectures shadow all aspects of human life

> As a result almost all aspects of human sociability, of the life of the species, are now shadowed by digital architectures
> 
> &#x2013; [The British Digital Cooperative: A New Model Public Sector Institution](https://thenextsystem.org/bdc) 

