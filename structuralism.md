# structuralism

> implies elements of human culture must be understood by way of their relationship to a broader, overarching system or structure
> 
> &#x2013; [Structuralism - Wikipedia](https://en.wikipedia.org/wiki/Structuralism) 

<!--quoteend-->

> the belief that phenomena of human life are not intelligible except through their interrelations. These relations constitute a structure, and behind local variations in the surface phenomena there are constant laws of abstract structure
> 
> &#x2013; [Structuralism - Wikipedia](https://en.wikipedia.org/wiki/Structuralism) 

![[images/structuralism.jpg]]

See:  [[post-structuralism]]. 

