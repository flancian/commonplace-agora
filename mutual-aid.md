# Mutual aid

Caring for each other.  Changing the world to make it more caring.

> Mutual aid is the radical act of caring for each other while working to change the world.
> 
> &#x2013;  https://www.versobooks.com/books/3713-mutual-aid

<!--quoteend-->

> Mutual aid projects are a form of political participation in which people take responsibility for caring for one another and changing political conditions. 
> 
> &#x2013; [Mutual aid (organization theory) - Wikipedia](https://en.wikipedia.org/wiki/Mutual_aid_(organization_theory)) 

