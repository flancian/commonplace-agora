# How can we achieve vaccine equity?

-   We should have Covid [[patent waivers]]
-   We should have Covid [[technology transfer]]

> In order for vaccine equity to be achieved, developing nations need to be supported in their pursuit of manufacturing vaccines on home soil. This starts with wealthy nations supporting the TRIPS Waiver, and pharmaceutical companies sharing the knowledge and technology needed for these countries to produce doses
> 
> -   [Vaccine Equity Issues We Need to Overcome in 2022](https://www.globalcitizen.org/en/content/covid-19-vaccine-equity-issues-to-overcome-2022/)

[[Cuba]] is doing it.

> Cuban government announced its plan to get these doses into the arms of those who need them in the Global South, including:
> 
> -   Solidarity prices for Covid-19 vaccines for low-income countries;
> -   Technology transfer where possible for production in low-income countries;
> -   Extending medical brigades to build medical capacity and training for vaccine distribution in partner countries.
> 
> -   [Cuba pledges “lifesaving package” of Covid-19 vaccine support to Global South&#x2026;](https://progressive.international/wire/2022-01-25-cuba-pledges-lifesaving-package-of-covid-19-vaccine-support-to-global-south-at-progressive-international-briefing/en)

