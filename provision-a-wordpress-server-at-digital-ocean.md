# Provision a WordPress server at Digital Ocean

Steps I usually do to get a WordPress server up and running at [[Digital Ocean]].


## Create the droplet

They do have a WP server in the marketplace, but I tend to do it from scratch.

Usually go with Ubuntu 18.04 LTS.

I always provision with an ssh key set up from the get go, rather than getting sent a root password.


## Security things

-   ufw
    -   enable http, https, and ssh
-   fail2ban
-   disable remote password access (usually done already)
-   but **do** set a root password and store in password manager, for those times you need to log in via the access console


## WP related apps

-   Apache
-   [[MySQL]]
-   PHP
    -   and a bunch of php extensions (php-xml, php-json, php-mysql, php-mbstring, php-zip, any others?)

