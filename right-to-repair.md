# right to repair

> the three pillars of the right to repair, which are: 1. products need to be designed for repairability; 2. spare parts and repair services need to be affordable; and 3. people should have access to the information they need to carry out repairs.
> 
> &#x2013; [[The UK’s new ‘right to repair’ is not a right to repair]]

<!--quoteend-->

> The availability of repair info is going to be critical for riding out a long-term crisis, both in terms of increasing the longevity of devices & minimizing the amount of travel that "approved technicians" need to make. Give people the resources they need to make repairs locally.
> 
> &#x2013; https://twitter.com/mcforelle/status/1240637926935089155


## In Europe

-   https://repair.eu


## [[Right to Repair in the UK]]

