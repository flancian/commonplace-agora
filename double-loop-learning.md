# double-loop learning

> For Argyris and Schön (1978: 2) learning involves the detection and correction of error. Where something goes wrong, it is suggested, an initial port of call for many people is to look for another strategy that will address and work within the governing variables. In other words, given or chosen goals, values, plans and rules are operationalized rather than questioned. According to Argyris and Schön (1974), this is single-loop learning. An alternative response is to question to governing variables themselves, to subject them to critical scrutiny. This they describe as double-loop learning. Such learning may then lead to an alteration in the governing variables and, thus, a shift in the way in which strategies and consequences are framed.
> 
> &#x2013; [Chris Argyris: theories of action, double-loop learning and organizational le&#x2026;](https://infed.org/mobi/chris-argyris-theories-of-action-double-loop-learning-and-organizational-learning/) 

