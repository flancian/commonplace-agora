# Journal pages

I do journal pages.  They work well as a regular writing prompt for me.  They kind of sit in a space between my garden and my stream.  I write stuff in the journal page first, then syndicate it out to my stream (the fediverse in my case) if it's something I want to share and think it would benefit from a bit of feedback/discourse.  The thing I write in the journal I'll also copy to the relevant page in my garden, for long-term storage.

I don't really go back and review my old journal pages.  But I do like to read other people's journal pages at anagora.

