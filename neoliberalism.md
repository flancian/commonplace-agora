# neoliberalism



## Definition


### Neoliberalism is an attack on our ability to build common spaces


## Donald Trump apotheosis of Neoliberalism

Failed business man running country, dismantling state and making money from it


## Modus operandi


### Neoliberalism ring fences common foods for profits


### Gives a fiction of choice when it has privatised essentials


### Individualism


## Criticisms


### Atomization and individualism seems stupidly inefficient


### Against individualism: if there's meaning in life it's in our relationships


### Self help can be a very individualistic pursuit sometimes


## Inconsistencies


### Neoliberalism relies heavily on a strong state to push trade agreements


### Neoliberalism makes massive use of public infrastructure


### Neoliberalism is not really a free market - it's a rigged market


## Resources


### George Monbiot: "Neoliberalism: the ideology at the root of all our problems"


#### https://www.theguardian.com/books/2016/apr/15/neoliberalism-ideology-problem-george-monbiot


### Radio Open Source: "Welcome to our neoliberal world"


#### http://radioopensource.org/welcome-neoliberal-world/


### Weekly Economics Podcast: "Beginner's Guide to Neoliberalism"


#### https://soundcloud.com/weeklyeconomicspodcast/neoliberalismthebasics

