# Boris Johnson is a liar

[[Boris Johnson]].

[Lies, damned lies: the full list of accusations against Boris Johnson | Boris&#x2026;](https://www.theguardian.com/politics/2021/dec/10/lies-accusations-boris-johnson-full-list-dishonesty-christmas-party)

> Johnson is no stranger to misinformation. He has made liberal use of it in the past – the Leave campaign he chaired made claims that the UK Statistics Authority later ruled “a clear misuse of official statistics”, while as prime minister he has repeatedly spread false information, lying about the implications of his Brexit deal for Northern Ireland. The UK Statistics Authority said his claim made in the House of Commons last Monday that crime had fallen by 14% was wrong.

"Johnson is no stranger to misinformation": nice way of putting it.

> And his relationship with the truth is now so loose that it is unclear how voters are supposed to distinguish between government announcements that are true or false statements designed to distract from the disintegration of his premiership.

"loose relationship with the truth": he's a liar mate.


## Because

Some documented lies:

-   Christmas partygate
-   Wallpapergate
-   Brexit spending
-   Misleading the Queen
-   Hillsborough disaster
-   Extramarital affair
-   Journalism
-   New hospitals


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Yes definitely]]

Seems to be a lot of incontrovertible evidence in the public domain.

