# agent-based modelling

> Agent Based Models are computer models that attempt to capture the behaviour of individuals within an environment. **They are more intuitive that mathematical or statistical models as the represent objects as we see them: as individual things in the world**.
> 
> &#x2013; [Agent Based Modelling: Introduction](http://www.geog.leeds.ac.uk/courses/other/crime/abm/general-modelling/index.html) 

Contrast with [[equation-based modelling]].


## History

> Agent Based Models to some extent evolved from [[Cellular Automata]] (CA), and because of this, and because one of the first useful CA models (the Schelling model) was by a social scientist and has been re-implemented many times with ABM, it is worth saying something about CAs before we then go on to look at ABM. 
> 
> &#x2013; [Agent Based Modelling: Introduction](http://www.geog.leeds.ac.uk/courses/other/crime/abm/general-modelling/index.html) 

^ [[Schelling's model of segregation]].


## Thoughts

One thing I wonder is how do ABMs deal with models of things that are not geographically situated together?  What I've seen so far (at least in NetLogo) feels very much like how do things that are physically situated together interact.  What about if you're not physically together?  Perhaps that's where network modelling comes in.  Or perhaps you just represent things in a way that the physical geographically is collapsed / doesn't matter.

