# Updating to org-roam 1.2.3

[[org-roam]] [1.2.3](https://github.com/org-roam/org-roam/releases/tag/v1.2.3) came out on 13th November 2020.


## Update steps

-   `SPC f e U` to invoke `configuration-layer/update-packages` (spacemacs' update packages command).
    -   Updates all packages, not just org-roam - is there a way to do just one?
    -   Open org-roam - it needs to update the DB from v9 to v10.
        -   This is taking a few minutes for my >1000 files.
        -   And it has frozen&#x2026; is it because I edited a file while it was in progress?
            -   I cancelled the operation.  Now if I do `org-roam-db-build-cache`, it doesn't do anything process any more files.
            -   Restarted, nothing happened.
            -   Superficially appears OK, but I'm wondering what will be the deal with those 500 or so files that didn't get processed?  I'll just use it and see.


## Things that broke

I noticed that my backlinks weren't publishing.  This might have already been happening anyway, and I just noticed.  But here were the issues:

-   function `org-roam--get-title-or-slug` has been deprecated (https://github.com/org-roam/org-roam/commit/face683e00c2fe6c45f6958f05bef1750e7372df).  `org-roam-db--get-title` seem to be the new way to do this.
-   the backlinks query was using from and to which are now source and desc (https://github.com/org-roam/org-roam/pull/1213/commits/399c94b121a124a42493d713fd955cef58767100)

So I've updated `commonplace/org-roam--backlinks-list` function with these changes.

I noticed [here](https://github.com/jethrokuan/dots/blob/0064ea2aab667f115a14ce48292731db46302c53/.doom.d/config.el#L495) that Jethro has a slightly different function for this these days - I wonder if the logic here would be worth adding to mine too.

In addition to this, while debugging I noticed I was looking at the wrong DB location - I was personally looking at the DB in the project folder, but org-roam was looking at the DB in .emacs.d.  I have a feeling my publish.el was also looking in the project folder, but not confirmed.  For now I've updated publish.el to explicitly look at emacs.d location with `(setq org-roam-db-location "/home/neil/.emacs.d/org-roam.db")`, but I'd actually rather they both looked in the project directory.  Will have to see what happened to my `.dir-locals.el`.

UPDATE: yeah it turns out `org-roam-db-location` needs setting in dir-locals: https://www.orgroam.com/manual.html#How-do-I-have-more-than-one-Org_002droam-directory_003f
.  This seems to have been the case for a while though, so not sure why this changed recently.


## New features

The main one of interest to me looks like **Revamp org-roam-dailies**.  Although, from watching the video, it doesn't seem to do anything that I wasn't already doing?  I use `org-roam-dailies-today`, and I already had `org-roam-dailies-capture-templates` set up.

Actually now I get the message - 

> ‘org-roam-dailies-today’ is an obsolete command (as of org-roam 1.2.2); use ‘org-roam-dailies-find-today’ instead.

