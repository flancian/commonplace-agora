# Interoperability

Technical mechanism for computing systems to work together &#x2013; even if they are from competing firms.

