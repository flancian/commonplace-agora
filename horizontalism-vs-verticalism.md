# Horizontalism vs verticalism

[[Political organisation]].

On the left, how do you go about [[organising]].  _Very_ broadly speaking - [[horizontalism]] being a more distributed [[anarchist]] approach, [[verticalism]] being a more [[communist]] approach with a strong organising party.

[[Inventing the Future]] talks about this a lot. Kevin Carson gives a [lengthy critique](https://c4ss.org/content/50849) of some of their critique of [[horizontalism]] or 'folk politics'.

> In order to make a revolution, large-scale, coordinated movements are necessary, and their formation is in no way counter to Anarchism. What Anarchists are opposed to is hierarchical, power-tripping leadership which suppresses the creative urge of the bulk of those involved, and forces an agenda down their throats. 
> 
> &#x2013; [[Anarchist vs. Marxist-Leninist Thought on the Organization of Society]]

<!--quoteend-->

> Experience with multitudinous networks has led their early enthusiasts to call for more self-ordering. [[Hardt and Negri]] (2017) clarify their embrace of leaderless move- ments by stressing the need for “the institutionalization of free and democratic forms of life”, organised enough to be “able to take hold of the common” (xx). To [[Zeynep Tufekci]] (2017), the networked “signal” of movements can be self-defeating without or- ganisational “capacity”.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

<!--quoteend-->

> He [[[Frantz Fanon]]] held that spontaneous energies must find institutional cohesion.
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

