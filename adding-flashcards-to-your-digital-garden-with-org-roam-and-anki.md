# Adding flashcards to your digital garden (with org-roam and Anki)

Since starting my wiki, in addition to a note-taking tool and a writing aid, I've wanted it to be a kind of [[personal textbook]] - something that helps me memorise the knowledge that I've captured and the ideas that I'm working on.  

One seemingly good way of doing that is by turning relevant parts of it into flashcards, and revising them with [[spaced repetition]].  Andy Matuschak has [a lot of notes](https://notes.andymatuschak.org/z4eXdSMJFv2qVGXSUEKH4vdcHBrLHcFY1ZGfC) on the benefits of spaced repetition, and one in particular on using it for [application, synthesis, and creation](https://notes.andymatuschak.org/zE8PK4UUAAWK6LEcmr8jja8JdxpUxcf1FUCX).  

If notes are the seeds in your garden, then they need a bit of TLC to grow into fully-fledged, fruit-bearing ideas.  Flashcards will be my reminder to water them regularly.


## Flashcards and org

Given I'm using [[org-mode]] and [[org-roam]] for my Wiki, I could use one of the native flashcards systems - e.g. [org-fc](https://github.com/l3kn/org-fc/), [pamparam](https://github.com/abo-abo/pamparam), or [org-drill](https://orgmode.org/worg/org-contrib/org-drill.html).  

But I'm most likely to do the flashcards on my phone, not at my desktop, and none of them will work with [[orgzly]].

So, [[Anki]] is a piece of software that will be work for this.  I've used Anki it on and off in the past - it's a libre software tool for flashcards that uses spaced repetition.  There's a cross-platform desktop version, a web version, and mobile apps, so you can do it in a bunch of places.

I could see from a screenshot at https://www.orgroam.com that someone was using some kind of Anki system with org-roam - [I asked in the forum](https://org-roam.discourse.group/t/org-roam-and-anki/589/2), and the tool in question is [anki-editor](https://github.com/louietan/anki-editor).

So I set that up - notes on how below. It's worth noting  that anki-editor is an org-mode thing, not org-roam specific, but as I'm trying to mix my flashcards and my org-roam zettelkasten-ish personal wiki, I'll probably have more of an org-roam slant here.


## Install Anki

First you'll need Anki.  You can get it [from the site](https://apps.ankiweb.net/), but as I'm on Ubuntu I just went for it straight from the repos.

```nil
sudo apt install anki
```


## Install AnkiConnect

[AnkiConnect](https://github.com/FooSoft/anki-connect) is an Anki extension that lets external apps interact with Anki - for example, creating cards in your decks.

I followed the [installation instructions](https://github.com/FooSoft/anki-connect#installation) and all worked fine.  I wasn't prompted to restart Anki like it said I would be, but I checked http://localhost:8765/ and it looked fine.


## Install anki-editor

[anki-editor](https://github.com/louietan/anki-editor) is the Emacs extension that let's you push your cards from your org files into your Anki decks.  It's on Melpa, so you can just install it however you would usually do so in your flavour of Emacs.  I'm using [[spacemacs]], so I added `anki-editor` in to `dotspacemacs-additional-packages` in my [.spacemacs](https://github.com/ngm/dotfiles/blob/master/spacemacs) and gave everything a refresh with `SPC f e R`.


## Creating flashcards

Now you can create flashcards in your org files, and push them to Anki via AnkiConnect.

`anki-editor-insert-note` will create a new flashcard. [Here's an example](https://gitlab.com/ngm/commonplace/-/raw/31ea46b1e2be45b18ff14e14232598842cd5181a/20200725132152-agency.org).  

Then `anki-editor-push-notes` will push it to Anki.


## Syncing Anki


### From desktop

The easiest way to sync your Anki decks is via [ankiweb](https://ankiweb.net/about).  (I don't think ankiweb is libre software, but you can set up your own self-hosted equivalent with [anki-sync-server](https://github.com/ankicommunity/anki-sync-server) if you want).

To use ankiweb, just click Sync from your desktop Anki, create an account on ankiweb, and then log in.


### To your (Android) mobile

Install [AnkiDroid](https://github.com/ankidroid/Anki-Android).  It's [on FDroid](https://f-droid.org/en/packages/com.ichi2.anki/).  Log in, hit sync, and away you go! 


## Improvements

Some things it might be nice to improve:

-   I feel like I'm maintaining the flashcards separately from the body of my notes - it's a bit of a duplication of effort.  It'd be good to get a flow where the flashcard is just part of the note as is, and I can pull it out without duplicating it.
-   I've only been pushing one flashcard at a time at the moment, when the buffer is open.  I'll probably add a Make step that iterates all my notes and publishes flashcards if found.
-   My flashcards are (probably?) noise that I don't want added to my published digital garden - perhaps they should be filtered out of the publish site.


## Summary

I've now got a fairly simple flow for making flashcards from my wiki notes.  I'm hoping this will have a dual purpose of helping me to memorise the things that I'm learning and thinking about, and will also prompt me to regularly [[tend to my wiki]].  

I'm sure I'll follow up with notes soon on how this is all working out.

