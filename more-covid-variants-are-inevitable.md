# More Covid variants are inevitable

[[Coronavirus]]

[What lies on the other side of the UK’s Omicron wave? | Omicron variant | The&#x2026;](https://www.theguardian.com/world/2022/jan/13/what-lies-on-the-other-side-of-the-uks-omicron-wave)


## Because

-   The key to reducing the risk of new variants is driving down case rates globally
-   But to date, [[vaccine hoarding]] and the widespread use of boosters in wealthier countries has left vast swathes of the world under-vaccinated


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

