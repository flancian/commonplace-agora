# How’s That Open Source Governance Working for You?

URL
: https://hackernoon.com/hows-that-open-source-governance-working-for-you-mphv32ng

Author
: [[Nathan Schneider]]

Discusses  [[free software governance]]: [[implicit feudalism]], [[Benevolent Dictator For Life]], [[The Tyranny of Structurelessness]].

[[There is a layer of governance infrastructure missing from the web]].

See [[CommunityRule]].   [[No Servers!  No Admins!]]

> Under the rhetoric of openness and meritocracy, an entrenched and disguised hierarchy reigns.

<!--quoteend-->

> Regardless, take a moment to notice the governance layers of the communities and projects you are part of. What is stated, and what is not? How would you make explicit the ways they are operating with now, and what do you wish their shared agreements would say?

