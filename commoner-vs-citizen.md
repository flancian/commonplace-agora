# Commoner vs Citizen

> [[Citizen]], also called “a national,” identifies a person in relation to the nation-state and implies that this is a person’s primary political role. The term “citizen” is often used to imply that noncitizens are somehow less than equal peers or perhaps even “illegal.” A more universal term is [[Commoner]].
> 
> &#x2013; [[Free, Fair and Alive]]

