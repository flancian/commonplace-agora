# Spritely

Spritely is a distributed social network written in Racket. 

-   https://dustycloud.org/blog/spritely/

[Spritely Goblins](https://dustycloud.org/blog/announcing-goblins/) is an [[actor model]] implementation in [[Racket]] that is part of it.

-   https://octodon.social/@cwebber/104813883090154862
-   https://dustycloud.org/blog/if-you-cant-tell-people-anything/

