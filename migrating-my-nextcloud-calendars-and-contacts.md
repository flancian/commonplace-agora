# Migrating my NextCloud calendars and contacts

From one [[Nextcloud]] instance to another.

I have them in NextCloud so that I can sync them to both a desktop app and my phone app.

I was tempted to use something more lightweight, like Radicale or Baikal.  But Radicale on YunoHost is old, and Baikal didn't work with LDAP and then I couldn't see how to import calendars.

So - NextCloud is it. I guess as a bonus, I get a web client for my calendars as well, if I want.


## Moving calendars and contacts between instances


### Calendars

-   Download my 'Personal' .ics file from the old Nextcloud instance.

-   Upload that to the new instance.
    ![[images/screenshots/nextcloud-import.png]]

-   Download [bank holidays calendar](https://www.gov.uk/bank-holidays/england-and-wales.ics) from gov.uk.  https://www.gov.uk/bank-holidays and import.


### Contacts

-   Download contacts from old location.  This will be a .vcf file.
    
    ![[images/screenshots/nextcloud-contacts-download.png]]

-   Upload into new location.
    
    ![[images/screenshots/nextcloud-contacts-import.png]]


## Updating client apps to point at new instance

You first need to get the links that the client apps are going to use.

-   CalDav link for your calendar:
    
    ![[images/screenshots/nextcloud-caldav-link.png]]

-   CardDav link for your contacts:

![[images/screenshots/nextcloud-carddav-link.png]]


### Desktop


#### Calendar

-   I couldn't get much to happen in the default calendar in Mint (California?).  Buttons in the interface working, nothing happening when I import a calendar.

-   So I tried with Evolution instead, and that works:

![[images/screenshots/evolution-new-caldav-calendar.png]]

-   Then as it turns out, once you've set a calendar up in Evolution, you can pick those calendars up in the Mint calendar if you want to.


### Android


#### Davx5

You need Davx5 to pull stuff from CalDav and CardDav for use by Etar.

For this I actually provide the credentials for the NextCloud instance, not the CalDav / CardDav links themselves.  (Might be nice if I could do that on the desktop, too.)

-   Install Davx5 (I prefer from Fdroid).
-   **Login with url and user name**
-   provide URL of NextCloud instance
-   check the things you want to sync.


#### Calendar

Etar.
In Etar I turned off syncing of all my work calendars.  Don't want to see them on my phone.


## Conclusion

It's pretty sweet when it all syncs up!

