# Web 3.0

A fuzzy, amorphous term for some of the upcoming trends for the web.

As might be expected, different people have different ideas for what it is.

Some themes:

-   [[Decentralisation]]
-   [[Semantic web]]
-   Spatial and immersive

-   Contrast with [[Web 1.0]] and [[Web 2.0]].


## My hot takes

Web 3.0 IMO should be: as weird as [[Web 1.0]], as easy to do and connect as [[Web 2.0]].  Agency, privacy, and ownership.

It should be more data ownership than self hosting. Self hosting is one route to ownership, but can be exclusionary.  You shouldn't need to be a server administrator.

There is massive blockchain hype around the decentralisation piece.  But this is putting the tech choice before the design choice.

There is a focus on some of the social and platform-based stuff, but what about the less social stuff? Banking, services, whatnot. ([How solid is Tim’s plan to redecentralize the web? — Irina Bolychevsky](https://shevski.com/writing/how-solid-is-tims-plan)) 

Web 3.0 should also be about (dis)connectivity.  [[Designing for intermittency]].  Moving away from 'always on'.  Embracing things that are able and happy to go offline.  To reduce energy consumption.  Blockchain is the opposite of this.  h/t to peter molnar in indieweb-chat for this thought.


## Readings

-   [Can the Real Web 3.0 Please Stand Up? - RTInsights](https://www.rtinsights.com/can-the-real-web-3-0-please-stand-up/)


## Quotes

> The web3 concept that is slowly congealing is an interesting inversion of web 2.0. Back then the idea was "build social websites and figure out the money part later." Today it's "build money stuff and figure out some non-speculative use for it later.""
> 
> &#x2013; https://mobile.twitter.com/Pinboard/status/1448007556656369666

