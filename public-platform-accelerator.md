# Public Platform Accelerator

> The PPA would be tasked with supporting the development of platforms that replicate existing platforms as well as new platforms that address future needs and concerns, but with a key difference. The platforms created would be not-for-profit, with operational models designed to serve their users and workers, not just maximise returns to its investors. The governance of each platform would be multi-stakeholder. Crucially, these platforms would hold anti-surveillance and data privacy as core values, as well as including mechanisms to allow participation and governance rights to be shared broadly amongst the user base.
> 
> &#x2013; [[Privacy, censorship and social media- The Case for a Common Platform]]

