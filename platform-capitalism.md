# platform capitalism

> By creating the digital infrastructure that facilitates online communities, platform companies have inserted value capture mechanisms between people seeking to interact and exchange online
> 
> &#x2013; [[Platform socialism]]

