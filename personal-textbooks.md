# Personal textbooks

I would like my wiki / knowledge base to facilitate the reinforcement of my knowledge about the things I'm interested in.  So for example, it could be used as a base for making flashcards in Anki that I use to improve recall about specifically the things I'm interested in. - more on this from Andy https://notes.andymatuschak.org/z4bR1HVvDUhMXDm5SJB4Tiw4xGbrm9AfXWgbc

-   https://www.remnote.io/homepage

Adding in little sketches and putting it in to a text adventure format, also helps this.  Turning it into a personal [[memory palace]].

