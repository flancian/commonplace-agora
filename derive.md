# dérive

-   "one of the basic pratices of [[Situationism]]"

> Dérives involve playful-constructive behavior and awareness of [[psychogeographical]] effects, and are thus quite different from the classic notions of journey or stroll. 
> 
> &#x2013; [[Theory of the Dérive]]

<!--quoteend-->

> In a dérive one or more persons during a certain period drop their relations, their work and leisure activities, and all their other usual motives for movement and action, and let themselves be drawn by the attractions of the terrain and the encounters they find there.
> 
> &#x2013; [[Theory of the Dérive]]

<!--quoteend-->

> Chance is a less important factor in this activity than one might think: from a dérive point of view cities have psychogeographical contours, with constant currents, fixed points and vortexes that strongly discourage entry into or exit from certain zones. 
> 
> &#x2013; [[Theory of the Dérive]]

<!--quoteend-->

> [[Psychogeographers]] advocate the act of becoming lost in the city. This is done through the dérive, or “drift”.
> 
> &#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

<!--quoteend-->

> Because **purposeful walking has an agenda**, we do not adequately absorb certain aspects of the urban world. This is why the drift is essential to psychogeography; it better connects walkers to the city.
> 
> &#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

[[Theory of the Dérive]] 

