# 2021-04-16

-   I created a [[CTZN]] account, I am neil@ctzn.one.  I like many things about [[Hypercore Protocol]] so interested to see how this works.  I am guessing it has more of a P2P model, similar to the Beaker chat, with the servers acting as 'pubs'.  That said, the idea of another chat silo doesn't fill me with joy, so I wonder where the data is stored.  I still think the [[IndieWeb]] (or [[Solid]]) approach is the best philosophy (for me personally).

-   Reading up a bit more on [[speculative outline]]s from [[Andy Matuschak]] - [here](https://notes.andymatuschak.org/Evergreen_notes?stackedNotes=z3PBVkZ2SvsAgFXkjHsycBeyS6Cw1QXf7kcD8&stackedNotes=z2uXyfV67dnWLUKg1iDbsrHk3DGjtNWTxSTah&stackedNotes=z6Mx6PrJjGCf2akGM9pvoZ5Nk3EozcZcc9zHx).  I like this idea that every time you write a new evergreen note, you add it to a speculative outline and let them grow organically.  On that point, I prefer [[Ton]]'s nomenclature - notes, [[notion]]s and [[emergent outline]]s.

-   I currently make far more notes than notions.  But this pipeline makes it clearer in my head about how notes become notions become outlines.  So I feel now I can see why I want to make more concerted effort to make notions.

-   A useful note for me from [[Andy Matuschak]] on this - [How to process reading annotations into evergreen notes](https://notes.andymatuschak.org/How%20to%20process%20reading%20annotations%20into%20evergreen%20notes).  I have plenty of reading annotations.  It's about processing them into notions.  Let's look at a recent one.
    
    -   I think while I'm looking at a topic that is new to me, my notions might often be questions.  This is fine:
    
    > Questions also make good note titles because that position creates pressure to make the question get to the core of the matter. [&#x2026;] The goal [&#x2026;] is to eventually drop the question mark, refactoring it into declarative/imperative notes.
    > 
    > &#x2013; [Prefer note titles with complete phrases to sharpen claims](https://notes.andymatuschak.org/Prefer%20note%20titles%20with%20complete%20phrases%20to%20sharpen%20claims)

-   [[Vanguard stack]]s.

