# 2022-02-20

-   I am writing this from [[MX Linux]]!  My notes on [[Setting up a new box]] have been super helpful - glad I made those.  I'm fleshing them out now as I set up this fresh box.

-   [[Problems with an old mouse]] now!  Freezing/dying occassionally.  What's going on, this stuff seemed to have all been ironed out.  Maybe the mouse is busted.
    -   Actually it's been OK for a while since plugging in to a different USB port.  Maybe it's the port.

-   [[syncthing]] is amazing.  I have no idea how it works most of the time, but it does (most of the time).  It just magically picked up a suggested peer, presumably because they're on the same network.

-   Had fun clicking around on [[Thompson Morrison]]'s wiki.
    -   Came across it as he linked to http://wellspring.fed.wiki/view/wiki-nature in the FedWiki chat.
    -   From there found stuff on complex systems, [[autopoesis]], etc.  Rad.
    -   Found a little sentence on [[Occupy and stigmergy]]  wiki.  Very interesting to me.
    -   I've added Thompson to [[My garden circles]].

-   Bookmarked: [[The Stigmergic Revolution]]

