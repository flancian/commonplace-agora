# 2021-05-02

-   I'd quite like a more automated way of copying my notes here in these daily logs more easily in to some kind of [[stream]] where people see it and reply.  I've still not quite cracked that nut in a way that I would like, so I just periodically manually copy stuff in to my stream.  (Mostly the Mastodon stream at the moment.  I'd prefer to push it to my website stream first, but that's even more friction.)  It's not the end of the world, but would be nice to have it all synced up.

-   [[Citizen Shift]].
    -   I saw Jon Alexander talk on this at TICTeC one year.  I liked the idea - make it easy to regularly exercise [[civic muscle]].  Not just every 4 to 5 years of an election cycle.

-   Listened: [[Nathan Schneider - Cooperatives, the Commons and Ownership]]

