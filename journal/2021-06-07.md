# 2021-06-07

-   all fork / no merge (found via [[Phil Jones]])
    -   https://twitter.com/gordonbrander/status/1348315480319135744
    -   some good food for thought on p2p [[knowledge commons]]

-   [[chemacs2]].  I've been wanting to give [[Doom Emacs]] a spin for a while, but also not wanting to be left stranded with stuff not working as I get familiar with it.  Looks like chemacs will help with that nicely - it lets you switch easily between configs.

