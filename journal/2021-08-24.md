# 2021-08-24

-   [[Complexity and the left]].
    -   I've come across quite a few books recently that make a connection between leftist politics, climate change and one or multiple of [[complexity science]], [[complex systems]], [[cybernetics]], and [[systems thinking]].
        -   [[Doughnut Economics]]
        -   [[Free, Fair and Alive]]
        -   [[Anarchist Cybernetics]]
        -   [[Emergent Strategy]]
        -   [[Cybernetic Revolutionaries]]
    
    -   What is the common thread of all of these?
    
    -   And [@lollonero](https://antinetzwerk.de/@lollonero) just shared [[Komplexität «Chaostheorie» und die Linke]]. Not translated yet but according to lollonero it is about how the left should catch up to complexity science.

-   Hmm, I wonder if you could take the simple idea of the doughnut from Doughtnut Economics and use it as a frame for a viable system that is somewhere in between horizontal and vertical organisation.  The hole being disorganised individualism, the overshoot outside the perimeter being too much central authority.  The tasty bit in the middle being a viable system that balances autonomy and purposeful direction for the movement.

