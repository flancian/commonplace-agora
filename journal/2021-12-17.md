# 2021-12-17

-   Interesting thoughts on [[wikilinks]] and [[slashlinks]] from [[Gordon Brander]].
    -   [[What if links weren't meant to be prose?]]

-   Listening: [[Jeremy Lent, "The Web of Meaning: Integrating Science and Traditional Wisdom to Find our Place in the Universe"]] 
    -   Like it. Intersection of [[science and spirituality]]. Qi and li. [[Objects and relations]].
    -   [[Dukkha]]. [[Eudaimonia]].

-   This weekend for [[node club]] I will be looking at [[value flows]] (and [[ValueFlows]]).  I keep on seeing the concept pop up, and respect the people that work on the protocol&#x2026; so I'd like to get a bit better handle on both.

