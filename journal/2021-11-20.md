# 2021-11-20

-   Looking at the [[Triad of Commoning]] for [[node club]] this weekend.
    -   It's what they call the collection of their patterns for commoning in [[Free, Fair and Alive]].
    -   Some patterns from the [[Provisioning Through Commons]] part:
        -   [[Use Convivial Tools]]
        -   [[Rely on Distributed Structures]]
        -   [[Creatively Adapt & Renew]]
        -   I dig it

-   [[Flancia Collective]]

