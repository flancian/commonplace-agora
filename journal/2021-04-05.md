# 2021-04-05

-   Read [[404 Page Not Found]].
    -   Good article on what we lost when web 1.0 became web 2.0.  How the bit social media platforms killed creativity and agency.
-   Kicked off an emergent outline - [[Reweirding the web]].
-   [[The Pattern Language of Project Xanadu]].
-   What I do when I'm [[Getting a phone]].

