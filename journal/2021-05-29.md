# 2021-05-29

-   [[YunoHost]] has a really interesting governance structure, involving special interest groups, a council, and consensus for decision making.
    -   https://yunohost.org/en/yunohost_project_organization
-   Did a bit of gardening on [[Interlinking wikis]].
-   Having a '[[nowtions]]' / 'top of mind' page in my digital garden is working well.  I add new notions to it as they crop up, and visit it regularly which works as lowkey spaced repetition.
-   "we cannot depend upon structures to do the work of culture" &#x2013; [[Free, Fair and Alive]]
-   Jammed: [Moderat - Seamonkey](https://bpitch.bandcamp.com/track/seamonkey)
-   [[Maggie Appleton]] has dropped an update to her awesome [[A Brief History & Ethos of the Digital Garden]] page, and it now has a section The Six Patterns of Gardening.  The patterns she lists chime excellently with how I view [[digital garden]]s.  Her page is the best intro to them, IMO.
    -   https://maggieappleton.com/garden-history
-   [[Culture and structure]]
-   [[IndieForums]] looks nice.  Seems like it might be a good catalyst for some [[Blogchains and hyperconversations]].
    -   https://indieforums.net
-   [[When did you join the IndieWeb?]]

