# 2021-08-28

-   Rereading [[All Watched Over by Machines of Loving Grace]] for Agora [[node club]].

-   Trying [[fixing a VTech My Laptop]].

-   [[Hyphalinks]]

-   [[Problems with SD Card on Android / LineageOS]]. Since moving to [[LineageOS]] ([[Installing LineageOS on Samsung Galaxy S5]]) I've had a bunch of problems with my SD Card.  Not sure if it's Lineage specific, or just Android 11 related.

