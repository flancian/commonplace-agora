# 2022-02-18

-   An unreasonable amount of my time today went on getting a monitor to display at the right resolution.   [[xrandr: Configure crtc 0 failed problems]].
    -   On the plus side I ended up trying out [[MX Linux]] in the process, mainly as a cheap way to avoid Wayland, but it seems pretty nice.  I like that it's built on [[Debian]], but not too far removed from it, and is XFCE rather than Gnome.

