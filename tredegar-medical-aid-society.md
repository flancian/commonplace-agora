# Tredegar Medical Aid Society

Really interesting to learn that the [[NHS]] was part inspired by the Tredegar Medical Aid Society (https://en.wikipedia.org/wiki/Tredegar_Medical_Aid_Society).

It was a mutual society providing health care free at the point of use in South Wales.  Nye Bevan, who spearheaded the foundation of the NHS, was from Tredegar.

h/t NEF podcast on public ownership (https://neweconomics.org/2019/02/weekly-economics-podcast-public-ownership-2.0)

