# Morecambe Bay

![[photos/heysham-promenade.jpg]]

The promenade at Heysham.  Blue sky with clouds, flat sea, and concrete promenade, stacked on top of each other.  The tide was in.

