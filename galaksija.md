# Galaksija

> The Galaksija computer was a craze in 1980s Yugoslavia, inspiring thousands of people to build versions in their own homes. The idea behind them was simple – to make technology available to everyone.
> 
> &#x2013; [[Socialism's DIY Computer]]

