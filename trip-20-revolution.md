# Trip 20: Revolution

A
: [[podcast]]

URL
: https://novaramedia.com/2021/11/07/trip-20-revolution/

Series
: [[#ACFM]]

> how the idea of political [[revolution]] ever became thinkable, and if it’s still thinkable today. Was the [[sexual revolution]] a real revolution? How did disillusionment with [[Soviet communism]] affect our political imagination? And who is the [[revolutionary subject]]?

