# Agroecology

-   A technique for [[sustainable agriculture]]
-   A [[convivial tool]] that can be used, shared and improved upon by anyone

[[I like agroecology]].

> Techniques for stable, eco-responsible agriculture such as permaculture and agroecology are convivial tools because anyone can use and share them, and contribute to their improvement.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> “the application of ecology to the design and management of sustainable agroecosystems”
> 
> “a whole-systems approach to agriculture and food systems development based on traditional knowledge, alternative agriculture, and local food system experiences”
> 
> “linking ecology, culture, economics, and society to sustain agricultural production, healthy environments, and viable food and farming communities”
> 
> Agroecology has also been described as the “science of [[sustainable agriculture]]”, and seeks to determine best method approaches for sustainable options." 
> 
> &#x2013; [Agroecology - P2P Foundation](https://wiki.p2pfoundation.net/Agroecology)

