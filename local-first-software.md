# Local-first software

See: [Local-first software: You own your data, in spite of the cloud](https://www.inkandswitch.com/local-first.html).

Local-first software:

> prioritizes the use of local storage (the disk built into your computer) and local networks (such as your home WiFi) over servers in remote datacenters.

<!--quoteend-->

> The data synchronization need not necessarily go via the Internet: local-first apps could also use Bluetooth or local WiFi to sync data to nearby devices


## Why?

-   [[Cloud software has benefits]], but [[Cloud apps are problematic]].
-   [[Centralised applications are authoritarian]].
-   Local-first software gives you agency and ownership of your data.
-   [[You shouldn't need permission to access your own files]].
-   [[Servers should provide a supporting role, not a central role]].

> Live collaboration between computers without Internet access feels like magic in a world that has come to depend on centralized APIs.


## Challenges

> In local-first apps, our ideal is to support real-time collaboration that is on par with the best cloud apps today, or better. Achieving this goal is one of the biggest challenges in realizing local-first software, but we believe it is possible:

<!--quoteend-->

> The developers of mobile apps are generally experts in end-user app development, not in distributed systems. We have seen multiple app development teams writing their own ad-hoc diffing, merging, and conflict resolution algorithms, and the resulting data sync solutions are often unreliable and brittle.


## Examples

> This is the case because a Git repository on your local filesystem is a primary copy of the data, and is not subordinate to any server.

