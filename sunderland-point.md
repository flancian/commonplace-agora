# Sunderland Point

I cycled to a tiny place called [[Sunderland Point]] today.

To get there you have to go via a small road through a [[salt marsh]] that may or may not be passable, depending on the tide.  I like salt marshes.

![[photos/sunderland-point.jpg]]

According to Wikipedia, "Sunderland is unique in the United Kingdom as being the only community to be on the mainland and yet dependent upon tidal access."

It's got a shitty history as being a port that was part of the slave trade.  Sadly there's quite a bit of that history around Lancaster.

