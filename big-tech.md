# Big tech

Generally refers to Google, Facebook, Amazon, Apple, Microsoft.

But seems to often refer to [[platform capitalism]] in general.  So your Twitters, Airbnbs, Netflix, etc, as well.  Even if not quite so big.

[[I dislike big tech]].

> Needless to say, web services are a powerful force that can shape politics and economics around the globe. Even the slightest updates to regulatory law in the US, where many of these platforms are based, can result in profound global reverberations that are difficult to predict.
> 
> &#x2013;  [Breaking Tech Open: Why Social Platforms Should Work More Like Email - The Re&#x2026;](https://thereboot.com/breaking-tech-open-why-social-platforms-should-work-more-like-email/) 

<!--quoteend-->

> Similarly, the large platforms are all renting Amazon cloud services for exorbitant monthly amounts, like Twitch ($15M/month), Linkedin ($13M/month), and Twitter ($7M/month on top of $10M/month to Google Cloud). This doesn’t take into account many other operating costs, like personnel salaries and software maintenance. 
> 
> &#x2013; [Toward a Digital Economy That’s Truly Collaborative, Not Exploitative](https://thereboot.com/toward-a-digital-economy-thats-truly-collaborative-not-exploitative/) 

<!--quoteend-->

> In the realm of digital technology, it is commonly known that today’s tech giants have built much of their empires on a code commons. The open meadows of collaboratively written code and generously shared repositories, published under permissive licences, are treated as fair game — and fenced off into proprietary products. 
> 
> &#x2013; [[Seeding the Wild]]

