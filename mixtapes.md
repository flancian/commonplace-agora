# Mixtapes



## In progress

A place to build up some playlists.


### ambient

-   Deaf Center - Social Lucy Waltz
-   Eluvium - Under the Water it Glowed
-   Windy & Carl - Beyond Asleep
-   Burial - Forgive
-   Aphex Twin - Stone in Focus
-   Eluvium - Zerthis Was a Shivering Human Image
-   Stars of the Lid - Another Ballad for Heavy Lids
-   Aix Em Klemm - The Luxury of Dirt
-   lusine - still frame
-   Eluvium - Everything to Come
-   Keith Fullerton Whitman - Twin Guitar Rhodes Viola Drone (For Lamonte Young)
-   Mount Kimbie - Four Years and One Day (too much discernible instruments??)


### piano magic

-   The Dead Texan - When I see Scissors I can't Help but Think of You
-   Nils Frahm - Forever Changeless


### sad electric

-   Yasume - The Prevailing Wind
-   Proswell - Chesterfield
-   brothomstates - vs bill yard


### hi energy / post punky

-   gogogo airheart - when the flesh hits
-   broadcast - america's boy


### for ellie

-   mr projectile - love here
-   prefuse 73 - afternoon love in
-   kettel - i live you
-   clark - ted
-   proem - take your pants off
-   yasume - rengoku
-   arovane - instant gods out of the box
-   lusine - still frame
-   tourist - elixir
-   rachel's - cuts the metal cold
-   eluvium - everything to come


### take your pants off

-   marumari - baby m
-   prefuse 73 - hot winter's day
-   jimmy edgar - i wanna be your std
-   proem - take your pants off


### nice

-   Papa M - Roses in the Snow
-   State River Widening - Among The Corn Rose
-   Dirty Three - I Really Should've Gone Out Last Night
-   Palace Music - Gulf Shores


### As I Lay Dying

-   August Born - Last Breath of the Bird
-   Eluvium - Everything to Come
-   Windy & Carl - Instrumental II
-   Palace Music - West Palm Beach
-   Garth Stevenson - A Love Song

