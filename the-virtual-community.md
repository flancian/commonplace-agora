# The Virtual Community

> We temporarily have access to a tool that could bring conviviality and understanding into our lives and might help revitalize the public sphere. The same tool, improperly controlled and wielded, could become an instrument of tyranny. The vision of a citizen-designed, citizen-controlled worldwide communications network is a version of technological utopianism that could be called the vision of "[[the electronic agora]]." In the original democracy, Athens, the agora was the marketplace, and more&#x2013;it was where citizens met to talk, gossip, argue, size each other up, find the weak spots in political ideas by debating about them. But another kind of vision could apply to the use of the Net in the wrong ways, a shadow vision of a less utopian kind of place&#x2013;the Panopticon.

<!--quoteend-->

> On the one side, the anti-corporate purity of the New Left has been preserved by the advocates of the 'virtual community'. According to their guru, Howard Rheingold, the values of the counter-culture baby boomers will continue to shape the development of new information technologies.
> 
> &#x2013; The Californian Ideology

