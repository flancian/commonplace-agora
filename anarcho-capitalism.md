# Anarcho-capitalism

> Anarcho-capitalism is a [[political philosophy]] and [[economic theory]] that advocates the elimination of centralized states in favor of a system of [[private property]] enforced by private agencies, [[free market]]s and the [[right-libertarian]] interpretation of [[self-ownership]], which extends the concept to include control of private property as part of the self. 
> 
> &#x2013; [Anarcho-capitalism - Wikipedia](https://en.wikipedia.org/wiki/Anarcho-capitalism) 

What a terrible, ridiculous concept.  Reification of private property and [[capitalism]] is not [[anarchism]].

