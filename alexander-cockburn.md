# Alexander Cockburn

> There is never finality in the display terminal's screen, but an irresponsible whimsicality, as words, sentences, and paragraphs are negated at the touch of a key. The significance of the past, as expressed in the manuscript by a deleted word or an inserted correction, is annulled in idle gusts of electronic massacre

