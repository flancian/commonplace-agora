# YunoHost Invidious upgrade gives "Not enough free space"

Trying to upgrade [[Invidious]] on [[YunoHost]].  It is giving the error:  '168261 Not enough free space on '/home/yunohost.backup/archives'.

I find this hard to believe, as there's 21Gb free on the server.  Invidious is a bit of a pain sometimes.

Looking into the actual install log, I see:

> Not enough space at /home/yunohost.backup/archives (free: 9481981952 / needed: 12674660449)

Dunno if that's bits of bytes, but it's saying roughly that the backup needs 11.8Gb of space, and there's only 8.8Gb available.

First of all, why the fuck is Invidious needing 11.8Gb of space for backups.  Second, I have 21Gb free on disk total.  What gives?


## <span class="timestamp-wrapper"><span class="timestamp">[2021-06-26 Sat]</span></span>

Sorted this out now - it was simply this:

> The table videos grows a lot and needs the most storage.   https://github.com/iv-org/documentation/blob/master/Database-Information-and-Maintenance.md

You can clean it up on YunoHost using following commands:

```bash
sudo su postgres
psql invidious -c "TRUNCATE TABLE videos"
```

