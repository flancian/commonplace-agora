# Solar Protocol

URL
: http://solarprotocol.net/

A fun (art?) project that is building a network of solar-powered servers that work together to host a web site.  Wherever currently has the most sun takes over the hosting.

Dunno how much of these things translate from art/fun to practical use, but good for raising questions and awareness about where we get our energy from.

Would be interesting to combine Solar Protocol with distributed content on something like [[Hyperdrive]] or [[IPFS]].

[[Solarpunk]].

