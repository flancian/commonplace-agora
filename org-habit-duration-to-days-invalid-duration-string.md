# org-habit-duration-to-days: Invalid duration string

org-habit seems not to like hourly repeaters.

https://git.savannah.gnu.org/cgit/emacs/org-mode.git/tree/lisp/org-habit.el#n159

Might be a conscious choice?  Or a bug?  Not sure.
For now, I don't need the thing where I have the hourly repeater to be styled as a habit.

