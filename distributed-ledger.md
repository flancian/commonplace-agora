# Distributed ledger

> The general point of ledger systems is to use peer-to-peer networks to verify the authenticity of a unique digital object.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> What matters about distributed ledger technology is the new and different affordances that it enables. It’s an open question who will be first and most influential in leveraging those affordances.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> [[Holochain]] is an attempt to secure an important beachhead for commoners, even if it will also be used, inevitably, for less elevated purposes. The appeal of ledger technologies, whether Holochain or others still in the works, is their potential to create durable new affordances for commoning.
> 
> &#x2013; [[Free, Fair and Alive]]

[Economic Event Protocol for Distributed Ledgers — Economic Networks](https://write.as/economic-networks/economic-event-protocol-for-distributed-ledgers)

