# Playing music on the web

Is there some kind of web player or protocol or similar that you can put a link to a track, and if the person has some legit access to it, it’ll play for them?

So say I link a few tracks on a page. One is on archive.org, so that’s fine for everyone, hit play and off you go. One is under copyright. If the viewer has it in their library they’ve configured their browser to know about on their machine somewhere, they can play it. If they’ve bought it on bandcamp, they can play it. If they have a Spotify account, they can play it.

Don’t know if that makes sense. But I’d like to just link to a track in a webpage and have it playable in the page, regardless of where the visitor ‘owns’ it.

-   song.link and odesli.co were recommended

