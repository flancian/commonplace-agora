# wiki links are my tags

For a brief period, I made a concerted effort to tag and categorise my posts.  It seemed like a means of picking out themes in my writing, maybe discovery for me and perhaps also for people visiting my site.

But it always felt a bit weird and disconnected for me - I've written something, now I have to think about where it fits in to some abstract notion of my content. And it's not fun.  It's just a thing that sits there staring at you in WordPress when you make a post.  Hey you, eat your greens, and tag your posts.  (If you don't give it a category, you get the 'Uncategorized' label of shame). So I kind of gave up on it again.  

But as I've started heavily hyperlinking stream posts to my wiki, I've realised that pretty much my [[wiki links are my tags]].   I'm just tagging as I'm linking things together.   And that's fun! (for me anyway&#x2026;). 

![[Wiki_links_are_my_tags/2020-05-09_11-18-18_screenshot.png]]

I've styled the wiki links diferent from other hyperlinks.  I quite like it personally, but part of me wonders if it's just visual noise.  I'd quite like if I could get WordPress to pick up those links and set them as tags, mainly just as a easy way to have them separate from the main body of the post.

