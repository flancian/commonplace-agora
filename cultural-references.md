# Cultural references

Been doing a small bit of wiki gardening on my [[books]] pages today.  Nothing major, just starting to link the different ideas from different books to the concepts they're discussing.  

I really like how [Nadia Eghbal](https://nadiaeghbal.com/)'s writings are peppered with cultural references.  When I'm getting to the point where I'm writing longer-form articles on an idea, I would like them to have a similar cross-pollination vibe.  Like [Mark Fisher](https://k-punk.org), too.

