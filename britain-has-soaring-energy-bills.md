# Britain has soaring energy bills

[[Britain]] has soaring [[energy bills]] in [[2022]].

> Households will face a record energy bill increase of 54% from April after the regulator lifted the cap on default tariffs to £1,971
> 
> &#x2013; [British households face record 54% energy bill rise as price cap is raised | &#x2026;](https://www.theguardian.com/money/2022/feb/03/uk-households-face-record-54-energy-bill-rise-as-price-cap-is-lifted) 

-   [Energy bills: what help is Sunak offering on the cost of living crisis? | Ene&#x2026;](https://www.theguardian.com/money/2022/feb/03/what-help-is-rishi-sunak-offering-cost-of-living-crisis-energy-price-cap)

