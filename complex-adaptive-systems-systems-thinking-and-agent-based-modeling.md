# Complex Adaptive Systems, Systems Thinking, and Agent-Based Modeling

> Systems thinking and complex adaptive systems theories share a number of components, namely emergence, self-organization, and hierarchies of interacting systems.

