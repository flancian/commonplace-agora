# COUNTDOWN TO THE CLIMATE SUMMIT #1: How did we get here?

URL
: https://www.cheerfulpodcast.com/rtbc-episodes/countdown-climate-summit-how-did-we-get-here

Topics
: COP / [[COP 26]] / [[Climate change]]

Featuring
: [[Alice Bell]]

Some background history on [[climate science]], [[climate denial]], and [[COP]]s.

