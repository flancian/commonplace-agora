# Digital public assets

We need to do more to ensure open government data is used for purposes with social value.  Data sets related to society are digital public assets that should be contribute to common wealth.

It has been misused in the past as a means to legitimate the privatisation of public services.

I need a few read throughs of this report to take it all in&#x2026; but it has a lot of good food for thought.

> It is this new relationship, in which the costs of producing digital public assets fall largely on the public sector and society, but the surplus value so often comes to be realised by large digital platform companies and the financial services industry, that perhaps most deserves our attention 
> today.
> 
> &#x2013; [Digital Public Assets: Rethinking value, access and control of public sector data in the platform age](https://www.common-wealth.co.uk/digital-public-assets.html)

<!--quoteend-->

> On the contrary, we might contend that the establishment of government infrastructure to collect and publish publicly-held data for use by the private sector constitutes taxpayer subsidisation of a commercially valuable resource
> 
> &#x2013; [Digital Public Assets: Rethinking value, access and control of public sector data in the platform age](https://www.common-wealth.co.uk/digital-public-assets.html)

<!--quoteend-->

> As the academic and open data advocate Rob Kitchin argues to the contrary, “[open] data might well be a free resource for end-users, but its production and curation is certainly not without significant cost.”
> 
> &#x2013; [Digital Public Assets: Rethinking value, access and control of public sector data in the platform age](https://www.common-wealth.co.uk/digital-public-assets.html)

<!--quoteend-->

> Digital public assets must be seen, from this perspective as an issue of decolonial politics and global justice.
> 
> &#x2013; [Digital Public Assets: Rethinking value, access and control of public sector data in the platform age](https://www.common-wealth.co.uk/digital-public-assets.html)

<!--quoteend-->

> If open data merely serves the interests of capital by opening public data for commercial re-use and further empowers those who are already empowered and disenfranchises others, then it has failed to make society more democratic and open.'
> 
> &#x2013; [Digital Public Assets: Rethinking value, access and control of public sector data in the platform age](https://www.common-wealth.co.uk/digital-public-assets.html)

