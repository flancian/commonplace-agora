# The Public Order Act already makes peaceful protest harder than in almost every other democracy

[[Public Order Act]]
[[Margaret Thatcher]]

[Attempting to ban protest is usually the mark of a repressive state. That’s n&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/23/priti-patel-police-and-crime-bill-banning-protest-britain)


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Concentrate and ask again]]

Don't know anything about it, but I can believe it. 

