# Effective freedom

[[Stafford Beer]].

Enough freedom to be free, not so much that the whole organism falls apart.

