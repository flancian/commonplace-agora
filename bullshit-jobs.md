# Bullshit jobs

Bullshit jobs&#x2026; creation of meaningless bureaucratic jobs to fulfil capitalism.  

c.f. strikes of garbage workers vs strikes of bankers and which one causes the most disruption.

> Reading about who still needs to show up for work in times of pandemic, reminds of @davidgraeber & Bullshit Jobs. We are experiencing how our societies would collapse if the nurses, waste collectors & teachers stopped showing up for work
> 
> &#x2013; https://twitter.com/LejlaSadiku/status/1241030820955148290

<!--quoteend-->

> We place [[capitalism]] in quarantine only to discover: 
> 
> -   The paramount importance of all [[maintenance]] work: transport, logistics, cleaning, health work, etc.
> -   The radical uselessness of financiers, consultants & vultures
> -   The absolute centrality of informal/family/support networks
> 
> &#x2013; https://twitter.com/acorsin/status/1237821553703882752

<!--quoteend-->

> We no longer live. We aspire. We work to get richer. Paradoxically, we find ourselves working in order to have a “vacation.” We can’t seem to actually live without working. Capitalism has thus completely occupied social life. 
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

