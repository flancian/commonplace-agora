# Labour movement

Industrial capitalism and the labour movement are historically intertwined.

-   previously some labour power was related to ability to cut off e.g. coal supply
-   currently becoming detached from each other
-   potential for labour movement not to have relationship to fossil capital, could be quite anticapitalist

