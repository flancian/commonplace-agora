# elisp koans



## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-04-01 Thu 20:29]</span></span>

Hmm, for some reason, they don't work correctly.  I evaluate the `elisp-koans.el` file as per the instructions, then try and run the test.  That gives an error about `ert-bound-p` not being present.

I added `(require 'ert)` at the top of elisp-koans.el and tried again.

That worked, but then when I run `elisp-koans/run-test` and run the test, it doesn't seem to find the test.  Hmm.

Oh - I hadn't evaluated the test as per the instructions - whoops!


## Bookmarks

-   https://github.com/jtmoulia/elisp-koans
-   https://www.reddit.com/r/emacs/comments/cd9akl/starting_elispkoans_library_for_learning_emacs/

<!--listend-->

-   [org-super-agenda/test.el at master · alphapapa/org-super-agenda · GitHub](https://github.com/alphapapa/org-super-agenda/blob/master/test/test.el)

