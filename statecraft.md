# Statecraft

> He distinguished “statecraft,” in which individuals have a diminished influence in political affairs because of the limits of representational government, from “politics,” in which citizens have direct, participatory control over their governments and communities.
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> While the state is the instrument by which an oppressive and exploitative class regulates and coercively controls the behavior of an exploited class by a ruling class, a government—or better still, a polity—is an ensemble of institutions designed to deal with the problems of consociational life in an orderly and hopefully fair manner.
> 
> &#x2013; [[The Communalist Project]]

