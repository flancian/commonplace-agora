# Sobornost

Encountered in [[The Quantum Thief]].

> Etymology: from Russian Соборность, "Spiritual community of many jointly living people", the philosophical and religious idea in early 20th-century Russia. 
> 
> &#x2013; [Sobornost | The Quantum Theif Wiki | Fandom](https://exomemory.fandom.com/wiki/Sobornost) 

