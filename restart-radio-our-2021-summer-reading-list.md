# Restart Radio: Our 2021 Summer Reading List

URL
: https://therestartproject.org/podcast/2021-summer-reading/

Publisher
: [[The Restart Project]]

Topics
: [[right to repair]]

Right to repair regulations announced in the UK.

> The rules that came into effect last week mean that repairing all new white goods, including dishwashers, washing machines, fridges, and TVs, must be better supported by the [[long-term availability of spare parts]]. 

<!--quoteend-->

> In the US, there was also positive news for Right to Repair. President Biden issues an executive order that gives the [[Federal Trade Commission]] increased powers to enforce rules helping independent repair compete.

<!--quoteend-->

> We also hear [[Steve Wozniak]] – co-founder of Apple – talking about his support for the right to repair and how he would not have gotten to where he is without it.

