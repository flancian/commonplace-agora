# 2020-10-24



## [[Adding a 404 page with .htaccess]]

I wanted to add a 404 page to my garden, especially as I'm currently renaming pages.  Might end up with some broken links.

Easy enough - I created a 404.org in org-mode.  It gets published as 404.html.

And in .htaccess, just needed to add:

```nil
ErrorDocument 404 /404.html
```


## Prep school for prison

> When children attend schools that place a greater value on discipline and security than on knowledge and intellectual development, they are attending prep schools for prison.
> 
> &#x2013; [[Angela Davis]]


## [[Updating to org-roam 1.2.2]]


## [[McKenzie Wark]]

Came across [[McKenzie Wark]] via the latest issue of [[STIR]].  Her work on the [[commodification of information]] sounds really interesting.  Will have to check out [[A Hacker Manifesto]] and other works.  The identification of the '[[Vectoralist class]]' back in 2003 sounds pretty on point.


## Changing the filename that org-roam gives new notes

