# Federated Wiki

I had known about Federated Wiki for a while, hearing about it through IndieWeb originally and Wikity.  But I never really grokked or explored it.

I then started getting in to personal wikis in 2019.

And then in 2021 after reading the chapter on Federated Wiki in [[Free, Fair and Alive]] and [[Mike Hales]] posting on social.coop about how some of the neighbourhood features works has made me understand more about the exciting ideas it encapsulates.

I'm not yet using it because I'm happy with my wiki in org-roam but I'd like to explore Federate Wiki and its ideas more, one way or another.

