# Protocol cooperativism

-   is controlling the [[means of production]] enough?
-   are worker-owned coops enough?
-   Power is currently too entrenched
-   "Yet there is nothing about the internet that necessitates that capital-centric way of creating wealth"
-   "[[Platform cooperativism]] is the notion the digital 'means of production', the platform, should be owned by, governed by and should enrich the participating value creators."
-   query: are platforms the [[digital means of production]]?
-   "As an approach and as a tactic, it is a straight extension of rudimentary 19th Century cooperativism into the digital age and cyberspace. In which case we should anticipate it working as it always has on the sidelines but never to impact the wider economy."
-   protocol cooperativism
-   it stopped (for a while) the early internet being dominated by corporations
-   commonly agreed ways of operating
-   "protocols are fundamentally cooperative, but there is room for any kind of institutional structures on the next layer."

not sure i get it yet&#x2026; so what is he saying is the problem with platform coops that protocol coops might solve?  what's the distinction between the two?

the problem is that platform coops still have a central platform operator at their core?  I guess I agree with that.  I think ideally various different things should operate across a shared protocol.  But isn't that protocol more at the infrastructure layer?  I guess if we're thinking about the OSI model, he's suggesting area specific protocols that sit on top of the various network layers.  references for me the statebook article, where they suggested the role of a state (if there is one) should be the underlying infrastructures and protocols, not creating a statebook. i think i agree with that.

the value add comes from managing trust and social relations?

https://platform.coop/blog/protocol-cooperativism/


## summary

https://platform.coop/blog/protocol-cooperativism

