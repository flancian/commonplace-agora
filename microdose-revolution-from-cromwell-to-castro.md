# Microdose: Revolution From Cromwell to Castro

A
: [[podcast]]

URL
: https://novaramedia.com/2021/11/05/microdose-revolution-from-cromwell-to-castro/

> In anticipation of the next Trip, the ACFM trio deliver a condensed but essential history of [[revolution]] from Oliver Cromwell to Fidel Castro, with stop-offs in France, America, Haiti, China, Spain and Russia.
> 
> What does it take to cook up a revolution? Is the [[French Revolution]] still relevant to our idea of radical social upheaval? Can there be unlimited political reforms without triggering a revolution?

Haitian Revolution.

