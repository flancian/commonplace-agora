# Open Building Institute

> an offshoot of Open Source Ecology, builds low-cost, modular houses that are ecological and energy-efficient using techniques that are open
> 
> &#x2013; [[Free, Fair and Alive]]

