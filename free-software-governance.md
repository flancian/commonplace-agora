# free software governance

I like [[Nathan Schneider]]'s [CommunityRule](https://communityrule.info/) site - listing a few different [[models of governance]] that might be followed in a free software project.  

> Like choosing a Creative Commons license, CommunityRule offers a palette of templates, from dictatorship to various flavors of democracy. 
> 
> &#x2013; [How’s That Open Source Governance Working for You?](https://hackernoon.com/hows-that-open-source-governance-working-for-you-mphv32ng) 

<!--quoteend-->

> being explicit matters. It helps avoid that [[the tyranny of structurelessness]], ensuring that the lines of responsibility are clear. A Rule also serves as a mirror, encouraging community members to ask whether the current structure really fits the nature of the community. Any Rule should include provisions for how the community can evolve its governance as it matures, as any community must.
> 
> &#x2013; [How’s That Open Source Governance Working for You?](https://hackernoon.com/hows-that-open-source-governance-working-for-you-mphv32ng) 

See [[implicit feudalism]].


## Examples

-   [[YunoHost]] has a really interesting governance structure, involving special interest groups, a council, and consensus for decision making.  https://yunohost.org/en/yunohost_project_organization


## Bookmarks

-   [[Online Communities Are Still Catching Up to My Mother's Garden Club]]
-   [What would a fediverse "governance" body look like?](https://socialhub.activitypub.rocks/t/what-would-a-fediverse-governance-body-look-like/1497)

