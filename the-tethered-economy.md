# The tethered economy

The move towards products being persistently linked to the seller after purchase.  By software and connectivity.  Bad news because the vendor has complete control over your use of the thing and how long it lasts.

> Imagine a future in which every purchase decision is as complex as choosing a mobile phone. What will ongoing service cost? Is it compatible with other devices you use? Can you move data and applications across devices? Can you switch providers? These are just some of the questions one must consider when a product is “tethered” or persistently linked to the seller. 
> 
> &#x2013; [The Tethered Economy by Chris Jay Hoofnagle, Aniket Kesari, Aaron Perzanowski&#x2026;](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3318712) 

<!--quoteend-->

> In my opinion the bigger issue and they need to be taken to task over this is the fact Xbox forces a online connection to setup or repair a console. In 10 years time that’s going to be 100+ million useless plastic boxes
> 
> &#x2013; https://twitter.com/Morfid_plays/status/1314136687061225472 

