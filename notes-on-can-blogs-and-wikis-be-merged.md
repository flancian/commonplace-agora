# Notes on "Can Blogs and Wikis Be Merged?"

&#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/) 

> Wiki and blogs have two different cultures, two different idioms, two different sets of values.

Maybe, but are they not both in complementary service of similar goals?

> Blogs are, in many ways, the child of BBS culture and mailing lists. They are a unique innovation on that model, allowing each person to **control their part of the conversation on their own machine and software while still being tied to a larger conversation** through linking, backlinks, tags, and RSS feeds.

<!--quoteend-->

> Blogs value a separation of voices, the development of personalities, new posts over revision of old posts. 

<!--quoteend-->

> They are read serially, and **the larger meaning is created out of a narrative that expands and elaborates themes over time**, becoming much more than the sum of its parts to the daily reader.

Kind of the idea that you are following a person, not a blog.  I'm sure Ton or someone else on the IndieWeb has mentioned that before.

> Through reading a good blogger on a regular basis, one is able to watch someone of talent think through issues

<!--quoteend-->

> Wiki is perhaps the only web idiom that is not a child of BBS culture. It **derives historically from pre-web models of hypertext, with an emphasis on the pre**. The immediate ancestor of wiki was a Hypercard stack maintained by Ward Cunningham that attempted to capture community knowledge among programmers. Its philosophical godfather was the dead-tree hypertext A Pattern Language written by Christopher Alexander in the 1970s.

<!--quoteend-->

> What wiki brought to these models, which were personal to start with, was collaboration. Wiki values are often polar opposites of blogging values. **Personal voice is meant to be minimized**. Voices are meant to be merged. 

Not so true for the personal wiki.  Maybe a personal wiki is wiki software but with different values for the output?

> Rather than serial presentation, wiki values **treating pages as nodes that stand outside of any particular narrative**, and attempt to be timeless rather than timebound reactions.

<!--quoteend-->

> Wiki iterates not through the creation of new posts, but through the refactoring of old posts. It shows not a mind in motion, but the clearest and fairest description of what that mind has (or more usually, what those minds have) arrived at. It values reuse over reply, and links are not pointers to related conversations but to related ideas.

<!--quoteend-->

> In most cases I’d say it makes sense for these to remain two conceptually distinct projects, except for the big looming issue which is with the open web shrinking it might helpful for these communities to join common cause and solve some of the problems that have plagued both blogging and wiki in their attempt to compete with proprietary platforms.

Interesting - Mike thinks they should be kept distinct.  However, I think his analysis of wikis is not thinking of them as **personal** wikis.  Some of the values Mike cites for wikis, like minimization of personal voice, do not apply for personal wikis.

