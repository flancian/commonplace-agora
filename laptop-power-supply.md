# Laptop power supply



## Difference Between an AC Adapter, Power Supply and Charger for Laptop Computers

> There is no difference between an AC Adapter, a power supply and a charger in reference to a laptop computer: all three functions are handled by the device's external power device. With laptops, all three names refer to the same device that's commonly referred to as a "power brick." There are other devices that individually serve as internal power supplies, external power supplies and battery chargers; however, all laptop power units provide both power and charging functions from an external source.
> 
> &#x2013; [Difference Between an AC Adapter, Power Supply and Charger for Laptop Computers](https://itstillworks.com/difference-between-ac-adapter-power-supply-charger-laptop-computers-12310708.html)

