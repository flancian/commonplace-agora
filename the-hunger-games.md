# The Hunger Games

Been watching The Hunger Games films.  Pretty good actually.  Quite pleasant to see full on [[Revolution]] against a decadent elite in a popular work of young adult fiction.

It's pretty grim and dark.  Quite full on for a young adult I would suggest, but who knows.

I guess I don't like the suggestion that a revolution needs a totemic leader.  That without Mockingjay, noone would be motivated.  I wonder what history suggests about this. 

It feels like if you rely on one individual for your revolution, then you have a single point of failure for counter-revolution.  But people like a symbol or a narrative with an emotional attachment, so they like an individual story.

For example - [[Jeremy Corbyn]].  Resurgence of socialism in the Labour party as a result of Corbyn and to some degree a cult of personality around him.  Subsequent discreditation of socialism in the Labour party by intense campaign against the personality of Corbyn.  Strength becomes weakness.  Plenty working class people generally liked Labour's manifesto, but did not want to vote for Corbyn.

