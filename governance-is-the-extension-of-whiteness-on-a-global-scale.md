# Governance is the extension of whiteness on a global scale

[[Governance]] is the extension of [[whiteness]] on a global scale.

[[Moten and Harney]].


## Because

&#x2026;


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Reply hazy, try again]]

Source (for me)
: [[Nathan Schneider]] https://twitter.com/ntnsndr/status/1481493938015928320

