# negative income tax

A negative income tax (NIT) and an unconditional basic income (UBI) are two ways of achieving a basic income guarantee (BIG). One gives a varying amount of money according to income, and the other gives the same amount to all and taxes different amounts back. However, among basic income supporters, some of us prefer UBI and some of us prefer NIT even despite the potential for their outcomes to be identical.


## See also


### [[basic income guarantee]]


## Resources


### http://www.scottsantens.com/negative-income-tax-nit-and-unconditional-basic-income-ubi-what-makes-them-the-same-and-what-makes-them-different

