# State socialism is good for the environment

[[State socialism]] is good for the [[environment]].


## Because

Kind of an empirical argument.

-   State policies in [[China]], [[Cuba]] and the [[Soviet Union]] were beneficial for the environment


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Concentrate and ask again]]

Source (for me)
: [[Salvatore Engel-Di Mauro]]

Not sure.   Salvatore seems fairly convinced, but I don't know enough.  Not necessarily a fan of state socialism either.  But hey if the claim is true, then that's a good thing.

