# Work

> In the morning we think of the evening, during the week we dream of the week-end, we sustain everyday life by planning the next vacation from it.
> 
> &#x2013; [[bolo'bolo]]

