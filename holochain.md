# Holochain

> Unlike Bitcoin and Ethereum (the two most prominent digital ledger technologies), Holochain is far more energy efficient and flexible in the way that it authenticates digital objects on networks.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> It is a lighter, more versatile set of software protocols than the blockchain.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Rather than relying on a single ledger — a “heavy” solution that requires the work of countless computers on the network — Holochain is a simpler, lightweight approach that lets every user have his or her own secure ledger and distributed storage system to store their personal data and digital identities.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> In this sense, Holochain is a tool based on a differentiated [[relational ontology]] (each user can express value in different, particular ways when interacting with people) while blockchain as a tool ultimately reflects an undifferentiated ontology (each user must accept the prevailing standard of the system).

<!--quoteend-->

> Holochain is a data integrity layer for distributed applications. Basically a way to have both the applications and their required data storage run on all of the users machines instead of centralizing it on a server.
> 
> It is a framework that is made to be agent-centric. That is to say, that all applications built on Holochain will enable the agent (person, device or collective) to communicate with other agents through a bunch of applications it has installed.
> 
> **This means that an agent stores all of the data connected to the agent and allows this data to be accessed by applications of the agents choice**. Agents also hold a share of other agents data, one piece of the collective dataset through the use of Distributed Hash Tables.
> 
> &#x2013; [Shiro - coordination for local food communities - App Ideas - Holochain Forum](https://forum.holochain.org/t/shiro-coordination-for-local-food-communities/642/2) 

<!--quoteend-->

> Harris-Braun claims that Holo Fuel will not simply be a substitute form of money that will end up replicating capitalism, but will instead propagate a “different grammatics of value.” Just as a different grammatical structure in a human language helps us to articulate different ideas and realities, the Holochain grammar is intended as a tool to express social forms of value — flows
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Instead of market-exchange being the dominant form of value, it is envisioned that Holochain-based apps will enable other forms of value to be expressed and circulated within networked communities — in other words, not just the money values represented by prices.
> 
> &#x2013; [[Free, Fair and Alive]]

-   [[Holochain and basic income]].
-   [[Holochain and commoning]].

