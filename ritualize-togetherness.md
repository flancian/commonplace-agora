# Ritualize Togetherness

> One of the most important ways to strengthen shared purpose and values is by ritualizing togetherness — meeting regularly, sharing deeply, cooking together, celebrating successes, candidly assessing fail- ures. This is essential to building a culture of commoning and a shared identity.
> 
> &#x2013; [[Free, Fair and Alive]]

