# Labour under Keir Starmer

> Labour’s proposed solution, however, was even more baffling. It proposed creating a “next-generation neighbourhood watch” by placing police hubs around the country and tapping technology like video doorbells and Whatsapp groups to help surveil communities.
> 
> &#x2013;  [Amazon is always watching](https://mailchi.mp/techwontsave.us/amazon-is-always-watching) 

<!--quoteend-->

> Let’s be very clear about what this would mean: Amazon Ring cameras and apps like Neighbors, its neighborhood watch app, would become even more central to policing. It’s an incredibly dystopian proposal.
> 
> &#x2013;  [Amazon is always watching](https://mailchi.mp/techwontsave.us/amazon-is-always-watching) 

