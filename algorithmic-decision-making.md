# algorithmic decision-making

When people talk about whether algorithms are good or bad, they pretty much always mean decision-making algorithms - something that makes a decision that affects a human in some way.  So for example long division is an algorithm, but it's not really having any decision making effect on society.  We're talking more about things like putting things in a category, making an ordered list, finding links between things, and filtering stuff out.  

They might be 'rule-based' expert systems, in that the creator programs in a set of rules that the system then executes, or more recently machine learning algorithms, where you train an algorithm on a dataset by reinforcing 'good' or 'bad' behaviour.  Often with these we can't always be sure how the algorithms has come to a conclusion.

[[Algorithmic bias and racism]].

