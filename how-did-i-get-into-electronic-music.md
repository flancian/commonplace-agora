# How did I get into electronic music?

Prompted by a conversation yesterday&#x2026;

I sometimes think probably some appreciation comes from listening to the music in computer games as a kid. (And, maybe, for the more experimental stuff, the screech of the tape loader on the [[Amstrad]]&#x2026; And the dial-up modem later on&#x2026;)

The first electronic songs that I can remember?  I remember my brother had a tape called '[The Ultimate Rave](https://musicbrainz.org/release/008423b2-4b59-4138-b2f7-14f227b9ec02)', from 92.  So that would have been around when I was 10.  I remember the Charly song from that, although the [album version is way better](https://www.youtube.com/watch?v=eEIfpHnoXdA).  I listened to Experience a lot.  He also had '[Rave 92](https://en.wikipedia.org/wiki/Rave_92)', I still remember a bunch of tracks from that fondly&#x2026;  On A Ragga Tip, The Bouncer, Ravin' I'm Raving.   

So then The Prodigy, The Shamen, a bit later the Chemical Brothers.  I have a distinct memory at some point of watching No Limits by 2 Unlimited on some music programme when at my grandparents house. 

My Mum liked Orbital, so we had a bunch of their early albums around - Snivilisation, Insides, the brown one.

At some point (around college time I guess?), I started getting into IDM.  Warp ([[Autechre]], Boards of Canada) and Skam (Jega) being the labels that kicked me off.  Where did that come from?  I think that was me going off into my own territory at that point, possibly with some assist from Michael.  I distinctly remember teaching myself how to [[make websites]] while listening to Tri Repetae on repeat.  College? Pre-uni?

It's mostly been the more IDM side of things since then.

