# tit for tat

> The success of the strategy, which is largely cooperative, took many by surprise. In successive competitions various teams produced complex strategies which attempted to "cheat" in a variety of cunning ways, but Tit for Tat eventually prevailed in every competition. 
> 
> &#x2013; [Tit for tat | Psychology Wiki | Fandom](https://psychology.fandom.com/wiki/Tit_for_tat) 

[[tit for tat with forgiveness]]

