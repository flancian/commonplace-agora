# Autonomous

&#x2013; Annalee Newitz

This was fun.  Easy to read and keeps you turning the pages.  Interesting themes of [[Free culture]], here focused on open sourcing / reverse engineering pharmaceuticals.  And the lengths to which those in control of [[intellectual property]] rights will go to enforce them.

Interesting side story of human / robot romance and [[gender identity]].

Finished: December 2019.

