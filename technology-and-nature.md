# Technology and nature

I don't have much of a fixed position but I'm definitely becoming more Luddite the more I age and reflect, Luddite in the original sense of the word as I understand it - i.e. rejecting a technology if it's not of benefit to society or is being used to repress the working class or is ecologically damaging.  

I push back against technology being perceived as all bad.  

I maybe align with 'fully automated luxury communism' as provocative as the name is&#x2026; the idea that really with all of our technological advances, we could be living in an age of plenty for all that is well within ecological bounds, if only it wasn't concentrated in the hands of a few.  The future is already here, it's just unevenly distributed, as William Gibson said.

That said, I do not believe in accelerationism, or technology for its own sake.

I haven't read the book by Aaron Bastani.  I think it can probably be taken a few different ways.  I don't think I know the ins and outs of the term.  I'm wary of it a bit, and I also don't like the word 'disruptions' - that's very Silicon Valley, the idea of disrupting things just for the sake of it.  I prefer maintaining things. 

I think it's a bit of a fine line - I don't wholesale buy into the idea that more technology is what we need to fix climate change.  I mean it's **possible** that that is genuinely what we do need now we've let it get so far.  But I prefer a cautionary approach to technology, only using it when necessary, not just for the sake of it.  I think we could definitely use what technology we've already got in a much more positive and fairer and less destructive way, that could already solve much of our problems.

[[Machines of Loving Grace]] is an interesting poem on this topic.

I studied [[predator-prey equations]] at university, and enjoyed making models of them.

I studied a Master degree in [[evolutionary and adaptive systems]], which looked at the ways in which natural processes could be used in technology.

