# British Digital Cooperative

> The BDC would be tasked with developing a surveillance-free platform architecture to enable citizens to interact with one another, provide support for publicly funded journalism, and develop resources for social and political communication.


## Resources

-   Report from [[Common Wealth]]: [The British Digital Cooperative: A New Model Public Sector Institution](https://www.common-wealth.co.uk/reports/the-british-digital-cooperative-a-new-model-public-sector-institution)
-   [A Cooperative Vision for Technological Innovation w/ Dan Hind | Listen Notes](https://www.listennotes.com/podcasts/tech-wont-save-us/a-cooperative-vision-for-SAweKYqRLys/)

