# Socialism



## Raymond Williams : socialism and ecology

> And this is particularly important for socialists to realize. For it’allows us to distinguish the real history and therefore a possible future from what is otherwise a very weak version of the environmental case, which is that we should revert from industrial society to the pre-industrial order which didn’t do this kind of damage.
> 
> &#x2013; [Socialism and Ecology // New Socialist](https://newsocialist.org.uk/socialism-and-ecology/) 

Making the case that it wasn't just industrialism that caused environmental damage (although there is a difference in degree).

It can go too far in the other direction.

> made the case that production is an absolute human priority, and that those who object to its effects are simply sentimentalists or worse; moreover that they are people who speak in bad faith, from their own comfort and privilege, about the effects of reducing poverty in the lives of others.

<!--quoteend-->

> This is in spite of the fact that a century and a half of dramatically increased production, though it has transformed and in general improved our conditions, has not abolished poverty, and has even created new kinds of poverty, just as certain kinds of development create under-development in other societies

