# Antilibrary of articles

I first thought about this is the context of a [[Layer 0]] when thinking about [[progressive summarisation]]. I then came across the term [[Antilibrary]], and that fits pretty well.

I realised that just by virtue of having 'layer 0', the source text, as part of the progressive summarisation process - I felt less bad about having hundreds of unread articles saved in Wallabag.  I feel good about it in fact!  It's the first step of note-taking - I know I have source material on the topic to come back to, recommended by someone I trust, when this topic comes back into my focus.  I can dig deeper into it then if I want.

This is very positive.  I had started to think of the wealth of information out there on the web as [[information overload]].  But now I can go back to thinking about it as an amazing resource, to be tapped into when needed.


## Links

-   [Surfacing My Anti-Library – Interdependent Thoughts](https://www.zylstra.org/blog/2021/02/surfacing-my-anti-library/)

