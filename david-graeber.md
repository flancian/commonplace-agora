# David Graeber

> His parents were working-class autodidacts; his mother, a garment worker, and his father a Communist who fought in the Spanish Civil War.
> 
> &#x2013; A Message for David Graeber, Ash Sarkar

