# UK fuel crisis 2021

This is not affecting me as I don't drive.  But I wonder what the knock-on effects will be.  And the main reason being a driver shortage so it's part of an overall supply chain crisis.  The stories about queues and some people's behaviour on the petrol station forecourts is all a bit foreboding towards your standard dystopian sci-fi resource crisis.

> “It’s fuel anxiety,” said Blason. “When that needle creeps down to the red or anywhere near then it’s panic stations. Where are we going to get it, who’s got it? It’s brought out a different side in people in all honesty.”
> 
> -   [‘It’s brought out a different side in people’: in a garage in the fuel crisis&#x2026;](https://www.theguardian.com/business/2021/oct/01/its-brought-out-a-different-side-in-people-in-a-garage-in-the-fuel-crisis)

