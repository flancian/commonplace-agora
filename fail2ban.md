# fail2ban



## Checking if someone is in jail

You have multiple jails - the one I'm usually interested in is `sshd`:

```shell
sudo fail2ban-client status sshd
```

-   [How to show all banned IP with fail2ban? - Server Fault](https://serverfault.com/questions/841183/how-to-show-all-banned-ip-with-fail2ban)


## Removing from jail

```nil
sudo fail2ban-client -i
```

then in interactive mode:

```nil
status sshd
set sshd unbanip XXX.XXX.XXX.XXX
```

-   [firewall - How to Unban an IP properly with Fail2Ban - Server Fault](https://serverfault.com/a/760324)

