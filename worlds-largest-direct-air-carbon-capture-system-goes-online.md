# World's Largest Direct Air Carbon Capture System Goes Online

URL
: https://www.vice.com/en/article/bvz45a/worlds-largest-direct-air-carbon-capture-system-goes-online

Summary
: "The Orca represents 40 percent of the world's direct air carbon capture capacity, but its overall capabilities are still minuscule and controversial."

[[Geoengineering]].
[[Direct air capture]].
[[Carbon capture and sequestration]].

