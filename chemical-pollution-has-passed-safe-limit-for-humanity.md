# Chemical pollution has passed safe limit for humanity

[[Chemical pollution]]

So sayeth scientists
[Chemical pollution has passed safe limit for humanity, say scientists | Pollu&#x2026;](https://www.theguardian.com/environment/2022/jan/18/chemical-pollution-has-passed-safe-limit-for-humanity-say-scientists?s=09)

> The cocktail of chemical pollution that pervades the planet now threatens the stability of [[global ecosystems]] upon which humanity depends, scientists have said.


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Ask again later]]

Sounds plausible.  Doesn't sound good.

