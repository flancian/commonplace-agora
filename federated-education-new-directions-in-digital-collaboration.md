# Federated Education: New Directions in Digital Collaboration

URL
: 

[Federated Education: New Directions in Digital Collaboration | Hapgood](https://hapgood.us/2014/11/06/federated-education-new-directions-in-digital-collaboration/)

Author
: [[Mike Caulfield]]

Date published
: 2014-11-06

Really good.  And interesting to note, predates [[The Garden and the Stream: A Technopastoral]].

Talks about [[learning in public]], through the lens of Arthur C. Clarke and his outlining of the idea of geostationary satellites and global positioning before they were developed by the US military.

I like this idea of the [[Kinneavy triangle]] as a [[lifecycle of information]]. Narrative (I), dialogue/persuasion (focused on the you), and [[exposition]] (focused on the 'it').

> An idea starts out with what it means to you, the “I” in this situation. Then it pings around a social network and is discussed (the “you” phase). And then in the final phase it sort of transcends that conversation, and becomes more expository, more timeless, less personal, more accessible to conversational outsiders.

He mentions Evernote and Delicious as the I part.  Twitter and blogging as the dialogic/persuasive part.  And wiki as best as expository.

> For the expository phase, it’s wiki that excels. By cracking open ideas and co-editing them, we turn these time-bound, person-bound comments into something more expansive and timeless. We get something bigger than the single point of view, smarter than any single person.

In terms I'm familiar with right now, I'd say something like:

| writing       | subject | tools  | dialectics |
|---------------|---------|--------|------------|
| [[narrative]] | I       | garden | thesis     |
| [[dialogic]]  | you     | stream | antithesis |
| exposition    | we/it   | agora  | synthesis  |

> So one thing I’m interested is how we create a system that allows information to flow in this way. One way might be to link up Evernote, Twitter, RSS Feeds and Wiki in a certain way.

<!--quoteend-->

> Another way is to start at the end technology — in this case wiki — and look at what it would take to make it work better in the other stages, the I and the You, the personal and the dialogic.

^ This is huge.  And what I think I'm going for with my site.  Well - combine the garden and the stream there.  Actually not just there.  And then the agora (or interlinking wikis, one way or another) layered on top of that.

But YES - the goal is to combine garden, stream, and agora.  The way one does that might take many forms.  My chosen form is some kind of indieweb / fediverse / agora mashup.

