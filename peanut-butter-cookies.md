# Peanut butter cookies

-   [Peanut butter cookies recipe - BBC Good Food](https://www.bbcgoodfood.com/recipes/peanut-butter-cookies)
    -   These are super simple and really good.  Not vegan - you need an egg.

-   [1 Bowl Vegan Peanut Butter Cookies - The Simple Veganista](https://simple-veganista.com/vegan-peanut-butter-cookies/)
    -   Not tried yet.

-   [Vegan Peanut Butter Cookies - They MELT in your mouth!](https://chocolatecoveredkatie.com/secret-peanut-butter-cookies/)
    -   these were way too crumbly.

