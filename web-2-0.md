# Web 2.0

> Internet culture historians since the mid-2000s have shown a marked distaste for the rhetoric that has guided Web 2.0, a term coined to describe the new social/participatory and interactive web of complex, pre-made user interfaces and architectures, where users became active consumers, creators, and participants of a service in which they had very little, if any, agency.
> 
> &#x2013; [[404 Page Not Found]] 

<!--quoteend-->

> And she argues that though Web 2.0 encourages the broader, ever more interconnected amateur web population to upload, share, record, and participate at increasing rates, it does so by ensuring the erasure of the personalized, Geocities-ugly "Welcome to my Home Page" aesthetic long hated by web designers and other members of the professional class. Users, in other words, must now operate within the hell of beautiful interfaces designed by experts. TL;DR: Website Eugenics.
> 
> &#x2013; [[404 Page Not Found]] 

<!--quoteend-->

> Web 2.0 saw the emergence of Web applications and the consumer’s ability to generate content via Social Media. 
> 
> &#x2013; [[Can the Real Web 3.0 Please Stand Up?]]

