# YunoHost 'Corrupted YAML read'

Updating some things on [[YunoHost]], and this error keeps popping up:

> The YAML metadata file associated with logs is damaged: '/var/log/yunohost/categories/operation/20210213-182315-app<sub>install</sub>-archivist.yml
> Error: Corrupted YAML read from /var/log/yunohost/categories/operation/20210213-182315-app<sub>install</sub>-archivist.yml (reason: expected '', but found ''
> in "", line 1, column 13:
> '********\*\*********'a'********\*\*********'r'********\*\*********'g'**\*\*** &#x2026;
> ^)'

There's a thread [here](https://forum.yunohost.org/t/le-fichier-yaml-de-metadata-associe-aux-logs-est-corrompu-md-file/8412/11]] ) (in French), and one [here](https://forum.yunohost.org/t/got-a-weird-yaml-file-error/8647/3) that's similar-ish. Neither seems to have a useful answer.

If I inspect the file that the error is complaining about, then it's full of junk.  Given  it's a log file, and it's corrupted, I just removed it, and that stopped the error appearing.

