# How To Take Smart Notes

-   source: [How To Take Smart Notes: 10 Principles to Revolutionize Your Note-Taking and &#x2026;](https://fortelabs.co/blog/how-to-take-smart-notes/)
-   tags: [[note taking]]

A book on note-taking from [[Sönke Ahrens]]. 

![[2020-07-04_09-12-21_screenshot.png]]

Mainly about breaking down ideas into smaller reusable chunks.  Let them have free form structure.  Have a good way to pull them back together again when you need them.

> The gist of the system is:
> 
> -   While reading, make notes using your own words. Only when we’ve actually understood the material can we express it in our own words
> -   Link these notes with other notes that could be related — this helps uncover patterns. Forming these connections also helps our memory, and recall becomes easier in the correct context
> 
> &#x2013; [Prateek Saxena](https://prtksxna.com/how-to-take-smart-notes/) 

![[2020-07-04_09-07-20_screenshot.png]]
Diagram of the smart notes flow, from [Prateek Saxena](https://prtksxna.com/how-to-take-smart-notes/) 

> we will be able to “efficiently turn our thoughts and discoveries into convincing written pieces and build up a treasure of smart and interconnected notes along the way.”

sounds good.

> He wrote only on one side of each card to eliminate the need to flip them over, and he limited himself to one idea per card so they could be referenced individually.
> 
> &#x2013; [How To Take Smart Notes: 10 Principles to Revolutionize Your Note-Taking and &#x2026;](https://fortelabs.co/blog/how-to-take-smart-notes/)

1.  Principle #1: Writing is not the outcome of thinking; it is the medium in which thinking takes place
2.  Principle #2: Do your work as if writing is the only thing that matters (I like the focus on writing)
3.  Principle #3: Nobody ever starts from scratch
4.  Principle #4: Our tools and techniques are only as valuable as the workflow
5.  Principle #5: Standardization enables creativity.  "Notes are like shipping containers for ideas" I like that.
6.  Principle #6: Our work only gets better when exposed to high-quality feedback
7.  Principle #7: Work on multiple, simultaneous projects
8.  Principle #8: Organize your notes by context, not by topic
9.  Principle #9: Always follow the most interesting path
10. Principle #10: Save contradictory ideas

> "By standardizing and streamlining both the format of our notes and the steps by which we process them, the real work can come to the forefront: thinking, reflecting, writing, discussing, testing, and sharing. This is the work that adds value, and now we have the time to do it more effectively."

true

> "Writers don’t think about a single, “correct” location for a piece of information. They deal in “scraps” which can often be repurposed and reused elsewhere. The discarded byproducts from one piece of writing may become the essential pillars of the next one. The slip-box is a thinking tool, not an encyclopedia, so completeness is not important."

