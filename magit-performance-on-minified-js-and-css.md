# Magit performance on minified JS and CSS

For a long, long time, [[Magit]] has been super slow for me when I have any minified files in the diff for staging.  It was one of those annoyances that I just kind of lived with for a while, but I revisited it today.

Someone else [here](https://emacs.stackexchange.com/questions/37219/magit-hunk-with-long-lines-set-default-hidden) was having the same problem.  They put it down to long lines.

There's a big discussion on that [here](https://emacs.stackexchange.com/questions/598/how-do-i-prevent-extremely-long-lines-making-emacs-slow).  It's been an Emacs problem for a while.

For me, the solution from there was the so-long.el package, which is included in Emacs >27.1 (Which I recently updated to - [[Installing Emacs from source]]).

After adding:

```elisp
(global-so-long-mode 1)
```

to my init file, the problem has pretty much gone.

It's still a teeny bit slow when there's a few minified files in magit status, but way, way better now.

