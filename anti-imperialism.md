# Anti-imperialism

-   socialism requires anti-imperialism (as imperialism is the principle part of capitalism)
-   there is no socialism that is not anti-imperialist
-   [[Rosa Luxemburg]] played a big part in its theorisation
-   Critique by Bukharin

