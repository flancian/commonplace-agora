# Gift economies are good

This is a bit poorly defined - good for what?

[[gift economy]]


## Epistemic status

Type
: [[notion]]

Gut feeling
: 8

Confidence
: 3

I don't understand well enough to exactly explain why I think this.  But I generally like the notion of non-market based transactions.  And non-reciprocal transactions (although I'm not 100% sure gift economies are considered non-reciprocal).

