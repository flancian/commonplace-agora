# Miller columns

Horizontal columns.  People seem to like it for browsing [[personal wikis]].

[[FedWiki]] has this.

I added something along these lines to my own site (https://commonplace.doubleloop.net)

I'm not convinced it should be implemented within a site though.  Feels kind of wrong pushing a navigation style on to the user.

It would be perhaps better at the browser-level e.g. [[Cartographist]]

But in fact as the author of that, says: 

> Additionally, it started to feel that I'm solving this problem on a wrong level. For example, a good window manager could replace Cartographist almost completely 
> 
> &#x2013; [Cartographist ‒ Szymon Kaliski](https://szymonkaliski.com/projects/cartographist/) 


## Twin pages

-   https://en.wikipedia.org/wiki/Miller_columns

