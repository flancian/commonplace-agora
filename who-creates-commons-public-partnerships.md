# Who creates commons-public partnerships?

Is it [[councils]]?  [[commoner]]s?  A combo?

> Either may initiate a CPP, but commoners retain control 
> over the process
> 
> &#x2013; [[Free, Fair and Alive]]

