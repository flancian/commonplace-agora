# bliki tooling

[[Bliki]] being the [[blog and wiki combined]].

Still not convinced about bliki as a word, partly a) because it sounds odd, b) it constrains you already to think of just blog and wiki features.

Still, will do for now as a start.


## What do I want?

-   something for both [[streams]] and [[the garden]].
-   the stream part is social


## What I'm currently doing

For the garden bit, I'm using [[org-roam]].  I actually write my stream bits first in org-roam, publish it to HTML, then just manually copy that HTML to WordPress and publish there for all the public stream stuff. As it's IndieWeb-enabled, WP gets me feeds for people to follow, and all the interactions you'd expect from streams - replies, likes, etc.

So it is manual until it hurts, but it doesn't hurt too much at present.  In fact, writing and hyperlinking with org-roam then copying it over is a lot more pleasant than writing straight in to WordPress.

But obviously there's quite a lot of redundancy there.


## Where I could go with it

I could use WordPress pages as my interlinked garden.  This would have the great benefit of having all the stream functionality OOTB.  I haven't explored WP for wiki pages much, but I know that [Ton does it](https://www.zylstra.org/blog/2018/04/adding-a-wiki-like-section/).   I think I personally won't do it this way as I find WordPress too much friction for me for writing, but having everything in one system is obviously a big boon.

I think if I could use [[Arcology]] combined with org-roam, that'd get me a pretty sweet bliki setup.  (With more on top, including some of the [[note-taking]] and [[sensemaking]] bits too).

But I think it'll be a while before I'm set up with Arcology, and even then, given it is static, it's missing a lot of the building blocks of the IndieWeb.  So I'll keep it as this manual Rube Goldberg device for now.

But, good to have a long-term goal&#x2026;

