# Salford

> Salford has a long and storied radical history. It was in The Crescent pub that Karl Marx and Friedrich Engels formed ideas that became central to the Communist Manifesto. 
> 
> &#x2013; [In Defence of Salford](https://tribunemag.co.uk/2019/12/in-defence-of-salford/) 

<!--quoteend-->

> In recent years, Salford’s left-wing council has been one of the few in the country to push back against the housing nightmare. Last month it announced the first new council houses in the area in 40 years – high-quality homes, provided for below market rent and excluded from Right to Buy. There are plans for hundreds more. 
> 
> &#x2013; [In Defence of Salford](https://tribunemag.co.uk/2019/12/in-defence-of-salford/) 


## Links

-   [Sensible Socialism: The Salford Model](https://tribunemag.co.uk/2021/01/sensible-socialism-the-salford-model)

