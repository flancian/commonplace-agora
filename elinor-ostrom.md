# Elinor Ostrom

-   https://doubleloop.net/?s=ostrom
-   [[Elinor Ostrom's Rules for Radicals]]: Cooperative alternatives beyond markets and states

> a resource arrangement that works in practice can work in theory

^ In a very small nutshell that sums up Ostrom's huge contribution to the commons.

