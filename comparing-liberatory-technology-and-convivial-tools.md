# Comparing liberatory technology and convivial tools

Feels like some overlaps between [[liberatory technology]] and [[convivial tools]].  How do they compare?

> One thinker who saw through the naïveté of Illich, the Homebrewers, and the Whole Earthers was the libertarian socialist Murray Bookchin. Back in the late sixties, he published a fiery essay called “Towards a Liberatory Technology,” arguing that technology is not an enemy of craftsmanship and personal freedom. Unlike Brand, though, Bookchin never thought that such liberation could occur just by getting more technology into everyone’s hands; the nature of the political community mattered. In his book “The Ecology of Freedom” (1982), he couldn’t hide his frustration with the “access-to-tools” mentality. Bookchin’s critique of the counterculture’s turn to tools parallels Dennett’s critique of the aesthetes’ turn to education eighty years earlier. It didn’t make sense to speak of “convivial tools,” he argued, without taking a close look at the political and social structures in which they were embedded.
> 
> &#x2013; [Making It | The New Yorker](https://www.newyorker.com/magazine/2014/01/13/making-it-2) 

So it seems  [[Evgeny Morozov]] is saying convivial tools are a bit technocratic, absent any political motivation.  Is that true?  He's saying it more in reference to Brand here, not necessary convivial tools.  But he does reference a naïveté of Illich.

I wonder if you put convivial tools as just one part of a larger, political framework (such as [[Free, Fair and Alive]] does), then this critique might go away.

