# Literature notes

> Whenever you read something interesting, take notes of the main points. Always write these main points in your own words: don’t copy and paste and be very selective with quotes, which can sometimes hide our lack of understanding of a text
> 
> &#x2013; [How to take smart notes - Ness Labs](https://nesslabs.com/how-to-take-smart-notes)

