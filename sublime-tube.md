# Sublime Tube

YouTube has a reputation for shit comments.  But it's not all bad.  I've noticed there's something about music videos on YouTube that seems to lead people to leave very deep and personal reflections on their life.  

Recollections of some great sadness or a long-ago memory of some revelatory moment.  Usually with tender responses.  I keep on stumbling on them.  Starting to build up a collection. 

I think it's because it's kind of public, and people have a desire to share, yet it's also _almost_ anonymous (noone really uses YouTube as a social network), so it's not like posting it to your friends on Facebook.

Collecting below, but without linking to the originals.  They are easy enough to find if you want, but I feel like they should be given some space.


## Magic mountain

> I took LSD and had the most profound experience looking out my window at a mountain. I was thinking about my mother who just recently passed and how there is so much we don't know or understand when it comes to life. We just happen to be here somewhere out in space - whatever that even means. At the end of the day all we can do is just be alive and ride this out. We're all experiencing this.


## Forgive

> I remember contemplating killing myself whilst listening to this on loop one cold winter afternoon. I was stood atop a cliff edge on the outskirts of my hometown - looking out into the distance. This song made me face the painful feelings inside me, memories of my childhood and the sadness I carried with me. This led me to that place, but it also helped me overcome those feelings and see everything as a timeless expanse, a story which continues to unfold.  I'm alive today and my story is far from over. I know now that we must forgive others not because they deserve it, but because we deserve peace.


## Love is all

> Earlier this month I sat for hours in hospital room witnessing my wife’s mother pass away. All week it rained and would let up only to start again. Dark rain and muted skies would give way to beams of sunlight on occasion, revealing massive cloud formations only to give way and repeat the cycle. This song perfectly sums up my emotional state between her labored breath. The brevity of life. Love is all you take with you, and leave behind. 


## Monky

> The longer you look at the monky, the sooner you realize that you are the monky. 


## Under the bridge

> Hello everyone or goodbye.. I'm homeless in a tent by the river with my cat. Her name is Air and she is a tortilla tortoiseshell. Obviously things aren't working out for me and my sock is wet, but I'll listen to this until my phone dies. Thank you for reading.

