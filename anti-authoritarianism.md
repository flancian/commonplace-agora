# Anti-authoritarianism

> Anti-authoritarianism is opposition to authoritarianism, which is defined as "a form of social organisation characterised by submission to authority", "favoring complete obedience or subjection to authority as opposed to individual freedom" and to authoritarian government. Anti-authoritarians usually believe in full equality before the law and strong [[civil liberties]]. 
> 
> &#x2013; [Anti-authoritarianism - Wikipedia](https://en.wikipedia.org/wiki/Anti-authoritarianism) 

<!--quoteend-->

> Sometimes the term is used interchangeably with [[anarchism]], an ideology which entails opposing authority or hierarchical organization in the conduct of human relations, including the state system.
> 
> &#x2013; [Anti-authoritarianism - Wikipedia](https://en.wikipedia.org/wiki/Anti-authoritarianism) 

