# Personal Wikis

To me, a **personal** [[wiki]] is (currently) a kind of digital form of a [[commonplace book]] - that is, an electronic scrapbook to record my thoughts along with snippets of things I see or hear elsewhere. 

The purpose is to help me [[note-take]], organise my thoughts, and think through new things.  A type of [[personal knowledge management]], I suppose.

I like the idea of calling a personal wiki a digital commonplace book, as there is lots of history to draw on from commonplace books.  

I tend to use commonplace book, personal wiki, and knowledge base fairly interchangebly.


## [[Wiki content]]

What do you put in a personal wiki?  


## [[Audience of a personal wiki]]


## [[Why have one?]]

> I find writing too hard to want to spend it on things that disappear
> 
> &#x2013; Martin Fowler ([What is a Bliki](https://martinfowler.com/bliki/WhatIsaBliki.html))


## [[How should you structure a personal wiki?]]


## [[Personal textbooks]]


## [[How personal is a personal wiki?]]


## [[Wikis I like]]

Some personal wikis for inspiration.


## [[Wiki tooling]]


## [[Wikis as hypertext]]

