# Improving my note-taking

I take notes on articles but it feels a bit of a piecemeal process at present.  I'd like to improve it.  [[How To Take Smart Notes]], [[Building a second brain]] et al could probably help.


## Context

Sometimes I read articles on my laptop.  Sometimes on my phone.  Sometimes on my Kobo.  The process is going to be slightly different for each.


## Current

When I'm reading articles at the moment on my laptop I create a page named after the article.  I put some metadata about the article, and then if present, the article's own summary of itself at the top.  I find it useful to have this in mind when reading.  I'll try to write a short idea about what I expect from the article too, to also help frame my reading of it.

As I'm reading through, I copy quotes that stand out to me.  If they're related to some other topic, I'll put them in to that topic's page, with a link back to the page for the article.  This way, I see all the pages that are referencing this article in the backlinks buffer, which is quite often a handy overview of the topics it covers.  These type of quotes are usually related to the setting of the scene of the article - they are defining their terms, and if I find those definitions useful, I'm pasting them in to the pages I have that also define those terms.

I've found this quite handy, but it probably goes against [[progressive summarisation]] a bit, where you have all your quotes collected together.  Maybe if I transcluded them in to the page.  That said, if they are just for terms definition, then it probably doesn't matter that much.

Sometimes I'll also copy quotes I'm interested in to the page specifically about the article, if they don't seem to sit anywhere else.  This usually suggests that they are this article's unique point.  

When I finish the article, I'll try to make a short summary.  I could definitely do this better, and this is where [[progressive summarisation]] could help.  I'll quite often share this summary or a version of it on social media.

