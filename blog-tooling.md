# Blog tooling

For my blog (my '[[stream]]') I use WordPress and all the IndieWeb plugins.

You get a ton of functionality out of the box with WP and the plugins.

The one thing I don't really like about WP is actually writing in it.  I don't like its editor.  I want to write in Emacs.  I adapted an existing [[Micropub]] client for Emacs and can use that, but it isn't really there yet in terms of all the functionality.  And how does stuff like Parse This work when you do it via Micropub?

