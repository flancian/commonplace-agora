# Wind power

> Blustery winter weather helped Great Britain’s windfarms set a record for clean power generation, which made up more than 40% of its electricity on Friday
> 
> &#x2013; [Windfarms in Great Britain break record for clean power generation](https://www.theguardian.com/environment/2020/dec/19/windfarms-in-great-britain-break-record-for-clear-power-generation) 

