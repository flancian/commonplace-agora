# Towards a Liberatory Technology

An
: [[article]]

URL
: https://theanarchistlibrary.org/library/lewis-herber-murray-bookchin-towards-a-liberatory-technology

Author
: [[Murray Bookchin]]

To help the noding of [[Liberatory technology]].

Reading this on my [[Kobo]] - got some highlights to copy over.

It's good and interesting so far.  Off top of head, I'd say Bookchin is certainly pro-technology.  He believes technological advances should enable us to live a more leisurely life.

> Technology must be viewed as the basic structural support of a society; it is literally the framework of an economy and of many social institutions

<!--quoteend-->

> is not surprising to find that the tension between promise and threat is increasingly being resolved in favor of threat by a blanket rejection of technology

^ Bookchin does not want this.

> There is a very real danger that we will lose our perspective toward technology, that we will neglect its liberatory tendencies, and, **worse, submit fatalistically to its use for destructive ends**

Right on.  That's what we definitely do not want.

Some interesting stuff around how historically in his opinion socialism (and anarchism?) has focussed too much on labour and toil.  Seizing the means of production, power to the workers, etc.  But without actually thinking on how technology can reduce toil.

> During this period, the propaganda of the socialists often sounded like a paean to toil; not only was toil "ennobling," but the workers were ex-tolled as the only useful individuals in the social fabric

<!--quoteend-->

> Instead of focusing their message on the emancipation of man from toil, socialists tended to depict socialism as a beehive of industrial activity, humming with work for all

<!--quoteend-->

> As we shall see,a new technology has developed that could largely replace the realm of necessity by the realm of freedom

O rly?  Wonder what this is?

> The demand for a guaranteed annual income is still anchored in the quantitative promise of technology—in the possibility of satisfying material needs without toil. This quantitative approach is already lagging behind technological developments that carry a new qualitative promise—the promise of decentralized, communitarian lifestyles, or what I prefer to call ecological forms of human association

Don't fully get this.  Interesting to see income guarantee mentioned though, back at the time of writing.

> purpose of this article is to explore three questions.
> 
> -   What is the liberatory potential of modern technology, both materially and spiritually?
> -   What tendencies, if any,are reshaping the machine for use in an organic, human-oriented society?
> -   And finally, how can the new technology and resources be used in an ecological manner—that is, to promote the balance of nature, the full development of natural regions, and the creation of organic, humanistic communities?

I like the fact he is very ecologically-minded.  Given this was written in the 60s, that's pretty precocious.

> I am asking a question that is quite different from what is ordinarily posed with respect to modern technology. Is this technology staking out **a new dimension in human freedom**, in the liberation of man? Can it not only liberate man from want and work, but **also lead him to a free, harmonious, balanced human community—an ecocommunity** that would promote the unrestricted development of his potentialities? Finally, can it carry man **beyond the realm of freedom into the realm of life and desire**? 

Waxing fairly lyrical here - technology staking out a new dimension in human freedom; leading to a free, harmonious, balanced human (eco)community; carrying folks beyond the realm of freedom into the realm of life and desire.  Don't get all [[All Watched Over by Machines of Loving Grace]] on me Murray!  To be fair he did say he was going to explore how technology could be liberatory both materially and spiritually.  In a way it is kind of saying technology should be counter to [[Alienation]].

> meaning of this qualitative advance has been stated in a rather freewheeling way by [[Vannevar Bush]]

Kind of cool to find this link between Bookchin and Bush.

> The abolition of mining as a sphere of human activity would symbolize, in its own way, the triumph of a liberatory technology

^ an example of liberatory technology.  Based on the assumption that mining is shit, and you don't really want humans to be doing it.  (Because it is "is the day-to-day actualization of man's image of hell".)

> James Watt's flyball governor, invented in 1788, provides an early mechanical example of how steam engines were self-regulated

He starts talking a bit cybernetically.  I think approvingly.  Cybernetics was in the air for sure in the 60s.  

> The question is whether a future society will be organized around technology or whether technology is now sufficiently malleable so that it can be organized around society

<!--quoteend-->

> do not claim that all of man's economic activities can be completely decentralized, but the majority can surely be scaled to human and communitarian dimensions. This much is certain: we can shift the center of economic power from national to local scale and from centralized bureaucratic forms to local, popular assemblies

Bookchin is generally a fan of [[decentralisation]].

> In our own time, the development of technology and the growth of cities has brought man's alienation from nature to a breaking point

[[Climate change]].  And this is written in the 60s, mind.

Bookchin talks about [[Free communities]].

> The population of the community is consciously limited to the ecological carrying capacity of the region

<!--quoteend-->

> Land management is guided entirely by ecological principles so that an equilibrium is maintained between the environment and its human inhabitants. Industrially rounded, the community forms a distinct unit within a natural matrix, socially and artistically in balance with the area it occupies

<!--quoteend-->

> Large-scale farming is permitted only where it does not conflict with the ecology of the region

<!--quoteend-->

> Every effort is made to blend town and country without sacrificing the distinctive contribution that each has to offer to the human experience. 

<!--quoteend-->

> The countryside, from which food and raw materials are acquired, not only constitutes the immediate environs of the community, accessible to all by food, but also invades the community

Town and country are very much intertwined.  Bookchin is O.G. [[solarpunk]].

> The [[Industrial revolution]] and the urbanized world that followed obscured nature's role inhuman experience—hiding the sun with a pall of smoke, blocking the winds with massive buildings, desecrating the earth with sprawling cities

<!--quoteend-->

> We became alienated from nature. Our technology and environment became totally inanimate, totally synthetic—a purely inorganic physical milieu that promoted the deanimization of man and his thought.To

<!--quoteend-->

> Culture and the human psyche will be thoroughly suffused by a new [[animism]]

I love this.  Animism.  I have seen this also recently in references to [[Indigenous and traditional ecological knowledge]].

> Actually, it is not that we lack energy per se to realize man's most extravagant technological visions, but we are just beginning to learn how to use the sources that are available in limitless quantity

Bookchin talks a bunch about agriculture.  He seems to know his shit. Hel also talks a bunch about renewable energy options.

Plenty on [[solar power]].

[[Tidal power]]:

> The French have already built an immense tidal-power installation near the mouth of the Ranee River at St. Malo with an expected net yield of 544 million kilowatt-hours annually

<!--quoteend-->

> In England, highly suitable conditions for a tidal dam exist above the confluence of the Severn and Wye rivers

[[Wind power]]:

> In England, for example, where a careful three-year survey was made of possible wind-power sites, it was found that the newer wind turbines could generate several million kilowatts, saving from two to four million tons of coal annually

[[Heat pumps]]: 

> The heat pump works like a mechanical refrigerator: a circulating refrigerant draws off heat from a medium, dissipates it,and returns to repeat the process

He suggests that they work best at a community level.

> Used locally and in conjunction with each other, they could probably meet all the power needs of small communities, but we cannot foresee a time when they will be able to furnish the electricity currently used by cities the size of New York, London or Paris.

And that at a regional level you can better achieve that town/country balance.

> With the growth of a true sense of regionalism every resource would find its place in a natural, stable balance, an organic unity of social, technological and natural elements

<!--quoteend-->

> A high standard of excellence, I believe, would replace the strictly quantitative criteria of production that prevail today; a respect for the durability of goods and the conservation of raw materials would replace the shabby, huckster-oriented criteria that result in built-in obsolescence and an insensate consumer society

[[Right to repair]], baby!  And [[Circular economy]] perhaps.

> Thus far every social revolution has foundered because the peal of the tocsin could not be heard over the din of the work-shop

Toil getting in the way again.

> With the arrival of mass production as the predominant mode of production, man became an extension of the machine, and not only of mechanical devices in the productive process but also of social devices in the social process

<!--quoteend-->

> When he becomes an extension of a machine, man ceases to exist for his own sake

<!--quoteend-->

> Free communities would stand at the end of a cybernated assembly line with baskets to cart the goods home

<!--quoteend-->

> In a liberated community the combination of industrial machines and the craftsman's tools could reach a degree of sophistication and of creative interdependence unparalleled in any period in human history. William Morris's vision of a return to craftsmanship would be freed of its nostalgic nuances. We could truly speak of a qualitatively new advance in technics—a technology for life.

<!--quoteend-->

> technology for life could play the vital role of integrating one community with another

<!--quoteend-->

> A technology for life must be based on the community; it must be tailored to the community and the regional level. On this level, however, the sharing of factories and resources could actually promote solidarity between community groups; it could serve to confederate them on the basis not only of common spiritual and cultural interests but also of common material needs

<!--quoteend-->

> Yet there is nothing in this society that would seem to warrant a molecule of solidarity. What solidarity we do find exists despite the society, against all its realities, as an unending struggle between the innate decency of man and the innate indecency of society

