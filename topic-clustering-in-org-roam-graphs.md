# Topic clustering in org-roam graphs

I would really like to get something like this working.

-   [Application of Graph Theory to Roam link network - Graphing Capabilities](https://org-roam.discourse.group/t/application-of-graph-theory-to-roam-link-network/61)

Hugo Cisneros has [something](https://hugocisneros.com/notes/) working that seems to be topic clustering on an org-roam graph:

![[2020-12-05_08-15-39_screenshot.png]]

