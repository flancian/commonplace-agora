# The Tyranny of Structurelessness

> Freeman described how "this apparent lack of structure too often disguised an informal, unacknowledged and unaccountable leadership that was all the more pernicious because its very existence was denied". As a solution, Freeman suggests formalizing the existing hierarchies in the group and subjecting them to democratic control. 
> 
> &#x2013; [The Tyranny of Structurelessness - Wikipedia](https://en.wikipedia.org/wiki/The_Tyranny_of_Structurelessness) 


## Links

-   [The Tyranny of Stuctureless](https://www.jofreeman.com/joreen/tyranny.htm)

