# The Telekommunist Manifesto

![[2020-07-12_20-52-23_screenshot.png]]

> The Manifesto covers the [[political economy]] of network topologies and cultural production respectively.

<!--quoteend-->

> Based on an exploration of [[class conflict]] in the age of international telecommunications, global migration, and the emergence of the information economy. 

<!--quoteend-->

> As a collective of intellectual workers, the work of Telekommunisten is very much rooted in the [[free software]] and [[Free culture]] communities. 

<!--quoteend-->

> This text is particularly addressed to politically motivated artists, hackers and activists


## [[Telecommunism]]


## [[Venture communism]]


## [[What is possible in the information age is in direct conflict with what is permissible]]

> However, a central premise of this Manifesto is that engaging in software development and the production of immaterial cultural works is not enough. The communization of immaterial property alone cannot change the distribution of material productive assets, and therefore cannot eliminate exploitation; only the self-organization of production by workers can.

<!--quoteend-->

> The challenge of extending the achievements of free software into free culture is addressed by connecting it to the traditional program of the socialist left, resulting in [[copyfarleft]] and offering the [[Peer Production License]] as a model.

<!--quoteend-->

> The internet started as a network that embodied the relations of peer-to-peer communism; however, it has been re-shaped by capitalist finance into an inefficient and un-free client-server topology.

-   https://networkcultures.org/blog/publication/no-03-the-telekommunist-manifesto-dmytri-kleiner/
-   https://www.networkcultures.org/_uploads/%233notebook_telekommunist.pdf

