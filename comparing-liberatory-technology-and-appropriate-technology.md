# Comparing liberatory technology and appropriate technology

Feels like some overlaps between [[Liberatory technology]] and  [[appropriate technology]].  How do they compare?

> I am borrowing this term from (still capitalist and hierarchical in its core) thinking of E.F. Schumacher. The aspect I want to remix into LibTech is „small-scale, decentralized, labor-intensive, energy-efficient, environmentally sound, and locally controlled”. All these requirements are fundamental, if we want the technology to support the social ecology, not to contradict it.
> 
> However we have to remember that – in the popular understanding – appropriate technology was developed as another incarnation of the „white man’s burden” – in a more or less paternalistic way. So, while we accept its technical specification, we should avoid referring to it as an ideological construct, as it inherently lacks two other, equally important, elements.
> 
> &#x2013; [Towards Liberatory Technology | Black Star Tech Group](https://blackstartech.wordpress.com/2015/10/28/liberatory-technology/)

<!--quoteend-->

> But still, even open and appropriate technology may be used to oppress people. The „magic sauce” here is in the way we put it into practice, implement and use it. And this has much more to do with the social and political control than with the technical solution itself. Eventually, the community itself is responsible and accountable for the way technology is used in their life. Outsourcing it is like outsourcing the governance or the economy – it is to outsource the freedom itself.
> 
> So, whenever we think about liberatory technology, the critical question is always: how this particular use of technology influences the levels of resilience, sustainability and empowerment of the community in question? 
> 
> &#x2013; [Towards Liberatory Technology | Black Star Tech Group](https://blackstartech.wordpress.com/2015/10/28/liberatory-technology/)

