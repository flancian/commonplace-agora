# Emacs and agency

Two things I really like about [[Emacs]] are its **longevity**, and the **[[agency]]** it gives you.  (In a way I guess longevity is a type of agency - you are not forced to stop using something when it stops being maintained).

Steve Yegge recently [wrote](https://medium.com/@steve.yegge/dear-google-cloud-your-deprecation-policy-is-killing-you-ee7525dc05dc) about how "[b]ackwards compatibility keeps systems alive and relevant for decades" and mentions Emacs as a prime example.  

He also has a great tl;dr of Emacs:

> GNU Emacs [&#x2026;] is a sort of hybrid between Windows Notepad, a monolithic-kernel operating system, and the International Space Station. It’s a bit tricky to explain, but in a nutshell, Emacs is a platform written in 1976 (yes, almost half a century ago) for writing software to make you more productive, masquerading as a text editor.
> 
> &#x2013; [Dear Google Cloud: Your Deprecation Policy is Killing You](https://medium.com/@steve.yegge/dear-google-cloud-your-deprecation-policy-is-killing-you-ee7525dc05dc) 

BSAG recently wrote about how easy it is to customise Emacs for your own needs:

> One of the delightful and surprising things about Emacs, as you get to know it better, is the depth of customisation which is available. Emacs can be a completely different editor for different people and for different purposes. Being able to tweak things on the fly and try them out before you commit to them, or even as a temporary fix to solve the particular problem you have right now, is empowering. 
> 
> &#x2013; [BSAG » Advising Emacs](https://www.rousette.org.uk/archives/advising-emacs/) 

Emacs is not perfect, and the barrier to entry is for sure a problem (there's an [epic thread](https://org-roam.discourse.group/t/the-state-of-org-roam-for-actual-beginners/494) on the org-roam Discourse discussing that).  But it's got some amazing qualities.

