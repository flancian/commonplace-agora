# Commons P2P wiki requirements

URL
: https://cryptpad.fr/pad/#/2/pad/view/L0+lq6UiUgxTn6xZku9xa6ULPNZSCfivOdWbLLjhUrQ/

A list of things required for decentralised wikis that combine together in a knowledge commons.


## Features

Not sure I agree with all of these.


### Automatic back-links

Could be provided by webmentions.


### Semantic links

Interesting.  Links to be given semantics.  rel attributes perhaps?


### Separation of page content from page metadata

Microformats could be an approach to this.


### Make categories to just be pages

Not really a feature of interlinking.


### There are a small number of page classes / types

Predefining types of things some in tension with bottom up wiki developement.


### Access control


### Edit history and reversion


### Viewing history


### Graphical rendering of the page graph


### Easy attribution tracking


### Effective search, maybe semantic


### Drag and drop from other pages


### Fine-grained related change indications


### Basic web compatibility


### Commenting and suggesting

