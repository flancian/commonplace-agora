# liberation theology

> In many Latin American countries, including Brazil after the 1964 military coup, authoritarian regimes took violent measures to silence dissidents, such as censorship, imprisonment, torture, and exile. Some of the most vocal critics of these measures were Catholic priests who sought to reorient the Church toward the organizing of the oppressed and the overcoming of domination. A key event in the formation of their movement, which would become known as “liberation theology,” was a 1968 conference of Latin American bishops held in Medellín, Colombia. At the landmark conference, the attendees learned of the dynamics of oppression in different countries, and collectively declared, “A deafening cry pours from the throats of millions of men, asking their pastors for a liberation that reaches them from nowhere else.”
> 
> &#x2013; [[Informatics of the Oppressed]]

