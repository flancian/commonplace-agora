# Property

> Property is, approximately, “who can do what with what” and it is a legal and social construct.
> &#x2013; [[Governing the Information Commons]]

<!--quoteend-->

> Property isn’t some naturally occurring thing that’s the same in all places and times. Every culture is different and every culture changes how they conceive of property over time.
> 
> &#x2013; [[Governing the Information Commons]]

