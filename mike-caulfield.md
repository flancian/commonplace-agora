# Mike Caulfield

Works on [[web literacy]].

Also is/was a proponent of a slower web than the social media streams.  See [[The Garden and the Stream]].

Posts on [[Federated wikis]] too.

[[Wikity]] was how I found my way in to the [[IndieWeb]].

