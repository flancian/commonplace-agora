# Doughnut Economics

I like how easily graspable as a visual rubric it is - don't let anyone go into the hole of the doughnut, i.e. have a baseline of equity for everyone, and don't go outside the outer edge - i.e. stay within planetary boundaries.

The bit in the middle, the doughnut, is a regenerative and distributive economy.


## Ecological ceiling

-   Climate change
-   Ocean acidification
-   Chemical pollution
-   Nitrogen & phosphorous loading
-   Freshwater withdrawals
-   Land conversion
-   Biodiversity loss
-   Air pollution
-   Ozone layer depletion


## Social foundation

-   Water
-   Food
-   Health
-   Education
-   Income & work
-   Peace & justice
-   Political voice
-   Social equity
-   Gender equality
-   Housing
-   Networks
-   Energy

![[doughnut-model.jpg]]

(CC-BY-SA 4.0 from https://commons.wikimedia.org/wiki/File:Doughnut_(economic_model).jpg)

