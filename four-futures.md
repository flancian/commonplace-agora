# Four Futures

&#x2013; Peter Frase.

I enjoyed this.  Looking at four possible socio-economic futures through the lens of science fiction.

They are: communism, socialism, rentism, and exterminism.

> Frase uses two intersecting spectrums to explore the potential futures he discusses. On the one hand, we have a spectrum running from inequality to hierarchy. On the other, we have scarcity to abundance. Mapping across these spectrums, Frase ends up outlining a typology of four futures: communism, rentism, socialism and exterminism.

Finished: April/May 2017.


## Articles

-   [Sim City 2k, Post-Capitalism, and ‘The Four Futures’. | by John Leavitt | Medium](https://medium.com/@LeavittAlone/sim-city-2k-post-capitalism-and-the-four-futures-b21de83381f7)

