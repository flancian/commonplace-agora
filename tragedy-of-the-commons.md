# Tragedy of the Commons

> Hardin’s “tragedy” thesis ought to be renamed “The Tragedy of Unmanaged, Laissez-Faire, Commons-Pool Resources with Easy Access for Non-Communicating, Self-Interested Individuals.”
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Even Hardin (1994) eventually acknowledged that the tragedy of the commons only applies to “unmanaged commons”, by which he meant simple open access resources with no use constraints. Of course, such resources are not commons, because it is the shared management that makes a resource a commons. 
> 
> &#x2013; [[dulongderosnay2020: Digital commons]]

