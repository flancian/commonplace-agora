# The Long Way to a Small, Angry Planet

by [[Becky Chambers]]

I read about a third of this a while back, then got into reading non-fiction at night.  But now I'm back on fiction at night again.

I've gotten back in to this pretty quickly - I really like it.  It's quite different to a lot of sci-fi - it's much more relationship based, less action heavy.  (So far at least).  It's quite gentle and gently-paced but by no means twee.  You get to know the characters and their feelings.  

