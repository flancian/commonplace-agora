# Bitcoin

> @aral @dsfgs @actualsteerpike No, Bitcoin is not a "net positive for the globe".
> 
> Bitcoin set out to disintermediate the banking system, but it failed.
> 
> What it produced was a horrendously inefficient energy-guzzling monstrosity, which only really empowers people who already had a lot of money in the economy prior to Bitcoin's invention. The usual suspects got richer out of Bitcoin and the banking system wasn't obsoleted by it.
> 
> &#x2013; [bob@epicyon.freedombone.net](https://epicyon.freedombone.net/@bob/105605793565967273)

<!--quoteend-->

> @dsfgs @bob @aral @actualsteerpike If I look at the metric of "is the banking system gone yet?" I notice that indeed, no, Bitcoin has not made even a ding in the banking system. The same crooks are running the same old international scams, politicians are still stuffing their ill-gotten cash in offshore accounts, and Bitcoin has made no difference.
> 
> I can also look at the independent variable of "are people spending bitcoins on stuff they actually need?", and indeed again no, nobody around here uses bitcoins, or accepts bitcoins as payment for goods or services.
> 
> So really on every measurable indicator, Bitcoin has been a failure.
> 
> &#x2013; [bob@epicyon.freedombone.net](https://epicyon.freedombone.net/@bob/105605859605610854)

<!--quoteend-->

> Both blockchain and digital currencies offer a great deal of potential, but Bitcoin itself is a deflationary, capitalist medium that functions primarily as an investment asset to concentrate wealth, and is mostly favored by right-libertarian hard money ideologues.
> 
> &#x2013; [Center for a Stateless Society » Review: A DisCO Manifesto](https://c4ss.org/content/52450) 

