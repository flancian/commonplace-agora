# Asymmetrical bandwidth

> Connections to private homes generally have greater download bandwidth than upload bandwidth. This means that you can get information from a data centre faster than you can get it from your neighbour. With the popular protocol HTTP, client requests tend to be small in comparison to server responses. So narrow upload bandwidth hinders people from serving content themselves.
> 
> &#x2013; [[Seeding the Wild]]

<!--quoteend-->

> Multiple-source ‘swarming’, as is used by peer-to-peer protocols like BitTorrent, can overcome the problem of asymmetrical upload/download bandwidth.
> 
> &#x2013; [[Seeding the Wild]]

