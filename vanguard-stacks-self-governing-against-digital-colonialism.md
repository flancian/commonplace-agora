# Vanguard Stacks: Self-Governing against Digital Colonialism

URL
: https://osf.io/pdjse/?view_only=09431d47da2542d1a9a88f9f615d455c

Author
: [[Nathan Schneider]]

It was renamed to [[Governable Stacks against Digital Colonialism]].


## Summary

-   Great article.
-   [[Digital self-governance]] as tactic against [[Digital colonialism]].
-   [[Is the phrase 'digital colonialism' problematic?]]
-   [[Vanguard stack]]s.
-   Has elements part of the [[Horizontalism vs verticalism]] discussion.
-   Seems to look for a synthesis between spontaneity (e.g. [[Horizontalism]]) and organisation ([[Verticalism]]).
-   The upshot being, I think, suggesting that we need tools and platforms (social media and otherwise) that not only connect us, but also help us organise.
-   It seems to be a combo of [[Prefiguration]] and [[Vanguardism]].
-   [[Is there a connection between prefiguration and vanguardism?]]


## Notes

> Page 1 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-17 Sat 21:50]</span></span>:
> Although acts of insurrection may attract more attention, one thing leaders and theorists of anticolonial resistance have stressed is the centrality of self-governance in everyday life as both a means and end of their movements.

<!--quoteend-->

> Page 2 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:22]</span></span>:
> [[Toussaint Louverture]]

<!--quoteend-->

> Page 3 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:23]</span></span>:
> power and resistance seem to emanate from self-organizing and the media that enable it. What are the comparable media of sovereignty today?

<!--quoteend-->

> Page 5 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:25]</span></span>:
> Theorist-practitioners of anticolonial resistance have frequently stressed the centrality of self-governance in everyday life as both the means and end of their movements, alongside acts of outright insurrection.

<!--quoteend-->

> Page 6 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:27]</span></span>:
> the platforms most available for online organizing are not well-suited for self-governing;

<!--quoteend-->

> Page 6 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:27]</span></span>:
> Campaigns of digital resistance often employ the same colonial firms they oppose.

<!--quoteend-->

> Page 7 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:28]</span></span>:
> “allocation of coordination rights.”

● same aŝ auʈority in networks? 

> Page 7 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:35]</span></span>:
> Individual users might have options on a platform to access or delete their own personal data, yet platform companies alone can develop products from the users’ data in aggregate

● imagine if your platfrm built a digital garden for you wth all the inghts the bjg tech platfrms knkw about ou Individual users might have options on a platform to access or delete their own personal data, yet platform companies alone can develop products from the users’ data in aggregate

> Page 9 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:38]</span></span>:
> What would a liberatory, networked, and democratic vanguard look like?

<!--quoteend-->

> Page 10 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:40]</span></span>:
> [[lumpenproletariat]] , “the most spontaneous and the most radically revolutionary forces of a colonized people”

<!--quoteend-->

> Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:41]</span></span>:
> decision without institution.

<!--quoteend-->

> Page 11 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:42]</span></span>:
> age of networks has only deepened the allure of spontaneity among radical theorists,

● arguing that networked spontaneity alone is not the answer i think. Nathan has background in occupy where lack of organisation was a problem long-term.

> Page 12 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:43]</span></span>:
> [[adrienne maree brown]] (2017) presents spontaneous self-organization in nature as a theory of social change.

<!--quoteend-->

> Page 12 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:46]</span></span>:
> Where there is spontaneity among the masses, it obtains power only through an organized and disciplined vanguard party, such as the one he would lead in Russia.

<!--quoteend-->

> Page 12 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:47]</span></span>:
> Rosa Luxemburg (1904) recoiled at the rigidity of Lenin’s vanguard, one molded by the discipline of the factory, army, and bureaucracy. Yet a communist regime came to pass in Germany

<!--quoteend-->

> Page 13 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:48]</span></span>:
> Her longing persists: a vanguard firm enough to gain power yet supple enough to wield it humanely.

<!--quoteend-->

> Page 13 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:51]</span></span>:
> He held that spontaneous energies must find institutional cohesion.

<!--quoteend-->

> Page 13 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:52]</span></span>:
> He celebrated the Paris Commune as a forerunner of the Russian soviets, regarding that uprising as “first and foremost a democracy”

<!--quoteend-->

> Page 14 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:55]</span></span>:
> dialectical humanism,”

<!--quoteend-->

> Page 15 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 07:59]</span></span>:
> turned from achieving state communism to commoning, so that through stewarding shared projects and resources, people can continually rediscover what they are seeking to achieve.

<!--quoteend-->

> Page 16 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:00]</span></span>:
> brown’s “[[emergent strategy]]” for activists revels not in conflict with corporate oppo-nents but in apparitions of friendship in online threads and tips for weaving consensus process.

<!--quoteend-->

> Page 17 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:05]</span></span>:
> the learning and planning that takes place among groups of people in spaces ungovernable to reigning institutions. Like the maroons of Saint-Domingue or the American South, study involves an order of its own, apart from the colonial university, a practice of impenetrable self-rule.

<!--quoteend-->

> Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:07]</span></span>:
> how tribal nations can produce sovereignty through “community-scale activities” such as cooperatives and assemblies in everyday life

<!--quoteend-->

> Page 18 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:08]</span></span>:
> the rejection of governance shape-shifts into a call for its reconstruction.

<!--quoteend-->

> Page 21 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:14]</span></span>:
> And yet these legacies also assert another kind of self-governing, the kind that is ungovernable and opaque and maroon from the vantage of the imperial capitals—a site of study and planning, conversation and connection.

<!--quoteend-->

> Page 21 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:20]</span></span>:
> The species of spontaneity most at home in the digital economy is “virality, ” the explosive spreading and equally rapid subsiding of online content.

<!--quoteend-->

> Page 22 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:21]</span></span>:
> On colonial platforms, too, users direct their energy for and against each other, gaining influence and affirmation through their jousts and, in doing, identifying themselves ever more deeply with the non-transferable reputation they obtain on the platforms.

<!--quoteend-->

> Page 23 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:23]</span></span>:
> Platforms optimize for chatter as “engagement,” not decision, resolution, or consensus. Community control is not in the spec.

<!--quoteend-->

> Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:25]</span></span>:
> the classic satire of mechanized capitalism, [[Modern Times]]

<!--quoteend-->

> Page 24 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:26]</span></span>:
> vanguard stacks”: an orientation toward ungovernable organizing under digital colonialism.

<!--quoteend-->

> Page 26 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:29]</span></span>:
> In the sense of Grace Lee and Jimmy Boggs’s dialectical humanism, vanguard stacks invite the people who use them to change their relationship with technologies, and to imagine different sorts of technologies, and to become changed themselves.

<!--quoteend-->

> Page 26 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:30]</span></span>:
> Vanguard stacks already lurk in the archaeology of colonial systems through legacies like Indymedia, an activist social network whose participatory servers and software prefigured the corporate “Web 2.0”

<!--quoteend-->

> Page 28 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:33]</span></span>:
> Vanguard stacks do not seek merely to improve the occupier. “Decolonization is not an ‘and,’ ” as Tuck and Yang (2012) put it. “It is an elsewhere.” I take inspiration

<!--quoteend-->

> Page 30 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:35]</span></span>:
> The lifeblood of the vanguard stack is not its tools but the self-governance surrounding them. Communities, families, and movements can assemble and adjust their stacks over time, wherever possible seeking to make their technological lives ever more vanguardist.

<!--quoteend-->

> Page 33 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 08:38]</span></span>:
> 2018).Amelia (2020) draws on the Haudenosaunee practice of wampum agreements to propose “ethical dependencies” in software, which would encode and enforce certain commitments up and down the stack.

<!--quoteend-->

> Page 37 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:46]</span></span>:
> What makes technology sovereign is when its stewards are the people who depend on it, protected from outside control by any legal or extra-legal means available.

<!--quoteend-->

> Page 37 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:46]</span></span>:
> The data, the algorithms, and the interfaces are for their users, rather than acting surreptitiously against them.

<!--quoteend-->

> Page 37 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:46]</span></span>:
> The other side of sovereignty is democracy —its guarantor and its everyday practice.

<!--quoteend-->

> Page 38 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:48]</span></span>:
> Mastodon and its ilk have shown that coun-tering hate speech doesn’t require the might of a global monopoly; it can be participatory and empowering.

<!--quoteend-->

> Page 40 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:49]</span></span>:
> [[economic disobedience]]” (cooperativa.cat/economic-disobedience), which includes refusing payment of unjust taxes or interest. Insurgents might choose not to submit the data

<!--quoteend-->

> Page 40 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:49]</span></span>:
> Insurgents might use colonial platforms for education and organizing, but if they have vanguard stacks they can go back to, they are more than just viral subjects—they are maroons, with swamps and forests of their own.

<!--quoteend-->

> Page 40 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:50]</span></span>:
> Goldman Sachs doesn’t care if you’re raising chickens,” the political theorist Jodi Dean (2011) has said. It’

[[Goldman Sachs doesn't care if you raise chickens]]

> Page 41 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:51]</span></span>:
> what we practice at the smal l scale sets the patterns for the whole system.”

<!--quoteend-->

> Page 41 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:51]</span></span>:
> As above, so below.” Dean doesn’t want such faith to distract her comrades from organizing for secular power.

<!--quoteend-->

> Page 42 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:52]</span></span>:
> They normalize among vanguards the otherwise elusive fact that better ways of organizing technology are possible— because we already do it ourselves.

<!--quoteend-->

> Page 43 <span class="timestamp-wrapper"><span class="timestamp">[2021-04-18 Sun 10:53]</span></span>:
> Will vanguard stacks become the specialty of reactionaries? The stack has already become a field of conflict. Failing to claim the vanguard position for liberation means ceding it for other purposes. There is

