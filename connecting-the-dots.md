# connecting the dots

While it's useful to break down ideas into fine-grained units, [[collecting the dots]], you have to connect them back together again to make sense of them.  A collection of dots isn't much use (although just navigating around them can be fun).

After connecting the dots, I kind of think you need to colour the picture in, and then hang it on the wall for others to see.

> Knowledge is not an accumulation of facts, nor is it even a set of facts and their relations. Facts are only rendered meaningful within narratives, and the single-page document is a format very conducive to narrative structure. 
> 
> &#x2013; [Open Transclude](https://subpixel.space/entries/open-transclude/) 

<!--quoteend-->

> People often get carried away when they discover the original vision of [[hypertext]], which involves a network of documents, portions of which are “[[transcluded]]” (included via hypertext) into one another. The implication is that readers could follow any reference and see the source material—and granted, this would be transformative. However, there’s a limit to the effectiveness of the knowledge network as a reading experience. “Hypertext books,” online books which are made up of an abundance of interlinked HTML pages, are mostly unpopular.
> 
> &#x2013; [Open Transclude](https://subpixel.space/entries/open-transclude/) 

[[Pattern recognition]].

