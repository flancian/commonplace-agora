# Automatic photo syncing from Android to NextCloud

This is really nice.  It should go a long way to replacing Google Photos, which was the last holdout for me of something that was useful on my Android phone that only Google provided.


## How I did it

You need the NextCloud app on your phone.  I got it from [[F-Droid]]: https://f-droid.org/en/packages/com.nextcloud.client/.

Then, if you turn on 'Auto upload' (in the sidebar of the app), that will let you find photo folders anywhere on your device (internal and on SD card), and set them to be auto-uploaded to your [[NextCloud]] instance.

I'd struggled with [[syncthing]] in the past for syncing photos, I can't remember the exact details now, but I think it was pointing it at a folder on SD card that it didn't own, something like that.  So the fact that NextCloud can work on folders that are on the SD card is great.  (I might be misremembering the syncthing problem.)

You can set various settings for how that folder is uploaded (remote folder location, upload only on wi-fi, etc).  For the remote folder I've kept it as the default /InstantUpload/Camera so far.

I use [[Open Camera]], which you can set to take photos to a folder on the SD card.

So et voila, I set Open Camera to take photos to /sdcard/DCIM/Camera, then set NextCloud to auto-upload /sdcard/DCIM/Camera to NextCloud.  I also have the NextCloud sync app set up on my desktop, so the photos pop up pretty quickly on my laptop, too!  It's nice.


## Things to tweak

-   I'd like some structure to the uploads, so I'm going to try the setting 'Use subfolders', which should put the uploaded photos in year/month folders
-   Can I just point the auto uploads at the Photos folder rather than InstantUpload?
-   I met set the photos to auto-delete as well from the camera, once I'm happy that everything is going safely to NextCloud and getting backed up
-   Migrate existing photos from Google Photos to NextCloud


## Resources

-   [How to set auto-upload on the Nextcloud mobile app - TechRepublic](https://www.techrepublic.com/article/how-to-set-auto-upload-on-the-nextcloud-mobile-app/)

