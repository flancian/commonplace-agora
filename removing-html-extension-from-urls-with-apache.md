# Removing .html extension from URLs with Apache

([[Apache]])

There's loads of snippets for doing this knocking around on the web.  This one worked best for me:

https://gist.github.com/davidvandenbor/f5a2c18c472ceb68d0dd

I just put it in .htaccess.

```nil
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteBase /

# Remove the .html extension
    RewriteCond %{THE_REQUEST} ^GET\ (.*)\.html\ HTTP
    RewriteRule (.*)\.html$ $1 [R=301]

# Remove index and reference the directory
    RewriteRule (.*)/index$ $1/ [R=301]

# Remove trailing slash if not a directory
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} /$
    RewriteRule (.*)/ $1 [R=301]

# Forward request to html file, **but don't redirect (bot friendly)**
    RewriteCond %{REQUEST_FILENAME}.html -f
    RewriteCond %{REQUEST_URI} !/$
    RewriteRule (.*) $1\.html [L]
</IfModule>
```

