# Platformism

> The most hard-core “organizational” anarchist trend is “Platformism,” which takes its name from the 1926 Platform advanced by Nestor Makhno, Peter Arshinov, Ida Mett and several others associated with the Paris-based émigré Russian anarchist paper Dielo Truda.
> 
> &#x2013; [Anarchist Organization and Vanguardism - In Defense of Leninism](http://www.bolshevik.org/1917/no29/NEFACarticle.html) 

<!--quoteend-->

> began with the publishing of the "[[Organizational Platform of the Libertarian Communists]]"
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

<!--quoteend-->

> Exiles of the Russian revolution, the Paris-based [[Dielo Trouda]] criticized the anarchist movement for its lack of organization, which prevented a concerted response to [[Bolshevik]] machinations towards turning the workers’ soviets into instruments of one-party rule
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

<!--quoteend-->

> The authors of the Platform were veteran partisans of the Russian Revolution. They helped lead a peasant guerilla war against Western European armies and later the [[Bolsheviks]] in the Ukraine, whose people had a history independent of the Russian Empire.
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

<!--quoteend-->

> So the writers of the Platform certainly spoke from a wealth of experience and to the historical context of one of their era’s pivotal struggles. But the document made little headway in its proposal of uniting class struggle anarchists, and is markedly silent in analysis or understanding on numerous key questions that faced revolutionaries at that time, such as the oppression of women, and colonialism.
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

