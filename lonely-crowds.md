# lonely crowds

> The proliferation of images and desires alienates us, not only from ourselves, but from each other. Debord references the phrase “[[lonely crowds]],” a term coined by the American sociologist David Riesman, to describe our atomization
> 
> &#x2013; [An Illustrated Guide to Guy Debord's 'The Society of the Spectacle'](https://hyperallergic.com/313435/an-illustrated-guide-to-guy-debords-the-society-of-the-spectacle/) 

