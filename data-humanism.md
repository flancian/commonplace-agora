# Data humanism

[- Data Humanism, the Revolution will be Visualized. - giorgia lupi - Medium](https://medium.com/@giorgialupi/data-humanism-the-revolution-will-be-visualized-31486a30dbfb#.fftsvgn90)

-   Takeaway: you can't just chuck data into a dataviz tool and expect insights
-   Insights are human.  A good way is sit down and draw before using a tool.

