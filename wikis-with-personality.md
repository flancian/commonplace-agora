# Wikis with personality

My favourite wikis to read are those that have some personality.  Random trains of thoughts, questions to self, just general half-bakedness.  These are usually a lot more fun to browse around on than those that read like a pre-print of a research paper.

