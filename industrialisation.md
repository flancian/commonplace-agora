# Industrialisation

> To many people there seemed to be a direct, one-to-one relationship between technological advances and social progress; a fetishism of the word "industrialization" excused the most abusive of economic plans and programs
> 
> &#x2013; [[Towards a Liberatory Technology]]

