# Communalism

A vision and political strategy, chief proponent [[Murray Bookchin]].

[[Directly democratic]], [[anticapitalist]], [[ecological]], and opposed to domination.  Governance through [[popular assemblies]] bound together in confederation.

> The excitement and solidarity on the ground has yet to coalesce into a political praxis capable of eliminating the current array of repressive forces and replacing it with a visionary, egalitarian—and importantly, achievable—new society. [[Murray Bookchin]] directly addresses this need, offering a transformative vision and new political strategy for a truly free society—a project that he called “Communalism.”
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> [[Bookchin]]’s Communalism circumvents the stalemate between the state and the street—the familiar oscillation between empowering but ephemeral street protest and entering the very state institutions designed to uphold the present order.
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> Communalism moves beyond critique to offer a reconstructive vision of a fundamentally different society—directly democratic, [[anticapitalist]], [[ecological]], and opposed to all forms of domination—that actualizes freedom in popular assemblies bound together in confederation. Rescuing the revolutionary project from the taint of authoritarianism and the supposed “end of history,” Communalism advances a bold politics that moves from resistance to social transformation.
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> Communalist politics suggests a way out of the familiar deadlock between the anarchist and Marxist traditions
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> Bookchin instead returns to the recurrent formation arising in nearly every revolutionary upsurge: [[popular assemblies]]
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> As [[David Harvey]] observed in his book [[Rebel Cities]], “Bookchin’s proposal is by far the most sophisticated radical proposal to deal with the creation and collective use of the commons across a wide variety of scales.”
> 
> &#x2013; [[The Next Revolution]]

<!--quoteend-->

> From Marxism, it draws the basic project of formulating a rationally systematic and coherent socialism that integrates philosophy, history, economics, and politics. Avowedly dialectical, it attempts to infuse theory with practice. From anarchism, it draws its commitment to antistatism and confederalism, as well as its recognition that hierarchy is a basic problem that can be overcome only by a libertarian socialist society.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> Communalism constitutes a critique of hierarchical and capitalist society as a whole. It seeks to alter not only the political life of society but also its economic life. On this score, its aim is not to nationalize the economy or retain private ownership of the means of production but to municipalize the economy.
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> Finally, Communalism, in contrast to anarchism, decidedly calls for decision-making by majority voting as the only equitable way for a large number of people to make decisions. Authentic anarchists claim that this principle—the “rule” of the minority by the majority—is authoritarian and propose instead to make decisions by consensus. [[Consensus]], in which single individuals can veto majority decisions, threatens to abolish society as such. 
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> Communalists would see their program and practice as a process. Indeed, a transitional program in which each new demand provides the springboard for escalating demands that lead toward more radical and eventually revolutionary demands
> 
> &#x2013; [[The Communalist Project]]

<!--quoteend-->

> Significantly, Communalists do not hesitate to run candidates in municipal elections who, if elected, would use what real power their offices confer to legislate [[popular assemblies]] into existence.
> 
> &#x2013; [[The Communalist Project]]

