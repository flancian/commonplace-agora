# Law of Requisite Variety

> This principle states that in order to be a sustaining organization, you must maintain an equilibrium in variety. This means that if you allow for no freedom to try things differently to your teams, your organization will not be able to adjust to changing environmental conditions. If you allow too much variety, chaos will prevent you from moving forward.
> 
> &#x2013; [Why Does Agile Fail?](https://wakingrufus.neocities.org/fail-agile.html):

<!--quoteend-->

> This is often called the Law of Requisite Variety or, after its founder [[Ross Ashby]], Ashby’s Law; this states that for any system to remain stable and to operate effectively in its environment, the system itself must display the same level of variety and flexibility as that of the environment that it operates
> 
> &#x2013; [[Anarchist Cybernetics]] 

