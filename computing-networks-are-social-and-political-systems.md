# Computing networks are social and political systems

> Computing networks are social and political systems. We should attempt to answer how the technical design of a network will influence the internal politics.
> 
> &#x2013; [[Information Civics]] 

<!--quoteend-->

> Every network has a political system which underpins its functions.
> 
> &#x2013; [[Information Civics]] 

<!--quoteend-->

> Every network architecture hides a power structure.
> 
> &#x2013; David de Ugarte (according to [[DisCO Manifesto]])

<!--quoteend-->

> Behind each node or computer there are living human beings with bodies that need nourishment, sleep and affection, and these topologies represent how they relate, whether supported by online networks or not.
> 
> &#x2013; [[DisCO Manifesto]]

