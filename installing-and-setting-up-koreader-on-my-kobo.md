# Installing and setting up koreader on my Kobo



## Installation

-   [[koreader]] homepage: https://github.com/koreader/koreader
-   installation via Linux: https://github.com/koreader/koreader/wiki/Installation-on-Kobo-devices

Installation all worked fine via the zip and script in this thread: https://www.mobileread.com/forums/showthread.php?t=314220

Unzip the zip script, run the script.  Bingo, as simple as that.

Launch koreader from the 'Nickel Menu' option in the normal menu.


## Configuration


### Wallabag

-   https://github.com/koreader/koreader/wiki/Wallabag

-   Go to the spanner and screwdriver icon.
-   Wallabag / Settings / Configure Wallabag server
-   save the settings empty to create the settings file, then connect the Kobo to laptop and edit `.adds/koreader/settings/wallabag.lua`
-   you need client credentials from Wallabag, which is in My Account / API clients management when you're logged in.  Create a New Client for Kobo.  (note&#x2026; Wallabag doesn't do OAuth, so you have to provide your username and password too, which is not great.  But it's [in discussion](https://github.com/wallabag/wallabag/issues/2800).)
-   Retrieve new articles from server.

