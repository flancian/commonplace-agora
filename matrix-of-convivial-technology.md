# Matrix of Convivial Technology

Described in the paper [[The Matrix of Convivial Technology - Assessing technologies for degrowth]].

Looks very handy for understanding some of the aspects of what it is for a tool to be 'convivial'.  

Interesting to note they make the distinction of convivial technology vs [[convivial tools]] - and seem to intentionally rule out institutions/organisations from their analysis tool (if I understood right).  Probably just to help narrow it down. Not 100% clear on that distinction yet, just be aware it exists.


## Five dimensions of convivial technologies

-   Relatedness
-   Accessibility
-   Adaptability
-   Bio-interaction
-   Appropriateness

