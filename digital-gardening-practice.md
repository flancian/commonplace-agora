# digital gardening practice

I don't really have one at the moment, but I'm getting in to a better 'daily log' habit.  I tend to start in a private journal and add bits from that in to the public daily log as and when it makes sense to share.  I'm starting to pop a few more work-related notes in during the workday, too.

