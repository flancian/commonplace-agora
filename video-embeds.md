# Video embeds

> Do you have a rationale for when you link to one or the other?

I would prefer to always link to Invd rather than Youtube.  The problem I've found is that I often get a message 'The media could not be loaded, either because the server or network failed or because the format is not supported.' for the videos that I link to.  I wonder if it is more prevalent for music videos because of copyright issues?  (Perhaps not, because presumably they'd get taken off Youtube too if there was a taken request?)

> I tend to prefer to give links to original sources (YT in this case), but find good to expose others to the much better interface of invd.

Also, if the link is embedded on your website, I also dislike the fact that linking to Youtube videos means the inclusion of Youtube tracking.  This might also be the case when linking from elsewhere when the video 'auto embeds'.  

That's a good point about linking to the original source.

One approach, at least on your own website, is to have a static image that links to the video, and a warning to the visitor that they will be tracked if they choose to click it.  This is what DuckDuckGo does and it seems like a good compromise.

If it is just a plain link where 'youtube' is cleary visible, it is OK I think - at least the visitor can tell that it is going to take them to Youtube and choose whether to click or not.  Still, I'd use [[Invidious]] if the video is working there.

> however I switched from https://invidio.us/watch?v=eTzLkc9bnIA to https://invidious.snopyta.org/watch?v=eTzLkc9bnIA as although the URL is more complicated I found performance is better.

Ah, that's interesting - I might try using a different Invidious URL. (I tried snopyta and it is much more performant).

Thanks for the question, it's reminded me that I really don't want to include Youtube videos on my site, at least not without the extra 'opt-in' step for the visitor.  I got lazy because Invidious wasn't always working, but I'm going to try and find a better solution!

