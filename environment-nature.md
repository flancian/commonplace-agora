# Environment & nature

-   [[Social ecology]]
-   [[Climate change]]
-   [[Places I like]]
-   [[Behaviour change or system change]]
-   [Reflections prompted by #ClimateStrike](https://rhiaro.co.uk/2019/09/reflections-climate) &#x2013; Amy Guy
-   [[Green New Deal]]
-   The New Alchemists
    -   scientific experiments in sustainable living
    -   kind of like the fact it wasn't a full on commune&#x2026; that doesn't jibe fully with my personality.
    -   "it almost looks like a parody" - funny how happy, healthy living has become that&#x2026;

-   Towns and cities
    -   living in a city vs living in a town, what are the environmental pros and cons?
    -   Garden stroll illusive realm video
        -   nice video at Chinese Centre for Contemporary Art
        -   made you think about gardens in the city. Are they just simulacra?  Do they really serve a purpose?

