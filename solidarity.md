# Solidarity

Sometimes I feel powerless when shit is happening on the other side of the world.  But it's always important to remember that solidarity is always needed for those in struggle.

I like the film [Nae Pasaran](https://naepasaran.com/) as an example of how international solidarity can make a difference.  At some scale.  It didn't stop Pinochet's dictatorship.  But it may have spared some lives.

> Under an authoritarian government that becomes ever more repressive as it doubles down on austerity policies, privatisation and precarisation, we will need as much international solidarity as possible. The Brazilian working class and the left is under attack of a vicious political project that is globally articulated with the most reactionary currents in the world. We can only beat the populist right with an internationalist working class movement. Our weapon is solidarity, our struggle is international.
> 
> &#x2013; [Lula Livre](https://tribunemag.co.uk/2019/11/lula-livre/), Tribune

.

> On his way to exile, he wrote that he is “very grateful to the solidarity of the people, brothers from Bolivia and the world who reach out with recommendations, suggestions and expressions of recognition that give us encouragement, strength and energy. They moved me to tears. They never abandoned me; I will never abandon them.”
> 
> &#x2013; [Bolivia’s Far-Right Coup](https://tribunemag.co.uk/2019/11/bolivias-far-right-coup/) 

