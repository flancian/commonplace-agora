# Vaccinations and acquired immunity mean that there is no longer much risk of becoming severely unwell from Covid

[[Covid vaccination]].  [[Coronavirus]].

[‘More people will die’: fears for clinically vulnerable as England axes plan &#x2026;](https://www.theguardian.com/world/2022/jan/22/more-people-will-die-fears-clinically-vulnerable-england-axes-plan-b)


## Except

-   For those [[suppressed immune system]]s.


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Signs point to yes]]

I believe it but the 'Except' is very important. 

