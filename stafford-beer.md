# Stafford Beer

> Beer’s admirers view his intelligence, breadth of knowledge, and willingness to think in unconventional ways as signs of misunderstood genius. On the other hand, his detractors paint a picture of a self-promoter who made grandiose claims that were not backed by his actual accomplishments
> 
> &#x2013; [[Cybernetic Revolutionaries]]

