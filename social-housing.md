# social housing



## Social housing estates

-   Alexandra road estate
-   Adelaide road estate
-   Lion Green Road
-   Spa green
-   Keeling house


## A home for all

Was an exhibition at the V&A.  On social housing projects in the UK.

This fact was pretty amazing: 

> In the mid-1970s nearly half the population of England and Wales lived in council houses

But as we know… 

> with the election of Margaret Thatcher in 1979 the housing sector underwent rapid privitisation. Local authority architects’ departments were disbanded, public land was sold off, and millions of council homes were bought[…]

![[A_home_for_all/2020-04-11_22-54-00_1546721627881.jpg]]

![[A_home_for_all/2020-04-11_22-55-09_1546721647137.jpg]]

![[A_home_for_all/2020-04-11_22-55-35_1546721660648.jpg]]

![[A_home_for_all/2020-04-11_22-56-02_1546721686490.jpg]]

