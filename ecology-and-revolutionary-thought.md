# Ecology and Revolutionary Thought

&#x2013; [Ecology and Revolutionary Thought | The Anarchist Library](https://theanarchistlibrary.org/library/lewis-herber-murray-bookchin-ecology-and-revolutionary-thought) 

> That essay’s groundbreaking synthesis of [[anarchism]], [[ecology]], and [[decentralization]] was the first to equate the grow-or-die logic of capitalism with the ecological destruction of the planet and presented a profound new understanding of capitalism’s impact on the environment as well as social relations.
> 
> &#x2013; [[The Next Revolution]]

