# The Radical Imagination of the Paris Commune

URL
: https://tribunemag.co.uk/2021/03/the-radical-imagination-of-the-paris-commune

Publisher
: [[Tribune]]

The [[Paris Commune]] as a resource for thinking about alternative ways of organising a society.

> The Commune becomes the figure for a history, and perhaps of a future, different from the course taken by capitalist modernisation, on the one hand, and utilitarian state socialism, on the other.

[[Communal luxury]].

> The thought and theory of a movement is unleashed only with and after the movement itself. Actions create dreams, and not the reverse.

