# community of gardens

The idea is that to a small degree we might be responsible for the upkeep of others' sites, such that our [[digital gardens]] are not quite so fenced off from each other.  It's sounds like something more than simply commenting on others' posts. It's a nice phrase, kind of a form of [[networked learning]]. 

> The garden metaphor is a compelling vision for what a blog can be. It implies that our thoughts can grow over time with the right kind of nurturing care. 
> 
> [&#x2026;] But sometimes it feels as though these gardens are enclosed.    Sure, a blog might allow comments, but this feels as though we are operating on a layer above the soil. Are others planting anything new, tending to the weeds in our garden, or are they talking to us from the fence that separates our garden from them?
> 
> &#x2013; [Community of Gardens — CJ Eller](https://blog.cjeller.site/community-of-gardens)

