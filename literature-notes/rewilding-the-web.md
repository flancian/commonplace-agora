# Rewilding the web

URL
: https://troynikov.io/rewilding-the-web/

> The web is a much duller place now than it was 20 years ago. A large part of the reason is that the early web was much more fragmented, creating many ecological niches which became inhabited by the various strange and delightful species that tend to flourish in such environments. 

<!--quoteend-->

> The average person could create a web site for free, no questions asked. People took the opportunity and ran with it, building millions of pages –- 38 million at last count, according to Yahoo. They helped make the web a more vibrant territory, no longer the citadel of nerds in the know. 
> 
> &#x2013; [Ghost Pages: A Wired.com Farewell to GeoCities | WIRED](https://www.wired.com/2009/11/geocities/) 

<!--quoteend-->

> Webrings, blogrolls, portals, and other mechanisms let people actually find the things on the web that, to them as individuals, were worth finding. 

<!--quoteend-->

> as more people got online and as using the web became more mainstream and less unusual, and eventually **more about interaction than one-way publishing**, the fragmentation gave way to a great wave of homogenization. 

<!--quoteend-->

> These are twin forces pulling in the same direction; to bring into existence new modes of creation and distribution that operate at human scale, not at the scale of power laws. Just as we are witnessing the revolt of the public tear through legacy institutions, we are about to witness a grass-roots revolt against the platforms, for and by the people for whom homogenization isn't satisfying. 

Patreon, Substack, and Ghost are listed as the tools for rewilding the web.  

> For now the ecosystem being created by these tools **just looks a lot like blogging but with paywalls**, tailored to those who want to disintermediate the platforms by owning their own brand, and who want to bypass legacy media institutions by being paid directly by their fans. The communication is still largely one-way. But this misses the larger potential.
> 
> What these new tools offer is actually friction.

Friction as a means to create niches.  Hmm, interesting.  Not convinced that I like the idea of needing friction.

> Through these mechanisms the contributors / subscribers / fans, now connected directly to each creator, represent a directly addressable community bound by taste on the one hand and commitment on the other.

I don't like the sound of this power dynamic.  Though later on it does seem to focus more on communities, not some figurehead of the communities.

> The platforms still have a role to play. Rather than being hosts and propagators of creativity directly, their function will shift to one of surfacing and discovery

Aggregators, things like Agora, maybe.  And IndieWeb streams would be better than platform streams.

> The communities are our families, private clubs, monasteries and brotherhoods, while the platforms will be the town square we congregate in.  

<!--quoteend-->

> A new kind of symbiosis, between the vast scale of the platforms and the human interaction of the communities is now being built. The weird will inherit the web.  

I believe exactly this - something between the cottage industry of web 1.0 and the social industry of web 2.0. 

Interesting the use of 'weird' here, too!

