# New UK tech regulator to limit power of Google and Facebook

[New UK tech regulator to limit power of Google and Facebook](https://www.theguardian.com/technology/2020/nov/27/new-uk-tech-regulator-to-limit-power-of-google-and-facebook)

Summary: The framing of this is all wrong.  It's all about innovation, competition, consumer choice, growth.  It should be about [[liberation]], [[user freedom]], [[agency]] and [[sustainability]].  Sounds like it'll be a weak opt-in 'code' that just moves the deckchairs around while the ship is sinking.

> Alok Sharma says dominance of a few big companies hurts innovation and curtails customer choice

Bullshit framing of innovation and customer choice.  See The Maintainers for this fetishisation of innovation.

> the Competition and Markets Authority (CMA) will gain a dedicated Digital Markets Unit, empowered to write and enforce a new code of practice on technology companies which will set out the limits of acceptable behaviour.

A code of practice sounds a bit weak.

> “But the dominance of just a few big tech companies is leading to less innovation, higher advertising prices and less choice and control for consumers. Our new, pro-competition regime for digital markets will ensure consumers have choice, and mean smaller firms aren’t pushed out.”

Why is everything in the language of business opportunity?

> The code will seek to mediate between platforms and news publishers, for instance, to try to ensure they are able to monetise their content; it may also require platforms to give consumers a choice over whether to receive personalised advertising, or **force them to work harder to improve how they operate with rival platforms.**

Interoperability would be good.

> “Only through a new pro-competition regulatory regime can we tackle the market power of tech giants like Facebook and Google and ensure that businesses and consumers are protected."

Pro-competition, such a poor framing.  [[User freedom]] might be a better one.

> It’s time to address that and unleash a new age of tech growth.

"Tech growth" - that's the last thing we need.

