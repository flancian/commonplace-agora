# 404 Page Not Found

-   https://thebaffler.com/salvos/404-page-not-found-wagner

Similar: [[How Blogs Broke The Web]];  [[Rediscovering the Small Web]]; [[The Web We Have to Save]].

About the way in which social media platforms homogenise what we see and create.

"how the drive for commercialization has consumed and shaped it for capital."

> These companies and platforms operate in part by devouring, appropriating, monetizing, exterminating, or burying on the 112th page of search results anything on the web that is even remotely interesting—especially anything amateur, anything ad hoc-ist. There is more and more an ethic of false equivalency between virality and substance (and I say that as someone whose blog went viral). Hence, they think, because this stuff isn’t profitable, it must mean nobody wants to see it; and so nobody does.

References [[Postmodernism, or, The Cultural Logic of Late Capitalism]].

[[Vaporwave]].

Platforms killed the creativity of the early web, now they cannibalise it too.

> It’s about time for a little revenge from the old internet

^ what does that mean?  How will it happen?

I would say via digital gardens, IndieWeb, personal sites, right?  Move from the client-server to everyone being able to easily publish, a la [[Seeding the Wild]].

