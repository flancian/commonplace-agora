# Privacy, censorship and social media- The Case for a Common Platform

URL
: https://www.mutualinterest.coop/2021/03/privacy-censorship-and-social-media-the-case-for-a-common-platform

Authors
: [[Thomas Hanna]] / [[Dan Hind]] / [[Mathew Lawrence]]

Publisher
: [[Mutual Interest Media]]

> The events of January 6th, and its aftermath requires us to step back and ask how we want to organise both the social media platforms, and the broader digital economy to which they belong.

Specifically taking the way in which the storming of the capitol was incited as an example of a problem with social media platforms.

> establish a digital sector that is substantially public and democratic in character.

Public digital sector as the alternative.

> three core institutional approaches that could be enacted independently according to specific political and social contexts, or jointly as part of a linked eco-system approach: a [[Public Platform Accelerator]] (PPA), public “[[data trusts]]”, and a network of [[Public Digital Cooperative]]s.

^ The main pillars of this public digital sector.

> We can, and must, build both the case for radical change and pieces of a new digital economy in the here and now. In this the new generation of co-operators are going to be crucial. **Every worker in the digital economy who refuses the siren call of the venture capitalists has it in them to make another world.**

^ I like this rallying cry&#x2026;

Summary: Interesting ideas. Public platform accelerator. Data trusts. Public digital cooperatives.

I wonder what each of these mean in practice exactly?  This is probably covered in [[A Common Platform:Reimagining Data and Platforms]].

I also wonder if it can be made more municipal.  It sounds a bit state-oriented from this article.  But I remember Dan Hind's podcast about the British Digital Cooperative having quite a municipal / local side to it.

