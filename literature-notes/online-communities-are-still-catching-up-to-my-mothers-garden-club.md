# Online Communities Are Still Catching Up to My Mother's Garden Club

URL
: https://hackernoon.com/online-communities-aint-got-nothing-on-my-mothers-garden-club-because-of-implicit-feudalism-gc2z34y4

Author
: [[Nathan Schneider]]

Publisher
: Hacker Noon
    
    > The Internet has been plagued by a phenomenon I call “[[implicit feudalism]]”: a bias, both cultural and technical, for generating absolutist fiefdoms. Think about it: When was the last time you participated in an election for a Facebook Group, or sat on a jury for a GitHub project? Implicit feudalism is how platforms nudge users to practice (and tolerate) nearly all-powerful admins, moderators, and “benevolent dictators for life.”
    > 
    > &#x2013; [[Online Communities Are Still Catching Up to My Mother's Garden Club]]

Software project governance should be participatory.

