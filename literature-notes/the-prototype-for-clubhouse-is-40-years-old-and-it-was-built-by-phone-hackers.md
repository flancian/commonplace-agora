# The Prototype for Clubhouse Is 40 Years Old, and It Was Built by Phone Hackers

URL
: https://onezero.medium.com/the-prototype-for-clubhouse-is-40-years-old-and-it-was-built-by-phone-hackers-71389d117cad

Author
: Claire L. Evans

> Phreaks spotted the weakness in their dominant technological infrastructure; within those, they carved out communities, shared knowledge, and indulged in a larger spirit of exploration. We are far less able to conduct ourselves this way today because we’re penned into flat user interfaces and kept distracted while our behaviors are farmed for profit. It’s nearly impossible to develop our own sense of place because we’re not stakeholders, we’re products.

Interesting - it's that idea of finding niches again. [[Rewilding the web]] discussed this.

Flat user interfaces homogonize us.  [[404 Page Not Found]] discussed this.

> We can only hope that somewhere, far from the polished facades of social media, deep in the glitches, in the broken, in-between spaces of the web, a new generation of phreaks is on the line

