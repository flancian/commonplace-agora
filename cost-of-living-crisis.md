# Cost of living crisis

> Without another Covid wave, it is the cost of living crisis and particularly its impact on families already struggling to meet their rent, put food on the table and pay their heating bills that will dominate people’s lives in the next two years.
> 
> &#x2013; [The Observer view on Britain’s growing cost of living crisis | Observer edito&#x2026;](https://www.theguardian.com/commentisfree/2022/feb/06/observer-view-on-britain-cost-of-living-crisis) 

