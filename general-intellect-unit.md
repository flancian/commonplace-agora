# General Intellect Unit

URL
: http://generalintellectunit.net/

Thing
: [[Podcast]]

Description
: Podcast of the Cybernetic Marxists. Examining the intersection of Technology, (Left) Politics, and Philosophy.

Topics
: [[cybernetics]] / [[marxism]] / [[technology]] / [[politics]] / [[philosophy]]

