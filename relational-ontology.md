# Relational ontology

> In a relational ontology, the idea is that relations between entities are more fundamental than the entities themselves. 
> 
> &#x2013; [[The Nested-I and Ubuntu Rationality: The Relational Ontology of the Commons]]

<!--quoteend-->

> It means that living organisms develop and thrive through their interactions with each other. That is the basis for their identity and biological survival. That is the basis for their aliveness.
> 
> &#x2013; [[The Nested-I and Ubuntu Rationality: The Relational Ontology of the Commons]]

<!--quoteend-->

> While individual organisms may have important degrees of agency, they can only be understood in the context of their myriad relationships and constraints by larger structures.
> 
> &#x2013; [[Free, Fair and Alive]]

Sounds a bit like [[Viable system model]]?  And [[Institutional Analysis and Development]] (re: [[polycentrism]])?

> As a social system based on how people come together to collaborate and sustain themselves, a Commons is based on a relational ontology.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> to see that everything is interdependent, and that our individual wellbeing depends upon collective well-being. Our polity must be “attuned to the relational dimension of life,” as Arturo Escobar puts it.
> 
> &#x2013; [[Free, Fair and Alive]]

