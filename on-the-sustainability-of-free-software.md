# On the Sustainability of Free Software

URL
: https://fsfe.org/freesoftware/sustainability/sustainability.html

Authors
: Erik Albers / FSFE
    
    > the [[FSFE]] demands the publication of a device's underlying source code under a free licence 30 at the end of support for any software necessary to run or modify the initial functioning of the device.

