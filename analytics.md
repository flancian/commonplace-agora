# Analytics

Some non-GA options:

-   umami https://umami.is​
-   matomo https://matomo.org/
-   plausible https://plausible.io/
-   fathom https://usefathom.com
-   simpleanalytics https://simpleanalytics.com/
-   cabin https://withcabin.com
-   goatcounter https://www.goatcounter.com/ (h/t http://mtsolitary.com on Digital Gardeners Telegram)

