# Guerrilla technology



## The gist

-   asymmetry of resources
-   there’s a need to think about guerrilla technology
-   the creative and strategic use of small technology to counter the misuse of more ‘advanced’ technology by those with access to orders of magnitude more resources
-   better understanding of the terrain
-   small units of organisation with mobility

> World War III [will be] a guerrilla information war with no division between military and civilian participation
> 
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)

<!--quoteend-->

> Radio, telegraph, and even the printing press all helped precipitate major socio-political transformations in the past, the latter famously helping enable the Christian Reformation.
> 
> &#x2026;loosely knit sets of global and regional networks, enabled by the internet, had begun to challenge the authority of nation-states in the same way that nation-states had challenged the authority of empires a century earlier.
> 
> &#x2013; [An Information Warfare Power Shift Thanks to Social Media](https://theintercept.com/2017/11/25/information-warfare-social-media-book-review-gaza/)

See also [[some of the discussion here]].


## Examples


### Post-quantum cryptography

I don’t have a great grasp of what’s going on with quantum computing, and most accounts suggest it is a long way from doing anything useful, but it does seem to be rapidly advancing. IBM is now at 53 qubits, Google at 72.  Supposedly the stage of the awfully-named '[quantum supremacy](https://www.newscientist.com/article/2217835-google-has-reached-quantum-supremacy-heres-what-it-should-do-next/)' has been reached.

Given the stated potential of quantum computers, it seems worrying that they will be the playthings of wealthy states and corporations. Barclays recently used it for some financial application for example. The liberatory potential could be vast, but will it be used for those ends? Not feeling particularly hopeful about that.

[Post-quantum cryptography](https://en.wikipedia.org/wiki/Post-quantum_cryptography) is a thing to at least deal with the capability of quantum computers to break classical cryptography.


### Decentralised manufacturing

-   e.g. the use of 3D printing technology in Gaza under blockade
-   https://3dprint.com/95097/project-glia-gaza-strip/

