# Pedagogy of the Oppressed

> Critical search would actively strive to increase the visibility of counterhegemonic intellectual traditions and of historically marginalized perspectives. We must build systems of information diffusion and circulation that seek to amplify critical voices and to cut across linguistic, national, racial, gender, and class barriers. 
> 
> &#x2013; [[Informatics of the Oppressed]]

