# commons.hour

> commons.hour is a [[participatory design]] venue under principles of [[design justice]], building [[tools for conviviality]] in the spaces of the coop’s commons: platform spaces, media spaces and venue spaces. 
> 
> -   [commons.hour - The programme - handbook trial](https://meet-coop-1.gitbook.io/handbook-trial/5-commons.hour/commons.hour-programme)


## Sessions

-   [[commons.hour #2]]

