# Planning to get Omicron in order to gain some immunity or get it over with is a terrible idea

[[Coronavirus]]
[[Omicron]]

[‘I have no intention of getting infected’: understanding Omicron’s severity |&#x2026;](https://www.theguardian.com/world/2022/jan/16/no-intention-of-getting-infected-understanding-omicrons-severity)


## Because

-   "This is a real-deal virus where there’s unpredictability"
-   "Some people can get very sick. Some people can get long Covid."
-   Some people unwittingly will then get immunocompromised people sick”, leading to hospitalization and death.
-   It’s not at all clear if recovering from Omicron would protect against future variants


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

