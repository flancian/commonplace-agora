# Hole punching

> A number of techniques exist for ‘hole-punching’ [[network address tables]], in order to allow connections to be made between two computers, neither of which are directly connected to the internet. These generally rely on having some sort of ‘rendez-vous server’ used in order for the NAT to allow the connection. That is to say, the technique only works with ‘help’ from a server which is not behind a NAT, which means that although the server’s role is minimal, it does not give us complete independence. 
> 
> &#x2013; [[Seeding the Wild]]

