# In and against the state

> Book from 1979 discussing the experience of working class people, mostly socialists, in working within the public sector in the late 1970s, or relying upon it as service provider; and the contradictions that reveals. 
> 
> &#x2013; [In and against the state](https://libcom.org/library/against-state-1979) 

tags: [[Socialism]]

