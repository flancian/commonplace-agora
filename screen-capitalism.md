# Screen capitalism



## Overview

About the negative aspects of the social industry.  

[aside: is this the best place for this page?  'screen capitalism' is quite a niche phrase.]

> The problem of the 21st century is cacophony. Too many people are yelling at the same time. Attentions fracture. Passions erupt. Facts crumble. It’s increasingly hard to deliberate deeply about complex crucial issues with an informed public. We have access to more knowledge yet we can’t think and talk like adults about serious things.
> 
> &#x2013; [Mark Zuckerberg doesn’t understand free speech in the 21st century](https://www.theguardian.com/commentisfree/2019/oct/18/mark-zuckerberg-free-speech-21st-century)


## Social industry

I like the use of the phrase 'social industry'.  I think it's a good framing to use rather than social media.  It brings to the fore the co-optation by industry of what Ton calls [[social software]], turning it into an industry.

> Astonishingly, the perfect storm of everyman broadcast-quality production, ubiquitous social media and indefatigable recommendation engines — originally intended to connect, inform, educate and entertain us — has morphed into an intensifer and accelerator of our most aberrant and anti-social human impulses — lust, paranoia, victimhood, violence, hatred and homicide.
> 
> &#x2013; [Microattunement: a Pattern Language to Wake Up Our Humanity. Part 1: Why the the time is now](https://medium.com/between-us/a-unity-of-unmerged-voices-a-proposal-to-synchronise-our-efforts-through-microattunement-870922e7082f)


## Social Taylorism

In the Novara interview, Seymour talks about how when using social media (controlled by social industry) you are in some ways interacting more with a machine than with other people.  Likes, retweets, etc, are part of this machinery.  These have become industrial abstractions of actual social relations.

Analagous in some ways I feel to how Taylorism abstracted the movements of skilled labourers into smaller and smaller discrete motions, which could then be mechanised and repeated monotonously without skill or craft.

Digital time-and-motion men have abstracted social interactions into meaningless facsimiles of real interaction, real desire and affection.

Better a social craft than a social industry I think.  Small tech and [[social software]] can be part of a that I think, but re-repurposing or even breaking some of the frames that industry co-opted and mechanised.  

Writing a blog post, or a considered reply to someone else's, takes more time and emotional craft than a like.  But it's more rewarding overall.  It's hopefully less alienating.

[Side note: Digital Taylorism is a thing - basically the achievement of Taylorism through digital technologies (and the extremes that then takes it to.)]


### Taylorist approach as a means of making people more predictable

Douglass Rushkoff said something interesting, and related to this: If you're betting on the future, you want the future to be as predictable as possible.  So you use digital technology to reduce human sponteneity and creativity.  Make people more predictable.  This is the anti-human ethos now driving digital society.  
([Revolutionary Left Radio: Alienation, Marxist Humanism, and the Digital Media Landscape w/ Douglas Rushkoff](https://revolutionaryleftradio.libsyn.com/douglas-rushkoff))

Humans are being optimised for technology.  It should be the other way around.


## Microsolidarity

> The solution to our ills is to pay enough attention to each other that we can recognise ourselves in our differences — so that we can use each other’s talents to do what needs to be done. To surrender the fantasy of self-determination to something much more practical — readying ourselves to do together what we can’t possibly do apart.
> 
> &#x2013; [Microattunement: a Pattern Language to Wake Up Our Humanity. Part 1: Why the the time is now](https://medium.com/between-us/a-unity-of-unmerged-voices-a-proposal-to-synchronise-our-efforts-through-microattunement-870922e7082f)


## See also

-   Digital capitalism
-   [[Surveillance capitalism]]
-   Information capitalism
-   Mental capitalism (Georg Franck)


## Resources

-   [#NovaraFM: Screen Capitalism: the Twittering Machine](https://soundcloud.com/novaramedia/novarafm-screen-capitalism-the-twittering-machine-1)
-   [The Twittering Machine](https://www.theguardian.com/books/2019/aug/11/the-twittering-machine-richard-seymour-review-social-media-industry)

(face-remap-add-relative 'variable-pitch '(:family "Roboto Slab" :height 140)) 
(variable-pitch-mode 1)

