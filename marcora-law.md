# Marcora Law

> Emilia Romagna is helped by a much better legal framework for the establishment and development of co-operatives. The Marcora Law gives workers the right to buy and convert a company should it plan to close, and has been credited to have saved around 14,500 jobs in the country. 
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]

