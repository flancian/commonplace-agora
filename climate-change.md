# Climate change

> [[G20]] governments, comprising the world’s biggest economies and including developed and developing countries, are responsible for about 80% of global greenhouse gas emissions, and about 85% of GDP.


## The response


### Potential social formations

The book [Climate Leviathan](https://www.versobooks.com/books/2545-climate-leviathan) gives a nice exposition of four types of climate crisis response, if you class them on the axes of capitalist vs non-capitalist and pro- or anti- planetary sovereignty.  (I don't fully grasp the idea of planetary sovereignty just yet).

|                | Planetary sovereignty | Anti-planetary sovereignty |
|----------------|-----------------------|----------------------------|
| Capitalist     | Climate Leviathan     | Climate Behemoth           |
| Non-capitalist | Climate Mao           | Climate X                  |

Just going off what I remember from listening to [Novara's interview with Geoff Mann](https://novaramedia.com/2019/06/28/climate-leviathan/), my very very crude recollection at the moment:


#### Climate Leviathan

Ultimately ineffectual.  Not something we want.  Essentially the ineffectiveness of COP writ large.  


#### Climate Behemoth

Something like Trump, who not only doesn't give a shit about the climate, but also wouldn't want to work with other countries on it anyway.


#### Climate Mao

If a response could be centrally planned by a non-capitalist nation state.


#### Climate X

Pockets of climate activism at various scales, e.g. city level, etc.

