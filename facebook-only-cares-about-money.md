# Facebook only cares about money

[[Facebook]] will make some changes around its policy on hateful content, but only from the threat of lost ad revenue.  Not from actually caring about the victims of it.

> “Let’s be honest,” said Moghal, "these tech platforms have generated income and interest from this divisive content; they won’t change their practices until they begin to see a significant cut to their revenue."

Sucks that only big companies pulling out can have an effect on FB.  But props to Stop Hate for Profit for putting pressure on companies.

https://www.theguardian.com/technology/2020/jun/29/how-hate-speech-campaigners-found-facebooks-weak-spot

