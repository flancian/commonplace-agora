# Luddites

>   If [the Luddites] had their way we wouldn’t be living in a world with ‘no technology’, we’d be living in a world where communities have a say in the technological decisions that will impact them.
> &#x2013;  [Why the Luddites Matter | LibrarianShipwreck](https://librarianshipwreck.wordpress.com/2018/01/18/why-the-luddites-matter/)
>   (via Harold Jarche https://twitter.com/hjarche/status/1455264342174273550)

