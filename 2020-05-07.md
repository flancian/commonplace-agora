# 2020-05-07



## Finished: Ctrl+S

Finished [[Ctrl+S]].  It was a page turner, no doubt.  Very visual, almost more a script for a film than a novel.  It's a fast-paced adventure story with a bit of a detective/whodunnit edge.

Not particularly nuanced or thought-provoking.  Cliched, but good fun.  The written equivalent of watching a blockbuster, I guess.

Set in a near future, where people 'ascend' to a [[virtual reality]] world built on quantum computers called SPACE.   For many it's a way of escaping from a dreary actual reality.

There's a race of artificial life creatures called Slif who have evolved within SPACE.

It's a bit gruesome in places, with this idea of harvesting and then recreating emotions from others.


## Books and hyperlinks

> I like books because they don’t have hyperlinks, I like the web because it does.
> 
> &#x2013; https://prtksxna.com/2020/04/13/books-hyperlinks/

