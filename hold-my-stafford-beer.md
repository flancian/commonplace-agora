# Hold my Stafford Beer

```plantuml
left to right direction
actor "Stafford Beer" as beer
usecase "Hold My" as hold

beer --> hold
```

![[hold-my-stafford.png]]

