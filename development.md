# Development

> Development is a term of political economy used by the US and European nations to prod "undeveloped" countries to embrace global commerce, resource extrativism, and consumerism along with improvements in infrastructure, education, and healthcare.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The harmful side-effects of "development" typically include ecological destruction, inequality, political repression, and cultural dispossession.
> 
> &#x2013; [[Free, Fair and Alive]]

