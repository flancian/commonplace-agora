# The Nested-I and Ubuntu Rationality: The Relational Ontology of the Commons

A section in the book [[Free, Fair and Alive]].

> Commoning has a different orientation to the world because its actions are based on a deep relationality of everything. It is a world of dense interpersonal connections and interdependencies. Actions are not simply matters of direct cause-and-effect between the most proximate, visible actors; they stem from a pulsating web of culture and myriad relationships through which new things emerge.

[[Relational ontology]].
[[Ubuntu Rationality]]. [[Nested-I]].

