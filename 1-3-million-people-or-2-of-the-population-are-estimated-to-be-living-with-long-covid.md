# 1.3 million people, or 2% of the population, are estimated to be living with long Covid

[[Long Covid]].  [[Coronavirus]]. [[United Kingdom]].


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

Source (for me)
: [Long Covid: nearly 2m days lost in NHS staff absences in England | Long Covid&#x2026;](https://www.theguardian.com/society/2022/jan/24/long-covid-nearly-2m-days-lost-in-nhs-staff-absences-in-england)

