# The role of software for a universal Right to Repair

A
: [[talk]]

URL
: https://www.sfscon.it/talks/the-role-of-software-for-a-universal-right-to-repair/

"Can [[free software]] help us fix software obsolescence?"

> The European [[Right to Repair]] movement is making progress, asking European policymakers for more ambitious and timely regulation to make products more repairable and long-lasting. But [[software obsolescence]] is increasingly undermining the success of Right to Repair – not to mention the growing threat of [[part pairing]], manufacturers’ use of software to restrict who can repair what. In a fast changing landscape, the free software community can be a crucial ally in pushing for a real, universal Right to Repair. The talk will present current threats and opportunities for the two movements to collaborate more closely

