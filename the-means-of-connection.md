# The Means of Connection

As in, seize the means of connection.  Alternative strategies to online communication.  (I suppose then, it could be 'seize the means of communication'.  But that's quite broad).

'Connection' here being somewhat polyscemic - I primarily mean connection to each other, through [[social software]].  It could expand out to mean digital infrastructure, too, though.  Like you could include Scuttlebutt in that.

Anyway, the name doesn't matter for now. 


### What the problem is

The issues associated with the social industry, as through a lens of Marxist theory.  Mainly as analysis of how they induce alienation in the pursuit of profits.

-   [[social industry]]
-   [[screen capitalism]]
-   [[attention economy]]
-   surveillance capitalism.  how does this tie in?


### What is to be done

Alternatives to these modes of connecting online.  Obviously IndieWeb, Fediverse, etc, but an overall view of what is the purpose of socialising online as well, and then from there what are the best methods of doing it.

-   [[infostrats]]

Could make analogies to the various [[logics of resistance]], but that could also be pushing it a bit.

Like, which of the strategies do we take?  Smash, tame, walkaway, erode?

I think probably we're  currently doing the tame+erode combo as Orin Wright suggests.  I don't think we can smash them, unless you smash capitalism as a whole.  Eroding - by building pockets of alternatives.  Taming is the regulatory route of things like Bolshevski's interoperability regulation's or Panoptykon's API regulations.


## TODO pull out the thing that Seymour was saying about them all being abstractions of love in some sense

