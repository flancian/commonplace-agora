# Cutting the Covid isolation period to five days is foolhardy and dangerous

[[Coronavirus]]


## Because

-   There is little scientific evidence to justify these reduced measures
-   Many people are at their peak of infectiousness at five days
-   Ending isolation on day five will mean that some people come out of isolation during the peak of viral shedding


## Notes

> In cutting the isolation period again, the government seems to be more concerned with the impact of absenteeism in workplaces, particularly in essential services and schools, than with reducing the spread of Omicron.


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Concentrate and ask again]]

Source (for me)
: [Cutting the Covid isolation period to five days is foolhardy and dangerous | &#x2026;](https://www.theguardian.com/commentisfree/2022/jan/13/cutting-covid-isolation-period-five-days-foolhardy-dangerous)

Don't have much prior info on this.  The science seems pretty sound.  I don't know enough about the counterarguments - why is it being reduced?

