# Political economy

> Political economy is the study of production and trade and their relations with law, custom and government; and with the distribution of national income and wealth.
> 
> &#x2013; [Political economy - Wikipedia](https://en.wikipedia.org/wiki/Political_economy) 

<!--quoteend-->

> It is then essential to produce a critical understanding of political economy in order to comprehend emerging trends in network topology and their social implications.
> 
> &#x2013; [[The Telekommunist Manifesto]] 


## Resources

-   [The Fundamentals of Marxism: Historical Materialism, Dialectics, & Political Economy](https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy)


## Flashcards


### Political economy


#### Front

What is political economy?


#### Back

The study of production and trade and their relations with law, custom and government

and with the distribution of national income and wealth.

