# Ctrl+S

Started reading April 2020.

This was [recommended in The Guardian](https://www.theguardian.com/books/2019/dec/13/best-recent-science-fiction-fantasy-horror-review).

When I first started reading it, it came across as if it was going to be a bunch of Boy's Own teenage adventure.  Computer games and a manic pixie dream girl.

But, fair dos, it's kept me pretty gripped.  Very fast-paced plot and easy to read.  Quite a few nice ideas tucked away here and there too, setting the scene of the near future a little.

-&#x2014;

Finished [[Ctrl+S]].  It was a page turner, no doubt.  Very visual, almost more a script for a film than a novel.  It's a fast-paced adventure story with a bit of a detective/whodunnit edge.

Not particularly nuanced or thought-provoking.  Cliched, but good fun.  The written equivalent of watching a blockbuster, I guess.

Set in a near future, where people 'ascend' to a [[virtual reality]] world built on quantum computers called SPACE.   For many it's a way of escaping from a dreary actual reality.

There's a race of artificial life creatures called Slif who have evolved within SPACE.

It's a bit gruesome in places, with this idea of harvesting and then recreating emotions from others.

Finished: May 2020.

