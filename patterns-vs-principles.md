# Patterns vs principles

[[Patterns]]

> Principles tend to make universal claims.  This is problematic because it is virtually impossible to find the same institutional structures, cultural beliefs, and social norms in different places and contexts.  
> 
> &#x2013; [[Free, Fair and Alive]]

