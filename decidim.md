# Decidim

> Free Open-Source participatory democracy, [[citizen participation]] and open government for cities and organizations

<!--quoteend-->

> originally developed for the Barcelona City government online and offline participation website

-   Strategic planning
-   [[Participatory budgeting]]
-   Initiatives and citizen consultations
-   Participative processes
-   Assemblies
-   Networked communication

