# I don't like likes

The social industry has abstracted our human interactions into mindless, repetitive actions, from which they can more easily derive profit.  The disbenefits are many - alienation; surveillance; manipulation.

It is [[social Taylorism]].  The craft of social relationships has been reduced to unskilled labour.  

Do they have any benefits?  We should analyse this before obligingly building them into our alternatives to the social industry.  I'm minded to forego them altogether - a 'like' is better replaced by a reply.  A 'repost' is better your own thoughts supplemented with a link.  It takes more time, but it is more rewarding.  Remove the metrics and the easily quantifiable behaviours.

The silos are the factories.  Return to the cottage industries of your own site or community instance.

> These platforms like Facebook, with their “Like” buttons could easily save the world by connecting us all and showing us how similar we all are. Or, they could ruin democracy and everything good that exists, and turn us all into compulsive labourers of their technology, mindlessly feeding personal data to the algorithms for access, pulling their slot machines 
> again and again.
> 
> -   [my biggest writing mistake](https://mailchi.mp/pjrvs/my-biggest-writing-mistake?e=21598617ac)

<!--quoteend-->

> **nadia**: I like being able to publish my messier, half-formed thoughts, but I get turned off by putting those next to a like count. It feels like the more likes you get, the more you start writing things to get likes, whereas the REALLY weird, unpopular stuff probably won’t get many likes at all.
> 
> [&#x2026;]
> 
> **kicks**: This is making me seriously reconsider ‘likes’—which I’ve let pass as a kind of low-effort but benign and gracious comment. But now as I look at your ‘notes’ page—not only am I convinced by what you’ve said—I think the absence of all the ‘share’/‘like’ icons really makes that page feel like a running conversation. With ‘like’ counts, I think I’d be distracted wondering which thoughts were the most highly admired—but, come on, what kind of bullshit is that for me to be thinking while looking through your private thought journal?? So maybe it alters reading too in a sick way?[2]
> 
> **nadia**: The problem with likes is it naturally draws your eye towards the most-liked stuff, instead of deciding for yourself what’s most interesting. It almost feels like I’d be taking agency away from the reader by doing that.
> 
> (Maybe I’m being a little sanctimonious—e.g. shorter thoughts probably draw ppl’s attention more than bigger paragraphs, there’s no way to totally avoid this problem—but I’d rather not add to it, either.)
> 
> &#x2013; [Nadia Eghbal, Re: Writing Hypertext](https://www.kickscondor.com/nadia-eghbal/) 

An alternative view:

> You may say, “likes on Facebook aren’t real interactions,” and maybe for you, they haven’t been and may not be in the future. But I’m here to tell you that, with sincerity, 
> generosity, effort and openness, they can be.
> 
> &#x2013; [Be kind, connect and create miracles: Make your social media "likes" matter](https://groknation.com/culture/social-media-likes/)

But if you remove the abstracted actions, people find a way to do it anyway.  Replies that just say '+1', etc.

> I remember it back in the day of forum software. Like scrolling through hundreds of messages that were all one words like >heart< and >love< and it made discussions impossible to follow if something got popular.
> 
> &#x2013; [comment by wakest](https://social.wake.st/@liaizon/103127273354956343)

I feel meaningful interaction has been abstracted away, in order to cope with having too much stuff to interact with.  Maybe it's not bad per se, but want to question it at least.  Personal approach of late is to experiment with following less people, but making more replies.

> Relatedly, contemporary fediverse interfaces borrow from surveillance-capitalism based popular social networks by focusing on breadth of relationships rather than depth. [&#x2026;] What if instead of focusing on how many people we can connect to we instead focused on the depth of our relationships? 
> 
> &#x2013; [Spritely: towards secure social spaces as virtual worlds](https://dustycloud.org/blog/spritely/) 


## Give your social posts some intrinsic value, less bothered about interaction?

When you share something online, you tend to want to feel it has value of some kind.  If the only way you feel they are valueable is when that value comes from likes and boosts, you might feel bad if you don't get those interactions.

I've been finding that as I'm thinking more of  my sharing of posts as something that I'm going to put in to my commonplace book thing, I'm not so fussed if it gets likes or boosts.  It's got value to me because it's helping me think about things and is going to be integrated in to some slow burner of an idea at some point.  As a result it's got kind of an intrinsic value to me, whether or not it gets interaction.

Why post it at all then?  I guess I still throw it out there on the socials, as I always like to get some kind of dialogue, because that's an amazing way to get feedback and help me explore what I think about things.

Another value of social posts is a means to making friends, so maybe likes and boosts do contribute to that.  Another value is getting information out there that you think is important, and surely boosts help with that.

Yet to be convinced that likes and boosts are the _only_ way of fulfilling those aims though.


## Where does the value in social posting come from?

-   sharing information
-   dialogue
-   making friends
