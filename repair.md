# Repair



## community repair events


## culture change


## problems

-   products are increasingly harder to repair


### e-waste

-   is one of the fastest growing waste streams in the world
-   

> Fifty million tonnes of e-waste is produced on an annual basis and that could grow to 120m by 2050 if left unchecked.

-   question: how big is it currently, relative to others?
-   the amount of household appliances failing within 5 years of their purchase has increased over time


### carbon emissions

-   CO2 emissions associated with device manufacture
-   "around 80% of a small electronic device’s carbon footprint — over the whole of its lifecycle — is emitted before it even reaches UK shores."


#### dodgy carbon accounting

-   "The UK is, however, very special. Not only for its mind-blowing historical carbon debt, but also for its current, very creative, carbon accounting"
-   "We must take responsibility for our emissions, wherever they occur."


## policy

-   [[right to repair]]
-   [[ecodesign]]
-   international repair day


## misc

-   [Collapse OS](https://collapseos.org/why.html)
-   Using products for longer is one of the simplest actions we can take towards climate justice


## articles

-   [Why taking responsibility for our carbon emissions means promoting the Right to Repair](https://www.opendemocracy.net/en/oureconomy/why-taking-responsibility-our-carbon-emissions-means-promoting-right-repair/)
-   [Giving old tech a second life](https://www.ft.com/content/990c7846-e5cf-11e9-9743-db5a370481bc)
    -   Also reusues the skills of people previously manufacturing

> According to Pete Seeger, "If it can’t be reduced, reused, #REPAIRED, rebuilt, refurbished, refinished, resold, recycled or composted, then it should be restricted, redesigned or removed from production". We couldn't agree more. #RightToRepair #EWWR2019

-   [[19 broken laptops]]

> That's because Apple has designed the MacBook Pro such that fixing even one key requires replacing the entire keyboard apparatus, as well as part of the metal enclosure and some other components

<!--quoteend-->

> It's not great for tech consumers that buying an expensive service plan is the only way to have peace of mind when buying a $2,500 device.


## Ecodesign Directive

-   EU's plans to reduce the environmental impacts of products
-   make products last longer, and are easier to repair and recycle


## Manchester Declaration

-   UK community repair movement
-   came together in October 2018
-   asking UK legislators, decision-makers, product manufacturers and designers, to promote Right to Repair


## Ecodesign Core

-   product design that allows replacing key componenets with the use of commonly available tools

-   spare parts availability for 7 to 10 years

-   access to technical and repair guides  for repairers, such as the wiring diagrams or exploded views of products


## Felipe Fonseca

-   platform coops for connecting actors in reuse and repair


## [[Repairability standard]]


## Bookmarks

-   [New Report – Electronics Repair and Maintenance in the European Union](https://electronicplanet.xyz/2021/05/05/new-report-electronics-repair-and-maintenance-in-the-european-union/)

