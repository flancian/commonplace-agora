# Surveillance technology

> the purported benefits of the kinds of technology implemented for surveillance purposes are commonly overinflated
> 
> &#x2013; [Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance](https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711) 

<!--quoteend-->

> Technologies in this field, much like the humans that create them, invariably have [[biases]] and inaccuracies, and these biases and inaccuracies disproportionately impact historically marginalized groups.
> 
> &#x2013; [Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance](https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711) 

<!--quoteend-->

> It’s no coincidence that there are so many links between neo-nazis, massive digital surveillance, and the tech industry. 
> 
> &#x2013; [Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance](https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711) 

