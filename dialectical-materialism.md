# Dialectical materialism

> Dialectical materialism is a philosophy of science and nature developed in Europe and based on the writings of Karl Marx and Friedrich Engels.
> 
> --[Dialectical materialism - Wikipedia](https://en.wikipedia.org/wiki/Dialectical_materialism) 


## Resources

-   [The Fundamentals of Marxism: Historical Materialism, Dialectics, & Political Economy](https://revolutionaryleftradio.libsyn.com/red-menace-the-fundamentals-of-marxism-historical-materialism-dialectics-political-economy)

