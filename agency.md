# Agency

> the capacity of individuals to act independently and to make their own free choices
> 
> &#x2013; [Agency (sociology) - Wikipedia](https://en.wikipedia.org/wiki/Agency_(sociology)) 

<!--quoteend-->

> In my mind, one of the core problems is a lack of agency. If Twitter or Facebook push out a feature that is destructive to the way we use their services, or they refuse to create tools that are plainly necessary, we can do little but complain. As individual, non-paying users, we have virtually no leverage.
> 
> &#x2013; [Anti-capitalist human scale software (and why it matters) | by Jesse Kriss | &#x2026;](https://medium.com/@jkriss/anti-capitalist-human-scale-software-and-why-it-matters-5936a372b9d) 

<!--quoteend-->

> There’s also a lack of agency on the building side of the things. With open systems, we have the opportunity to create and run our own variants of systems that better suit our purposes. When APIs and app stores are tightly controlled, even our ability to usefully augment the system is curtailed.
> 
> &#x2013; [Anti-capitalist human scale software (and why it matters) | by Jesse Kriss | &#x2026;](https://medium.com/@jkriss/anti-capitalist-human-scale-software-and-why-it-matters-5936a372b9d) 


## [[Why is agency important?]]


## Anki


### Basic Card


#### Front

What is agency?


#### Back

the capacity of individuals to act independently and to make their own free choices

