# economic disobedience

URL
: http://cooperativa.cat/economic-disobedience/

> includes refusing payment of unjust taxes or interest. 
> 
> &#x2013; [[Vanguard Stacks: Self-Governing against Digital Colonialism]]

