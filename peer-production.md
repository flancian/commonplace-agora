# Peer production

Are there various types?  The one I have heard most about is [[Commons-based peer production]].

> In peer production, value is created not in exchange from a gift by another, but in exchange for **the emotional and spiritual value, and recognition, that derives from working on a project of common value**. 
> 
> &#x2013; [Gift Economy - P2P Foundation](https://wiki.p2pfoundation.net/Gift_Economy) 

"the emotional and spiritual value, and recognition, that derives from working on a project of common value" sounds like a pretty good driver of value.

