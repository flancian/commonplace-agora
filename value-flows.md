# Value flows

Flows of [[value]] beyond monetary.

> Conventional money, as designed and used, cannot express important types of value
> 
> &#x2013; [[Free, Fair and Alive]]

Other flows of value that we care about: e.g. [[ecological flows]], the social relationships of [[gift economies]], people’s contributions to [[commons]].

> Dollars, euros, and other state currencies don’t let us see the flows of value that matter most — ecological flows, the social relationships of gift economies, people’s contributions to commons.
> 
> &#x2013; [[Free, Fair and Alive]]


## Flows?

> "As a society, we have a pretty good understanding of objects and how to manipulate them, but we’re not as good with flows," says 
> [[MetaCurrency]] project founder [[Arthur Brock]].
> 
> &#x2013; [[Free, Fair and Alive]]


## What are some value flows?

-   e.g. within a community
    -   social contributions
    -   reputation
    -   work performed
    -   care work
    -   community sentiment
-   ecological flows (what does this actually mean?)
-   the social relationships of gift economies
-   people's contributions to commons


## DisCO flows of value

[[DisCO]]s have something to say on value flows: [Reimagining the origin and flows of value](https://mothership.disco.coop/DisCO_in_7_Principles_and_11_Values#6:_Reimagining_the_origin_and_flows_of_value).

-   Livelihood work: productive market value (the DisCO’s goods and services are paid for);
-   Love Work: productive pro-bono value (the commons created through self-selected volunteer work);
-   [[Care Work]]: reproductive work value (towards the collective and among its members, see above).


## Implementations

-   [[ValueFlows]] as a formal protocol for describing and tracking these other flows of value.
-   Holochain?

> In other words, the [[Holochain]] protocols would function as a grammar for the system, or language for building apps that name flows of value within a community, such as social contributions, reputation, work performed, care work, even community sentiment.
> 
> &#x2013; [[Free, Fair and Alive]]

