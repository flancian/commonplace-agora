# A commons is a politics of belonging

A claim from [[George Monbiot]] as quoted in [[Free, Fair and Alive]].

A [[commons]] is a [[politics of belonging]].

> Columnist George Monbiot has summed up the virtues of the commons nicely: “A commons &#x2026; gives community life a clear focus. It depends on democracy in its truest form. It destroys inequality. It provides an incentive to protect the living world. It creates, in sum, a politics of belonging.”
> 
> &#x2013; [[Free, Fair and Alive]]


## Epistemic status

Type
: [[claim]]

Gut feeling
: 7

Confidence
: 4

I don't fully know what a politics of belonging is.  But I can see how commons fosters belonging.

