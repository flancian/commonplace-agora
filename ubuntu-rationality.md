# Ubuntu Rationality

> We use Ubuntu Rationality to refer to a way of thinking that seeks to align individual and collective well-being. The Kenyan Christian religious philosopher and writer John Mbeti trans- lated the word Ubuntu in this way: “I am because we are and, since we are, therefore I am.” The individual is part of a “we” — and, in fact, of many “we’s.” The two are deeply intertwined. 
> 
> &#x2013; [[Free, Fair and Alive]]

