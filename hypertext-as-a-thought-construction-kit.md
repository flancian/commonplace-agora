# Hypertext as a Thought Construction Kit

An interesting looking paper shared by [[Phil]].  

> This paper focuses on the ways associative thinking was implemented in computers.

From the URL, it looks like it's from 1993.

[Hypertext as a Thought Construction Kit](http://www.mindorg.com/hypertext/Echt93.htm)

