# Rethinking infrastructure: the case for social investment

URL
: https://www.cheerfulpodcast.com/rtbc-episodes/rethinking-infrastructure

Publisher
: [[Reasons to be Cheerful]]

> President [[Joe Biden]] sparked a major debate when he described spending on [[social care]] and [[childcare]] as an investment in infrastructure. We’re exploring why the question of what counts as ‘[[infrastructure]]’ really matters. US policy expert Julie Kashen talks us through the US infrastructure debate then Professor Sue Himmelweit from the Women’s Budget Group explains why it could have far-reaching implications around the world.

[[Care work is infrastructure]].

