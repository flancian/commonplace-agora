# Coal

> Beijing has won international praise for announcing that it will stop funding coal projects in the developing world – but it is still heavily reliant on the fossil fuel for rapid economic growth at home
> 
> &#x2013; [Can China help end the world’s addiction to coal? | News | The Guardian](https://www.theguardian.com/news/audio/2021/sep/29/can-china-help-end-the-worlds-addiction-to-coal)

