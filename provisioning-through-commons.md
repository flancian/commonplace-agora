# Provisioning Through Commons

Part of the [[Triad of Commoning]].


## Patterns

-   Make & Use Together
-   Support Care & Decommodified Work
-   Share the Risks of Provisioning
-   Contribute & Share
-   Pool, Cap & Divide Up
-   Pool, Cap & Mutualize
-   Trade with Price Sovereignty
-   [[Use Convivial Tools]]
-   Rely on Distributed Structures
-   Creatively Adapt & Renew

