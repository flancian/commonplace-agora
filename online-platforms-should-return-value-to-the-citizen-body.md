# online platforms should return value to the citizen body

Rather than advertisers or private shareholders.

> The value created  by the users of the private platforms is captured by owners and advertisers. The platform architecture proposed here will return that value to the citizen body in the form of a better understanding of the social world, and greater power to address problems within it. 
> 
> &#x2013; [The British Digital Cooperative: A New Model Public Sector Institution](https://thenextsystem.org/bdc#a-socialist-agenda-for-digital-technology) 

