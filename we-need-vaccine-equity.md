# We need vaccine equity

We need [[vaccine equity]].

> But the inequitable distribution of the COVID-19 vaccine on the global sphere isn’t just an issue of morals or ethics. When it comes to an infectious disease that does not respect borders, COVID-19 and emerging variants such as Delta will continue to be a threat to the international community as long as it exists anywhere in the world.
> 
> &#x2013; [What Is Vaccine Equity and Why Is It So Important?](https://www.globalcitizen.org/en/content/what-is-vaccine-equity-covid-19/)


## Justification

-   It's unethical to privilege some countries over others with regards to public health
-   COVID-19 will continue to be a threat to the international community as long as it exists anywhere in the world
-   The Omicron wave has highlighted the risk posed to everyone by variants. New ones could prove more lethal.


## But

-   [[We do not have vaccine equity]]


## And

-   National leaders will not prioritise another population over their own; realistic plans must treat [[vaccine nationalism]] as a given
-   Those with production assets are the ones who can vaccinate their people


## So&#x2026;

-   We should have Covid [[patent waivers]]
-   We should have Covid [[technology transfer]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

I'm not an expert, but it just seems wrong ethically, and additionally seems to make it more likely for variants to appear.

