# Unix philosophy



## Positives of unix philosophy

-   One of the nice things about the Unix philosophy approach and gluing components parts together, rather than using one monoculture system, is that the work you put into learning each component is transferable. e.g. if I use R or metabase to get stats from one system I have (e.g. my time tracking), those skills I can reuse for stats for any other thing.

