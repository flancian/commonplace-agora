# Self-hosting conundrum

After having taken the time to [[set up]] [[YunoHost]], I've saved myself a little money and feel empowered.

However, I now have no direct imperative to financially support some orgs/projects I like (GreenNet, Wallabag, etc).

I could self-host **and** donate, but then it probably ends up costing more if you factor in time and money combined.

I wonder if the purity of 'self-host everything' goes a little too far.

I think perhaps my preferred middle ground would be ownership of data, and remunerating orgs I like to provide apps that operate on that data.

The [[Personal data store]] approach.  

See also [[Free software economics]] I guess.

