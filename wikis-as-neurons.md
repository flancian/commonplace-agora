# Wikis as neurons

When you 'copy' the idea from another wiki, and have it as a bi-link, that's like a connection between the two.  A bridge where someone might jump from one to the other.
I had this thought as I had copied antistatic gardens concept.
Made me think of synaptic gaps.
Wikis that fire together, wire together?
Interwiki bidirectional links as the synaptic connections?
Networks of wikis, like a neural network?
Bit tenuous, but fun to think on.

Parallels a lot of what I thought previously about Wikis being neuronal. 

I wonder if maybe just another avenue of approaching that is the Wikis being rhizomatic connections of Wikis we need connections of wiki sites, blogs, blikis, wiki logs whatever you want to call it, but to facilitate those connections between them is important, very important. 

