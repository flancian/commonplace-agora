# Public Money, Public Code

-   https://publiccode.eu/

> We want legislation requiring that publicly financed software developed for the public sector be made publicly available under a Free and Open Source Software licence. If it is public money, it should be public code as well.

