# Stream-first

I like this approach for combining garden and stream into a bliki/wikilog. The stream is full of links to the wiki and with backlinks you can see a nice narrative picture of when notes where contributed to. (More than just a changelog). And with the RSS (or microformats) feed others can subscribe and respond to ideas as they occur.

My time line posts contains a lot of [[backlinks]] to wiki content.

The [[stream]] is an entrypoint into the [[garden]]. I water my [[garden]] to help it grow.  I get the water from the [[stream]].

The [[Roam]] approach to [[note-taking]] is to start with your daily page, and then link to things from there.  This makes a lot of sense to me.  You just start with whatever is currently on your mind, and that goes in the stream, but links out to things in the wiki.  Probably one thing to think about though is what in the stream do I want to be [[public versus private]] - that would change the workflow a bit.

I do like this stream-first approach.  I quite often end up writing new things straight into the wiki though once I've ended up in there. They might not go into my stream, at least straight away.  That seems fine though - if I keep on thinking about them, they'll end up there at some point.

