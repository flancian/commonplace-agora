# Provisioning

> a nonmarket version of production
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Trained to see the dismemberment of complex production processes as efficient and natural, and its segregation from consumption as a core premise of “the economy,” economists tend to overlook a more elegant, practical approach to provisioning — [[commoning]].
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The goal of Provisioning through Commons is not maximum efficiency, profit, or higher Gross Domestic Product. It aims simply to meet needs and provide a stable, fair, satisfying, and ecologically minded way of life.
> 
> &#x2013; [[Free, Fair and Alive]]

