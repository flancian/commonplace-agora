# Setting up a spacemacs literate config file

I spent a bit of time switching my spacemacs config in to a [[Literate configuration]] style.


## Why?

I use [[spacemacs]] **a lot**.  For coding, for writing, for work, for organisation, for my personal knowledge management.  It's central to to most of my day-to-day activities on my computer.  I've been using for maybe 4 years now, and have built up quite a mess of a configuration file with various tweaks over the years.

Given how much I use it, it seems sensible to give a bit of TLC to this configuration.  I've tried to comment it as I've tweaked it, but it still has gotten messy.  I've learned and copied a lot from other people's configs, so endeavouring to make my own readable to others seems like a good thing to do.

I'm also just keen to try out org-babel for some literate coding, and this is an easy way in to doing that.


## What I did

Here's what I've done to get started on this.


### File reorganisation

First off, I put my existing .spacemacs file into a folder structure where you can split it up into multiple files.

```shell
mkdir ~/.spacemacs.d
cp ~/.spacemacs .spacemacs.d/init.el
```

In classic refactoring fashion - just do this simple thing, and make sure that things still work.


### Extract out user-config into own file

OK, after that, the next refactoring was to move move everything in the user-config function out in to it's own file, .spacemacs.d/user-config.el.

When its in its own file, you can source that from init.el like so:

```elisp
(defun dotspacemacs/user-config ()
  (let ((uc (concat dotspacemacs-directory "user-config.el")))
 (message (concat "ds-d" dotspacemacs-directory))
 (load-file uc)))
```

(above source came from here: [Spacemacs configuration in a structured org-mode file : emacs](https://www.reddit.com/r/emacs/comments/7ntc6p/spacemacs_configuration_in_a_structured_orgmode/))

Again, check that everything still works fine afterwards.


## Move config into an org file and set up tangling on it

The section on [Converting your existing config files](https://leanpub.com/lit-config/read#leanpub-auto-converting-your-existing-config-files) in Literature Configuration was really helpful for this.  It recommends a great path for moving things incrementally to the literate configuration.  Essentially you start off with one huge source block, and gradually split that up in to smaller blocks with `org-babel-demarcate-block`.

I did that, putting each new source block in to a logical org heading, and adding a bit of description and narrative around them.  Each time I split out a new bit, I ran `org-babel-tangle` to check things were still working.

I have the following config properties at the top of the file:

```org
#+property: header-args:elisp :tangle ~/.spacemacs.d/user-config.el :comments org
```

which says for all the elisp source blocks in this file, combine them together in to user-config.el when I tangle the file.  And also include my narrative as comments.

So my user-config is now **literally** all in [[My Spacemacs User Config]] here in my wiki.  As in, that is the file that I use to generate my `.spacemacs/user-config.el` that spacemacs runs.  (The source is [here](https://gitlab.com/ngm/commonplace/-/blob/master/my-spacemacs-user-config.org).)

I like it, and think it works pretty well for config files.  I'm going to try it on my org-roam config next.


## Resources

-   [Read Literate Configuration | Leanpub](https://leanpub.com/lit-config/read)
-   [Introduction to Literate Programming](http://www.howardism.org/Technical/Emacs/literate-programming-tutorial.html)
-   [Spacemacs configuration in a structured org-mode file : emacs](https://www.reddit.com/r/emacs/comments/7ntc6p/spacemacs_configuration_in_a_structured_orgmode/)

