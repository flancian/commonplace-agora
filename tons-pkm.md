# Ton's PKM

Ton describes in detail his [[PKM]] system, which he has recently refined and moved much of it to [[Obsidian]].

![[images/tons-pkm.png]]

I like Ton's early statement that this is foremost about a system, not about a particular tool.  Of course our tools do influence us, but as much as possible should support the system, not drive it.  Ton also wishes to avoid vendor lock-in.  This means both owning the data, but also then not becoming too locked in to a tool itself.


## Notes on tracking tasks

> allowing me to e.g. filter tasks on context or needed amount of focus through tasks

I do this with tags and org-agenda.

> As a side note: I do not use my mobile to look at or add tasks, or mark them completed

For my work todos, yes, the same.  Very different for my personal todos - I use orgzly roughly as much as I do Emacs for these.

> described in my previous post on my use of Obsidian, I have a hierarchical folder structure of areas of activity, with project folders within them. Each project folder contains a file titled ‘0 [project name] things to do’, where I keep the list of actions currently relevant for that project. If there are sub projects, then each of those has a similar own list of tasks, which are transcluded into the general project list.

Could do with org roam agenda but otherwise actions org, notes in roam.

> The root list contains all the areas of activity, and for each project within an area the project’s task lists is transcluded

This is similar to org-agenda.

> At the start of each project I run a script that creates all the necessities for a project. This includes automatically making the task list for a new project, adding a handful of common tasks to it, and adding the link to the root list

This could useful to have standard set of tasks per project.

> Both the root task list and the #urgent search filter I have pinned as starred searches, so I can directly go to them from the Obsidian interface

Starred searches similar to org-agenda filters.

> When a task is done, I copy and delete it from the task list, and paste it into the day log (see previous post). That way the day log contains all the things I’ve completed that day, plus anything else that came along and wasn’t on the task lists. (I use the day log for time sheets and the weekly review)

Better day logging would be handy.


## Writing notes

> Using Obsidian for my work, day logs, and task management came later, and that covers the hierarchical part of my PKM system. 

<!--quoteend-->

> Using Obsidian for my work, day logs, and task management came later, and that covers the hierarchical part of my PKM system. The note taking part is the networked part of it. The system works for me because it combines those two things and has them interact: My internal dialogue is all about connected ideas and factoids, whereas doing activities and completing projects is more hierarchical in structure. 

<!--quoteend-->

> That’s how a Note starts. I may copy some text into at some point, and summarise it over time, or add other context in which I encountered the same thing again. Notes are ‘factoid’ like, resources written down with the context added of how I found them and why I was interested. 

<!--quoteend-->

> Notions usually are about concepts pertaining to vision, values and practices. 


## Links

-   [100 Days in Obsidian, Pt. 1 – Interdependent Thoughts](https://www.zylstra.org/blog/2020/10/100-days-in-obsidian-pt-1/)
-   [100 Days in Obsidian Pt 2: Hierarchy and Logs – Interdependent Thoughts](https://www.zylstra.org/blog/2020/10/100-days-in-obsidian-pt-2-hierarchy-and-logs/)
-   [100 Days in Obsidian Pt3: Tracking Tasks – Interdependent Thoughts](https://www.zylstra.org/blog/2020/10/100-days-in-obsidian-pt3-tracking-tasks/)
-   [100 Days in Obsidian Pt 4: Writing Notes – Interdependent Thoughts](https://www.zylstra.org/blog/2020/11/100-days-in-obsidian-pt-4-writing-notes/)
-   [100 Days in Obsidian Pt 5: Flow and Workspaces – Interdependent Thoughts](https://www.zylstra.org/blog/2020/11/100-days-in-obsidian-pt-5-flow-and-workspaces/)
-   [100 Days in Obsidian Pt 6: Final Observations – Interdependent Thoughts](https://www.zylstra.org/blog/2020/11/100-days-in-obsidian-pt-6-final-observations/)

