# Setting up Calibre-Web and koreader (via YunoHost and NextCloud)

Calibre-Web is a web interface for your Calibre library.

I set it up, and then realised it's probably not that needed for, given that I'm just syncing to one device.  It's probably simpler just syncing directly from laptop to Kobo.  But, it was easy enough to do.

I have a [[Kobo]] and I use [[koreader]] on it.  koreader can download books from a Calibre-web install.

![[images/calibre-web.png]]


## Installing Calibre-web

-   really easy to install on YunoHost
-   during the install there's an option to create a shared folder where it will look for your ebook.  The default is a directory that is also available in NextCloud:  `/home/yunohost.multimedia/share/eBook`.  This comes in handy for syncing.


## Syncing Calibre library

-   assuming you have [[Calibre]] on your local box already with your library in it
-   sync Calibre to somewhere Calibre-web can see it
    -   two options I could think of here - syncthing and [[NextCloud]]
    -   I love syncthing and use it a lot, but haven't fiddled much with it on YunoHost yet.
    -   I thought I would use NextCloud instead then, as it already includes the 'Shared multimedia' folder that Calibre-web default install used
    -   You need the NextCloud app installed on your desktop in order to sync.  By default it'll sync to a folder in your home directory called 'NextCloud'
-   I had an existing Calibre library in a different place on my box, so I moved it to the eBook folder in my NextCloud folder (instructions on moving your library as per [How to Backup, Move and Export Your Library in Calibre - Calibre Blog](https://blog.calibre-ebook.com/how-to-backup-move-and-export-your-library-in-calibre/))


## Pulling the books down to koreader

Calibre-web exposes your books as an OPDS catalog.  koreader has built-in support for OPDS catalogs so you can browse them straight from koreader. (https://github.com/koreader/koreader/wiki/OPDS-support)

-   Go to `OPDS catalog`
-   Choose `Add new catalog`
-   put the address of your Calibre-web install and your YunoHost user creds (LDAP working on all these apps is super handy)
-   the list of things comes up, but then
-   It appears that only some catalogs work
    -   e.g. unread books works
    -   others show "catalog not found"


### <span class="timestamp-wrapper"><span class="timestamp">[2021-02-17 Wed]</span></span>

For some reason the opds catalog browsing has stopped worked from koreader - it just gives 'catalog not found' all the time.


## Misc Notes

Is it worth it?  I'm not convinced, if you're just using it on one device - maybe just easier to sync directly.

That said, syncing the library to NextCloud seems like a reasonable idea though (though bearing in mind a sync is not a backup, so why not just have a backup)

I'm using [[Z-Library]] to download books that I've paid for already via the Kobo store, but want to read DRM-free on koreader.  I own them dammit, I can read them wherever I want.

