# Gevulot

Thinking about privacy, and what we choose to reveal and not reveal on online, following the session in Utrecht.  It made me remember ‘gevulot’ from the book [[The Quantum Thief]].

> Gevulot is a form of privacy practised in the Oubliette. It involved complex cryptography and the exchange of public and private keys, to ensure that individuals only shared that information or sensory data that they wished to. Gevulot was disabled in agoras.
> 
> https://exomemory.fandom.com/wiki/Gevulot

Gevulot comes from Hebrew meaning “boundary”.

I think gevulot is the speculative fiction version of the [[Personal Data Store]].

