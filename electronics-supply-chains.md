# electronics supply chains

Omana George of [[Electronics Watch]] discusses some of the problems in [[electronics supply chains]], and the general anti-union and anti-organising practices of tech firms.

-   [Restart Podcast Ep. 60: Helping electronics workers improve conditions](https://therestartproject.org/podcast/electronics-watch/)

-&#x2014;

[[supply chains]].

