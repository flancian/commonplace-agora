# Bourgeoisie

[[Capitalist class]].

> The BOURGEOISIE—or capitalists—are the class of people who own the means to produce (land, factories, tools, and materials), and employ laborers to do the work of production
> 
> &#x2013; [[A People's Guide to Capitalism]]

