# Nexta-Live

> the [[Telegram]] channel which played a key role last year in organising protests against [[Belarus]]’s vengeful president Alexander Lukashenko.  At its peak Nexta had 2 million subscribers, making it the largest channel of its kind in eastern Europe

