# I like gift economies

[[I like]] [[gift economies]].

Because:

-   [[gift economies build community]]
-   &#x2026;

I don't understand them well enough to argue strongly if/why they are better than X.

But I like them as an idea.  They relate to a bunch of other ideas that I like (commons, peer production, mutual aid).  Seems worth more attention.

And I generally like the idea of non-market alternatives.  And the idea of non-reciprocal transactions (although I'm not 100% sure gift economies are considered non-reciprocal).

One thing that makes me a little hesistant though, is the idea that giving a gift produces an implicit debt.  If it's built on that intent, it is still built around a notion of debt.  Which doesn't sound great.


## Epistemic status

Type
: [[feeling]]

Strength
: 5

