# Spotify is going to ruin podcasting

[[Spotify]] is going to ruin [[podcasting]].

By enclosing them.

> And&#x2026; @spotify announces it's killing another open podcast, and turning it into proprietary Spotify audio that you can only listen to on Spotify's awful player. I was a huge supporter of @Gimletmedia and it's disappointing to see Spotify systematically lock up its podcasts.
> 
> &#x2013; https://mobile.twitter.com/mmasnick/status/1456032413952446464 

<!--quoteend-->

> No longer just a music streaming service, Spotify now regards itself as an audio platform and podcasting as its new centre of gravity. Part of the appeal of podcasts for Spotify is that they represent a different type of listening: rather than an album every two or three years from a favourite artist, there is new content every week, at least. Such frequency of output reinforces consumer loyalty – and so subscribers – far better than any single artist can, which may be why Spotify is so hesitant to let Rogan go, or even just to curb some of his more extreme opinions. 
> 
> &#x2013; [The Joe Rogan v Neil Young furore reveals Spotify’s new priority: naked capit&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/28/joe-rogan-neil-young-spotify-streaming-service)

