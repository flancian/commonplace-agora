# Building my own IndieWeb system

I was thinking of building my own system to move away from WordPress.

Artanis was one thought.  But I might prefer using Arcology actually.


## Artanis

Thinking of migrating away from WordPress.  How to do it?

I'd like to write it using Guile and artanis.

Follow Barry's transformative flow which is using sinatra (on which artanis is based).

Start: test it with one of the exported markdown files with it's microformats markdown.

Would it not be better to start with something a bit easier?
But good to have a project&#x2026;

I guess just have a crack, and see where we get.

-   read markdown file and generate post from that?  how does transformative do it?
-   i definitely want my content in git, either md files with frontmatter, or mf2
-   colt seems to pull things from git, so that's interesting&#x2026;
-   i'll avoid building an editor by simply using micropub, like barry
-   so yeah, to begin, transform markdown files, on the fly, from the filesystem
-   can iterate from there

See here for examples of file storage formats for indieweb: https://indieweb.org/file-storage
The storage section on p3k is helpful: https://indieweb.org/p3k

Rather than having to parse yaml frontmatter, would it not be of use to simple use scheme?
vis a vis some of the reasons guix uses scheme for package declarations, vs how nix did it.
might not be as relevant here.

could make use of sxml->html&#x2026;
https://dthompson.us/rendering-html-with-sxml-and-gnu-guile.html

decide between:

-   sxml for the whole doc
    -   this is probably closer to jf2
-   sxml for the frontmatter
    -   markdown for the content
    -   html+mf2 for the content

for long form articles, barry seems to store the content in the mf2 json as either html (https://github.com/barryf/content/blob/master/2017/05/acquiescence.json) or markdown (https://barryfrost.com/2017/10/how-we-make-remote-working-work)

that's cool, i can see how i could adopt this approach but using sxml and markdown in the content field.
i'll try that to begin.

i'm not convinced it'll work so well for long form articles though.  it'll be weird in the editor.


### Next

-   pare it back to a minimal install.
-   a single 'get', that does some test parsing of an example file with mf2->sxml and md parsing.

