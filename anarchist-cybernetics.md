# Anarchist Cybernetics

URL
: https://bristoluniversitypress.co.uk/anarchist-cybernetics

Strapline
: "Control and Communication in Radical Politics"

[[Anarchism]]. [[Cybernetics]].

Came across it via [[Nick Sellen]] post on [[CoTech]] forums - https://community.coops.tech/t/experience-implementing-a-disco-distributed-cooperative-organisation/2809/7

It looks part of this ongoing question of [[Horizontalism vs verticalism]]

> With a focus on communication and how alternative social media platforms present new challenges and opportunities for radical organising, it sheds new light on the concepts of self-organization, consensus decision making, individual autonomy and collective identity.

<!--quoteend-->

> aim of this book is to illustrate in detail how a perspective taken from cybernetics and brought to anarchist organising can help show how these lessons from experience can be constructively heeded to inform future political organising.

<!--quoteend-->

> By separating organisational function from organisation form, anarchist cybernetics highlights how anarchist and radical left organising can maintain its commitment to selforganisation in new political terrains and, crucially, points towards how institutions can be reshaped to create space for genuinely democratic participation.

<!--quoteend-->

> The three central concepts that underpin this framework are self-organisation, functional hierarchy and manytomany communication. 


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-07-18 Sun]</span></span>

Back to reading and still loving this book.  The writing is clear and lucid, and it's filling in background gaps in anarchist knowledge for me, at the same time expanding in new directions around the Viable system model.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-06-26 Sat]</span></span>

I'm early on but enjoying it so far - lucidly written.

It's a positive view of cybernetics - about how it can provide means of self-organisation without recourse to hierarchical state.  

I've not gotten there yet, but seems it'll primarily focus on Beer's [[Viable System Model]].

Tiqqun hasn't been mentioned yet.  I don't know much here but Id say any disjunct would simply come from different views of what the philosophical purpose of cybernetics is.  Growth, [[homeostasis]], or [[autopoesis]]?


### <span class="timestamp-wrapper"><span class="timestamp">[2021-06-27 Sun]</span></span>

I came across VSM originally via Cybernetic Revolutionaries. It piqued my interest there as seeming like a healthy mix of the horizontal and the vertical for organising. 

But in some sense in Chile, it was imposed from above. I like the idea in anarchist cybernetics that perhaps it will grow from below.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-07-03 Sat]</span></span>

Reading Chapter 2 I am stoked.  It (briefly) mentions [[slime mould]] and [[bird flocks]] as examples of [[self-organisation in nature]].  I studied this kind of thing in [[Evolutionary and adaptive systems]].  And it mentions the potential for decentralised [[Political organisation]] afforded by the [[many-to-many communication]] of e.g. [[social media]].  Like [[Free, Fair and Alive]] this book is tying together a lot of my interests in one place.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-08-07 Sat]</span></span>

Finished!  Excellent stuff.  It's right at the forefront of thinking on the [[Horizontalism vs verticalism]] question for me right now.  Maybe [[Neither Vertical Nor Horizontal: A Theory of Political Organisation]] should come next in the reading.  Interesting how cybernetics is used by various people for various ends.  I guess it's fairly loosely defined, so you have to define your terms if you're basing something on it.  Swann does that well.  Particularly of interest to me is the chapter on alternative social media as a means of facilitating the communication side of anarchist cybernetics.  Many-to-many communication.  I will probably return to that chapter the most.  The rest of the book is a very cogent argument for how Beer's VSM can bridge the horizontal and the vertical.  I'm pretty sold on that idea - whether it's via VSM or something else, but that general principle of the bridge. VSM does seem to address it very nicely.


## Chapters


### 2011: The Year Everything Nothing Changed


### Radical Left Organisation and Networks of Communication


### Anarchism and Cybernetics: A Missed Opportunity


### Communication (Part II): Building Alternative Social Media

> In this chapter, I want to attempt just such an analysis of mainstream social media and a subsequent envisioning of alternative social media, a vision of the kind of digital communication platforms that might help facilitate selforganisation. 


## Thoughts

> I like the VSM perspective of being an analytical tool rather than an organisation structure, seems to have a lot of power from that.
> 
> &#x2013; [[Nick Sellen]], [Experience implementing a DisCO (distributed cooperative organisation)? - #7 &#x2026;](https://community.coops.tech/t/experience-implementing-a-disco-distributed-cooperative-organisation/2809/7) 


## Quotes

See also backlinks - most quotes probably reside elsewhere in other page.

> In society there exist basic units (individuals, associations, communes, etc.) which have to possess autonomy, and which can cooperate and federate on a voluntary basis with the other units.

<!--quoteend-->

> The freedom that parts of an organisation have to make tactical choices, it will be suggested, is limited in important ways by both overarching strategies and grand strategies, that respectively set out the goals and worldviews of the organisation.

<!--quoteend-->

> while these two traditions are both centrally concerned with selforganisation, they approach it in quite different ways: cybernetics from an interest in effectiveness, even efficiency; anarchism from the standpoint of social and political freedom.

<!--quoteend-->

> I want to argue here, is that the relationship between strategy and tactics can be framed and articulated in such a way as to be wholly consistent with the ideals of selforganisation and participatory democracy that animate anarchism.

[[Anarchist Cybernetics - raw highlights]].

