# Wikity

I think Wikity was what first set me on the path to the [[IndieWeb]], somehow.  I can't remember exactly how, but somehow I followed a trail from it to the IndieWeb wiki.

It was kind of a [[personal wiki]] with an emphasis on the social element.  Which is what I'm interested in with [[collaborative memory palaces]].

> What does “wikified social bookmarks” mean? Well, like most social bookmarking tools, we allow for people to host private sites, but encourage people to share their bookmarks and short notes with the world. And while the mechanisms are federated, not centralized, we allow people to copy each other’s bookmarks and notes, just like Delicious or Pinboard.
> 
> &#x2013; [Wikity, One Year Later | Hapgood](https://hapgood.us/2016/11/20/wikity-one-year-later/) 

<!--quoteend-->

> -   We don’t bookmark pages. We bookmark and name ideas, data, and evidence. A single page may have multiple bookmarks for the different ideas, theories, and data referenced in the page.
> -   We provide a simple way of linking these “idea” bookmarks, so that finding one idea leads naturally to other ideas. Over time you create an associative map of your understanding of an issue.
> -   As we revisit pages over time, we expand and update them, building them out, adding notes, sources, counterarguments, summaries, and new connections.
> 
> &#x2013; [Wikity, One Year Later | Hapgood](https://hapgood.us/2016/11/20/wikity-one-year-later/) 

