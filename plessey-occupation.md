# Plessey occupation

Learned of this from [[Working Class History]].  Something about the fact it was a capacitor factory grabs my interest.

> The occupation of the Bathgate plant of Plessey Capacitors in 1982 provides an interesting example of collective action taken by a mainly female workforce against their multinational employer.
> 
> &#x2013; [Fighting plant closure: Women in the Plessey occupation, 1982 - Patricia Findlay](https://libcom.org/history/fighting-plant-closure-women-plessey-occupation-1982) 

<!--quoteend-->

> This particular dispute has important implications both for the involvement of women in industrial action, and for the debate about the most effective strategies to counter the power of multinational corporations, particularly in the case of plant closure.
> 
> &#x2013; [Fighting plant closure: Women in the Plessey occupation, 1982 - Patricia Findlay](https://libcom.org/history/fighting-plant-closure-women-plessey-occupation-1982) 

