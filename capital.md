# Capital

[[Karl Marx]]. [[Capitalism]].

> Capital’s three volumes were written to provide a theoretical arsenal to a workers’ movement for the revolutionary overthrow of the system—and to do so on the most scientific foundation possible
> 
> &#x2013; [[A People's Guide to Capitalism]]

