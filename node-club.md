# node club

A week-by-week communal [[drift]] through the [[Agora]].  It is perhaps one good way to [[Ritualize Togetherness]] in [[Anagora]] / [[Flancia Collective]].


## History

[[vera]] posted in Matrix:

> What do you think about node of the week, where we pick a node per week to all write about and/or stoa on
> 
> like an agora book club
> 
> &#x2013; https://matrix.to/#/!WhilafaLxfJNoigHCj:matrix.org/$XIRrdpblqdJ6lxz-p3mJHRcdja2fpEr5FjfS-XmInIs 


## Rules

There aren't really any rules.  At the beginning we put a few nodes in to a pool and then voted on which one we'd all look at together.  Sometime later we switched to each simply listing an interesting node they're looking at, and people do the same one if they want, or not.  Dip in, dip out week-by-week.  No pressure.

**The first rule of node club is: you do talk about node club, if you want to.**


## Why I like it

It could stimulate the community aspect of Agora, to be conversational / goal-oriented. It feels a bit like [[Blogchains and hyperconversations]]. It also feels a bit like an async wikipedia edit-a-thon, but highlighting the federated rather than centralised wiki approach a la [[A Platform Designed for Collaboration: Federated Wiki]].  A communal bit of [[knowledge commoning]] to [[Ritualize Togetherness]].


## Future

It'd be interesting I think to plot the paths we've taken through nodes.  And to graph the constellations we've worked on.   


## Interesting nodes

For me personally a fun way to seed the node suggestions for a new week is a harvest of some of the forward links from the previous week's node. As it's all about making connections after all. We grow the Agora with [[creeping rootstalk]]s&#x2026;

See https://doc.anagora.org/node-club for past interesting nodes of the week.

