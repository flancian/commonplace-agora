# web sign-in

-   https://indieweb.org/Web_sign-in
-   https://indieweb.org/IndieAuth

> The process of using your domain to log in to sites and services is called web sign-in and is implemented via a protocol called IndieAuth, an extension of OAuth used for decentralized authentication.
> 
> &#x2013; [Your Website Is Your Passport - Desmond Rivet](https://desmondrivet.com/2020/02/12/website-identity) 

<!--quoteend-->

> It's entire reason for being is to answer the simple question: does the person behind the browser have control over the URL that they are claiming as their own?
> 
> &#x2013; [Your Website Is Your Passport - Desmond Rivet](https://desmondrivet.com/2020/02/12/website-identity) 

<!--quoteend-->

> &#x2026;being a completely decentralized protocol, it's much more in keeping with the spirit of the IndieWeb. 
> 
> &#x2013; [Your Website Is Your Passport - Desmond Rivet](https://desmondrivet.com/2020/02/12/website-identity) 

<!--quoteend-->

> If your goal is to make a social network out of the world wide web, there is a certain elegance to the idea of leveraging DNS as a user registration system.
> 
> &#x2013; [Your Website Is Your Passport - Desmond Rivet](https://desmondrivet.com/2020/02/12/website-identity) 

