# Zettelkasten and GTD

> The [[Zettelkasten]]. "It's like the [[GTD]] for intellectual progress" What it is, and why you want one. (thread)
> 
> -   https://twitter.com/Conaw/status/1129788853189955584

<!--quoteend-->

> First &#x2013; Why GTD doesn't work for intellectual work
> 
> GTD is all about ****stress free productivity****, it works by creating a system you can trust with commitments and deadlines, so you can let things fall out of working memory without getting dropped
> 
> &#x2013; https://twitter.com/Conaw/status/1129788853189955584

<!--quoteend-->

> You can't put a deadline on insight though. Making real progress on an intellectual problem requires drawing connections between things you hadn't connected before.
> 
> Can't predict when you'll get the idea that ties everything together or realize you were asking the wrong question
> 
> &#x2013; https://twitter.com/Conaw/status/1129788853189955584

