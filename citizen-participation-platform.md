# citizen participation platform

Platforms for citizens to participate in the running of their municipality.


## Features

Have features to facilitate things like:

-   Strategic planning
-   [[Participatory budgeting]]
-   Initiatives / proposals / citizen consultations
-   Participative processes
-   Assemblies
-   Networked communication
-   Debates
-   Voting
-   [[Collaborative legislation]]


## Examples

-   [[Decidim]]
-   [[Your Priorities]]
-   [[Consul]]
-   [[Agora]]?


## Misc

Perhaps citizen engagement platforms are a means to facilitate [[Libertarian municipalism]].  


## Links

-   [[Building an open infrastructure for civic participation]]

