# Manufactured iSlaves

From [[Goodbye iSlave]]: manufactured iSlaves are those addicted and constantly attached to their devices.

> many companies that have made their fortunes off the immaterial labor of legions of “manufactured iSlaves” dutifully clicking “like,” uploading photos, and hitting “tweet” all without any expectation that they will be paid for their labor. Indeed, in Qiu’s analysis, what keeps many “manufactured iSlaves” unaware of their shackles is that they don’t see what they are doing on their devices as labor.
> 
> &#x2013; [[Shackles of Digital Freedom]] 

