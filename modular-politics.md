# Modular Politics

A paper on it: [{2005.13701} Modular Politics: Toward a Governance Layer for Online Communities](https://arxiv.org/abs/2005.13701)

> Drawing on the paradigm of [[Institutional Analysis and Development]], this paper proposes a strategy for addressing this lapse by specifying basic features of a generalizable paradigm for online governance called Modular Politics.

