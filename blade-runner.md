# Blade Runner

A flawed classic.  I've watched this a few times.  Most recently I watched the final cut.  Undeniably a classic but with some big problems.

It's a stylish film and was probably pretty stunning in 1982.  The dystopian sci-fi future is fantastically done, the set design and lighting is perfect, and the dark crowded streets really set the scene.  I'm not sure if Blade Runner was one of the first films to start this idea of dystopian futures being predominantly Asian, but it feels a bit off-key now.  Maybe it was less of a cliche at the time.

There's some good performances, Rutger Hauer as Roy Batty is weird and wonderful.  It feels a bit eighties at times, mostly in costume, but hey fashion comes and goes, right.

The huge problem for me is the scene between Deckard and Rachael that feels very strongly like rape.  I felt uncomfortable watching that scene.  I've read a few opinions on it online afterwards, and there's plenty of debate about it.  The main disagreements seem to be around either - is it rape if it's a replicant, or actually she wants it, but she just doesn't know her own emotions, and Deckard knows them better.  The first is horrible, the second is very dangerous.  Maybe there's some more subtlety to the second view given that she is a replicant.  But that could have been explored without sexual violence.  And I don't think the film was even trying to present it this way as a means to get you thinking - it just feels like some men in film 40 years ago might have thought it was a perfectly normal interaction in the bedroom.

While it's a piece of film history, I don't think I'd call it a top ten favourite, even without this scene.  But with it in, I honestly couldn't recommend it to others, or at least only with a caveat.  Maybe I need to think about why that is, given that there's lots of gruesome violence and murder, and that doesn't bother me the same.

