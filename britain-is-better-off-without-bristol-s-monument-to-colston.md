# Britain is better off without Bristol’s monument to Colston

Articulated here: [The Guardian view on the ‘Colston Four’: taking racism down | Editorial | The&#x2026;](https://www.theguardian.com/commentisfree/2022/jan/07/the-guardian-view-on-the-colston-four-taking-racism-down) 

Fuck yes.

Not in a public celebratory view, anyway.  It's still in a museum to reckon with the past.

[[Colston]].  [[Slavery]].


## Because

-   Colston was a slave trader, one of Britain's wealthiest
-   [[Acknowledging historic injustices is part of building a more equal society today]]
-   [[The built environment contains cultural messages]]


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Without a doubt]]

