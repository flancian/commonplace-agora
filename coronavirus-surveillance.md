# Coronavirus surveillance

> COVID-mitigating applications like health surveys or [[contact tracing]] only work if a critical mass of the population uses them. 
> 
> &#x2013; [Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance](https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711) 

<!--quoteend-->

> Integrating mobile apps that exploit location data to track whether someone may have come in contact with a COVID carrier does not add more hospital beds, doctors or nurses in poverty stricken areas. 
> 
> &#x2013; [Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance](https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711) 

<!--quoteend-->

> Integrating mobile apps that exploit location data to track whether someone may have come in contact with a COVID carrier does not add more hospital beds, doctors or nurses in poverty stricken areas. 
> 
> &#x2013; [Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance](https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711) 

<!--quoteend-->

> The answer lies in not just an aggressive response to the pandemic, but an aggressive response to structural inequality.
> 
> &#x2013; [Big Tech Won’t Save Us: The Case for Social Transformation over Coronavirus Surveillance](https://medium.com/@macprac/big-tech-wont-save-us-the-case-for-social-transformation-over-coronavirus-surveillance-d167163b711) 

