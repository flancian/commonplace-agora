# Errors migrating a WordPress site from MySQL 5.7 to 8 via Duplicator

I was migrating WordPress from an Ubuntu 18.04 server to a Ubuntu 20.04 server.  I was getting a bunch of errors when installing the Duplicator package on the new server.  

For example: 

> ****ERROR**** database error write 'Variable 'character<sub>set</sub><sub>client</sub>' can't be set to the value of 'NULL'' - [sql=/\*!40101 SET character<sub>set</sub><sub>client</sub> = @saved<sub>cs</sub><sub>client</sub> \*/;&#x2026;]

I thought they were probably related to a MySQL mismatch - the old server was MySQL 5.7, the new server MySQL 8. First of all I upgraded the old server to MySQL 8.  But for some reason that still didn't fix it.  The installation still seemed to work, and the WordPress site looked OK, but better to resolve the errors now rather than them come back and bite later on.

So what ultimately seemed to resolve it was I changed in the Duplicator settings the method with which it exports the DB from mysqldump to PHP mode.  This removed the errors.

