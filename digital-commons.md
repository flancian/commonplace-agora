# Digital commons

Stuff like information, data, culture and knowledge which are maintained as a shared resource by an online community.

For example, projects in the area of:

-   [[free software]]
-   [[free culture]]
-   [[open data]]
-   [[open access]]

Wikipedia is a well known one.


## What is a digital commons?

> Digital commons are a subset of the commons, where the resources are data, information, culture and knowledge which are created and/or maintained online. 
> 
> &#x2013; [[dulongderosnay2020: Digital commons]]

<!--quoteend-->

> The digital [[commons]] are a form of commons involving the distribution and communal ownership of informational resources and technology.
> 
> &#x2013; [Digital commons (economics) - Wikipedia](https://en.wikipedia.org/wiki/Digital_commons_%28economics%29) 

Distribution and communal ownership of informational resources and technology.  That could cover a lot of stuff&#x2026;

> Resources are typically designed to be used by the community by which they are created.
> 
> &#x2013; [Digital commons (economics) - Wikipedia](https://en.wikipedia.org/wiki/Digital_commons_%28economics%29) 

^ that's true of commons in general, but seems like it wouldn't need to be quite the case for digital commons&#x2026; in that they are easier to be used beyond the community that creates them.   

> The distinction between digital commons and other digital resources is that the community of people building them can intervene in the governing of their interaction processes and of their shared resources.
> 
> &#x2013; [Digital commons (economics) - Wikipedia](https://en.wikipedia.org/wiki/Digital_commons_%28economics%29) 

<!--quoteend-->

> Digital commons is a (the?) commons centred on digital media and digital devices - might include many kinds of automation, guidance of material systems, provisioning and transportation of material goods etc. bringing a major focus on the digital capability and literacy of the commoners. It's a material commons of code, devices, media, enabled and stewarded by cultural capability (aka skill)
> 
> &#x2013; [[Mike Hales]] https://social.coop/@mike_hales/107430505867111451


### Examples of digital commons

The [[free software]] movement in general is called a digital commons (by Wikipedia at least).

> Examples of the digital commons include wikis, open-source software, and open-source licensing.
> 
> &#x2013; [Digital commons (economics) - Wikipedia](https://en.wikipedia.org/wiki/Digital_commons_%28economics%29) 

[[Free culture]], [[public domain]], [[open data]] and [[open access]] are mentioned in [[dulongderosnay2020: Digital commons]].

This research publication from [[IPPR]] seems to focus on data as a digital commons: 

> It requires a broader understanding of data as a public resource, but one of an exceptional kind: that there is space for market-based approaches, and for some direct state regulation, but that data will increasingly often require the creation of new forms of ownership and control, out of the hands of either market or state institutions.
> 
> &#x2013; [[Creating a digital commons]]


## Commoning a digital commons


### Social life of a digital commons


### Stewarding a digital commons

How would you protect it from enclosure?
i.e. how would you [[Actively Thwart Enclosure & Cooptation]] in a digital commons?

> Typically, information created in the digital commons is designed to stay in the digital commons by using various forms of licensing, including the [[GNU General Public License]] and various [[Creative Commons]] licenses. 


### Provisioning a digital commons


## Compared to knowledge commons

I wonder how digital commons compares to [[knowledge commons]]?  Perhaps just that some knowledge commons are digital commmons, and some digital commons are knowledge commons?


## Issues

> It is also worth noting that a commons can get the mix of collective control and individualism wrong. A group may exert a suffocating presence on the individual, or on certain types of individuals. Patriarchy is a problem in many subsistence and digital commons despite women’s significant role in commoning.
> 
> &#x2013; [[Free, Fair and Alive]]

[[Tragedy of the digital commons]]? (there's a paper on it, not read it though)

