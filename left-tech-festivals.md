# Left-tech festivals

-   Abandon Normal Devices - https://www.andfestival.org.uk/events/and-festival-2021/
-   EMF Camp - https://www.emfcamp.org/
-   Our Networks - https://ournetworks.ca/
-   MozFest - https://www.mozillafestival.org/en/
-   Reclaim Futures - https://www.reclaimfutures.org/
-   Theorizing the Web - https://www.theorizingtheweb.org/

