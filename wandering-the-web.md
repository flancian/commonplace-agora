# Wandering the web

> No, the web was much more of an adventure. It was a place that you wandered to discover new areas, like exploring the vast open seas. A new virtual space that lead to all kinds of strange, interesting, exciting places. This is what the web was like, at least, in our collective imagination.
> 
> &#x2013; [Rediscovering the Small Web - Neustadt.fr](https://neustadt.fr/essays/the-small-web/) 

See: [[cyberflâneur]]. 

