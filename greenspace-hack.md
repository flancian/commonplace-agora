# Greenspace Hack

URL
: https://greenspacehack.com

Crowdsourced documentation of greenspaces, as a source of grassroots input into urban planning decisions&#x2026;

> To provide a publicly available open map documenting green spaces

<!--quoteend-->

> Despite the promise and potential of crowdsourced data for use in policy-making, a lack of knowledge and experience exists when combining crowdsourcing approaches with new technology platforms, such as Internet of Things (IoT), which can translate to barriers in adoption rates and relevance to stakeholders involved in the planning process.
> 
> This project is looking to overcome such barriers to ensure the potential health benefits of greenspace are maximised, through innovative, community-focussed evidence gathering. 

<!--quoteend-->

> On the other hand, crowdsourced data is growing in popularity and use, capturing information through citizen engagement to provide more localised, individual and frequent information. This opens up new possibilities for planners, enabling the detail and voices of city dwellers to be the fabric from which planning strategies are built.

