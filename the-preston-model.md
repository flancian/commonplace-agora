# The Preston Model

[[Community wealth building]].

A form of [[municipal socialism]].


## What is it?

Community wealth building utilises:

-   anchor institutions
-   living wage expansion
-   community banking
-   public pension investment
-   worker ownership
-   municipal enterprise tied to a procurement strategy at the municipal level.

(via [Community wealth building - Wikipedia](https://en.wikipedia.org/wiki/Community_wealth_building))

> a holistic framework for integrating community, cooperative, and public assets into a mutually supporting system of local economic prosperity
> 
> &#x2013; [Infographic: The Preston Model](https://thenextsystem.org/learn/stories/infographic-preston-model) 

An attempted transition to [[localism]] and [[cooperativism]] inspired by Cleveland in the US and [[Mondragon]].

[[Anchor institutions]] are big part of it.  For example, council, university, hospital, investing in the local area.


## Outcomes/Benefits

> It is estimated that the Preston model has added £200 million to the city’s economy while significantly reducing unemployment. 
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]


## Resources

-   [Infographic: The Preston Model](https://thenextsystem.org/learn/stories/infographic-preston-model)
-   [‘Poverty was entrenched in Preston. So we became more self-sufficient’ | Loca&#x2026;](https://www.theguardian.com/society/2017/feb/14/poverty-was-entrenched-in-preston-so-we-became-more-self-sufficient)
-   [In 2011 Preston hit rock bottom. Then it took back control | Preston | The Gu&#x2026;](https://www.theguardian.com/commentisfree/2018/jan/31/preston-hit-rock-bottom-took-back-control)
-   [The Alternatives: how Preston took back control – podcast | Politics | The Gu&#x2026;](https://www.theguardian.com/politics/commentisfree/audio/2018/jan/31/the-alternatives-how-preston-took-back-control-podcast)

