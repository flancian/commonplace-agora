# Winter of discontent

> In the late 1970s the Labour party was in office, but only in a limited sense was it in power. Trade unions were at the peak of their political and social influence. At two general elections in 1974 – the first producing a minority government, the second a government with a majority of three – voters had narrowly decided that Labour was the best party to work with the unions, hopefully to the benefit of the country. The two Labour premiers that followed, Harold Wilson and then Jim Callaghan, were not dominant figures but fixers – political lion tamers to those Labour voters who, rightly or wrongly, feared the unions.
> 
> For four years, the lion tamers’ tricks worked reasonably well. A “social contract” was negotiated between the unions and the government, which met union demands such as increases to welfare benefits in return for workers accepting modest, often below-inflation pay increases. The arrangement did not produce a particularly dynamic economy but a fairer one: by the late 1970s, Britain was as equal as it has ever been.
> 
> Yet in the autumn of 1978, workers who had had enough of pay restraint started to rebel. Strikes spread, and the government failed to persuade trade unionists to call them off. As everyday essentials from food delivery to healthcare were disrupted, just like now, the government’s lack of power was suddenly obvious.
> 
> Contrary to the story usually told about the 1970s – which has been set by the right – the Callaghan government’s support did not collapse. At the 1979 election, the Labour vote actually went up by 75,000. Many Britons refused to accept that Labour’s power-sharing approach to government – which much of Europe still follows – was now obsolete. For anyone hoping that this autumn’s crises will quickly dissolve support for Boris Johnson’s government, the loyalty of voters to Callaghan is a cautionary tale.
> 
> What decisively changed the balance of power in Britain in the late 1970s was a surge of support for the Conservatives. With clear rhetoric and ruthless timing, as the winter of discontent raged, Thatcher offered voters not compromise with the unions but domination of them: a Britain where employers would be much more powerful than workers once again. That is the world we still live in – for now. The value suddenly placed on lorry drivers and other essential workers this autumn could just be the beginning of that world’s unravelling.
> 
> -   [A winter of discontent is unlikely to dissolve the Tories’ support | Andy Bec&#x2026;](https://www.theguardian.com/commentisfree/2021/oct/01/winter-discontent-tories-support-crisis)

