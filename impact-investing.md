# Impact investing

> BRED has loaned $3 million so far, all of it from Seed Commons, a network of funds looking to invest in [[co-ops]]. Khatib helped start it after her experience trying to get the bank loan. The funders include foundations, some government sources, and individual philanthropists. These so-called impact investors are “looking for places that can hold their savings and use it to build a more just economy,” says Brendan Martin, co-director of Seed Commons.
> 
> &#x2013; [Worked-Owned Businesses Are Having a Moment](https://www.bloomberg.com/news/features/2021-03-31/employee-co-ops-need-financing-these-impact-investors-want-to-help)

