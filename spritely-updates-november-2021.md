# Spritely Updates! (November 2021)

A
: [[podcast]]

URL
: https://fossandcrafts.org/episodes/38-spritely-updates-november-2021.html

Series
: [[FOSS and Crafts]]

[[Spritely]] all still sounds really interesting, and like it's getting a lot closer to prime time.

Would like to learn more about [[Spritely Brux]], the identity / [[petnames]] stuff.

Cool some of it has been ported to [[Guile]], too.

