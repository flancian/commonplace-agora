# Speeding up org-export and org-publish

I publish my personal wiki via org-publish.  ([[Publishing org-roam via GitLab CI]]).

It is shockingly slow. Around when I first introduced the pipeline, it took about 6 minutes - pretty slow.  Currently, as of Feb 2022 with over 3000 org files, it's consistently taking around **30 minutes**!!!  This is horrendous.  


## Speed over time

I can full out the builds time from my gitlab pipeline runs.
I can pull out the number of files over time from git (https://blog.benoitblanchon.fr/git-file-count-vs-time/)

![[Speed_over_time/2022-02-05_12-55-25_screenshot.png]]

It judders around a bit, likely down to the fact that gitlab runners can just take different amount of time, but it's a **reasonably** linear trend.  So it kind of looks like the more files, the more build time.  So the amount of time publishing a file in the problem.


## Some things to try


### Suppressing output messages

Every single file that is published outputs a message

> Publishing X with org-html-publish-to-html

It's quite nice to see progress through the files, but not really neccesary and output is usually a performance problem.

```emacs-lisp
(advice-add 'org-publish-all :around 'silence)

(defun silence (orig-func &rest args)
  (let ((inhibit-message t))
    (apply orig-func args)))
```

This reduced the time from about 30 to 24 minutes.

It would be useful to see error messages though - I'll see if those still get output when I encounter one.


### Turning off mode hooks

```emacs-lisp
(defun org-publish-ignore-mode-hooks (orig-func &rest args)
  (let ((lexical-binding nil))
    (cl-letf (((symbol-function #'run-mode-hooks) #'ignore))
      (apply orig-func args))))
```

```emacs-lisp
(advice-add 'org-publish :around #'org-publish-ignore-mode-hooks)
```


### Incremental builds

One problem is potentially that the rebuild is from scratch each time.  If I could make it incremental, that would obviously be a lot faster.  But goes a bit against the principle of CI builds.


## Resources

-   [How to speed up org export/publish? : emacs](https://www.reddit.com/r/emacs/comments/mmdeei/how_to_speed_up_org_exportpublish/)

