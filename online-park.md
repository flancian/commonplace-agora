# online park

Another greenspace metaphor for better online spaces - the [[online park]].  See also: [[digital gardens]].

-   [To Mend a Broken Internet, Create Online Parks | WIRED](https://www.wired.com/story/to-mend-a-broken-internet-create-online-parks/)

