# Software/services that I use

[[Free software]] where possible.  Self-hostable (although not necessarily self-hosted) where possible.


## Operating systems

-   currently [[Linux]] [[Mint]]
-   I'd quite like to give [[Guix]] a go


### history

-   AMSDOS (on the [[Amstrad CPC 464]])
-   Windows, I think 3.1, on a laptop my Dad had from work
-   Windows 98 on our first family PC (did I ever use 95?)
-   first Linux was either Mandrake or Mandriva - around 2001?
-   used fedora for a little bit?
-   OSX at work for a bit, Windows at work for a bit
-   ubuntu
-   debian
-   mint mate


## Text editing

-   [[spacemacs]]


### history

-   used to predominantly use vim
-   Once upon a time, Visual Studio with VsVim


## Coding

-   [[Emacs]] / [[spacemacs]]


## Productivity


### tasks/todos/gtd

-   [[org-mode]]
-   [[orgzly]]


### calendar

-   hosted nextcloud with GreenNet
-   Linux client: gnome calendar app
-   Android client: Etar


## Personal knowledge management

-   [[org-roam]]
-   [[Wallabag]]
-   [[koreader]]


## Communications


### email

-   protonmail
-   [[mu4e]]


### chat

-   [[IRC]] via [[znc]] and [[ERC]]
    -   mainly just for IndieWeb IRC at the moment


## Utilities

-   [[AnySoftKeyboard]]

-   syncthing
-   tmux
-   keepassxc


## Hosting


### servers

-   [[GreenHost]]


### domains

-   iwantmyname
-   gandi

