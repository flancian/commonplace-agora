# Publishing vs tinkering

> But I find the relationship between publishing and tinkering to be a mutual one. The more you publish, the more you tinker your means of publishing. The more you tinker, the more you publish about your means of tinkering (among other things). Both feed into each other quite well if you let them. This blog is a testament to that. I hope I can keep reminding myself to do both — to not just fuss with the means of correspondence but to also play chess.
> 
> &#x2013; [Correspondence — CJ Eller](https://blog.cjeller.site/correspondence?pk_campaign=rss-feed)

