# Selectively publishing to the stream

As I try the [[stream-first]] approach, a comment from [Bruno](http://winck.org/) at the [Garden and Streams](https://indieweb.org/2020/Pop-ups/GardenAndStream) session sticks in my head - along the lines that he had experimented with software where pretty much everything was written in to his wiki first, with simply a flag to say 'also publish this to my public stream'. 

I find that interesting as I just posted something to my stream in my wiki (a tech note to myself about Chromium disk usage), that I don't feel a particular benefit to posting to a public stream - I can't imagine anyone really wanting it popping up in their social readers.  

BUT I do want it in my own chronological timeline (as well as my longer-term garden), as I find it useful to be able to look back when something first happened.  I want to [[record the journey as well as the destination]], so to speak.

You see quite a few IndieWeb people do something along these lines, with a full 'firehose' stream you can follow, but also a more restricted subset of 'stuff I think other people will be most interested in'.

