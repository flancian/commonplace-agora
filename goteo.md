# Goteo

> Goteo is a [[crowdfunding]] site which focuses on projects which, apart from giving individual rewards, also generate a collective return through promoting the [[commons]], open source code and/or free knowledge.
> 
> &#x2013; [Goteo - Wikipedia](https://en.wikipedia.org/wiki/Goteo) 

<!--quoteend-->

> Goteo differs from conventional crowdfunding websites by requiring that projects actually advance the principles of commons.
> 
> &#x2013; [[Free, Fair and Alive]]

