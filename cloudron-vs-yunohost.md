# Cloudron vs Yunohost

These are my rough notes from when I was looking for a self-hosting platform.  I tried both [[Cloudron]] and [[YunoHost]], and eventually stuck with YunoHost.

-&#x2014;

YunoHost has more apps and a stronger ethos. It would be a no-brainer for me, but for a couple of things:

-   I don't think installed apps run in any kind of isolation on Yunohost, whereas Cloudron is all containers
-   Cloudron apps seem well maintained, whereas Yunohost looks more hit and miss, dependent on volunteer contributions

So it kind of swings on if I have time to properly maintain it, which was partially what I was hoping to avoid having to do.

But now just reading more about Yunohost's aims - https://yunohost.org/#/faq and https://yunohost.org/#/project_organization - I feel like I can't **not** support them.  If if needs a bit more time going in, so be it&#x2026; gotta live your principles if you can.  So that's kind of made my mind up.  I'll definitely start with Yunohost for personal stuff and switch only if I hit some major bumps.

If this was for work purposes, I would probably have to choose Cloudron right now, for the sake of robustness.

An excellent thread on the topic here: https://forum.cloudron.io/post/10860

A good post from [socialism.tools](https://socialism.tools) that mirrors my thoughts pretty closely: [Cloudron vs. YunoHost - the self-host app showdown](https://socialism.tools/cloudron-yunohost-self-host-app-showdown/) 


## Approach

Cloudron basically sets up docker and then pulls in containers.

YunoHost installs Debian and configures the software directly on top of that, mingled together.


## Ethics

https://yunohost.org/#/faq and https://yunohost.org/#/project_organization 

The fact YunoHost has the Anarchist Library as an app tells you something about its ethics :)  (though I think it's partly enabled by it building on top of [[Debian]], which also bundles that library IIRC).


## Security model

> Yunohost is best if you are price sensitive but it doesn't use containers and really depends if you want to take that risk.

<!--quoteend-->

> > keeping those apps updates is a lot of work. I imagine it's a lot of thankless work.
> 
> > It's a side effect of not using containers.

Interesting, yunohost seems to be idealogically opposed to things such as docker and ansible: https://forum.yunohost.org/t/suggestion-lemmy-une-alternative-federee-a-reddit/10508/2


## Resources

-   https://news.ycombinator.com/item?id=17999854
-   https://news.ycombinator.com/item?id=22191416

