# Taylorism

> Taylorism is a methodology developed in the early 20th century that breaks every action, job, or task into small and simple segments which can be easily analyzed and taught. It was designed to optimize the efficiency of assembly-line factories. It prioritizes breaking work into well defined pieces of work which can be estimated precisely, and workers who fulfill a defined roll which can be trained for, and therefore easily replaceable. This approach was hugely successful in manufacturing at reducing labor costs, but it should be noted that it had a downside in the fact that it alienates workers by (indirectly but substantially) treating them as easily replaceable factors of production
> 
> &#x2013; [Why Does Agile Fail?](https://wakingrufus.neocities.org/fail-agile.html) 

