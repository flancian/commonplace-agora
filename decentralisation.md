# Decentralisation

\#+ROAM<sub>ALIAS</sub> "Decentralization"

Decentralisation can apply to lots of things.

> As Vitalik Buterin once layed out, it helps to see decentralisation as having multiple dimensions, in his view these three: technical, political, and logical. A technically decentralised network with a single developer influencing its progress is politically centralised. And if all nodes have to share the same data (the modus operandi of blockchains) it is logically centralised.
> 
> &#x2013; [Redecentralize Digest — April 2021 — Redecentralize.org](https://redecentralize.org/redigest/2021/04/) 

I see the purpose as reducing imbalances of authority and power, as much as is possible.

I guess I'm most interested currently in how it applies politically and technologically.


## [[Technological decentralisation]]


## Political decentralisation

[[Anarchism]]. 


## Decentralised organisataions

-   [Rich Bartlett's list of decentralised org handbooks](https://hackmd.io/s/Skh_dXNbE#)


## Interesting diagram of control vs structure

https://twitter.com/seldo/status/1486563446099300359/photo/1

