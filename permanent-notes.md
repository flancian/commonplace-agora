# permanent notes

> Once a day (ideally), go through the notes you created in the first two steps, and turn them into permanent notes. These are more detailed, and carefully written to capture your exact thought or idea. It’s an atomic process: one index card should correspond to one idea and one idea only.
> 
> &#x2013; [How to take smart notes](https://nesslabs.com/how-to-take-smart-notes)

