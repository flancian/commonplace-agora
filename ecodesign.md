# ecodesign

-   Ecodesign Work Plan. Intended to be adopted later this year, the document will guide Commission’s work on many of the most wasteful products until the end of its mandate

-   need to include concrete steps aimed at tackling all of the most wasteful and unrepairable products present on the market today, starting with smartphones, printers, laptops, and other ICT devices

