# Epistemic disclosure

Quite like this idea.

-   https://maggieappleton.com/epistemic-disclosure

According to Maggie there, "The rationalist and Effective Altruist communities have been particularly enthusiastic about the pattern."  As something of an irrationalist, I'd like to counterpoint that I'm enthusiastic about it too.

I've been doing it in my garden for a bit.

-   I've started playing with epistemic disclosure on notes in my digital garden, as well as thinking of different types of notes, e.g. definitions, feelings, claims.  See e.g. [[gift economy]] (definition), [[I like gift economies]] (feeling) and [[gift economies build community]] (claim).
    -   I'm liking it so far - it's basically following ideas around [[titling evergreen notes]], and the distinction between notes and [[notion]]s, that I already knew about for a while, but actively putting it in to practice a bit more.
    -   It's working in the sense of making me reflect more on what I think.  As soon as I present something as a feeling or a claim, I feel more compelled to elaborate and try and justify it.  Part of moving from [[Collector to Creator]].
    -   That said, I don't want it to add friction to just writing stuff, so I'll just do it when I feel like it for now.  No pressure.

-   Continuing the addition of epistemic disclosure to notes in my digital garden, I've been adding an 'agreement vector' to any claims that I make.
    -   I started using the possible answers from Magic 8-Ball for this.  e.g. "[[Without a doubt]]", "[[Ask again later]]", "My sources say no".
    -   Because: a) it's funny; b) they contain both direction and magnitude of agreement, which is handy; c) I prefer to have backlinks to a phrase rather than a number.

