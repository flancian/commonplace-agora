# Community land trusts

> Community land trusts are a great way to decommodify land, take land off speculative markets permanently, and mutualise control and benefits of real estate. CLTs help keep land under local control and allow it to be used for socially necessary purposes (e.g. organic local food) rather than for marketable purposes favoured by outside investors and markets.
> 
> &#x2013; David Bollier, Stir to Action Issue 30

