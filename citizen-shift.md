# Citizen Shift

-   https://www.citizenshift.info/
    
    > this approach prompts us to think of ourselves instead as Citizens, and to ask what it is we can all participate in and contribute to. The core proposition is that this shift in thinking provides a far more generative platform for ideas, innovation, social and environmental justice, and indeed economic prosperity.

