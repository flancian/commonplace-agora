# Made myself a logo with PlantUML

```plantuml
skinparam backgroundcolor transparent
skinparam nodebackgroundcolor technology

node double
node loop
double -> loop
loop -> double
```

![[images/doubleloop.png]]

