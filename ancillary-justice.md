# Ancillary Justice

Ann Leckie.  I think I read this in 2019.  I remember enjoying it, although not being _completely_ blown away.  The use of pronouns was really interesting though, and the idea of ships' AIs inhabiting human bodies.

