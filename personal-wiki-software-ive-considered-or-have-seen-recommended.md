# Personal wiki software I've considered or have seen recommended

-   [Gollum](https://github.com/gollum/gollum): "A simple, Git-powered wiki with a sweet API and local frontend."
-   [ikiwiki](https://ikiwiki.info/): "Ikiwiki is a wiki compiler. It converts wiki pages into HTML pages suitable for publishing on a website. Ikiwiki stores pages and history in a revision control system such as Subversion or Git."
-   [Zettlr](https://www.zettlr.com/): "A powerful Markdown editor for researchers and journalists."
-   Markdown and a git forge: gitlab or github do pretty printing of markdown pages in your repo (and org pages too, as it happens)
-   [TiddlyWiki](https://tiddlywiki.com/): "A non-linear personal web notebook".  Looks like it fulfils the usecase pretty well, and appears to have a plugin for git integration.
-   [[org-brain]]: I tried org-brain a few times, and it never really clicked for me.
-   [[Roam]]: what org-roam is based on.
-   [[FedWiki]]

