# Free, Fair and Alive

URL
: https:://freefairandalive.org

Subtitle
: The insurgent power of the [[commons]]

Authors
: [[David Bollier]] / [[Silke Helfrich]]

I read it in 2021.  I loved it.

Partly as it draws threads between topics of great interest to me - commons, complexity science, commons-public partnerships, digital commons, patterns.  It even ticked my wiki box.

It's also a very practical manifesto for political / social organising. I really like this about it.  Its feel like a guidebook to (one form of) [[Anti-capitalism]].

As they say:

> this book is not just to illuminate new patterns of thought and feeling, but to offer a guide to action. 

It also full of real-world examples of commoning already in action.

Since reading it I'm dipping back in to it and looking at the specific patterns and ideas in more depth.  The pattern-style they use makes it really usable.  See [[Triad of Commoning]].

It has three main parts:

-   part 1, discussing the commons and commoning and what they are and their insurgent potential.
-   part 2, outlining a whole bunch of patterns for commoning that you might use.  See [[Triad of Commoning]].
-   part 3, about how to grow the Commonsverse and challenge the hegemonic power.

Some sections I particularly liked:

-   [[A Platform Designed for Collaboration: Federated Wiki]]
-   [[Complexity Science and Commoning]]
-   [[The Nested-I and Ubuntu Rationality: The Relational Ontology of the Commons]]


## Discussion


### How does it relate to Ostrom / Governing the Commons?

> Not a reinterpretation  - but a shifted orientation, yes. Subtitle of FFA is "the insurgent power of the commons". Ostrom is certainly not insurgent! Somewhat propertarian in her basic framing, akin to 'Austrian' economics (Hayek etc). Whereas FFA is counter-enclosure, counter-extractivism, post-propertarian. Hence common-ING as its pivot, the radical, situated practice, rather than commonS, the co-optable, abstract, political-economic form.
> 
> &#x2013; @mike<sub>hales</sub> -   https://social.coop/@mike_hales/106280463656699063


## How it was written

I found it great to discover that this book was researched and composed using [[FedWiki]].

https://occitanie.social/@yala/107322947574130084


## What's the title about?

> This is reflected in our title, which describes the foundation, structure, and vision of the commons: Free, Fair and Alive. Any emancipation from the existing system must honor **freedom** in the widest human sense, not just libertarian economic freedom of the isolated individual. It must put **fairness**, mutually agreed upon, at the center of any system of provisioning and governance. And it must recognize our existence as living beings on an Earth that is itself **alive**.

Bit of similarity to [[Doughnut Economics]] there - **free** and **fair** is a bit like the social foundation, **alive** is a bit like the avoidance of planetary overshoot.


## Quotes

You will find **many** quotes from FFA dotted around my wiki.  Check out the backlinks.  


### Unfiled

> What we really need today is creative experimentation and the courage to initiate new patterns of action. We need to learn how to identify patterns of cultural life that can bring about change, notwithstanding the immense power of capital.

<!--quoteend-->

> The new structure can help us envision different sorts of community, social practices, and economic institutions — and above all, a new culture that honors cooperation and sharing.

<!--quoteend-->

> We came to realize that if we aspire to social and political transformation but try to do so using the language of market economics, state power, and political liberalism, we will fail.

