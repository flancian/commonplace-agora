# Culture and structure

> we cannot depend upon structures to do the work of culture. 
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Transparency is not just about legal arrangements and procedures, but about social practices that build trust.
> 
> &#x2013; [[Free, Fair and Alive]]

Coops are structure.  Commoning is culture. [[Coops can turn in to bad actors if there is no culture of commoning]].

