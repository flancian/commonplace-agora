# Caroline Shenaz Hossein on 'Black Banker Ladies' and the Social Economy

A
: [[podcast]]

URL
: https://david-bollier.simplecast.com/episodes/caroline-shenaz-hossein-on-black-banker-ladies-and-the-social-economy

Featuring
: [[Caroline Shenaz Hossein]] / [[David Bollier]]

Series
: [[Frontiers of Commoning]]

> Among millions of Black women in Africa, the Caribbean, and North America, [[ROSCA]]s, or 'rotating savings and credit associations', are trusted alternatives to racialized, exclusionary systems of formal banking. The self-organized, informal pooling of money among friends and neighbors offer a way to help people amass the money to buy a used car, pay for school, and meet other household expenses. Professor Hossein of the University of Toronto at Scarborough, in Ontario, Canada, discusses the resourcefulness and resilience of the Black social economy despite attacks by many state authorities and mainstream banks.

[[Social and Solidarity economy]]

