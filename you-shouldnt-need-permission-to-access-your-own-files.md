# You shouldn't need permission to access your own files

> In old-fashioned apps, the data lives in files on your local disk, so you have full agency and ownership of that data: you can do anything you like, including long-term archiving, making backups, manipulating the files using other programs, or deleting the files if you no longer want them. You don’t need anybody’s permission to access your files, since they are yours. You don’t have to depend on servers operated by another company.
> 
> &#x2013; [Local-first software: You own your data, in spite of the cloud](https://www.inkandswitch.com/local-first.html) 

