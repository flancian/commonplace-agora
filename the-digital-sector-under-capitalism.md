# The digital sector under capitalism

-   [[Big tech is a huge part of capitalism]]
-   [[Digital architectures shadow all aspects of human life]]
-   [[For big tech, user benefits are secondary to business model]]

> Advertisers prefer to work their magic on isolated, and preferably anxious, individuals who can be persuaded that competition and consumption, not collaboration and conviviality, are the answer to their troubles.
> 
> &#x2013; [The British Digital Cooperative: A New Model Public Sector Institution](https://thenextsystem.org/bdc#a-socialist-agenda-for-digital-technology) 

