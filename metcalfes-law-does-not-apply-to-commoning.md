# Metcalfe's law does not apply to commoning

[[Flancian]] asked: 

> [[commoning]] probably scales with [[Metcalfe's law]], as it's a social activity?

I replied:

-   Re: Metcalfe's law - hmm interesting. I feel like commoning tends to prefer [[federation]]. So keeping groups small in a sense, but then connecting these smaller groups. Commoning maybe works best scaling out rather than scaling up.
-   E.g. [[Rely on Heterarchy]] and [[Rely on Distributed Structures]] patterns
-   I almost feel like the larger an individual commons gets, the more it tends towards tragedy. Or at least the governance system has to get much more robust.
-   Might be wrong there though.


## Epistemic status

Type
: [[notion]]

Confidence
: 2

Gut feeling
: 5

Not too sure about this.  Just a hunch.  But feels worth exploring.  It generally fits with my values of small things federating.

