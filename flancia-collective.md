# Flancia Collective

My take, which is just one of many: I think/hope Flancia Collective is and can be:

[[Commoner]]s. Purveyors of [[Liberatory technology]] and [[Convivial tools]].

[[revolutionary socialist]], [[Anti-capitalist]], [[anarchist]]. 

Rhizomatic. ([[rhizome]])

Positive and social. Feminist.  Caring.

A bit weird. With hat tips to Dadaists ([[Dadaism]]), Surrealists ([[Surrealism]]), Situationists ([[Situationism]]) et al.

Enemies of [[Moloch]].  Fans of [[protopia]].

A distributed, cooperative organisation.  And possible more formally a [[Distributed Cooperative Organisation]].

