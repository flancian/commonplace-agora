# Stono Rebellion

> a slave revolt that began on 9 September 1739, in the colony of South Carolina. 
> 
> &#x2013; [Stono Rebellion - Wikipedia](https://en.wikipedia.org/wiki/Stono_Rebellion)

First heard of this via [[Celebrate People's History: The Poster Book  of Resistance and Revolution]].

