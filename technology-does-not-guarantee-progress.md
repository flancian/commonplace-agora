# Technology does not guarantee progress

> Technology does not guarantee progress. It is, instead, often abused to cause regress.
> 
> &#x2013; [[Goodbye iSlave]] 

