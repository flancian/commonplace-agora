# palimpsest

> an object or piece of writing with new material superimposed over earlier writings
> 
> &#x2013; [Psychogeography: a way to delve into the soul of a city](https://theconversation.com/psychogeography-a-way-to-delve-into-the-soul-of-a-city-78032) 

