# Public land ownership

> Prior to Margaret Thatcher’s election in 1979, public land ownership was the norm rather than the exception in UK cities. From the mid-nineteenth century onwards, municipal governments acquired huge amounts of urban land to reflect their growing importance as providers of infrastructure, housing, and green space.
> 
> &#x2013; [[The Privatisation of Manchester]]

<!--quoteend-->

> Since the dawn of the neoliberal period in the 1980s, however, public land has been subject to privatisation on a massive scale. In his recent book The New Enclosure, Brett Christophers found that a staggering 10% of Britain’s total landmass has been privatised during this period, equivalent to land worth £400 billion.
> 
> &#x2013; [[The Privatisation of Manchester]]


## Sell offs

-   https://council-sell-off.thebureauinvestigates.com (h/t [@jdaviescoates](https://social.coop/@jdaviescoates/106241193709364547))

