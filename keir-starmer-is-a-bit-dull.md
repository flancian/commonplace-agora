# Keir Starmer is a bit dull

[[Keir Starmer]].

-   This article seems to confirm the assertion that Keir Starmer is a bit dull.  [‘Stop talking about the problem – fix the bloody thing!’ Keir Starmer on Bori&#x2026;](https://www.theguardian.com/politics/2022/jan/19/stop-talking-about-the-problem-fix-the-bloody-thing-keir-starmer-on-boris-johnsons-parties-and-his-plan-to-win-power)


## Epistemic status

Type
: [[claim]] (or is it a [[feeling]]?)

Agreement level
: [[Signs point to yes]]

For some reason I feel bad saying it.  But it's commonly levelled at him that he doesn't have enough charisma to win voters over, and signs do indeed point to yes.

