# Separation of article concerns

I just wrote a big ol' blog post about [[indiewebifying my event discovery and RSVPs]].  Thinking about it just now, however, it's a bit of a mish-mash between **why** I wanted to do it, and **how** I did it.

For someone coming to the post who is new to the IndieWeb, it's probably bit off-putting (and maybe fuel for the fire of 'IndieWeb is too complicated').  And for someone who already knows about the IndieWeb, but isn't using WordPress, they might skip over the hows and in the process miss some of the whys.

So in future I might try and split these kinds of articles into two - a 'why' post, and a 'how I did it' post.  The 'why' post will kind of be my [[behaviour-driven development]] specs, so to speak, and probably mostly links to various pattern pages on the IndieWeb wiki.  And the 'how' post will get into the weeds of one very specific implementation, liberally referring back to the 'why' post.

I think that would work well and make the articles a bit more reusable and less niche.

