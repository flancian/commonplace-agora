# Anti-trust and big tech

Anti-trust won't necessarily work.

> simply increasing competition amongst largish for-profit corporations with the same surveillance capitalist business model is unlikely to meaningfully address the major underlying problems and outcomes of the platform economy. In fact, in some instances, around things like data collection, surveillance, and privacy, increased competition may only make things worse.
> 
> &#x2013; [[Privacy, censorship and social media- The Case for a Common Platform]]

