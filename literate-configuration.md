# Literate configuration

The application of [[Literate Programming]] to configuration files.

The book [Literate Configuration](https://leanpub.com/lit-config/read) is great on this, and gives a good overview of why you might do it and why literatre programming works pretty well for config files.

