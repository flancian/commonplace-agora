# Commodity

> Simply put: a commodity is defined as something that was made through human labor, satisfies a demand, and is produced for the purpose of exchange. If I bake a loaf of bread to eat, then it’s just bread. But if I bake it in order to sell it, then the loaf becomes a commodity
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> The producer of commodities no longer lives directly on the products of his own labor: on the contrary, he can live only if gets rid of these products
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> the defining aspect of commodities is that they are produced for exchange
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> How are they exchanged? What determines their values in relation to other commodities
> 
> &#x2013; [[A People's Guide to Capitalism]]

^ [[Exchange-value]].

