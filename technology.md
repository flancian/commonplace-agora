# Technology

> "Technology" is a word about as specific as "sports" or "food" or "animal".
> 
> &#x2013; [Ontology Technological Dynamics - dredmorbius](https://ello.co/dredmorbius/post/klsjjjzzl9plqxz-ms8nww) 

I like technology (insofar as you can like an abstract concept).  I have been interested in computer [[programming]] since young, and have a background as a software developer.  So when I say 'technology', I probably subconsciously lean towards meaning computers and surrounding technologies, worth bearing that in mind.

I'm critical of how technology is used, and very much into the use of technology for [[liberatory purposes]].  If it's not a net positive for humanity and the world in which we live, then what's the point.

> Technology is not neutral. We're inside of what we make, and it's inside of us. We're living in a world of connections — and it matters which ones get made and unmade.
> 
> &#x2013; Donna Harraway

I like the [[free software]] movement, and use a lot of it.  

I'm interested in technology as used for communication, so yeah big fan of the [[Internet]] and am generally a fan of [[technological decentralisation]].

I am interested in the intersection of [[technology and nature]].


## Other stuff

A grab bag of things I've jotted down but haven't really got into any order&#x2026;

-   [[AI and ethics]]
-   [[Intellectual property]]
-   [[Digital regulation]]
-   History of the [[textile industry]]
-   Limbo: virtual experience of asylum
-   [[IoT]]
-   [[Sustainable technology]]
-   [[Cybernetics]]
-   [[My Devices]]
-   [[System/360]]
-   [[Smart cities]]
-   [[Algorithmic bias and racism]]
-   [[Online content moderation]]
-   [[organisational tech policies]]
-   [[VPNs]]
-   [[Networks]]
-   [[predator prey modelling]]
-   [[evolutionary and adaptive systems]]
-   [[Repair]]

