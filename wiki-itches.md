# Wiki itches

-   [X] ignore certain pages (e.g. sitemap) in graphviz export
-   [ ] in selection, pull out all the bolded parts and put them into a bullet list (for [[progressive summarisation]])


## publishing

-   [ ] use git-timemachine to publish previous versions and history
    -   not sure how much this is needed on publish.  does anyone care?
-   [ ] use git log to pull out and publish recent changes
    -   rather than linking to commits on gitlab

