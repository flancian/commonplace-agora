# Pueblo Revolt

I saw the poster in  [[Celebrate People's History: The Poster Book  of Resistance and Revolution]].

> an anticolonial movement to remove the Spanish colonial presence in what is now known as the upper Rio Grande Valley
> 
> &#x2013; [Pueblo Revolt - Wikipedia](https://en.wikipedia.org/wiki/Pueblo_Revolt) 

<!--quoteend-->

> Although commencing a day prematurely, thousands of Indigenous warriors engaged in a ten day offensive that forced the settler community (including Tlaxcala servants, mestizo residents, detribalized Natives known as genízaros, and Pueblo allies) to relocate hundreds of miles south to El Paso del Norte.
> 
> &#x2013; [Justseeds | The Pueblo Revolt](https://justseeds.org/product/the-pueblo-revolt/) 

<!--quoteend-->

> The anticolonial struggles of Po’pay and his contemporaries remain a specter of the potential and possibility of Indigenous resistance to settler colonialism.
> 
> &#x2013; [Justseeds | The Pueblo Revolt](https://justseeds.org/product/the-pueblo-revolt/) 

[[Anti-colonialism]]


## Bookmarks

-   https://justseeds.org/product/the-pueblo-revolt/

