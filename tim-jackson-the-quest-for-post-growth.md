# Tim Jackson & the Quest for Post Growth

A
: [[podcast]]

URL
: https://david-bollier.simplecast.com/episodes/tim-jackson-the-quest-for-post-growth-ZXxy04GQ

Series
: [[Frontiers of Commoning]]

Featuring
: [[Tim Jackson]] / [[David Bollier]]

> Ecological economist Tim Jackson has spent over three decades investigating what a post-growth economy might look like and how to pursue it. His 2009 book '[[Prosperity without Growth]]' became a landmark exploration of this topic. Now, more than a decade later, Jackson’s thinking has evolved in some new and unexpected ways. His new book, '[[Post Growth: Life After Capitalism]]', urges economics to expand its narrow, hyper-rational frameworks, and draw on insights from the worlds of art, culture, philosophy, storytelling, and the human quest for meaning.  

