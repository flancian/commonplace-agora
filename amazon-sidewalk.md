# Amazon Sidewalk

> The new wireless mesh service will share a small slice of your Internet bandwidth with nearby neighbors who don’t have connectivity and help you to their bandwidth when you don’t have a connection.
> 
> &#x2013; [Amazon devices will soon automatically share your Internet with neighbors | A&#x2026;](https://arstechnica.com/gadgets/2021/05/amazon-devices-will-soon-automatically-share-your-internet-with-neighbors/) 

<!--quoteend-->

> By default, Amazon devices including Alexa, Echo, Ring, security cams, outdoor lights, motion sensors, and Tile trackers will enroll in the system.
> 
> &#x2013; [Amazon devices will soon automatically share your Internet with neighbors | A&#x2026;](https://arstechnica.com/gadgets/2021/05/amazon-devices-will-soon-automatically-share-your-internet-with-neighbors/) 

