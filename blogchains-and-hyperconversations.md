# Blogchains and hyperconversations

I don't really [[like likes]].  On the big silos of the social industry they have become weaponised; a kind of social Taylorism, where the craft of building social relationships has been reduced to unskilled labour - just another way of automating us.

Even on the open web, where they are not designed to distract, likes are still a bit of a weak form of interaction.  I think they have their place, but I want something a bit _more_.  Something more than comments below a post, too.  They're a bit constrained - in hock to the main body of text above.  


## Blogchains

I came across the idea of [[blogchains]] the other day, on [Tom Critchlow's blog](https://tomcritchlow.com/2019/07/17/blogchains/) I believe.  The word is from [Venkatesh Rao](https://www.ribbonfarm.com/2019/03/21/constructions-in-magical-thinking/), and the very tl;dr is that it's a string of short, ad-hoc blog posts that build on a theme.  That's cool, and tied in with a wiki is kind of how I see me builing up ideas over time.  

But where the idea gets really interesting (for me) is when it extends to [cross-site blogchains](https://tomcritchlow.com/blogchains/networked-communities/) and [open blogchains](https://tomcritchlow.com/2019/10/31/new-blogging-2/).  These are more open-ended, involving two or more people conversing and building on a theme, simply by posting to their blog about it and linking the posts together.  Kind of a webring, but for posts rather than sites.

There's definitely something to be said for the long-form, turn-based conversation.  One of the best conversations I have had recently was a long email chain.  And some of the thoughts that have stuck with me the most are ones I've written as a long reply to someone else's open question or musings on a topic.


## Hyperconversations

The blogchain thing reminded me of something Kicks wrote about a few months back - [hyperconversations](https://www.kickscondor.com/comments/the-hyperchat-modality/).  It's a chat between friends, conducted across blogs and wikis.  Less formal than a blogchain - no predetermined theme. 

> It’s very informal and fluid. It’s completely simple: just leaving messages for each other on our sites.
> 
> &#x2013; [The Hyperchat Modality](https://www.kickscondor.com/comments/the-hyperchat-modality/) 


## Conversations that last

I think what they're both getting at, is using [[social software]] to have distributed conversations that last more than just an hour or two.

Chris wrote about the [temporality of social media](https://boffosocko.com/2019/12/08/on-the-caustic-focus-on-temporality-in-social-media/).  

> Taking this a level deeper, social is thereby forcing us to not only think shallowly, but to make our shared histories completely valueless.

Shallow conversations disappear off the timeline pretty quickly.  As mentioned, I don't think this is true just for Twitter and Facebook though.  It's more a problem of the medium.

> Relatedly, contemporary fediverse interfaces borrow from surveillance-capitalism based popular social networks by focusing on breadth of relationships rather than depth. [&#x2026;] What if instead of focusing on how many people we can connect to we instead focused on the depth of our relationships? 
> 
> &#x2013; [Spritely: towards secure social spaces as virtual worlds](https://dustycloud.org/blog/spritely/) 

Not to rag on likes and reposts too much.  I do them plenty.  There's a time and place for everything.  And I'm not saying that I want to have to sit down and write a 500 word blog post every time I want to say hi to a friend.  But!  I would definitely like some more conversations that last.   

So who's up for a blogchain, or a hyperconversation?

