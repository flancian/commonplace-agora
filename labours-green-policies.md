# Labour's green policies

-   [[green investment fund]]
-   mass retrofitting programme
-   pledge to decaronise steel
-   "net zero and nature test" for every policy

> During its party conference in Brighton, Labour set out measures including an annual £28bn [[green investment fund]], a mass retrofitting programme, a pledge to decarbonise steel and a “net zero and nature test” for every policy.
> 
> &#x2013; [Climate experts give cautious welcome to Labour’s green policies | Labour con&#x2026;](https://www.theguardian.com/politics/2021/oct/01/climate-experts-give-cautious-welcome-to-labours-green-policies)

[[Labour Party]].

