# Twitter

> Twitter released internal research showing it “amplifies more tweets from rightwing politicians and news outlets than content from leftwing sources.” That’s something anyone paying attention has known for a while, but I guess it’s good to see the platform at least admit it
> 
> &#x2013; [Twitter knows it’s spreading right-wing lies](https://mailchi.mp/techwontsave.us/twitter-knows-its-spreading-right-wing-lies)

<!--quoteend-->

> So yes, it’s good to have proof of something we’ve known for a while, but don’t give Twitter too much credit when it’s still amplifying right-wing content and its CEO is not only complicit, but pushing right-wing trash of his own
> 
> &#x2013; [Twitter knows it’s spreading right-wing lies](https://mailchi.mp/techwontsave.us/twitter-knows-its-spreading-right-wing-lies)

