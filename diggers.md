# Diggers

> sometimes seen as forerunners of modern anarchism
> 
> &#x2013; [Diggers - Wikipedia](https://en.wikipedia.org/wiki/Diggers)

<p class="verse">
"In 1649, to St. George’s Hill,<br />
<br />
a ragged band they called the Diggers came to show the people’s will.<br />
<br />
They defied the landlords, they defied the laws,<br />
<br />
they were the dispossessed reclaiming what was theirs.<br />
<br />
We come in peace, they said, to dig and sow,<br />
<br />
we come to work the land in common, and to make the wasteland grow.<br />
<br />
This earth divided, we will make whole, so it can be a common treasury for all.<br />
<br />
The sin of property we do disdain,<br />
<br />
no one has the right to buy and sell the earth for private gain.<br />
<br />
We work, we eat together, we need no swords.<br />
<br />
We will not bow to masters, or pay rent to lords.<br />
<br />
We are free men, though we are poor.<br />
<br />
You diggers all stand up for glory, stand up now.<br />
<br />
From men of property, the orders came.<br />
<br />
They sent the hired men and troopers to wipe out the Diggers’ claim.<br />
<br />
Tear down their cottages—destroy their corn.<br />
<br />
They are dispersed—only the vision lingers on.<br />
<br />
You poor take courage. You rich take care.<br />
<br />
The earth is a common treasury for everyone to share."<br />
<br />
&#x2013; "The World Turned Upside Down” by Leon Rosselson<br />
</p>

