# It's not in Covid-19's own interests to become more mild over time

[[Coronavirus]]

> Once a virus has spread via a person, it no longer needs that person, and doesn’t care whether they get sick or die afterwards. 
> 
> [Tuesday briefing: Pressure on PM over latest No 10 lockdown party | | The Gua&#x2026;](https://www.theguardian.com/world/2022/jan/11/tuesday-briefing-pressure-on-pm-over-latest-no-10-lockdown-party)

<!--quoteend-->

> The Alpha variant was more virulent than the original Covid virus that emerged from China, and the Delta variant was more virulent still. Omicron is somewhat milder, but there is no guarantee that future variants will be.
> 
> [What lies on the other side of the UK’s Omicron wave? | Omicron variant | The&#x2026;](https://www.theguardian.com/world/2022/jan/13/what-lies-on-the-other-side-of-the-uks-omicron-wave)

