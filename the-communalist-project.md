# The Communalist Project

Essay in [[The Next Revolution]].

> In the opening essay, “The Communalist Project,” Bookchin situates [[Communalism]] vis-à-vis other left ideologies, arguing that the world has changed significantly from the times that birthed [[anarchism]] and [[Marxism]]; he contends that these older ideologies are no longer capable of addressing the new and highly generalized problems posed by the modern world, from global warming to postindustrialization.

Outlines the ways in which capitalism is bad.  Then outlines in which Marxism, anarchism, and revolutionary syndicalism are flawed.

I like much of what he's saying about something in between the organisational tactics of anarchism and Marxism, with a decidely ecological focus.  The municipal approach appeals to me too.

The way it's presented in this essay though, I feel like it straw mans anarchism and communism pretty heavily.  When Bookchin critiques them, he is critiquing a very particular subset of each of them, ideas that aren't held that widely I'm sure.  That said, he knows what he's talking about on both, so I'm assuming it's more nuanced than that, and backed up by other essays of his.

> Many thinkers of the postmodern age have obtusely singled out science and technology as the principal threats to human well-being, yet few disciplines have imparted to humanity such a stupendous knowledge of the innermost secrets of matter and life, or provided our species better with the ability to alter every important feature of reality and to improve the well-being of human and nonhuman life forms.

