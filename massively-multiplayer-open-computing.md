# Massively multiplayer open computing

A video by [[Paul Frazee]] about [[Beaker Browser]].

-   https://invidio.us/watch?v=x3ShGXYCPWQ

Paul states some of the goals of Beaker: 

-   more software freedom (no code hidden away on a server)
-   lowering the barriers to creating and publishing an app or a website
-   more opportunity
-   having fun - keeping the web individual and diverse

It's very adjacent to [[IndieWeb]] to me.  Everyone has their own profile drive, which is kind of like your personal website.  All the data is yours - it's attached to your hyperdrive.  [[Own your data]].  And apps access the data in your hyperdrive, you don't send anything to them.  

One very nice thing with Beaker, you get your Beaker profile just by running the browser - you don't need to set up and maintain a server.  ([[No Servers!  No Admins!]])  You also get an easy to maintain address book, where you can basically follow other people.

I like the idea of being able to fork apps easily, too.  It's as if you were using Facebook, but you wanted to change part of the interface, and you could, because you have immediate access to the source and can just fork it and tweak it.

