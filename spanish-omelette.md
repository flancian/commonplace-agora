# Spanish omelette

-   source: https://hurrythefoodup.com/quick-spanish-omelette/

-   Grate the potato (peel the skin if it looks manky).
-   Dice the onion.
-   Heat the oil in the pan on medium heat and add the potato and onion with a pinch of salt.
-   Stir every now and then, cooking for about 7 minutes until you have a nice golden brown edge to it.
-   In the meantime, beat the eggs in a mixing bowl, again with a pinch of salt.
-   When the potato-onion mix is golden brown, add it to the eggs in the mixing bowl. This starts to cook the egg.
-   Put it all back into the pan and cook on a low heat with a lid on for 5 minutes.
-   After this, take the lid off and get a plate that is wider than the pan. Place the plate face down onto the pan. With a firm grip on the handle and the plate, flip it. You can also use the pan's lid for this.
-   Now slide the omelette back into the pan the other way up. If you’re not sure how to do it we urge you to check out this video. It’ll make life easier.
-   Cook for another 5 minutes on low heat.
-   Awesome, your omelette is then ready. Serve with a sauce of your choice, and enjoy!
-   Que aproveche!

