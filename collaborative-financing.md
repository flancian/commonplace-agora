# Collaborative financing

> Collaborative financing consists of pooling money from individuals,
> the community, and the wider public to finance common wealth.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> Historically, collaborative financing has included such models as [[mutual credit societies]] and [[insurance pools]], [[cooperative finance]], [[community-controlled microfinance]], and [[local currencies]].
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> In recent times, [[crowdfunding]] has been taken these capacities to new levels in both small and very big projects.
> 
> &#x2013; [[Free, Fair and Alive]]

