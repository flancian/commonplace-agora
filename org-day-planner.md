# org-day-planner

I'd like to make something like Goalist, but that uses org-mode as a backend.

I find the Goalist interface excellent for a really fine-grained day plan.  But it's not libre software, and the file format is not easily shareable.

It would be a fun project to recreate some of its functionality using org-mode as the underlying system.

It'd probably make sense to build it as a native Android ap, but If I did it myself, I'd probably use vue.js as a means to learning that.


## TODO

-   [X] Add in current time marker
-   [X] Start intervals from time of first TODO
-   [ ] Display DONE items in separate calendar with visibility toggle
-   [ ] Parsing nested TODOs
-   [ ] Error handling
-   [ ] Writing back to file
-   [ ] Colour based on category of item
-   [ ] Show items only for current day
-   [ ] Allow arbitrary positioning of items in the todo file - so you can split it into sections


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-05-13 Thu 20:24]</span></span>

Wow, this for obsidian looks nice. https://github.com/lynchjames/obsidian-day-planner


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-03 Wed]</span></span>

Separate file of active timestamps.
Increase decrease time. Move up and down.
Set which files to include.

It's kind of a fancy agenda view, I guess.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-06 Sat]</span></span>

OK, so [[Quasar]] would be a decent option it seems.  Plenty useful components out of the box.  It has [QCalendar](https://quasarframework.github.io/quasar-ui-qcalendar/docs) too, which I might be able to crib something from.

I saw a bunch of handy looking Vue calendar components at Made with Vue [Calendars page](https://madewithvuejs.com/calendars).  I wonder, can you use non-Quasar Vue components easily enough in Quasar?  To be seen.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-06 Sat 17:41]</span></span>

Made good progress on this today.  Got basic parsing set up with [[orga]], and basic rendering of items with [[Quasar]] and QCalendar.

Still plenty plenty needs doing though.  Not a small project by any means.  A good learning one.  I'd just like to get it to the point where I can use it on single files that I probably create elsewhere, increase/decrease the time on the items, and then save back.

First, just be able to read a single file and display it usefully.  It would be read-only and I would edit it probably in orgzly for now.

I wonder if that requires use of a store?


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-12 Fri 19:47]</span></span>

Really good progress.  Picking up some Vue bits and pieces which is great.  Added a store via Vuex today, and learning some of the details of how getters, mutations and actions work with that.  Quasar makes it super simple to add these bits of plumbing in.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-03-13 Sat 21:20]</span></span>

Good work on it today.  Refactoring some of the way active tasks are set, using the store and an array of tasks in there.  Vue is very very nice for handling state updates and that just ripping through to all of the display.  Quasar is really nice for giving a bunch of UI components to work with.

So where have I got?  Some basic parsing of org files.  Basic display of the info in there and a bit of interaction for clicking and seeing more details.  It's still all read-only.  The next big step would be allowing for making changes to the times of tasks, and then writing that back to file.  That'll be a tough nut to crack I think.

