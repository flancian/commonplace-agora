# Interlinking wikis

How could you connect [[digital gardens]] together?  And why would you? 


## Why?

[Stian Håklev](http://reganmian.net/blog/) posted an interesting question and on the Digital Gardeners telegram group:

> I'm curious how people feel about comments and interaction? And also interactivity between digital gardens in general (like paths connecting the parks of a city? :)). 

[[Chris]] has talked enthusiastically about interlinking wikis before (e.g. during the [[Gardens and Streams IndieWeb session]]), so I'm sure there's something to it.  For me, I think because I already get the interactivity goodness on my [[stream]] and my articles, it's something I haven't generally been that interested in for my wiki notes thus far.  

I can definitely see the appeal of backlinks between wikis, but only in an abstract sense at the moment.  

The utility is in the [[networked thought]].
I guess for me it comes down to whether I see the utility in all of this connectivity on specifically my [[evergreen notes]], as opposed to my stream posts.

[[A Platform Designed for Collaboration: Federated Wiki]] has lots on this.

> This shift from the standard wiki to a form of writing based on “one person, one wiki, in a federated environment,” may sound like a step backward from the Wikipedia style of open collaboration. But in fact the effect is quite the opposite: **giving online platforms to individual voices while bringing them together into a shared neighborhood of wikis results in a richer, more robust commons**.
> 
> &#x2013; [[A Platform Designed for Collaboration: Federated Wiki]]

[[How does linking gardens facilitate a richer commons?]]

Drilling down. Commons in FFF being a shared governed resource. What's the actual value of the output of a commons of joined gardens?


## What?


### Comments

Stian has some use cases for which he would like the interactivity:

> I know comments have gotten a bad rep on the internet, attracting spam or trolls etc, but on the other hand I feel really frustrated when I can't leave comments on Andy Matuschak's notes&#x2026; 

I think Webmentions would work well here.  You would write a comment as a post on your own site, and then this will notify Andy.  He can choose to do whatever he wants with this comment (display the comment, display it as a backlink, ignore it completely, not display it at all, if he prefers).  This way you can write a comment on whatever you want and the receiver chooses what to do with it.

> Or another example - I just looked at Salman's site about Deliberate rest (https://notes.salman.io/deliberate-rest), and thought that I just took some notes about attention restoration therapy from Deep Work  - https://notes.reganmian.net/deep-work&#x2026; Of course I could tell him here (I am :)) but that "doesn't scale"&#x2026; 

Webmentions would work for this too - as just a simple 'mention', not necessarily a comment.  Salman would be notified automatically that your note references his note. Salman could choose to display it as a backlink, if he liked.

> Short-term, I am looking at adding at least page-level comments to my blog, using a Gatsby plugin and probably externally hosted comments. 

Adding webmention support to receive comments could work here.


### Annotations

> Also interested in experimenting with annotations, for example embedding Hypothes.is directly in the pages&#x2026; 

Kartik Prabhu has a [nice article](https://kartikprabhu.com/articles/marginalia) about receiving annotations on his posts via webmentions.


### Backlinks

> Long-term, I'm interesting in thinking about more structured ways of interlinking digital gardens - whether it looks more like interwiki links, blog backlinks, or something else, I'm not sure. I have some notes I'll publish once I organize them a bit more.


### Combining content

Sharing blocks of content easily between individual gardens.


## How?


### 'Multiplayer'

Roam is now [talking about 'multiplayer'](https://twitter.com/Conaw/status/1307107745397604354). I'm presuming this means linking between individuals' wikis.  I like the idea of [[interlinking wikis]], but imagining it's going to be pretty closed in Roam.  


### WikiWeb

Bill Seitz has lots of ideas about a more open [WikiWeb](http://webseitz.fluxent.com/wiki/WikiWeb). 

> aka Wiki Sphere; parallel to BlogWeb, the universe of (clusters of) WikiSpaces


### FedWiki

FedWiki is obviously doing great things in this space.  

However, a downside of FedWiki (as far as I understand it) is that it is one platform, one tool, that facilitates the federation.  A [monoculture](https://indieweb.org/monoculture).


### Agora

I would prefer a more distributed, protocol-based approach.  [[Agora]] works in this sense as each individual can write their garden whichever way they see fit, and just needs to fit in to some accepted formatting protocols to be aggregated.


### IndieWeb

See also the [[IndieWeb]] approach, where the onus is less on aggregation and more on a certain protocol of communication.  

FedWiki does seem to offer much simpler actions for copying content from one wiki to another.  Agora doesn't have this built in as such.  IndieWeb maybe more so (retweets, quotes, likes, etc, are catered to through webmentions)


### [[Collective Knowledge Management]]

What Athens Research is calling this.

> CKM is a way for groups of people to create, connect, and compound knowledge, from research and documentation to ideas and conversations.
> 
> &#x2013; [Season 2 of Athens — A Collective Vision](https://athensresearch.ghost.io/season-2/) 


### massive.wiki

https://massive.wiki/

Seems similar to Agora in some ways - combining individual collections of markdown files in to something bigger.


### bopwiki

https://floss.social/@Valenoern/104748723877868120

If I understand, the intent is to allow users of ActivityPub easily add things from their stream in to a wiki.


## Links

-   [[Commons P2P wiki requirements]]
-   [[Distributed wikis on solid]]
-   [GitHub - IndyVerse/indywiki: Web3Native Tinkerable Wiki](https://github.com/IndyVerse/indywiki)
-   [[Federated Education: News Directions in Digital Collaboration]]
-   [[A Platform Designed for Collaboration: Federated Wiki]]


## Log


### <span class="timestamp-wrapper"><span class="timestamp">[2021-05-16 Sun]</span></span>

I guess there is a distinction between:

-   federated wikis
-   aggregated wikis
-   distributed wikis

perhaps?  Similarities, overlaps, possibly they're the same.  Would be good to differentiate.


### <span class="timestamp-wrapper"><span class="timestamp">[2021-12-27 Mon]</span></span>

Increasingly convinced that the distributed/federated digital garden approach is the way to go for knowledge commoning. I say this because I've been doing it for a while and it is working well in practice.

Plenty to crib from the ActivityPub model, I think - where you have local, global, and 'those-you-follow' timelines. Similarly, you could have local, 'those-you-follow', and global gardens. I'd in fact prefer it if this model worked more on some kind of liquid democracy style, where there's some rippling out effect that lives between the following and global timelines. I guess perhaps there's some repost/boost equivalent?

And as with ActivityPub, where it's kind of absent, I'd want for there to be a 'group' concept that isn't based on infrastructure, just on interest groups.

As with streams, my ideal is probably some middleground of Indieweb and ActivityPub approaches.

