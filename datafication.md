# Datafication

> Datafication is now understood canonically as a process that transforms many aspects of everyday life into data, and data into value, and it is considered a “legitimate means to access, understand and monitor people’s behavior
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> datafication suggests that data production and circulation are significant in themselves, regardless of what the data might contain
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

<!--quoteend-->

> Put another way, datafication opens out spaces for citizens to participate in by generating data, auditing government data, or creating alternative data analytic systems, but these spaces for action still follow the dominant logic of big data optimization
> 
> &#x2013; [[Undoing Optimization: Civic Action in Smart Cities]]

