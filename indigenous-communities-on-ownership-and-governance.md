# indigenous communities on ownership and governance

There's a chapter on this in [[Future Histories]].

> Indigenous communities, including the Maori and equivalent populations in Australia and Canada, have a lot to teach us about governance and the natural world.

