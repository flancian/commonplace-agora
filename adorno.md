# Adorno

![[images/adorno.png]]

> we live on the culture we criticize
> 
> &#x2013; Adorno and Horkheimer
