# Platform socialism

I'm reading the book about platform socialism at the moment after listening to the podcast [[Platform Socialism and Web3]].


## An idea

> the organisation of the [[digital economy]] through the social ownership of digital assets

<!--quoteend-->

> democratic control over the infrastructure and systems that govern our digital lives.


## A book

A
: [[book]]

Author
: [[James Muldoon]]

> A deep sense of [[technological determinism]] pervades our present era

<!--quoteend-->

> Reclaiming our sense of [[collective self-determination]] requires a new kind of [[platform economy]]. How do we imagine an alternative that is neither private oligarchy nor unaccountable state bureaucracy – an alternative outside of rule by [[Big Tech]] or [[Big State]]? The answer lies in new forms of [[participatory governance]] and [[decentralised governance]] which place human freedom over profits and ensure the benefits of technology are equally distributed. It involves citizens’ active participation in the design and control of socio-technical systems rather than their after-the-fact regulation by a technocratic elite. I call this idea platform socialism – the organisation of the digital economy through the social ownership of digital assets and democratic control over the infrastructure and systems that govern our digital lives.

<!--quoteend-->

> Rather than just trying to fix Facebook, we should start to imagine what better alternatives could take its place

<!--quoteend-->

> This movement is not a quest for an ideal or harmonious society but is driven by antagonistic practices and a resistance to commodification and exploitation. It gestures beyond piecemeal reforms and the bland crisis management and troubleshooting that characterises much of our present response to Big Tech

<!--quoteend-->

> It is strategically unsound to always be on the defensive, waiting to protest the latest round of capitalist tech innovation

<!--quoteend-->

> The approach advocated here is a threefold strategy of resisting, regulating and recoding existing digital platforms


### Reading log


#### <span class="timestamp-wrapper"><span class="timestamp">[2022-01-29 Sat]</span></span>

First chapter was a good intro, the next two are pretty heavy, focusing on the problems with Facebook and Airbnb. I get that it's necessary for a thesis, if you're going to propose something new then you need to examine what's currently wrong. That's fine but no longer new to me, and sometimes a bit draining just hearing about the problems. But the author stated early on that they will go on to discuss their proposals for how to change things. I'm eager to get to these parts.


## Bookmarks

-   [Regulating Big Tech is not enough. We need platform socialism | openDemocracy](https://www.opendemocracy.net/en/regulating-big-tech-is-not-enough-we-need-platform-socialism/)

