# The commodification of social activities

[[social Taylorism]].

> One of the key critiques of mainstream social media platforms is that they produce individualist subjectivities through how their various functions mould online behaviour. Addressing such critiques, through alternative social media platforms, should involve the development of architectures that are designed to move behaviour away from a commodification of the activities.
> 
> &#x2013; [[Anarchist Cybernetics]]

