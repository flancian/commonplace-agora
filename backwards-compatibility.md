# Backwards compatibility

A way to make things last.

Steve Yegge recently [wrote](https://medium.com/@steve.yegge/dear-google-cloud-your-deprecation-policy-is-killing-you-ee7525dc05dc) about how "[b]ackwards compatibility keeps systems alive and relevant for decades" and mentions [[Emacs]] as a prime example.  

