# Is the phrase 'digital colonialism' problematic?

I've come across it in a couple of places now.

[[Lizzie O'Shea]] talks about something like it in a chapter of [[Future Histories]] (an online article of roughly the same content here - [[Frantz Fanon Against Facebook: How to Decolonize Your Digital-Mind]]).  I'm not sure if she uses exactly that phrase - I'll have to go back and check.

[[Nathan Schneider]] uses it in  
[[Vanguard Stacks: Self-Governing against Digital Colonialism]].

I have huge respect for both.  But relating the horrors of colonialism to the misdemeanours of the big tech platforms seems remarkably insensitive to me.

In Nathan's article he references a lot of papers that have used the term, and suggests that there is some general convergence towards its usage.  I'm sure all of these authors have wrestled with its usage, so there must be some rationale behind it.

Before I could be happy using it myself, I'd need to read up on that prior history.  I'd also need to know precisely what is being referred to by 'digital colonialism'.  Is it just the activities of big tech platforms?  Or is it something else?

