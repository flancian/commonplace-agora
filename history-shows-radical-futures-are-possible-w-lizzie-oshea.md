# History Shows Radical Futures Are Possible w/ Lizzie O'Shea

URL
: https://www.buzzsprout.com/1004689/3747218-history-shows-radical-futures-are-possible-w-lizzie-o-shea

Publisher
: [[Tech Won't Save Us]]

Interview with [[Lizzie O'Shea]]

> how learning about history can empower us to imagine more radical futures

[[Future Histories]].

Talks about [[Toussaint Louverture]] at 10:31.  And his appropriation by venture capitalist Ben Horowitz.

