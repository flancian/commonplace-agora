# Especifismo

Started reading [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]] as [[Especifismo]] is [[node club]] node this week.

Sounds really good so far.  Seems very adjacent to [[Anarchist Cybernetics]] in it's push towards organisational structures within anarchism.

Sounds related to [[Platformism]] too.

> born out of nearly 50 years of anarchist experiences in South America
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]


## Three key points

-   The need for specifically anarchist organization built around a unity of ideas and [[praxis]]
-   The use of the specifically anarchist organization to theorize and develop strategic political and organizing work.
-   Active involvement in and building of autonomous and popular social movements, which is described as the process of "[[social insertion]]"

What does 'specifically anarchist' actually mean?


## Specifically anarchist

> Especifists inherently state their objection to the idea of a synthesis organization of revolutionaries or multiple currents of anarchists loosely united
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

Interesting.  What (specifically) is a synthesis organisation?  Ah OK -  [[Synthesis anarchism]].

> North American anarchists have also offered their experiences of synthesis organization as lacking any cohesiveness due to multiple, contradictory political tendencies
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

<!--quoteend-->

> the Especifists criticize these tendencies for being driven by spontaneity and individualism and for not leading to the serious, systematic work needed to build revolutionary movements
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

<!--quoteend-->

> A particular stress of the Especifismo praxis is the role of anarchist organization, formed on the basis of shared politics, as a space for the development of common strategy and reflection on the group’s organizing work
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

All sounds a bit like some of the critiques of [[Occupy]] - in that it failed because there was no organisational structure.  See e.g. [[Horizontalism vs verticalism]], [[Anarchist Cybernetics]].

> On organization, the Especifists call for a far deeper basis of anarchist organization than the Platform’s “theoretical and tactical unity,” but a strategic program based on analysis that guides the actions of revolutionaries
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]


## Social insertion

[[social insertion]].

> Especifismo’s conception of the relation of ideas to the popular movement is that they should not be imposed through a leadership, through “mass line,” or by intellectuals
> 
> &#x2013; [[Especifismo: The Anarchist Praxis of Building Popular Movements and Revolutionary Organization]]

