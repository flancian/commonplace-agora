# Production is socialised, profit is privatised

Came across this idea from Breht O'Shea: [Revolutionary Left Radio: Challenging Capitalism: Envisioning a Socialist Wor&#x2026;](https://revolutionaryleftradio.libsyn.com/socialist-workplace) 

He said that the idea is from Engels' [[Socialism: Utopian and Scientific]].

Same idea here from [[Mariana Mazzucato]]:

> What this means is that we have socialized the risk of innovation but privatised the rewards.
> 
> -   [Five minutes with Mariana Mazzucato: "We have socialised the risk of innovati&#x2026;](https://blogs.lse.ac.uk/politicsandpolicy/5-minutes-with-mariana-mazzucato/)

