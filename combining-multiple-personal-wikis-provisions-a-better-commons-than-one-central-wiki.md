# Combining multiple personal wikis provisions a better commons than one central wiki

This idea is embodied by the section in Free, Fair and Alive entitled [[A Platform Designed for Collaboration: Federated Wiki]].

Wikipedia tries to define an objective truth on the world's knowledge.  Is that possible?  Maybe for some topics, but in many others, the phenomenon of edit wars would suggest not.

Wikipedia defines itself on the removal of voice and subjectivity.

If you can combine multiple personal wikis side-by-side in to one, you can retain voice and subjectivity.

Doesn't it just become a mess?  Isn't some editorial overview important?

