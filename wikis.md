# Wikis



## What **is** a wiki?

Quite an amorphous term these days.

> soft security, editability of both content + metadata, recent changes, permalinks, editable namespaces. A healthy dose of eventualism
> 
> &#x2013; SJ, Agora Discuss

<!--quoteend-->

> Wiki is perhaps the only web idiom that is not a child of BBS culture. It derives historically from pre-web models of hypertext, with an emphasis on the pre. The immediate ancestor of wiki was a [[HyperCard]] stack maintained by Ward Cunningham that attempted to capture community knowledge among programmers. Its philosophical godfather was the dead-tree hypertext A Pattern Language written by Christopher Alexander in the 1970s.
> 
> &#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/) 

<!--quoteend-->

> Wiki iterates not through the creation of new posts, but through the refactoring of old posts. It shows not a mind in motion, but the clearest and fairest description of what that mind has (or more usually, what those minds have) arrived at. I	t values reuse over reply, and links are not pointers to related conversations but to related ideas.
> 
> &#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/) 


## Is a **personal** wiki an oxymoron?

> What wiki brought to these models, which were personal to start with, was collaboration. Wiki values are often polar opposites of blogging values. **Personal voice is meant to be minimized**. Voices are meant to be merged. 
> 
> &#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/) 

Not so true for the personal wiki.  Maybe a personal wiki is wiki software but with different values for the output?

> Rather than serial presentation, wiki values **treating pages as nodes that stand outside of any particular narrative**, and **attempt to be timeless rather than timebound reactions**.
> 
> &#x2013; [Can Blogs and Wiki Be Merged? | Hapgood](https://hapgood.us/2016/02/22/can-blogs-and-wiki-be-merged/) 

This is still true for personal wikis.

