# Insulation

> A second analysis has found high gas prices mean the energy bills of people living in poorly insulated homes will rise by up to £246 a year. Insulate Britain, a protest group demanding a legally binding national plan to fund low-carbon retrofits of all homes by 2030, has blocked motorways, A-roads and the port of Dover in recent weeks
> 
> &#x2013; [UK’s home gas boilers emit twice as much CO2 as all power stations – study | &#x2026;](https://www.theguardian.com/environment/2021/sep/29/uks-home-gas-boilers-emit-twice-as-much-co2-as-all-power-stations-study) 

