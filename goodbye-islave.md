# Goodbye iSlave

-   https://www.press.uillinois.edu/books/catalog/77shx5qp9780252040627.html

Outlines the thesis of [[iSlavery]].

> A tough and confrontational book, Goodbye iSlave nevertheless maintains hope for meaningful resistance.
> 
> &#x2013; [[Shackles of Digital Freedom]] 


## Criticisms

(from [[Shackles of Digital Freedom]])

-   references to slavery.  While Qiu does it in a nuanced way, and legally speaking what he refers is in fact slavery, some of the comparisons to slavery historical detract from and diminish the argument.

-   the suggested modes of resistance. They maybe fall under the brackets of ethical consumerism, and perhaps an uncritical assumption that all technology can be liberatory if harnessed right.

