# The Cybernetic Hypothesis

[[Tiqqun]].

An [[anarchist]] critique of [[cybernetics]].


## Links

-   [notes on the cybernetic hypothesis | Richard Hall's Space](http://www.richard-hall.org/2017/07/07/notes-on-the-cybernetic-hypothesis/)

