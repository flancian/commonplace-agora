# Public Digital Cooperatives

> Finally, we propose a network of Public Digital Cooperatives that would establish digital development labs in disinvested communities. Each would be overseen by citizens assemblies with a mandate to ensure that the projects funded develop the infrastructure of a more complete social, economic and political democracy. The labs would work with other public institutions, from the post office and the central bank to the universities and the library system, to develop new, commonly held resources. In this way, we as citizens will have eyes on the shape and direction of the digital economy,, and will be able to take a far more active role in the conscious planning of our shared future
> 
> &#x2013; [[Privacy, censorship and social media- The Case for a Common Platform]]

The [[British Digital Cooperative]] fleshes this out.
