# NeurAnim

The final year project from my MSc ([[Evolutionary and Adaptive Systems]], way back in 2008&#x2026;) was working on a visualisation tool for neural networks called NeurAnim.

I found a screenshot of it on SourceForge.  (Remember SourceForge?!)

![[images/neuranim.jpg]]

Apparently I used Fedora then?  I don't remember that to be honest.

