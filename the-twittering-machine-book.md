# The Twittering Machine (book)

by Richard Seymour.


## First chapter.  'We are all connected'.

Focused mainly on some of the problems with the [[social industry]].  Spun slightly different than most accounts, focusing on the profligacy with which we are all writing these days.  Touched on trolling, fake news, etc.  Some psychoanalysis, some theory, e.g. mentions of society of the [[spectacle]].

Doesn't feel like it's telling anything novel so far - we already know the social industry is rife with problems.  It is more urbane than most accounts though.


## Second chapter: we are all addicts

Second chapter discusses addiction to the social industry.  Interesting little history of Facebook and likes that I hadn't read before.  People are becoming less social.

