# Control of computing infrastructure

I had not really thought much about the tech firms in this light before - of the undue control they have on computing infrastructure.  (I think the author here including both hardware **and** software platforms in 'infrastructure').

> In all the global crises, pandemics and social upheavals that may yet come, those in control of the computers, not those with the largest datasets, have the best visibility and the best – and perhaps the scariest — ability to change the world.
> 
> &#x2013;  [Privacy is not the problem with the Apple-Google contact-tracing toolkit](https://www.theguardian.com/commentisfree/2020/jul/01/apple-google-contact-tracing-app-tech-giant-digital-rights) 

I don't know if it's a bigger problem or not than [[surveillance capitalism]] though.  They both seem like big problems, in tandem.  

The distinction between harvesting data and running the platform seems pretty neglible, too.  Unless maybe he's talking about things like Amazon Web Services more than things like Facebook?   

Dunno.  Regardless, cool to see both [[right to repair]] and [[IndieWeb]]-adjacent stuff mentioned together as modes of resistance against big tech.

