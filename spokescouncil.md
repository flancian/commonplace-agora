# Spokescouncil

> For [[Occupy]] in particular, the evolution that involved moving strategic decision making from the general assembly – marred by timewasting and the involvement of those not active in the camps, including tourists attracted by the media – to the spokescouncil marked a clear attempt to develop organisational processes in response to emerging challenges.

