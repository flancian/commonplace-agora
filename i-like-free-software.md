# I like free software

[[I like]] [[Free software]].

Because&#x2026;


### selfish reasons

-   [[I have benefitted hugely from free software]]


### social reasons

-   [[Free software enacts 'from each according to their ability, to each according to their need']]
-   [[Free software affords digital self-governance]]


## Epistemic status

Type
: [[feeling]]

Strength
: 8

How well I could explain why
: 3

