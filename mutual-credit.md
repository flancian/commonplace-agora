# Mutual credit

[[mutual credit societies]]

> >  digital forms of cash which aren't based on proof-of-work or proof-of-stake and are relatively benign
> 
> Mutual credit. But it's not really a form of cash.
> 
> https://social.coop/@bhaugen/107351792640939303

