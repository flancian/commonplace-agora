# complex systems

> a system composed of a large number of independent simple components that locally interact in an independent and nonlinear fashion, exhibit self-organization through inter- actions that are neither completely random nor completely regular and are not influenced by some central or global mechanism, and yield emergent behavior at large scales that is not predictable from observation of the behavior of the components.
> 
> &#x2013;  [[Complex Adaptive Systems, Systems Thinking, and Agent-Based Modeling]]

Handy image from Wikipedia on the different areas:

![[images/complex-systems-organizational-map.jpg]]
By Hiroki Sayama, D.Sc. - Created by Hiroki Sayama, D.Sc., Collective Dynamics of Complex Systems (CoCo) Research Group at Binghamton University, State University of New York, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=12191267


## Complex systems and complex adaptive systems

> [[Complex adaptive systems]] are special cases of complex systems that are adaptive in that they have the capacity to change and learn from experience. Examples of complex adaptive systems include the stock market, social insect and ant colonies, the biosphere and the ecosystem, the brain and the immune system, the cell and the developing embryo, the cities, manufacturing businesses and any human social group-based endeavor in a cultural and social system such as political parties or communities.
> 
> &#x2013; [Complex system - Wikipedia](https://en.wikipedia.org/wiki/Complex_system) 


## Examples

> Common examples given are [[ecosystems]], financial markets, [[the brain]], [[ant colonies]], economies, and many other examples where large numbers of constituents independently interact on a local level that yield some unanticipated nonlinear outcome at scale.
> 
> &#x2013;  [[Complex Adaptive Systems, Systems Thinking, and Agent-Based Modeling]]

