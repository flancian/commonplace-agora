# Alternative social media

[[Alternative media]]. [[social media]].

> Perhaps, rather than producing entirely new alternative platforms, the task for radical developers is to identify suites of existing platforms that are most useful to selforganisation and to then find ways of integrating these in ways that allow for coordination (for example, by being able to send and receive WhatsApp messages from a platform like Loomio).
> 
> &#x2013; [[Anarchist Cybernetics]] 

<!--quoteend-->

> More expansive than [[Crabgrass]], [[Lorea]] networks allowed for customisable profile pages and dashboards, wikis, collaborative writing pads, blogs, task managers, status updates and private messaging, affiliations with and between groups and a federal structure for groups and networks.
> 
> &#x2013; [[Anarchist Cybernetics]] 

<!--quoteend-->

> any alternative digital platform ought to have a direct link between whatever decisionmaking functions it houses and the facetoface decisionmaking procedures of the organisation using it.
> 
> &#x2013; [[Anarchist Cybernetics]] 

<!--quoteend-->

> An alternative platform would need to ensure that filtering for noise and overload was, at the very least, open to democratic oversight, if not the product of direct participatory engagement, for instance through popular content floating to the surface, as it were, through a voting procedure.
> 
> &#x2013; [[Anarchist Cybernetics]] 

<!--quoteend-->

> When we explore alternative social media platforms that are designed specifically for selforganisation, we need to remember that these ought to be seen as aids for offline organising, and as such, as infrastructures that support facetoface discussion and action rather than as an entirely separate sphere that radical politics
> 
> &#x2013; [[Anarchist Cybernetics]] 


## What might it look like?

-   https://twitter.com/normative/status/1454074111450308609

