# org-mode and orgzly sync issues

I sync my [[org]] files between [[Emacs]] on my desktop and [[orgzly]] on my phone using [[syncthing]].

At the moment I'm finding they get out of sync reasonably frequently, and you get an error in orgzly saying 'Local and remote notebooks are different'.

My steps to resolve are:

-   on desktop, copy \`<sub>GTD.org</sub>\` to \`<sub>GTD.org.desktop</sub>\`
-   in orgzly, from the Notebooks view, choose to override remote notebook
-   that means syncthing syncs \`<sub>GTD.org</sub>\` back on to the deskop
-   then in Emacs, do an ediff of \`<sub>GTD.org</sub>\` and \`<sub>GTD.org.desktop</sub>\` and pick out the bits you want from each
-   things will sync OK from there

