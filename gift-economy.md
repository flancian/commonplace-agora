# gift economy

[[I like gift economies]] because [[gift economies build community]].

"A gift economy is a system of exchange based on gift giving."

I remember hearing about gift economies a long time ago in relation to [[Free software]].  I'm interested now in where gift economies fit in to alternative [[Flows of value]] beyond the monetary.  And how they relate to [[Mutual aid]] and [[commoning]].

So at the outset: what do I think a gift economy is?  So I guess its the 'exchange' of things to pass 'value' from one person to another.  It's not bartering, though, because it's a one way thing - at least there's no expectation of someone giving you something back there and then, when you give a gift.

Presumably in the frame of a gift 'economy', this isn't about giving gifts for festivities like a birthday.  It's about gifting being a means of provisioning?

I've also seen the term as '[[gift culture]]', which I think maybe I might like more.  But perhaps there's a reason at times to refer to it as an economy.


## Relation to commoning

Let's go to [[Free, Fair and Alive]] for their take on it.

The phrase 'gift economy' pops up a couple of times in [[Free, Fair and Alive]] and alive, but not so frequently as for me to think its a central tenet of it.  That said, when it **is** mentioned, it seems very closely linked with commons and commoning.  Maybe its perhaps so intrinsically linked they don't feel the need to explicitly make the link?  Not sure.

> Commons – Commoning – Commoner. A brief excursion into the etymology of these terms: each word connects the Latin words cum and munus. Cum (English “with”) denotes the joining of elements. **Munus — which is also found in the word “municipality” — means service, duty, obligation, and sometimes gift.**
> 
> &#x2013; [[Free, Fair and Alive]]

One of the patterns of commoning from [[Free, Fair and Alive]] is [[Contribute Freely]].  Which feels very similar to gifting.

> Contribute freely means giving without expecting to receive anything
> of equivalent value, at least not here and now. It also means that when
> people do receive something, it is without feeling the need to recip-
> rocate in direct ways.
> 
> &#x2013; [[Free, Fair and Alive]]

They reference [[The Gift]] by Lewis Hyde which is probably worth a look&#x2026;

> It is no accident that the words “commons” and “municipality” share the same etymology with the root Latin word munus, which combines the meanings “gift” and “duty.”
> 
> &#x2013; [[Free, Fair and Alive]]

Interesting this (historical at least) overlap between gift and duty.

> Property must be able to support social cooperation, ecological stewardship, and nonreciprocal gifting.
> 
> &#x2013; [[Free, Fair and Alive]]

It feels quite core to their notion of property, which is quite core to their overall thesis.

> We have seen, for example, how the Couchsurfing website, a gift economy of lodging for travelers, morphed from a hospitality commons into a commercial travel service several years ago.
> 
> &#x2013; [[Free, Fair and Alive]]

So here it seems like a gift economy helped to build a commons.  (That got enclosed eventually, but that's not the point here&#x2026;)


## Relation to peer production

> [&#x2026;][[peer production]] is not a form of reciprocity-based gift economy, but non-reciprocal communal shareholding. [&#x2026;] However, both peer production and the gift economy proceed from the same 'gifting spirit' and are therefore related. 
> 
> &#x2013; [Gift Economy - P2P Foundation](https://wiki.p2pfoundation.net/Gift_Economy) 

Suggestion here is that gift economy is based on an expectation of reciprocity - not sure if everyone would define it that way.

> In peer production, value is created not in exchange from a gift by another, but in exchange for the emotional and spiritual value, and recognition, that derives from working on a project of common value. 
> 
> &#x2013; [Gift Economy - P2P Foundation](https://wiki.p2pfoundation.net/Gift_Economy) 

By these definitions, peer production sounds nicer to be honest.


## Lean Logic

Let's see what [[David Fleming]] says.

> In other words, gifts cannot be taken at face value. They are instruments of social cohesion, creating networks of exchange and obligation which are not provided by market exchange, as Lewis Hyde notes[&#x2026;]
> 
> &#x2013; [[Lean Logic]], [Gifts](https://leanlogic.online/glossary/gifts/)

Lewis Hyde again.

> Giving lives in a domain ranging from deep symbolism to basic housekeeping. It is the currency of the small-scale resilient community. It is sometimes magic, in that some kinds of gift can be given over and over again without loss—love, for example, and (in a different sense, since time cannot really be replenished) the gift of reciprocal service to each other. The “reciprocal” part happens, but not by arrangement. This is the opposite of transparency: you cast your gift upon the waters, and what comes back is trust.
> 
> &#x2013; [[Lean Logic]], [Gifts](https://leanlogic.online/glossary/gifts/)


## Principles

As suggested by Charles Eisenstein:

> 1.  Over time, giving and receiving must be in balance.
> 2.  The source of a gift is to be acknowledged.
> 3.  Gifts circulate rather than accumulate.
> 4.  Gifts flow towards the greatest need.
> 
> &#x2013;  [The gift economy and community exchanges - REconomy : REconomy](http://reconomy.org/what-you-can-do/alternative-banking-and-currencies/the-gift-economy-and-community-exchanges/) 

As outlined by [[Maggie Appleton]]:

> Gift economies have a few key qualities. They are&#x2026;
> 
> 1.  Loosely and informally tracked - it is bad form to calculate the exact value of gifts you receive, or keep an obsessive log of it the way we do with business finances
> 2.  Indirect - it is not always two individuals giving back and forth. Instead gifting is generalised to a whole community where you may give to people who have not given directly to you
> 3.  Delayed - we do not reciprocate gifts the minute we receive them, but instead wait to repay the offering at a later date
> 
> &#x2013; [The Gift Economy](https://maggieappleton.com/gift-economy) 

<!--quoteend-->

> In his brilliant book [[The Gift]]: The Erotic Life of Property, Lewis Hyde points to two types of economies. In a commodity (or exchange) economy, status is accorded to those who have the most. In a gift economy, status is accorded to those who give the most to others.


## Examples

Simple examples:

> You have probably invited people over for a meal and been invited to meals in return, may have given and received children’s clothing with friends and acquaintances, given and received favours from other people.  
> 
> &#x2013; [The gift economy and community exchanges - REconomy : REconomy](http://reconomy.org/what-you-can-do/alternative-banking-and-currencies/the-gift-economy-and-community-exchanges/) 

Community examples:

-   give and take days

B&amp;H refer to blood donation as a gift economy.

> The classic example of this (not necessarily conscious) dynamic is the gift economy of blood donation.
> 
> &#x2013; [[Free, Fair and Alive]]

There's stuff like [[Freegle]] - perhaps more formalised and mediated via the platform, but its essentially giving and receiving without any expectation of payment.  So probably counts as gift economy.

Science:

> Lest we think that the principles of a gift economy will only work for simple, primitive or small enterprises, Hyde points out that the community of scientists follows the rules of a gift economy. The scientists with highest status are not those who possesses the most knowledge; they are the ones who have contributed the most to their fields. A scientist of great knowledge, but only minor contributions is almost pitied – his or her career is seen as a waste of talent.
> 
> -   [The Gift Economy](https://www.context.org/iclib/ic41/pinchotg/)

Where are they not appropriate?

> Life necessities like food, shelter, and water are not good candidates for a gifting system. Which is why we tend to gift luxury items, novelties, and socks.
> 
> &#x2013; [The Gift Economy](https://maggieappleton.com/gift-economy) 

Interesting.  Something doesn't feel right about the claim that market economies are inherently better for life necessities though.


## Further reading / listening

-   [The Gift Economy](https://maggieappleton.com/gift-economy) ([[Maggie Appleton]])
-   [Gift Economy - P2P Foundation](https://wiki.p2pfoundation.net/Gift_Economy)
-   [[The Gift]] (Lewis Hyde)
-   [[Sacred Economics]] (Charles Eisenstein)
-   [The Gift Economy](https://www.context.org/iclib/ic41/pinchotg/)
-   [[Maggie Appleton on Open Source as a Gift Economy]]


## Epistemic status

Type
: [[definition]]

Understanding
: 3

Got a loose general idea on this - not strong on it.  Just done a little bit of research.

