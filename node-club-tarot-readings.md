# Node Club Tarot Readings

My first thought on this is that I'll try and use the nodes drawn to inspire [[prefigurative]] visions of the future.  Little microsnippets of [[speculative fiction]].


## Reading #1

I drew:[[galaksija]] / [[timechain]] / [[2021]] / [[Ask]] / [[Cimbalom Street Musicians]] / [[arcosanti]].

This is my understanding of what the things are:

-   [[galaksija]]. A homebuild computer from yugoslavia in the 1980s.
-   [[timechain]]. "Time is your most valuable currency".  timechain.com looks like some cryptocurrency grift, but time as currency I like.
-   [[2021]]. The year 2021.
-   [[Ask]]. The act of asking something.
-   [[Cimbalom Street Musicians]].  Pretty awesome sounding music played on a hammered dulcimer.  I had never seen such instruments before.
-   [[arcosanti]].  An experimental town related to [[Arcology]].

OK&#x2026; so a rough sketch:

The year is [[2021]], as imagined by a science fiction writer from 1921.

A future [[solarpunk]] vertical city.  An arcology.  Like [[Metropolis]] if it was not-Moloch.  The citizens love cimbalom.  As you walk through the city, it is your soundtrack, like the [[zither]] from [[The Third Man]].

The main currency of the city is [[timebanking]].  (It makes me think a little of Time in [[The Quantum Thief]].)

Everyone has a [[Galaksija]], a [[convivial tool]] that people can freely adapt for their own purposes.

The Galaksija's are networked together in to the Galaksija Brain.  The city is [[participatory]] and [[municipalist]], and city-wide decisions are made by [[Ask]]ing the citizens through the Galaksija Brain.


## Reading #2

I drew: [[ideology requires consensus thinking]] / [[radio equipment wishlist]] / [[Anti-authoritarian networking]] / [[maintain]] / [[ways to run code]] / [[Repairability standard]] 

-   [[ideology requires consensus thinking]].  It is hard to escape hegemony.
-   [[radio equipment wishlist]].  A mostly empty node, but within a few clicks you are in the land of [[amateur radio]].
-   [[Anti-authoritarian networking]]. Designing a network such that it avoids implicit imbalances of authority.  See also [[Authority in networks]].
-   [[maintain]].  The act of maintenance.  I will probably riff on [[Hail the maintainers]] here.
-   [[ways to run code]].  Compilication and interpretation.
-   [[Repairability standard]].  The attempt to produce a standard means of describing how repairable something is.

What may be a thread here?

Maintaining a network of repairable radios in an anti-authoritarian topology.  Perhaps with the distrubution of anti-consensus samizdat software across the airwaves, a la the code broadcasts in [[Socialism's DIY Computer]].  Interpreted by the receiver.

