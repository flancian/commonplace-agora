# The Future of Digital Cooperatives

A webinar by [[Common Wealth]].

Recording here: [The Future of Digital Co-ops: How policy can help scale the sector - YouTube](https://www.youtube.com/watch?v=8ugDaXEMCG0&ab_channel=CommonWealth)


## Misc Notes

Some things needed that would help grow coops, digital ones in particular:

-   Public procurement via coops
-   Policy to support the growth of co-ops
    -   its skewed towards setting up traditional businesses right now
-   Education to promote coop model
    -   you don't learn about coops in business school
-   Political will
    -   someone in power needs to care about and support coops
-   Relationship between coops and unions
    -   a bit strained at times
    -   unions come at the problem from below
    -   politicians come at the problem from above
    -   cooperatives come at the problem from below
-   Federation and internationalisation will help
    -   Resource sharing between coops around the world

