# Building the new web in the shell of the old

-   see: [[Build the new world in the shell of the old]]

People currently say the IndieWeb is too difficult to use for most people - I'd say this is possibly true right now (though it's a lot more nuanced than people make out).

You can use micro.blog, i.haza.website, etc, it's reasonably easy.  And regardless to be part of the indie web you don't need all the advanced bells and whistles of micropub, webmentions, social readers, etc, from day one.  You just need your own space on at your own address.

But anyway.  The point I wanted to capture here is that you've got to start somewhere.  We have to be working on alternatives, even if they're not 100% finished yet.  You need something ready to go for the next time Facebook does a Cambridge Analytica and people are looking for somewhere to go to.

> Designing a dream city is easy; rebuilding a living one takes imagination.
> 
> &#x2013; [[Jane Jacobs]]

<!--quoteend-->

> Phreaks spotted the weakness in their dominant technological infrastructure; within those, they carved out communities, shared knowledge, and indulged in a larger spirit of exploration. We are far less able to conduct ourselves this way today because we’re penned into flat user interfaces and kept distracted while our behaviors are farmed for profit. It’s nearly impossible to develop our own sense of place because we’re not stakeholders, we’re products.
> 
> &#x2013; [[The Prototype for Clubhouse Is 40 Years Old, and It Was Built by Phone Hackers]]
