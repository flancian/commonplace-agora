# Alternative economics

> I've spent more than 20 years as an anti-capitalist organizer. I've seen so many innovative non-capitalist projects fail because they can't sustain themselves on volunteer labour and donations. Cooperatives, social enterprises, and B Corps offer a third alternative to a) struggling along in the margins, or b) selling out to for-profit capitalists.
> 
> https://mastodon.nzoss.nz/@strypey/104815476417325619

