# October (book)

By [[China Miéville]].  A history of the Russian revolution(s).

-   <span class="timestamp-wrapper"><span class="timestamp">[2021-02-13 Sat] </span></span> Enjoying this so far.  Fun to read, pulls you along in the narrative.
-   <span class="timestamp-wrapper"><span class="timestamp">[2021-02-20 Sat] </span></span> About a quarter through. Still enjoying it, but starting to get a bit lost with all the different people involved.  It's quite dense.  Just at the February 1917 revolution at the moment.  Stuff going on between the Duma and the Soviet Executive Committee.
-   <span class="timestamp-wrapper"><span class="timestamp">[2021-02-21 Sun] </span></span> Forgot to mention, but learning about [[The Comma Strike]] from October was fascinating.  It definitely highlights the power of strong unions and strike action.

