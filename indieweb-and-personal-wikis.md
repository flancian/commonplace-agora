# IndieWeb and personal wikis

IndieWeb sometimes feels pretty [[stream]] focused. 

However, [[FedWiki]] came out of one of the early IWCs, so I guess it's always been a thread! 

Plus I'm pretty sure it was through [[Wikity]] somehow that I first heard about IndieWeb. 


## Whys

Lots of the IndieWeb [whys](https://indieweb.org/why) apply.


## Building blocks

Which of the [building blocks](https://indieweb.org/Category:building-blocks) apply to a personal wiki?  i.e. which apply to [[the garden]]?

They definitely apply to the stream for a lot of social stream stuff.  Microformats, webmentions.

I'm not convinced about the need for web actions to copy material from wiki to wiki. I'm doing manual til it hurts here, and it's pretty painless. Though doing a webmention for letting people do back links if they wanted could be interesting.

