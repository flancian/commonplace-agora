# Better Call Saul

I've been really enjoying Better Call Saul lately (early 2020).  I didn't get into it that much when it first aired - maybe it was too soon after having just finished [[Breaking Bad]], and it didn't seem as good.  But with a bit of time having passed, I'm really into it.  

It's cool to get some of the back history filled of the characters from Breaking Bad.  I don't think it's _quite_ as well scripted and shot etc as well as Breaking Bad.  But that's a tough comparison to meet, and it's pretty good.

