# How would you link IndieWeb and Agora?

[[Flancian]] [asked](https://matrix.to/#/!JNfHvclamcmFVqsTug:matrix.org/$qRE08LMhvxkxxfVMWb7NDrrYPvsfXNjYtNjICSK_G3A?via=matrix.org&via=fairydust.space): "does it make sense to think of federation agora <-> indieweb?"

I replied:

That's a really good question

In some sense IndieWeb is just having a website of your own on a domain of your own

So e.g. flancia.org is already IndieWeb

Then you get the webmentions stuff for interacting peer-to-peer between sites

I'd say a digital garden is very much IndieWeb

And I guess I think of Agora on one level as a garden aggregator.

Agora is kind of like for gardens what micro.blog is for streams.

micro.blog uses a lots of the IndieWeb protocols, but makes it a lot easier for non-developers to have one.

Like you literally just set up an account.

