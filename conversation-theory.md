# Conversation theory

[[Gordon Pask]].

> Pask’s work on conversation (for example, Pask, 1976) is wide-​reaching, but fundamentally his argument proposes that it is not simply communication which is synonymous with organisation, but communication understood as conversation.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> It is through interactions between actors in which agreement is reached that systems become organised.
> 
> &#x2013; [[Anarchist Cybernetics]]

<!--quoteend-->

> Pask’s work shows that an interactive, conversational approach to communication and the co-​creating of sensible, meaningful content is consistent with a cybernetic position on self-​organisation.
> 
> &#x2013; [[Anarchist Cybernetics]]

-   [Conversation Theory in One Hour — Presentation](https://pangaro.com/conversation-theory-in-one-hour.html)

