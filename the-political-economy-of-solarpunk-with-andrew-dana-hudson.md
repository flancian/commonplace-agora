# The Political Economy of Solarpunk (with Andrew Dana Hudson)

A
: [[podcast]]

URL
: https://thefirethisti.me/2021/11/17/94-the-political-economy-of-solarpunk-with-andrew-dana-hudson/

Part of
: [[The Fire These Times]]

Featuring
: [[Andrew Dana Hudson]] / [[Joey Ayoub]]

[[Solarpunk]].

-   [[Cyberpunk was about technologies that abstract human relationships with the world]]
-   [[Solarpunk is about technologies that de-abstract human relationships with the material world]]
-   [[Cyberpunk's anxieties are urban decay, corporate power and globalisation]]
-   [[Solarpunk's anxieties are global social injustice, the failures of late capitalism, and the climate crisis]]

