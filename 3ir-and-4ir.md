# 3IR and 4IR

The third and fourth industrial revolutions.  Digital and cyber-physical advancements.  Will they be for good or bad?

I don't like the name themselves, to be honest.  Revolutions of industry is not what we need.  We need revolutions of society.  That said, the technologies themselves could be liberatory if not simply used for capital accumulation.

> they are rapidly changing civilization, for better or worse depending on one's position, and making dramatic new orientation to work and labor possible.
> 
> &#x2013; [[Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi]]


## 3IR

> The Third Industrial Revolution (3IR), also known as the digital revolution, started in the 1960s, but exploded in the late 1980s and 1990s, and is still expanding today.  This revolution refers to the advancement of technology from analog electronic and mechanical devices to the digital technologies we have now.  The main technologies of this revolution include the personal computer, the internet, and advanced information and communications technologies, like our cell phones.
> 
> &#x2013; [[Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi]]


## 4IR

The genesis of the term 4IR is from Klaus Schwab of the World Economic Forum.  So pretty mainstream.  

> The Fourth Industrial Revolution (4IR), also known as the Cyber-Physical Revolution, is marked by technological and knowledge breakthroughs that build on the digital Revolution that are now fusing the physical, digital and biological worlds (including the human body).  The main technologies of this revolution include advanced robotoics, CNC (computer numeric control), automation, 3D printing, biotechnology, nanotechnology, big data processing, artificial intelligence, and autonomous vehicles.
> 
> &#x2013; [[Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi]]


## Good or bad?

Can they be liberatory?

> Their value and intent will be determined by humanity.  They will either aid humanity in our collective quest for liberation, or they will help further our species' inhumanity towards itself and mother earth.  
> 
> &#x2013; [[Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi]]

We need to make sure they are not just more tools of capital accumulation and subjugation.

> One thing is painfully clear, and that is if these technologies remain the exclusive property of the capitalist class and the transnational corportations they control, these technologies will not be used for the benefit of the majority of humanity, but to expand the methods of capital accumulation and further consolidate the power of the 1% that rule the world.
> 
> &#x2013; [[Jackson Rising: The Struggle for Economic Democracy and Black Self-Determination in Jackson, Mississippi]]

McKenzie Wark in [[Capital is Dead]] namechecks the 4IR as an example of the idea of capitalism disrupting itself.  (From Wark's perspective, as an example of tech trying to say they are doing capitalism but better.)

