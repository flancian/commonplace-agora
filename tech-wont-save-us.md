# Tech Won't Save Us

-   https://techwontsave.us/

An excellent [[podcast]] that gives a critical eye on the tech industry.

Hosted by Paris Marx.

