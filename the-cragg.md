# The Cragg

A recent discovery for me near my new home town is The Cragg - a mini-peak nestled in the outskirts of the [[Forest of Bowland]].

It's a short (hilly!) bike ride from my home in [[Lancaster]].

Right now, the journey there has a lot of lapwings in the fields, with their distinctive calls, and fields full of sheep, lambs, and cows.

From The Cragg you get a close up view over towards a small hilltop windfarm of eight turbines.  I love to watch them.

![[photos/the-cragg-turbines.jpg]]

And it has absolutely stunning vistas - you can see the Yorkshire Dales, the [[Lake District]], and [[Morecambe Bay]].  You're accompanied pretty much all of the way by [[Clougha Pike]].

I feel very lucky to have it so close by.


## The Cragg towards Bowland in December

![[photos/the-cragg-towards-bowland.jpg]]

