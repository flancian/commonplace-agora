# Epistemology vs ontology

[[Epistemology]]. [[Ontology]].

Discussed in the context of [[cybernetics]] in [[General Intellect Unit - The Cybernetic Brain, Part 1: Ontological Theatre]] in reference to [[The Cybernetic Brain]].  There, [[Ontology]] is much preferred over [[Epistemology]].

I might be reaching / misunderstanding a bit here, but [[agent-based modelling]] feels a bit like ontology, and mathematical modelling a bit like epistemology.

