# Value

> The predominant explanation of value in mainstream economics today is the neoclassical theory of “[[marginalism]].” It assumes that a commodity’s worth is determined by laws of [[supply and demand]], mediated by what they term “marginal utility"
> 
> &#x2013; [[A People's Guide to Capitalism]]

[[Labour theory of value]].

> One type of value does not cause the other (how useful an item is does not determine its value on the market; nor does its value on the market determine how useful an item may be to us
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> In fact, it’s precisely the fact that an item has no use-value to its producer that allows it to be an exchange-value
> 
> &#x2013; [[A People's Guide to Capitalism]]

<!--quoteend-->

> Simply put, you can either live in your house, or you can sell your house. You can’t have your cake and eat it too.
> 
> &#x2013; [[A People's Guide to Capitalism]]

^ what about non-rivalrous goods?

