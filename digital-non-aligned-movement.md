# digital non-aligned movement

> Echoing the Cold War-era [[Non-Aligned Movement]] among countries caught between the United States and the Soviet Union, some have been calling for a “digital non-aligned movement” that asserts many diverse sovereignties against the duelling forces of Silicon Valley and Shenzhen (Freuler 2020; Mejias 2020)
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

