# Two Loop Model

A [[theory of change]].

![[images/two-loop-model.png]]

-   [Two Loop Model](https://www.systemsinnovation.io/post/two-loop-model)
-   [Hospicing The Old](https://medium.com/thefarewellfund/hospicing-the-old-16e537396c4b)
-   [Pioneering a New Paradigm - Berkana Institute](https://berkana.org/resources/pioneering-a-new-paradigm/)
-   [How I Became a Localist | Deborah Frieze | TEDxJamaicaPlain - YouTube](https://www.youtube.com/watch?v=2jTdZSPBRRE)

