# Bashar al-Assad's decade of destruction in Syria

URL
: https://www.theguardian.com/news/audio/2021/jun/02/bashar-al-assads-decade-of-destruction-in-syria-podcast

Publisher
: [[The Guardian]]

[[Syria]].  [[Bashar al-Assad]].

> as Assad lost control of vast areas of Syria following the uprising, he was at maximum vulnerability. But the intervention of Iran and Russia proved decisive. Now, after a widely discredited election victory with 95% of the vote, Assad begins his fourth term as president of a country deeply scarred by war. 

