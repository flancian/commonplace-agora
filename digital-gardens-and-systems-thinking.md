# Digital gardens and systems thinking

A discussion on [[systems thinking]] on the Digital Gardeners telegram group led me to think: "do [[digital garden]]s foster systems thinking?"

I personally think so.  They have a focus on interconnectedness of notes, synthesis, and emergence of new ideas.

[[Chris Aldrich]] linked to [[Ross Ashby]]'s commonplace book, and posited that Ashby's system would have influenced his own systems thinking.

If the synthesis side of systems thinking is more about the relationships between nodes, then perhaps our digital tools need space for more advanced types of linking?  See e.g. https://org-roam.discourse.group/t/add-link-tags-feature/171

