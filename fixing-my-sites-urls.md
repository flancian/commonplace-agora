# Fixing my sites URLs

Making my site URLs more sensible, I thought I could just do what I do for ox-agora and intercept and slugify the title as the filename during export.

-   But then I've realised that links still point to the non-slugified filename.
-   I could do what I also do in ox-agora, and rewrite the links during publish.
-   Or maybe I should just fix up all the filenames themselves once and for all.

Even if I don't want the timestamp prefixes locally, either, it strikes me that trying to make sure my filesystem filenames are always also suitable as URLs might be a fool's errand, and that writing something that gives me control at publish time might well be the best solution.

