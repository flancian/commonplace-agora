# My garden circles

For various topics, people that I have some existing mental model of, that I would look in their garden for some useful thoughts.  Not necessary that I agree with.  Being in one of these lists assumes the person has a fairly easily searchable [[digital garden]] somewhere, not just a social media stream.

See [[How I take notes]]. [[Garden circle]].

[MaggieAppleton/digital-gardeners](https://github.com/MaggieAppleton/digital-gardeners) is probably a useful list to look at for new gardens to peruse on topics. And https://tiny.cc/digital-gardeners


## Knowledge management

-   [[Ton]] (https://www.zylstra.org/blog/wiki-frontpage/)
-   [[Phil Jones]]
-   [[Bill Seitz]]
-   [[Maggie Appleton]]
-   [[Flancian]]
-   [[Andy Matuschak]]
-   [[Gordon Brander]]


## Decentralised technology

-   [[IndieWeb]] Wiki
-   [[Free, Fair and Alive]] (a book, but with a well defined pattern language)
-   [[P2P Foundation Wiki]]


## Commoning / political organisation

-   [[Free, Fair and Alive]] (a book, but with a well defined pattern language)
-   [[patternlanguage.commoning.wiki]] (wikification of FFA)
-   [[Mike Hales]]
-   [[Lean Logic]] (newly discovered as of Dec 2021, still don't have good mental model but seems interesting)
-   [[P2P Foundation Wiki]]


## Politics, governance

-   [[Nathan Schneider]] - not quite a garden, but easy enough to search and huge respect for Nathan's work https://nathanschneider.info/open-work/


## Revolution

-   [[Flancian]]


## Theology

-   [[Maya]]


## Coding

-   [[Phil Jones]]
-   [[vera]]
-   [[Flancian]]

