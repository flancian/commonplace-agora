# Maria Spiridonova

> I would like very much to single out is the gallant leader of the Left Socialist Revolutionary Party, Maria Spiridonova, whose supporters were virtually alone in proposing a workable revolutionary program for the Russian people in 1917–18. Their failure to implement their political insights and replace the [[Bolsheviks]] (with whom they initially joined in forming the first Soviet government) not only led to their defeat but contributed to the disastrous failure of revolutionary movements in the century that followed.
> 
> [[The Communalist Project]]

