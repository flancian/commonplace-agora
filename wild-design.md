# Wild Design

-   https://monaidepaula.com/Wild-Design-1

> Wild Design is a framework evolving from the insights brought by the brazilian practice of gambiarra but expanding into any design practice in which human agency is not formally engineering processes, but dynamically developing into as well as together with systems. 

![[2021-10-16_14-28-37_screenshot.png]]

