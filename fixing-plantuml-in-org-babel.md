# Fixing PlantUML in org-babel

-   "Exception in thread "main" java.awt.AWTError: Can't connect to X11 window server using ':1' as the value of the DISPLAY variable."

-   Seems to be because it isn't running headless.  Which, from `plantuml-java-args`, it looks like it should be.  However, when running via babel, it appears those args aren't used?

-   I made a fix by editing `\~/.emacs.d/elpa/26.3/develop/org-plus-contrib-20201116/ob-plantuml.el` and adding in "-Djava.awt.headless=true" to the args passed to the jar.

Changing:

```emacs-lisp
(t (list java
	 "-jar"
	 (shell-quote-argument (expand-file-name org-plantuml-jar-path))))))
```

to 

```emacs-lisp
(t (list java
	 "-jar -Djava.awt.headless=true"
	 (shell-quote-argument (expand-file-name org-plantuml-jar-path))))))
```

-   But this is temporary - it will be overwritten when org-plus-contrib package is updated.

-   And how come it was working a couple of weeks ago - what changed?

