# Uber



## <span class="timestamp-wrapper"><span class="timestamp">[2022-02-19 Sat]  </span></span>  [Uber drivers entitled to workers' rights, UK supreme court rules | Technology&#x2026;](https://www.theguardian.com/technology/2021/feb/19/uber-drivers-workers-uk-supreme-court-rules-rights)


### From [[TWSU]] chat:

> Apparently Uber are trying to spin this as only applicable to the plaintiffs? Ridiculous that this decision could be handed down and yet it will be left in the hands of regulators to actually crack down on Uber’s behaviour

<!--quoteend-->

> Uber won’t accept anything until forced. It will be fascinating to see how this all plays out. It could have implications far beyond Uber.

<!--quoteend-->

> My understanding is that it opens an arena for further confrontation, but that arena is in territory Uber wishes it weren't. It will require more struggle on the side of drivers to realise gains from this, but it's a big win in enabling them to advance that strugge.

