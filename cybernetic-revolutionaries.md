# Cybernetic Revolutionaries

Author
: [[Eden Medina]]

about [[Project Cybersyn]]

Page 40 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 03:56]</span></span>:
technologies are the product not only of technical work but also of social negotiations

Page 41 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 03:57]</span></span>:
technologies are not value-neutral but rather are a product of the historical contexts in which they are made

Page 46 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:02]</span></span>:
Technologies are historical texts. When we read them, we are able to read history

Page 57 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:19]</span></span>:
this connection between cybernetics and Chilean socialism came about, in part, because Beer and Popular Unity, as Allende’s governing coalition was called, were exploring similar concepts, albeit in the different domains of science and politics

Page 58 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:20]</span></span>:
political innovation can spur technological innovation.
 
Page 59 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:22]</span></span>:
technology can shape the path of political history by making certain actions possible

Page 60 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:23]</span></span>:
historical readings of technology can make visible the complexities internal to a political project

Page 61 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:24]</span></span>:
Chile was not able to implement its political dream of democratic socialism or its technological dream of real-time economic management

Page 63 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:26]</span></span>:
The more I reflect on these facts, the more I perceive that the evolutionary approach to adaptation in social systems simply will not work any more. . . . It has therefore become clear to me over the years that I am advocating revolution

Page 67 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:30]</span></span>:
major theme in Beer’s writings was finding a balance between centralized and decentralized control, and in particular how to ensure the stability of the entire firm without sacrificing the autonomy of its component parts.
 
Page 70 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:34]</span></span>:
how do you create a system that can maintain its organizational stability while facilitating dramatic change, and how do you safeguard the cohesion of the whole without sacrificing the autonomy of its parts

Page 78 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:41]</span></span>:
brought together ideas from across the disciplines—mathematics, engineering, and neurophysiology, among others—and applied them toward understanding the behavior of mechanical, biological, and social systems.23

Page 78 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-07 Fri 04:41]</span></span>:
for Beer, cybernetics became the “science of effective organization
\#+end<sub>quote</sub>

Page 90 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-09 Sun 08:40]</span></span>:
Keller instead argues that the cybersciences also emerged as a way to embrace complexity and “in response to the increasing impracticality of conventional power regimes

Page 92 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-09 Sun 08:43]</span></span>:
He embraced complexity, emphasized holism, and did not try to describe the complex systems he studied, biological or social, in their entirety

Page 92 <span class="timestamp-wrapper"><span class="timestamp">[2021-05-09 Sun 08:43]</span></span>:
To put it another way, Beer was more interested in studying how systems behaved in the real world than in creating exact representations of how they functioned

-   [[There are different strands of cybernetics]]

