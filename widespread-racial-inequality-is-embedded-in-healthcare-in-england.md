# Widespread racial inequality is embedded in healthcare in England

Widespread [[racial inequality]] is embedded in [[healthcare in England]]

https://www.theguardian.com/society/2022/feb/13/radical-action-needed-to-tackle-racial-health-inequality-in-nhs-says-damning-report

> “ **Ethnic inequalities in health outcomes are evident at every stage throughout the life course, from birth to death**,” says the review, the largest of its kind. Yet despite “clear”, “convincing” and “persistent” evidence that ethnic minorities are being failed, and repeated pledges of action, no “significant change” has yet been made in the NHS, it adds.
> 
> &#x2013; [Radical action needed to tackle racial health inequality in NHS, says damning&#x2026;](https://www.theguardian.com/society/2022/feb/13/radical-action-needed-to-tackle-racial-health-inequality-in-nhs-says-damning-report)


## Epistemic status

Type
: [[claim]]

Agreement level
: [[Most likely]]

