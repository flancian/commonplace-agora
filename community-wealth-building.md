# Community wealth building



## Five principles

-   Plural ownership of the economy.
-   Making financial power work for local places.
-   Fair employment and just labour markets.
-   Progressive procurement of goods and services.
-   Socially productive use of land and property.


## Challenges

> The problem is that although this offers a solution emphasising growth, and the potential for a more democratic model of ownership to emerge, there can be strong tendencies towards simple local protectionism. Leadership candidates speaking about [[localism]] must ensure it’s more than a simply a listening exercise and they should be looking forward for their solutions, not into the past.
> 
> &#x2013; [[Can municipal socialism be Labour’s defence against ongoing austerity?]]


## Articles

-   [The principles of community wealth building | CLES](https://cles.org.uk/what-is-community-wealth-building/the-principles-of-community-wealth-building/)

