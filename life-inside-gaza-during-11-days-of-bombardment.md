# Life inside Gaza during 11 days of bombardment

URL
: https://www.theguardian.com/news/audio/2021/may/21/life-inside-gaza

Publisher
: [[The Guardian]]

Discusses the impact bombardment has had within [[Gaza]].

Also [[Fatah]], [[Hamas]], [[Palestinian Liberation Organization]], and the [[Palestinian Authority]].

