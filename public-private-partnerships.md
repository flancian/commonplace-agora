# public-private partnerships

Contrast with [[commons-public partnerships]].

> PPPs are typically vehicles for developing infrastructure for water supply or sewage management, or building roads, bridges, schools, hospitals, prisons, or public facilities such as swimming pools and playing fields.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> These are often good-faith attempts to address pressing social problems through contract-based collaborations between businesses and government.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> PPPs are based on fundamentally incompatible objectives — the state’s obligation to protect the public good and private businesses’ desire to maximize profits. In practice, many public-private collaborations function less as partnerships than as disguised giveaways.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> PPP can let a company acquire equity ownership of public infrastructure such as roads, bridges, and public facilities for a long period — fifteen, thirty, even ninety-nine years — and then manage them as a private market asset.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The state and the corporate sector both pretend that PPPs are a healthy, wholesome arrangement that benefits everyone and solves the lack of public funds. In truth, a great many PPPs amount to a marketization of the public sector that extracts more money from citizens, surrenders taxpayer assets to businesses, and neutralizes public accountability and control.
> 
> &#x2013; [[Free, Fair and Alive]]

