# Written history of hypertext

Reading through the sites of longtimers of the web, like [[Ton]]'s site, and more recently [[Phil Jones]]' and [[Bill Seitz]]' wikis, and what they link to, makes you realise that there's such a ton of written history out there.  It's a real treasure trove.  And that it's important that it has been recorded out on the open web, not in some silo that might have vanished and the history lost.

Also makes me realise that much of what I'm thinking about has been thought about before!  There's some great resources out there.  Phil calls it a [[renewal]] or a reboot.  It's good to have history to read through.

