# Socially necessary labour-time

> The socially necessary labor-time is the average amount of time that it takes to produce a commodity “under the conditions of production normal for a given society and with the average degree of skill and intensity of labor prevalent in that society.” That is to say: what is the average time, using the common tools and technology within society, that it will take to construct a table? If the average is two hours—then this determines the table’s exchange-value
> 
> &#x2013; [[A People's Guide to Capitalism]]

