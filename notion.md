# notion

A conceptual note.  A term from [[Ton]]. Similar to/same as [[Evergreen notes]], I think, but I prefer 'notion' as a memorable term.

Notes become notions.

> Notions are conceptual notes taken from my own work and experience mostly, and give my own perspective on these concepts. [&#x2026;] As they are more conceptual than factual I started calling them Notions to distinguish them from the other more general resource Notes.
> 
> &#x2013; [100 Days in Obsidian Pt 4: Writing Notes – Interdependent Thoughts](https://www.zylstra.org/blog/2020/11/100-days-in-obsidian-pt-4-writing-notes/)

