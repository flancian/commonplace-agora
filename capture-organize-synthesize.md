# Capture-organize-synthesize

Some golden stuff here on how existing tools get these wrong - [[Unconscious R&D]]

> -   Why do file systems force you to name a file (synthesize), and place it in a folder (organize) before you can write in it (capture)?
> 
> -   Why do Word Processors present you with a blank page (synthesize) instead of offering scratch notes (capture) that are relevant (organize) to your writing goals?
> 
> -   Why do we expect ourselves to create good ideas from nothing (synthesize)? It’s much easier to generate ideas when you have lots of material (capture) clustered by themes and relationships (organize)1.
> 
> &#x2013; [[Unconscious R&D]]

<!--quoteend-->

> Mixing up the order of capture-organize-synthesize causes friction. It forces us to make decisions before we’re ready. This friction manifests as blank page anxiety, creative block, lost ideas. That’s the feeling of the system fighting you. Most of us try to overcome it through sheer force of will.
> 
> &#x2013; [[Unconscious R&D]]

