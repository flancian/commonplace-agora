# Repair Acts

-   https://repairacts.net/
    
    > Repair Acts is an international and multidisciplinary network of people working on topics relating to repair, care and maintenance cultures.
    > 
    > Broadly these cultures focus on applied, artistic, scholarly and civic practices, which deal with the care, upkeep, maintenance and reuse of objects, materials, buildings, systems and processes.
    > 
    > We view repair, care and maintenance as the challenge to move away from the rhetorics of the ‘new’ as a means of progression and innovation.

