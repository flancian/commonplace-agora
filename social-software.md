# social software

It's a phrase I learned from [[Ton]].

> Social software in that light is the first batch of tools that really use the new affordances internet and mobile tech give us (easy sharing, easy group forming and networking, low threshold participation, largely time and place independent) by employing human relationships as navigational structure. 
> 
> &#x2013; [Social Software @ Work – Interdependent Thoughts](https://www.zylstra.org/blog/2009/10/social_software_1/) 

<!--quoteend-->

> To me blogs and wikis are the original social software.
> 
> &#x2013; [The Blog and Wiki Combo – Interdependent Thoughts](https://www.zylstra.org/blog/2019/06/the-blog-and-wiki-combo/) 

