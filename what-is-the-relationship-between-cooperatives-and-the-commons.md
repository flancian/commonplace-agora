# What is the relationship between cooperatives and the commons?

They are both related to self-ownership and self-governance.

[[Cooperatives]] are more structure, [[commons]] is more culture?

Coops need a culture of commoning.  [[Coops can turn in to bad actors if there is no culture of commoning]].

Coops can be stewards of the commons.

> You might wonder why cooperatives are often cited as examples of the commons when in fact many of them seem to produce for and sustain themselves entirely from the market.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> The reason is that many cooperatives have not found cultural means to Keep Commons & Commerce Distinct.
> 
> &#x2013; [[Free, Fair and Alive]]


## Resources

-   [[Nathan Schneider - Cooperatives, the Commons and Ownership]]

