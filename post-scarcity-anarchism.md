# Post-Scarcity Anarchism

> His 1968 essay “Post-Scarcity Anarchism” reformulated anarchist theory for a new era, providing a coherent framework for the reorganization of society along ecological-anarchistic lines
> 
> &#x2013; [[The Next Revolution]]

