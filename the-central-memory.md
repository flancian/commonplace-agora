# The Central Memory

Thoroughly enjoying [The Central Memory](https://www.cyberneticforests.com/music/) by The Organizing Committee - quite a work of art.  AI-assisted 'Cyborg Pop' with references to [[Allende]], [[Project Cybersyn]], [[Stafford Beer]], [[Deleuze and Guattari]], [[Situationism]], and it all sounds a bit like a Stereolab album.  (Found [via](https://www.kickscondor.com/the-organizing-committee/) [[Kicks Condor]])

