# Nodes or patterns

In the Agora.

> -   ward said a pattern was a solution to a problem. it usually states what the problem is and how to fix it
> -   I like the word pattern because it is non technical and approachable. I really try to infuse tech culture with accessible ideas because STEM is so inherently elitist

<!--quoteend-->

> -   I really like pattern as a proposed solution to a problem; I like the pragmatism.
> -   but in that case not every node is a pattern; not all nodes are about solutions.
> -   so currently I'd vote for keeping node club as is, and perhaps also going back to wikilinks everywhere as the name for the web extension repo and associated integration project.
> -   that way we keep pattern for something closer to actionable, and align with Ward.
> -   wdyt?

<!--quoteend-->

> -   I do like pattern though even if it might not fit here.
> -   I recall from [[Design Patterns]] and [[Patterns of Enterprise Application Architecture]] back in the day that each pattern is presented as just one possible solution to a problem, and whether it is the 'right' one or not is very much dependent on context.
> -   Philosophically I very much like this fact that they are regarded as subjective / contextual. Because I believe most things in life are.
> -   I would actually say node `= 'problem', subnode =` pattern.  A subnode is just one subjective take on a thing, one possible 'answer' to a node's 'question', dependent on context.
> -   I think this aligns with anagora being a chorus of voices, each one subjective and contextual, and not an attempt at a definitive definition of anything.
> -   but anyway just waffling, node is good

