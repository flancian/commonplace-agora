# Lenovo Thinkpad T450s

This is actually my first Thinkpad.  I got it second hand.  I really like the build of it.  The keyboard is really nice.  The display is good. The trackpoint thing is pretty handy when working on a train too.
I like the fact is has a dedicated button for mic on/off.


## Issues I've had


### Not powering on

Completely out of the blue this morning, I couldn’t power it on. No signs of life at all - no LEDs lighting up, no POST or BIOS, nothing. It was fine the evening before and definitely had some charge in the battery. The power supply was fine too, but same problem when connected to mains - no signs of life from the laptop.

It seems to be a reasonably common issue and wasn’t too tricky to fix (in the short-term at least…). According to [this YouTube video](https://www.youtube.com/watch?v=hF2tviRT_kY) this is Thinkpads’ short circuit protection mode. I don’t know what caused the short. A couple of [reports on the Lenovo forum](https://forums.lenovo.com/t5/ThinkPad-T400-T500-and-newer-T/T450s-won-t-power-on-No-lights/m-p/3451399#M112407) say it happens to them every time they close the lid. (In the video he says a short could be caused a power surge, metal touching inside one of the ports, or… a Windows update?? Not sure about that last one.)

From the video, first thing to try is a paperclip for 15 seconds in what [the manual](https://download.lenovo.com/pccbbs/mobiles_pdf/t450s_ug_en.pdf) calls the emergency-reset hole… no joy from that. The second is to open up the laptop and disconnect the internal battery for 15 seconds, then reconnect. That worked. Lenovo themselves have a [pretty handy detailed video](https://support.lenovo.com/us/en/videos/vid100569) for getting to and disconnecting the battery, along with the manual they provide it is very helpful.

I’d like to determine the source of the short, if it reoccurs it could hint at a problem on the board.


#### <span class="timestamp-wrapper"><span class="timestamp">[2021-07-27 Tue]</span></span>

This has been happening much more frequently of late.  Discussed here: https://talk.restarters.net/t/thinkpad-450s-frequently-goes-into-shortcircuit-mode/5317

Perhaps a problem with the battery when under a high drain?

Probably getting a new battery/batteries is the way to go.  They're not cheap.


### Trackpad stopping working on Linux Mint

Occassionally some of the touchpad gestures stop working in Linux Mint. (I think it’s after closing the lid and reopening.)

The following gets it going again:

```bash
sudo modprobe -r psmouse
sudo modprobe psmouse
```

Basically turning the driver off and on again. 


## Spare parts


### Batteries

Not got one yet, but perhaps GreenCell is a good bet? https://greencell.global/en/search?controller=search&orderby=position&orderway=desc&search_query=lenovo+t450s

