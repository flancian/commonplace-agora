# PALACES FOR THE PEOPLE: the future of public libraries

A
: [[podcast]]

URL
: https://www.cheerfulpodcast.com/rtbc-episodes/palaces-for-the-people-future-libraries

Series
: [[Reasons to be Cheerful]]

Featuring
: [[Eric Klinenberg]]

"[[libraries]] help build [[social solidarity]] and should be a priority for government investment"

[[Social infrastructure]]

