# Exiting the Vampire Castle

URL
: https://www.opendemocracy.net/en/opendemocracyuk/exiting-vampire-castle/

Author
: [[Mark Fisher]]

[[Class privilege]].  [[Class analysis]].  [[intersectionality]].

> We need to learn, or re-learn, how to build comradeship and solidarity instead of doing capital’s work for it by condemning and abusing each other. This doesn’t mean, of course, that we must always agree – on the contrary, we must create conditions where disagreement can take place without fear of exclusion and excommunication.


## Responses

-   [All hail the vampire-archy: what Mark Fisher gets wrong in 'Exiting the vampi&#x2026;](https://www.opendemocracy.net/en/opendemocracyuk/all-hail-vampire-archy-what-mark-fisher-gets-wrong-in-exiting-vampire-castle/)

