# Kate Raworth on Why Our Times Demand 'Doughnut Economics'

URL
: https://david-bollier.simplecast.com/episodes/kate-raworth-why-our-times-demand-doughnut-economics

Publisher
: [[Frontiers of Commoning]]

Featuring
: [[Kate Raworth]] / [[David Bollier]]

This is a lovely interview.  Raworth is full of energy and very inspiring.  I never finished the book yet (a reflection on me, not the book&#x2026;) but Doughnut Economics sounds very [[Commons]] friendly.

I like the breakdown of economy into [[market]], [[state]], [[care]] and [[commons]]. 

Touches a bit on the municipal / city approach.

[[Doughnut Economics]] is simply a great visual representation of a goal. What then are the economics to meet that goal?

