# Deleuze and Guattari and complex systems

> Deleuze’s philosophy appears to fit neatly with interpretations of the world as complex systems, especially in ecology (Robinson and Tansey, 2006), in urban planning (Batty, 1969, 1994, 2005) and in economic spheres (Noell, 2007).
> 
> &#x2013; [[How Could we Study Climate-Related Social Innovation? Applying Deleuzean Philosophy to the Transition Towns]]

<!--quoteend-->

> Berressem points out the links between Deleuze and Guattari’s ecological or ecosophical thought and the development of complexity theories 11 , with particular reference to issues of adaptability and transformation. As Protevi (1999: 3) describes, ‘societies have thresholds at which they adopt or change behavioural traits’.
> 
> &#x2013; [[How Could we Study Climate-Related Social Innovation? Applying Deleuzean Philosophy to the Transition Towns]]

[[Deleuze and Guattari]].   
[[complex systems]].

