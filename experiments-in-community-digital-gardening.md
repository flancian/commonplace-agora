# Experiments in community (digital) gardening

Currently an [[emergent outline]].


## Notes

-   [[node club]]
-   [[A Platform Designed for Collaboration: Federated Wiki]]
-   [[Interlinking wikis]]
-   [[Blogchains and hyperconversations]]
-   Notes vs notions
-   [[Ubuntu Rationality]]
-   [[Nested-I]]
-   [[rhizome]]


## Notions

-   Digital gardens are better when combined
-   Combining digital gardens allows new ideas to emerge
-   Community digital gardening is a way to build a knowledge commons
-   Community digital gardening is a way to build communities of practice
-   [[Combining multiple personal wikis provisions a better commons than one central wiki]]

Communal digital gardening or community digital gardening?


## Rough

Over in Anagora, for the last few weeks we've been undertaking a regular '[[node club]]'.  Here I'll talk about what it is, why we do it, and why you should get in on it.


### What

What is node club?  Well it's kind of like a book club, except in node club rather than specifically books, we gather around 'nodes of the week' - semi-arbitrary topics in our collective digital garden that we decide together to focus on. A node can be on anything, really - it could be about a poem, a film, a book, a concept, a question. Each week, we pick a node, and each person makes notes and writes a little bit about that node in Anagora.  

So far it maybe all sounds a bit like a regular Wikipedia edit-a-thon.  But Anagora is not Wikipedia - it is a garden of gardens.  There is no central canonical page that we're all editing together, no 'final copy' where we're trying to wrangle out a definitive, objective description of something.  In node club we each pen a personal spin on things in our own self-owned space, and the Agora glues these sub-nodes together in to one page where you can see a chorus of voices together.


### Why


#### Why have a node club?

So why have a node club?  A few reasons.  Most simply, it's a nice way of building community.  You discuss a topic with a group of friends, you learn together.  It's a way of having a [[hyperconversation]] - somewhere where you're not just throwing your thoughts out and everyone else is relegated to "the comments".  We're all equal partners in this.

Beside that, I see it also as a means of discovery - a bit of a collective Situationist drift, a communal derive of cyberflaneurs wandering through the gardens (and  cultivating parts of them as we pass through).  We decide on a node of the week in a 'stoa' in Anagora, and a great way of harvesting the candidates from one week to the next is to look at the forward links from last week - what did we link to?  Is that where we should go next?  We decide where to amble to next, not in hock to the rage engines and the currently trending hashtags.

That said - beyond the drift, I think another purpose of communal gardening could also in fact be more directed and goal-oriented - a way for communities of practice to build a knowledge commons on their topics of interest.  The chorus of voices writing a song rather than jamming.  A group of hyphapunks noding together.  The drift is notes, the praxis is notions.


#### And from the perspective of why distributed

Anagora is [[a garden of gardens]].  Digital gardens are, in part, personal wikis, and by that token inherently individual.  But when taken in combination, they can form a knowledge commons.

And for sure, you could do all this in a central wiki like Mediawiki.  Wikipedia is truly remarkable, but it depends on objectivity and the removal of voice.  For many things, this isn't the way to go, and [[Combining multiple personal wikis provisions a better commons than one central wiki]].

> This shift from the standard wiki to a form of writing based on “one person, one wiki, in a federated environment,” may sound like a step backward from the Wikipedia style of open collaboration. But in fact the effect is quite the opposite: giving online platforms to individual voices while bringing them together into a shared neighborhood of wikis results in a richer, more robust commons.
> 
> -   [[Free, Fair and Alive]]


## Agora-independent

It's worth reiterating that doing this in Anagora is just one way of doing it.  FedWiki is the pedigree for the chorus of voices.  What I like about the Agora is that all you need is your notes in Markdown files in a git repository, and the Agora will do the rest in combining them together.

See also: [[Interlinking wikis]].

