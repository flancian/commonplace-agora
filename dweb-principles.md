# DWeb principles

-   https://getdweb.net/principles/

-   Technology for human agency
-   Distributed benefits
-   Mutual respect
-   Humanity
-   Ecological awareness

