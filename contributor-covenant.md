# Contributor Covenant

URL
: https://www.contributor-covenant.org/

> Some open source projects attract enough contributors that a community forms. A healthy open source community centers the shared values and norms of its members. While not all of these values are the exactly the same from community to community, there is a set of core values and norms that are essential in a just and equitable software commons.
> 
> Contributor Covenant is a code of conduct that you can adapt to express both these fundamental shared values, and the special norms and values that distinguish your own community.

