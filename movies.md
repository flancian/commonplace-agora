# Movies

<div class="abstract">
  <div></div>

Some films I've liked (or at least watched) over the years, in no particular order.

(Probably best to assume there will be some spoiler alerts&#x2026;)


</div>


# Table of Contents

-   [Children of Men](#orgb109b0e)
-   [Knives Out](#org0c1eb96)
-   [Brick](#org14b3c83)
-   [Garden of Words](#org2e9d781)
-   [Tron Legacy](#orga186522)
-   [Miller's Crossing](#orgec96a0c)
-   [Tron](#org33828e0)
-   [Pan's Labyrinth](#org64b5d6c)
-   [Monty Python and the Holy Grail](#org9d868b0)
-   [Eternal Sunshine of the Spotless Mind](#org82aefbd)
-   [The Matrix](#org6e931c5)
-   [Mad Max: Fury Road](#org7c6c24c)
-   [Back to the Future](#org258d804)
-   [Raiders of the Lost Ark](#orgd272fda)
-   [Gattaca](#org093dc54)
-   [Her](#org902167d)
-   [You Were Never Really Here](#org939c18b)
-   [Blade Runner](#org8d276e8)
-   [Adam Curtis stuff](#org8a7e3b4)
-   [The Speed Cubers](#orgad06607)
-   [To watch](#orge6166ad)


<a id="orgb109b0e"></a>

## [[Children of Men]]


<a id="org0c1eb96"></a>

## Knives Out

About 2 hours long, but you wouldn't notice - just non-stop enjoyment.  A modern spin on the murder mystery genre.  Great ensemble performance, Daniel Craig remarkably enjoyable as a Southern US sleuth.


<a id="org14b3c83"></a>

## Brick

A kind of Shakespeare-cum-neo-noir-cum-high-school-drama. Rian Johnson's first film.  A long time since I watched it, must rewatch after seeing Knives Out (Johnson's latest), I remember loving it at the time.  


<a id="org2e9d781"></a>

## Garden of Words

A short and interesting anime about two strangers who meet in a park and forge a connection despite many barriers.  They have a profound effect on each others' lives as they get to know each other.

Gentle and philosophical on life.  It looks gorgeous, with some beautiful backdrop illustration.  Makes you think about the nature of human connection.


<a id="orga186522"></a>

## [[Tron Legacy]]


<a id="orgec96a0c"></a>

## Miller's Crossing


<a id="org33828e0"></a>

## Tron


<a id="org64b5d6c"></a>

## [[Pan's Labyrinth]]


<a id="org9d868b0"></a>

## Monty Python and the Holy Grail


<a id="org82aefbd"></a>

## Eternal Sunshine of the Spotless Mind


<a id="org6e931c5"></a>

## The Matrix


<a id="org7c6c24c"></a>

## Mad Max: Fury Road


<a id="org258d804"></a>

## Back to the Future


<a id="orgd272fda"></a>

## Raiders of the Lost Ark


<a id="org093dc54"></a>

## Gattaca


<a id="org902167d"></a>

## Her


<a id="org939c18b"></a>

## You Were Never Really Here

Brutal film.  A bit of a modern day Taxi Driver.  Someone losing it in a bad world.

Joaquim Phoenix is superb.  

It's amazingly directed.  Really minimal but effective.  The soundtrack by Jonny Greenwood is perfect for the film.

The plot is very grim.  Maybe also a bit far fetched.  But based on a book by Jonathon Ames.  The film is impressive in the amount of stuff that it leaves out.  It alludes to a lot of stuff that is probably much more fleshed out in the book.


<a id="org8d276e8"></a>

## [[Blade Runner]]


<a id="org8a7e3b4"></a>

## [[Adam Curtis stuff]]


<a id="orgad06607"></a>

## [[The Speed Cubers]]


<a id="orge6166ad"></a>

## To watch

-   Finally Got the News

