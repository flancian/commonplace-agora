# Thoughts on Ostrom’s Rules for Radicals, Chapter 2

[[Elinor Ostrom's Rules for Radicals]].

Originally here:  https://doubleloop.net/2018/02/12/thoughts-ostroms-rules-radicals-chapter-2/

The chapter starts by outlining Garret Hardin’s tragedy of the commons argument.  In short, my understanding of the argument is that due to the inherent selfishness of individuals the commons are doomed to overuse — unless they are turned into private property, or turned over to the state, and unless the users of a resource are regulated through coercion. Hardin’s paper is more generally about population limits and his views appear quite bluntly Malthusian.

Having seen functioning commons, Ostrom disagreed with Hardin’s analysis.  She studied commons that worked (and also those that didn’t), and captured her analysis of what made a commons sustainable in her work “Governing the Commons.”

Up to this point I had it in mind that Ostrom’s work would debunk completely all of Hardin’s arguments. But I think it’s a bit more subtle than that. My initial thoughts were of disappointment – the 8 design rules that Ostrom observed in sustainable commons didn’t feel that radical. The mention of boundaries, monitoring, policing, sanctions, fines (paid to those policing!), and a judiciary were not what I was expecting. Having come across ideas recently around the end of policing, both in fiction like [[The Dispossessed]], or non-fiction like, well, [[The End of Policing]], it wasn’t as radical as I was perhaps naively hoping for.

> Boundaries and effective rules will only work if they are policed in some way. Norms of good behaviour, while useful, are unlikely to be sufficient in protecting a commons on their own.
> 
> Clearly, some kind of punishment or sanction is necessary as it is unlikely that good will on its own will prevent abuse of the commons.

These sentences felt like they could come out of Hardin’s article.

It feels like tacit acknowledgement of the commons ‘tragedy’ and a need for coercion, just whereas Hardin advocated it through private property or state ownership here it simply moves to the local level. Am I missing something? Does direct local participation make policing less coercive, more benevolent?

It seems like the debate (as I’ve seen it) has been somewhat polarised and Hardin is an easy straw man against tragedy because of his neo-Malthusian tendency. But that in fact the possibility of overuse does need to be addressed, and his being a crank doesn’t obviate that. Ostrom provides a gentler, more local way to tackle it. Which is great, but think I was somewhat naively hoping that Ostrom’s observations would show that people are naturally awesome and altruistically cooperative.

As suggested by Erik (@eloquence): “Even with a strong culture of sharing you’ll have internal outliers and new community members not brought up in the culture. But I don’t think that fact alone is enough to speak of a “tragedy”.”

I think this is a good point. The reality of overuse doesn’t mean the inevitability of tragedy.

Aaron (@aaron) makes the point, in reference to Governing the Commons, that: “what really makes the book Great is that it’s empirical, and draws conclusions from real examples, rather than theories like “people are cooperators” or “people are selfish.”

Michel (@Antanicus) noted an interesting side to the fact that Ostrom’s rules are not particularly leftist: “it means there’s actually room for a broad discourse around the commons as a new mode of production”.

In addition, I think there’s subtle distintions to be made in policing, justice, and power that I need to learn more about.

-   The difference between security forces of the state and security forces of the polis
-   The difference between punitive justice and restorative justice/transformative justice
-   The difference between “abolishing power” and organizing it in a different way.
    https://www.reddit.com/r/Anarchism/comments/43bco0/police_in_rojava/czhz3xv/

Also, as an aside, Erik also looked at it from the tech perspective: “For information commons overuse is usually desirable, but they need their own codes of conduct to thrive, so perhaps some concepts from Ostrom’s work are transferable.”

The rules for sustainable commons, as summarised by Wall are:

-   clearly defined boundaries
-   fit local circumstances
-   direct participation in rulemaking
-   effective monitoring and policing
-   sanctions
-   low-cost conflict resolution (an agreed judicial body)
-   recognition of rights to organise
-   commons work within wider systems

([[Governing the Commons]] would be the place to go for more detail.)


## Elsewhere in the garden

Notes that link to this note (AKA [[backlinks]]).

-   [[Elinor Ostrom's Rules for Radicals]]
-   [[2021-05-22]]
