# commons-public partnerships

> An agreement of long-term cooperation between commoners and state institutions to meet specific needs.
> 
> &#x2013; [[Free, Fair and Alive]]

<!--quoteend-->

> A clever twist on the public/private partnership is the commons/public partnership in which [[commoner]]s act as working partners with municipal governments in tackling important need.
> 
> &#x2013; [Hacking the Law to Open Up Zones of Commoning - Resilience](https://www.resilience.org/stories/2020-11-17/hacking-the-law-to-open-up-zones-of-commoning/) 

I've also seen it called [[public-commons partnerships]].  I think they both refer to the same thing.

> a reversal to the familiar model of development in which public resources and community assets are transferred into private hands
> 
> &#x2013; [[How 'Public-Common Partnerships' Can Help Us Take Back What's Ours]]

<!--quoteend-->

> the opportunity for resources to move not just the other way (from private to public) but also from public to ‘the commons’ – not just collective ownership but a model which has decentralised democratic governance built into it.
> 
> &#x2013; [[How 'Public-Common Partnerships' Can Help Us Take Back What's Ours]]

<!--quoteend-->

> Public-common partnerships are co-owned and co-governed infrastructures – what we call a ‘joint enterprise’ – where the establishment of a common association is essential to making decisions and controlling resources autonomously from, but in partnership with, state authorities
> 
> &#x2013; [[How 'Public-Common Partnerships' Can Help Us Take Back What's Ours]]

<!--quoteend-->

> common associations – which may variably take the legal form of a consumer cooperative, mixed cooperative, or community interest company – have both the capacity and obligation to redirect surplus towards supporting and expanding other public-common partnerships
> 
> &#x2013; [[How 'Public-Common Partnerships' Can Help Us Take Back What's Ours]]

<!--quoteend-->

> Importantly, public-common partnerships won’t come about solely through local governments passing policy. Whereas the politicians who pushed through public-private partnerships and private finance initiatives were in cahoots with a cadre of financiers who served to make a profit, public-common partnerships need a cadre drawn from social movements
> 
> &#x2013; [[How 'Public-Common Partnerships' Can Help Us Take Back What's Ours]]

<!--quoteend-->

> social movements can force transparency onto opaque political and business decision-making, raising the political costs of ‘business as usual’ and helping to open up new political possibilities. Secondly, they can act as a catalyst for the formation of a common association, providing the initial lifeblood of any joint enterprise.
> 
> &#x2013; [[How 'Public-Common Partnerships' Can Help Us Take Back What's Ours]]

<!--quoteend-->

> As the expansive nature of PCPs leads to a more diverse and complex web of commonly-directed utilities and resources, we can increasingly guarantee collective and sustainable access to food, energy, water, housing and other basic human rights
> 
> &#x2013; [[How 'Public-Common Partnerships' Can Help Us Take Back What's Ours]]

<!--quoteend-->

> Such a radical democratisation of ownership and governance functions as a ‘training in democracy’ while producing the stable material conditions upon which increased political participation becomes possible. It is a project of an ever-deeper democratisation of society
> 
> &#x2013; [[How 'Public-Common Partnerships' Can Help Us Take Back What's Ours]]

<!--quoteend-->

> through bringing together local communities, municipal governments and socially-responsible experts – can ensure the benefits of urban development are democratically and equitably distributed towards meaningful social outcomes
> 
> &#x2013; [[How 'Public-Common Partnerships' Can Help Us Take Back What's Ours]]


## How does the relationship work?

> State institutions provide vital legal, financial, and/ or administrative support to commoners, and commoners provide services to each other and the broader public.
> 
> &#x2013; [[Free, Fair and Alive]]


## [[Who creates commons-public partnerships?]]


## What are some examples?

> Examples include community-driven Wi-Fi systems, care such as nursing and eldercare, and neighborhood-managed projects implemented with government support.
> 
> &#x2013; [[Free, Fair and Alive]]

