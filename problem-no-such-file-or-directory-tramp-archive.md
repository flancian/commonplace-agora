# Problem: no such file or directory tramp-archive

After doing a [[spacemacs]] package update, I started getting this error for pretty much everything I was trying to do.

> no such file or directory tramp-archive

I turned on `debug-on-error` and then it seemed to be coming from [[helm]]. 

I searched around for it and there's a commit related to tramp-archive in helm [a few hours ago](https://github.com/emacs-helm/helm/commit/6e8a9319dcad9dc817fd5b40db559e96d47ea7b4).

Whatever it is, it hasn't fixed it, and perhaps it's actually the thing that has broken it.

I tried to rollback to an earlier helm version, via the instructions [here](https://stackoverflow.com/questions/42743518/rollback-package-version-in-spacemacs) - but couldn't get that to work.

In the end I've just switched the completion framework from helm to ivy for now, in my `dotspacemacs-configuration-layers`.

I guess I'll try helm again in a couple of days, but ivy's working fine for now.


## <span class="timestamp-wrapper"><span class="timestamp">[2021-03-20 Sat]</span></span>

This is resolved upstream in helm now: [emacs-helm/helm#2394 Cannot open load file: No such file or directory, tramp-&#x2026;](https://github.com/emacs-helm/helm/issues/2394) 

So I've switched back to the helm layer and all is well.  ivy was fine, while I was using it, but I'm just more familiar with helm.

