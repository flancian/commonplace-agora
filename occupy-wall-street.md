# Occupy Wall Street

[[Occupy]]

> At the Occupy Wall Street encampment in 2011, reporters would arrive and be transfixed by the media centre – the nerve centre, the centre of power because it was media (Schneider 2013, 36). And media was powerful indeed, as it drew thousands upon thousands of people into what began as a small, precarious protest. Videos of police attacking activists, in particular, bred sympathy and participants, and a feeling that the movement might be on the brink of sparking some kind of revolution. At least at first. By the following year, the videos didn’t work the same way. As an activist mon- itoring the analytics data noticed at the time, “Riot porn is losing its luster for mass online consumption”
> 
> &#x2013; [[Governable Stacks against Digital Colonialism]]

