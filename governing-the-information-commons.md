# Governing the Information Commons

URL
: https://www.glizzan.com/2020/03/04/governing-the-information-commons.html

> norms around ownership are so taken for granted that even projects expressly devoted to empowering users by wrestling ownership and control from big companies still encode these norms

Nice article about building [[governance]] layers into platforms.  Following [[Elinor Ostrom]]'s [[8 principles for managing a commons]].  The part that resonated most for me on first reading is the need to understand the many patterns of governance that exist.  From there we can move from the simple defaulting to [[private property]] based models.

> One vital thing that all of us can do is learn more about governance. The world is full to bursting with varieties of governance systems for us to learn about - whether it’s the [[sortition]]-based democracy of ancient Athens or [[consensus]] based models used by Quakers or the many, many forms of governance which are used by communities that have been marginalized by dominant forces in our society. 

<!--quoteend-->

> By learning about these systems, we not only open our minds to new possibilities, we also learn to see patterns in what kinds of structures work in which situations.

<!--quoteend-->

> People in power want us to be overwhelmed by complexity and uncertainty, so we’ll cede our right to self-determination to them. But we can learn to be patient with complexity and comfortable with uncertainty. They want us to be frustrated with the messiness of democracy and with each other, but we can learn to understand, support, and forgive each other.

