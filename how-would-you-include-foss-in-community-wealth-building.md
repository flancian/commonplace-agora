# How would you include FOSS in community wealth building?

How would you include FOSS in [[Community wealth building]]?

Maybe hire local tech coops to work on implementing / improving FOSS for the council maybe.  But with the rule that improvements are always contributed back.

Municipal sponsorship of FOSS could be a good financial model too.
