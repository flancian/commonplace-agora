# Repair and jobs

> To say nothing of the impact on jobs: landfilling a kiloton of ewaste creates <1 job; recycling that waste creates 15 jobs, while repairing it creates 200 good, local jobs that can't be offshored (you don't send a phone overseas for repair).
> 
> https://pluralistic.net/2021/05/26/nixing-the-fix/#r2r

-   [The Repair Jobs Revolution - iFixit](https://www.ifixit.com/Right-to-Repair/Jobs-Revolution)

